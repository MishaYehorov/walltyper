<?php
if ($verified_user) {
	$userquery = @ mysqli_query($baglan,"select * FROM user WHERE nick='$verified_user'");
	$userlist = @ mysqli_fetch_array($userquery);
	$user_id = $userlist["id"];
	$useravatar = GetBigAvatar($user_id);
}
?>
<body class="bgleft">
<div class="container-fluid">
	<?php if($verified_user): ?>
		<div class="row">
			<div class="col-xs-12 profile">
				<div class="pull-left">
					<?php
					if ($profilresmi == "1")
					{
						if ($useravatar){
							echo "<a target=\"main\" href=\"profil.php?u=".urlencode($verified_user)."\"><div class=\"image\" style=\"background-image: url('".$useravatar."')\"></div>";
						} else {
							echo "<a href=\"upload.php\"><div class=\"image\" style=\"background-image: url('images/no_profile.jpg');\"></div></a><a target=\"main\" href=\"profil.php?u=".urlencode($verified_user)."\">";
						}
					} ?>
				</div>
				<div class="pull-left info">
					<b><?=$userlist["isim"];?></b><br/>
					<small class="text-muted">(<?=$userlist["nick"];?>)</small>
				</div>
				</a>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 text-center">
				<div class="btn-group btn-group-xs" role="group">
					<a href="sozluk.php?process=privmsg" target="main" class="btn btn-primary">&nbsp;<span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>&nbsp;<?=$language[button_message]; ?>&nbsp;</a>
					<a href="sozluk.php?process=imha" target="main" class="btn btn-primary">&nbsp;<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>&nbsp;<?=$language[button_bin]; ?>&nbsp;</a>
					<a href="sozluk.php?process=onlines" target="main" class="btn btn-primary">&nbsp;<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>&nbsp;<?=$language[button_event]; ?>&nbsp;</a>
				</div>
			</div>
		</div>
	<?php endif;
	include ("vampara.php")?>
</div>
<br/>
<form id="frm" action="sozluk.php" method="GET" target="gostert">
	<ul class="nav nav-left">
		<li><a  href="sozluk.php?process=gstat" target="main"><? echo $language[general_stats]; ?></a></li>
		<li><a  href="sozluk.php?process=s_enBegenilenEntryler" target="main"><? echo $language[most_1]?></a></li>
		<li><a  href="sozluk.php?process=s_enBegenilmeyenEnryler" target="main"> <? echo $language[most_2]?></a></li>
		<li><a  href="sozluk.php?process=s_gHaftaEnIyiler" target="main"> <? echo $language[most_3]?></a></li>
		<li><a  href="sozluk.php?process=s_gHaftaEnKotuler" target="main"> <? echo $language[most_4]?></a></li>
		<li><a	href="sozluk.php?process=s_enCokEntryGirlimisBaslik" target="main"> <? echo $language[most_5]?></a></li>
		<li><a	href="sozluk.php?process=s_enCokOkunanBasliklar" target="main">	<? echo $language[most_6]?></a></li>
		<li><a	href="sozluk.php?process=s_gununIlkTanimlari" target="main">	<? echo $language[most_7]?></a></li>
		<br><br>
		<li><a  href="sozluk.php?process=s_enCokEntryGirenler" target="main">	<? echo $language[most_8] ?></a></li>
		<li><a  href="sozluk.php?process=s_enAzEntryGirenler" target="main">	<? echo $language[most_9] ?></a></li>
		<li><a  href="sozluk.php?process=s_enCopculer" target="main">	<? echo $language[most_10] ?></a></li>
		<li><a  href="sozluk.php?process=s_mesaj" target="main">	<? echo $language[most_11] ?></a></li>
		<li><a  href="sozluk.php?process=s_enBegenilenYazarlar" target="main">	<? echo $language[most_12] ?></a></li>
		<li><a  href="sozluk.php?process=s_enSevilmeyenYazarlar" target="main">	<? echo $language[most_13]?></a></li>
		<br><br>
		<li><a  href="sozluk.php?process=s_aylaraGoreEntry" target="main"> <? echo $language[most_14] ?></a></li>
		<li><a  href="sozluk.php?process=s_mevsim" target="main"> <? echo $language[most_15] ?></a></li>
		<li><a  href="sozluk.php?process=s_cinsiyet" target="main"> <? echo $language[most_16] ?></a></li>
		<li><a  href="sozluk.php?process=s_tema" target="main">   <? echo $language[most_17] ?></a></li>
		<br><br>
		<hr style="color: #fff; width:99%; float: left;">
		<?
		$act = @mysqli_query($baglan,"SELECT tarih FROM stat");
		$Rs = @mysqli_fetch_array($act);

		$hDate = $Rs["tarih"];

		echo "<center>$language[lastupdate]<br> $language[last_updated] ($hDate)</center>";
		?>
	</ul>
</form>
</body>
</html>

