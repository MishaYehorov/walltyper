<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<div class="container-fluid">
	<?php
	if ($_GET['verified_user'] or $_GET['verified_kat'] or $_POST['verified_user'] or $_POST['verified_kat']) {
		die();
	}

	if (!$verified_user)
	{
		echo "$language[author_NOT]";
		die;
	}

	?>
	<div class="row">
		<div class="col-xs-12">
			<a href="sozluk.php?process=zirve" class="btn btn-primary"><? echo $language[zirvetor]; ?></a>
			<a href="sozluk.php?process=zirveEkle" class="btn btn-primary"><? echo $language[zirvetor_add]; ?></a>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title"><? echo $language[new_activities]; ?></h3>
				</div>
				<table class="table">
					<thead>
					<tr>
						<th>#</th>
						<th><? echo $language[activity_date]; ?></th>
						<th><? echo $language[activity_name]; ?></th>
						<th><? echo $language[activity_category]; ?></th>
						<th><? echo $language[activity_place]; ?></th>
						<th><? echo $language[activity_organizator]; ?></th>
					</tr>
					</thead>
					<tbody>
					<?
					$sorgu = "SELECT * FROM zirve where durum !='kapalı' ORDER by `tarih` ASC limit 10";
					$sorgulama = @mysqli_query($baglan,$sorgu);
					if (@mysqli_num_rows($sorgulama) > 0)
					{
						$sirala = 1;
						while ($kayit=@mysqli_fetch_array($sorgulama))
						{
							$id = $kayit["id"];
							$gun = $kayit["tarih"];
							$zirve = $kayit["zirve_ismi"];
							$kategori = $kayit["kategori"];
							$mekan = $kayit["mekan"];
							$organizator = $kayit["organizator"];

							$zirveSorgu = "SELECT * FROM zirveuser WHERE `zirve_id` = '$id'";
							$zirveSorgulama = @mysqli_query($baglan,$zirveSorgu);
							$toplamKatilimci = @mysqli_num_rows($zirveSorgulama);

							echo "
		  <tr>
		    <td>$sirala</td>
		    <td>$gun</td>
		    <td><a href=\"sozluk.php?process=zirvedetay&id=$id\">$zirve</a> - $toplamKatilimci</td>
		    <td>$kategori</td>
			<td>$mekan</td>
			<td>$organizator</td>
		  </tr>
		";
							$sirala ++;
						}
					}

					?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title"><? echo $language[closed_activities]; ?></h3>
				</div>
				<table class="table">
					<thead>
					<th>#</th>
					<th><? echo $language[activity_date]; ?></th>
					<th><? echo $language[activity_name]; ?></th>
					<th><? echo $language[activity_category]; ?></th>
					<th><? echo $language[activity_place]; ?></th>
					<th><? echo $language[activity_organizator]; ?></th>
					</tr>
					</thead>
					<tbody>
					<?
					$sorgu = "SELECT * FROM zirve where durum ='kapalı' ORDER by `tarih` ASC limit 10";
					$sorgulama = @mysqli_query($baglan,$sorgu);

					if (@mysqli_num_rows($sorgulama) > 0)
					{
						$sirala = 1;

						while ($kayit=@mysqli_fetch_array($sorgulama))
						{
							$id = $kayit["id"];
							$gun = $kayit["tarih"];
							$zirve = $kayit["zirve_ismi"];
							$kategori = $kayit["kategori"];
							$mekan = $kayit["mekan"];
							$organizator = $kayit["organizator"];

							$zirveSorgu = "SELECT * FROM zirveuser WHERE `zirve_id` = '$id'";
							$zirveSorgulama = @mysqli_query($baglan,$zirveSorgu);
							$toplamKatilimci = @mysqli_num_rows($zirveSorgulama);

							echo "
		  <tr>
		    <td>$sirala</td>
		    <td>$gun</td>
		    <td><a href=\"sozluk.php?process=zirvedetay&id=$id\">$zirve</a> - $toplamKatilimci</td>
		    <td>$kategori</td>
			<td>$mekan</td>
			<td>$organizator</td>
		  </tr>
		";
							$sirala ++;
						}
					} ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

