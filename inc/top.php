<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/><?php

//seo modifikasyonu

if ($seoMode == 1)
{
	//karıştır butonu
	$btn_karistir = "karistir.htm";
	$btn_istatistik = "istatistikler.htm";
	$btn_dun = "dun.htm";
	$btn_sukela = "iyisinden.htm";
	$btn_ukte = "ukte.htm";
	$btn_rastgele = "rastgele.htm";
	$btn_sozluk = "$language[dictionaryName].html";
	$btn_author = "yazar_girisi.htm";
	$btn_register = "kayit_ol.htm";
	$btn_iletisim = "iletisim.htm";

}
else
{
	$btn_karistir = "left.php?t=mix";
	$btn_istatistik = "sozluk.php?process=stat";
	$btn_dun = "sozluk.php?process=yesterday";
	$btn_sukela = "sozluk.php?process=sukela";
	$btn_ukte = "sozluk.php?process=kuyu";
	$btn_rastgele = "sozluk.php?process=rand";
	$btn_sozluk = "nedir.php?&q=$language[dictionaryName]";
	$btn_author = "login.php";
	$btn_register = "register.php";
	$btn_iletisim = "sozluk.php?process=iletisim";
	$test = "left_user_news.php";
}

if($verified_user)
	$sorbakalim=mysqli_query($baglan,"UPDATE user SET son_online=".tarihYarat("YmdHi")." where nick='$verified_user'");

?>
<body class="bgtop">
<div class="navbar navbar-custom-menu navbar-fixed-top">
	<div class="container-fluid">
		<div>
			<ul class="nav nav-pills pull-right">
				<li>
					<a onclick="top.left.location.href='left.php?t=tod';top.main.location.href='nedir.php'" href="sozluk.php?process=language&ln=en">中文</a>
				</li>
				<li>
					<a onclick="top.left.location.href='left.php?t=tod'; top.main.location.href='nedir.php'" href="sozluk.php?process=language&ln=en">english</a>
				</li>
			</ul>
			<ul class="nav nav-pills">
				<li>
					<a title="<? echo $language[description_sss];?>" target="main" href="nedir.php?q=<?=$language[topbutton_sss_long];?>" onclick="top.main.location.href='nedir.php?q=<?=$language[topbutton_sss_long]?>"><? echo $language[topbutton_sss];?></a>
				</li>
				<li>
					<a  title="<? echo $language[description_sukela];?>" target="main" href="<?=$btn_sukela;?>" onclick="top.main.location.href='<?=$btn_sukela;?>'"><? echo $language[topbutton_sukela]; ?></a>
				</li>
				<li>
					<a title="<?=$language[description_contact];?>" target="main" href="<?=$btn_iletisim;?>"><?=$language[topbutton_contact];?></a>
				</li>
				<?php /*if ($verified_user): */?><!--
					<li>
						<a target="left" href="left.php?t=buddy" onclick="top.left.location.href='left.php?t=buddy'"><?/* echo $language[buddyList];*/?></a>
					</li>
				--><?php /*endif; */?>
			</ul>
		</div>
	</div>
</div>
<div class="navbar navbar-default navbar-fixed-top">
	<div class="container-fluid">
		<div class="navbar-header">
			<a href="" onclick="top.main.location.href='sozluk.php?process=rand'; top.left.location.href='left.php?t=tod'" class="navbar-brand"><img src="../images/new_logo_1.png"></a>
		</div>
		<div class="top-orange-menu" id="navbar-main">
			<ul class="nav navbar-nav">
				<li>
					<a title="<? echo $language[description_stats];?>" target="left" href="<?=$btn_istatistik;?>"><? echo $language[topbutton_stats];?></a>
				</li>
				<?php if ($verified_user): ?>
					<li>
						<a title="<?=$language[description_wish]?>" target="left" href="<?=$btn_ukte?>" onclick="top.left.location.href='<?=$btn_ukte?>'"><?=$language[topbutton_wish]?></a>
					</li>
					<li>
						<a title="<?=$language[description_extras];?>" target="main" href="sozluk.php?process=cekmece"><?=$language[topbutton_extras];?></a>
					</li>
					<li>
<!--						<a title="--><?//=$language[description_extras];?><!--" target="main" href="sozluk.php?process=cekmece">--><?//=$language[topbutton_extras];?><!--</a>-->
<!--						<a  target="main" href="sozluk.php?process=category_user" >Category</a>-->
						<a  target="left" href="<?=$test?>" onclick="top.left.location.href='<?=$test?>'">News</a>
					</li>
					<?php if($verified_kat == "admin" or $verified_kat == "mod") : ?>
						<li>
							<a title="<?=$language[description_admin];?>" target="left" href="sozluk.php?process=admsidebar"><?=$language[topbutton_admin];?></a>
						</li>
					<?php endif; ?>
					<?php if ($verified_kat == "gammaz"): ?>
						<li>
							<a title="<?=$language[description_spy];?>" target="main" href="sozluk.php?process=adm&islem=ispit"><?=$language[topbutton_spy];?></a>
						</li>
					<?php endif; ?>
				<?php else : ?>
					<li>
						<a title="<?=$language[description_dictionary];?>" target="main" href="<?=$btn_sozluk ?>" onclick="top.main.location.href='<?=$btn_sozluk?>'"><? echo $language[dictionaryName];?></a>
					</li>
				<?php endif; ?>
			</ul>
			<form action="nedir.php" id="fg" method="post" target="main" class="navbar-form navbar-left" role="search">
				<div class="input-group create-topic">
					<input type="text" class="form-control" placeholder="<?=$language[button_newTopic];?>" accesskey="b" id="q" name="q">
					<span class="input-group-btn">
						<button type="submit" class="btn btn-info"><?=$language["bring"]?></button>
					</span>
				</div>
				<!--<button type="button" class="btn btn-default" onclick="ara();"><?/*=$language["search"]*/?></button>
				<button type="button" class="btn btn-default" onclick="idara();">#</button>-->
			</form>
			<ul class="nav navbar-nav navbar-right">
				<?php if($verified_user): ?>
					<li>
						<a title="<?=$language[description_control];?>" target="main" href="sozluk.php?process=onlines">&nbsp;<span class="glyphicon glyphicon-cog" aria-hidden="true"></span>&nbsp;</a>
					</li>
					<li>
						<a title="<?=$language[description_exit];?>" target="main" href="logout.php"><?=$language[topbutton_exit];?></a>
					</li>
				<?php else: ?>
					<li>
						<a  title="<?=$language[description_enter];?>" target="main" href="<?=$btn_author;?>"><?=$language[topbutton_enter];?></a>
					</li>
					<li>
						<a title="<?=$language[description_register];?>" target="main" href="<?=$btn_register;?>"><?=$language[topbutton_register];?></a>
					</li>
				<?php endif; ?>
				<?php
				$userquery =@ mysqli_query($baglan,"select * FROM user WHERE nick='".$verified_user."'");
				$userlist =@ mysqli_fetch_array($userquery);
				$user_id = $userlist["id"];?>
			</ul>
		</div>
	</div>
</div>
<!--<table cellspacing="0" cellpadding="0" style="margin:0;padding:0 ; width:100%">
	<tr>
		<td>
			<table cellpadding="0" cellspacing="1" class="nav">
				<tr>
					<td onmousedown="md(this)" onmouseup="bn(this)" onmouseover="ov(this)" onmouseout="bn(this)" class="but" onclick="top.left.location.href='left.php'">
						<a  title="<?/* echo $language[all];*/?>" target="left" href="left.php">&nbsp;<?/* echo $language[all];*/?>&nbsp;</a></td>
					<td onmousedown="md(this)" onmouseup="bn(this)" onmouseover="ov(this)" onmouseout="bn(this)" class="but" onclick="top.left.location.href='<?/*=$btn_karistir;*/?>'">
						<a  title="<?/* echo $language[description_mix];*/?>" target="left" href="<?/*=$btn_karistir;*/?>">&nbsp;<?/* echo $language[topbutton_mix];*/?>&nbsp;</a></td>
					<?/* if ($verified_user) { */?>
						<td onmousedown="md(this)" onmouseup="bn(this)" onmouseover="ov(this)" onmouseout="bn(this)" class="but" onclick="top.left.location.href='left.php?t=buddy'">
							<a target="left" href="left.php?t=buddy">&nbsp;<?/* echo $language[buddyList];*/?>&nbsp;</a></td>
					<?/* } */?>
					<td colspan="2" onmousedown="md(this)" onmouseup="bn(this)" onmouseover="ov(this)" onmouseout="bn(this)" class="but" ">
					<a  title="<?/* echo $language[description_stats];*/?>" target="left" href="<?/*=$btn_istatistik;*/?>">&nbsp;<?/* echo $language[topbutton_stats];*/?>&nbsp;</a></td>
					<?/*
					if (!$verified_user)
					{
						echo"
				<td onmousedown=\"md(this)\" onmouseup=\"bn(this)\" onmouseover=\"ov(this)\" onmouseout=\"bn(this)\" class=\"but\" onclick=\"top.main.location.href='$btn_sozluk'\">
				<a  title=\"$language[description_dictionary]\" target=\"main\" href=\"$btn_sozluk\">&nbsp;$language[dictionaryName]&nbsp;</a></td>";
					} */?>

					<?/*
					if ($verified_user)
					{
						echo"
				<td  onmousedown=\"md(this)\" onmouseup=\"bn(this)\" onmouseover=\"ov(this)\" onmouseout=\"bn(this)\" class=\"but\" onclick=\"$btn_rastgele'\">
				<a  title=\"$language[description_random]\" target=\"main\" href=\"$btn_rastgele\">&nbsp;$language[topbutton_random]&nbsp;</a></td>
				<td  onmousedown=\"md(this)\" onmouseup=\"bn(this)\" onmouseover=\"ov(this)\" onmouseout=\"bn(this)\" class=\"but\" onclick=\"sozluk.php?process=cekmece'\">
				<a  title=\"$language[description_extras]\" target=\"main\" href=\"sozluk.php?process=cekmece\">&nbsp;$language[topbutton_extras]&nbsp;</a></td>
				<td onmousedown=\"md(this)\" onmouseup=\"bn(this)\" onmouseover=\"ov(this)\" onmouseout=\"bn(this)\" class=\"but\" onclick=\"top.main.location.href='sozluk.php?process=onlines'\">
				<a  title=\"$language[description_control]\" target=\"main\" href=\"sozluk.php?process=onlines\">&nbsp;$language[topbutton_control]&nbsp;</a></td>
				<td onmousedown=\"md(this)\" onmouseup=\"bn(this)\" onmouseover=\"ov(this)\" onmouseout=\"bn(this)\" class=\"but\" onclick=\"javascript:if(confirm('$language[message_exit]')) top.main.location.href='logout.php';return false\">
				<a  title=\"$language[description_exit]\" target=\"main\" href=\"javascript:if(confirm('$language[message_exit]'))location.href='logout.php';return false\">&nbsp;$language[topbutton_exit]&nbsp;</a></td>
				<td onmousedown=\"md(this)\" onmouseup=\"bn(this)\" onmouseover=\"ov(this)\" onmouseout=\"bn(this)\" class=\"but\" onclick=\"top.main.location.href='yazar/$verified_user'\">
				<a  title=\"$language[description_me]\" target='main' href=\"yazar/$verified_user\">&nbsp;$language[profil]&nbsp;</a></td>
				<td onmousedown=\"md(this)\" onmouseup=\"bn(this)\" onmouseover=\"ov(this)\" onmouseout=\"bn(this)\" class=\"but\" onclick=\"top.main.location.href='$btn_iletisim'\">
					<a title=\"$language[description_contact]\" target=\"main\" href=\"$btn_iletisim\">&nbsp;$language[topbutton_contact]&nbsp;</a></td>"
						;
						if ($verified_kat == "admin" or $verified_kat == "mod")
						{
							echo"
					<td onmousedown=\"md(this)\" onmouseup=\"bn(this)\" onmouseover=\"ov(this)\" onmouseout=\"bn(this)\" class=\"but\" onclick=\"top.left.location.href='sozluk.php?process=admsidebar'\">
					<a  title=\"$language[description_admin]\" target=\"left\" href=\"sozluk.php?process=admsidebar\">&nbsp;$language[topbutton_admin]&nbsp;</a></td>";
						}

						if ($verified_kat == "gammaz")
						{
							echo"
					<td onmousedown=\"md(this)\" onmouseup=\"bn(this)\" onmouseover=\"ov(this)\" onmouseout=\"bn(this)\" class=\"but\" onclick=\"top.main.location.href='sozluk.php?process=adm&islem=ispit'\">
					<a  title=\"$language[description_spy]\" target=\"main\" href=\"sozluk.php?process=adm&islem=ispit\">&nbsp;$language[topbutton_spy]&nbsp;</a></td>";
						}
					}
					else
						echo "
					<td onmousedown=\"md(this)\" onmouseup=\"bn(this)\" onmouseover=\"ov(this)\" onmouseout=\"bn(this)\" class=\"but\" onclick=\"top.main.location.href='$btn_author'\">
					<a  title=\"$language[description_enter]\" target=\"main\" href=\"$btn_author\">&nbsp;$language[topbutton_enter]&nbsp;</a></td>
					<td onmousedown=\"md(this)\" onmouseup=\"bn(this)\" onmouseover=\"ov(this)\" onmouseout=\"bn(this)\" class=\"but\" onclick=\"top.main.location.href='$btn_register'\">
					<a title=\"$language[description_register]\" target=\"main\" href=\"$btn_register\">&nbsp;$language[topbutton_register]&nbsp;</a></td>
									<td onmousedown=\"md(this)\" onmouseup=\"bn(this)\" onmouseover=\"ov(this)\" onmouseout=\"bn(this)\" class=\"but\" onclick=\"top.main.location.href='$btn_iletisim'\">
					<a title=\"$language[description_contact]\" target=\"main\" href=\"$btn_iletisim\">&nbsp;$language[topbutton_contact]&nbsp;</a></td>
					";
					*/?>
				</tr>

				<tr>
					<td onmousedown="md(this)" onmouseup="bn(this)" onmouseover="ov(this)" onmouseout="bn(this)" class="but" onclick="top.left.location.href='left.php?t=tod'">
						<a  title="<?/* echo $language[description_today]; */?>" target="left" href="left.php?t=tod">&nbsp;<?/* echo $language[topbutton_today]; */?>&nbsp;</a></td>
					<td onmousedown="md(this)" onmouseup="bn(this)" onmouseover="ov(this)" onmouseout="bn(this)" class="but" onclick="top.left.location.href='left.php?t=yes'">
						<a  title="<?/* echo $language[description_yesterday]; */?>" target="left" href="left.php?t=yes">&nbsp;<?/* echo $language[topbutton_yesterday]; */?>&nbsp;</a></td>
					<?/* if ($verified_user) { */?>
						<td onmousedown="md(this)" onmouseup="bn(this)" onmouseover="ov(this)" onmouseout="bn(this)" class="but" onclick="top.left.location.href='afterme.php'">
							<a target="left" href="afterme.php">&nbsp;<?/* echo $language[afterMe];*/?>&nbsp;</a></td>
					<?/* } */?>
					<td onmousedown="md(this)" onmouseup="bn(this)" onmouseover="ov(this)" onmouseout="bn(this)" class="but" onclick="top.main.location.href='nedir.php?q=<?/*=$language[topbutton_sss_long]*/?>'">
						<a  title="<?/*=$language[description_sss]*/?>" target="main" href="nedir.php?q=<?/*=$language[topbutton_sss_long]*/?>">&nbsp;<?/*=$language[topbutton_sss];*/?>&nbsp;</a></td>
					<td onmousedown="md(this)" onmouseup="bn(this)" onmouseover="ov(this)" onmouseout="bn(this)" class="but" onclick="top.main.location.href='<?/*=$btn_sukela;*/?>'">
						<a  title="<?/* echo $language[description_sukela];*/?>" target="main" href="<?/*=$btn_sukela;*/?>">&nbsp;<?/* echo $language[topbutton_sukela]; */?>&nbsp;</a></td>

					<?/*
					if ($verified_user)
					{
						echo "
				<td onmousedown=\"md(this)\" onmouseup=\"bn(this)\" onmouseover=\"ov(this)\" onmouseout=\"bn(this)\" class=\"but\" onclick=\"top.left.location.href='$btn_ukte'\">
				<a  title=\"$language[description_wish]\" target=\"left\" href=\"$btn_ukte\">&nbsp;$language[topbutton_wish]&nbsp;</a></td>";
					}

					if (!$verified_user)
					{
						echo"
					<td onmousedown=\"md(this)\" onmouseup=\"bn(this)\" onmouseover=\"ov(this)\" onmouseout=\"bn(this)\" class=\"but\" onclick=\"top.main.location.href='$btn_rastgele'\">
					<a  title=\"$language[description_random]\" target=\"main\" href=\"$btn_rastgele\">&nbsp;$language[topbutton_random]&nbsp;</a></td>";
					}
					*/?>

					<td colspan="4" style="margin:0;padding:0;">
						<table border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td style="height:10px;white-space:nowrap;padding:1px;font-size:x-small">
									<form action="nedir.php" id="fg" method="post" target="main">
										<?/*=$language[top_ask];*/?>: <input class="input" style="height:18px" accesskey="b" id="q" name="q" size="27" />
									</form>
								</td>

								<td onmousedown="md(this)" onmouseup="bn(this)" onmouseover="ov(this)" onmouseout="bn(this)" class="but" onclick="displayText()" onclick="javascript:document.getElementById('fg').submit();return false">
									<a title="<?/*=$language[search3_title]*/?>" href="javascript:document.getElementById('fg').submit()">&nbsp;<?/*=$language[bring]*/?> &nbsp;</a></td>

								<td onmousedown="md(this)" onmouseup="bn(this)" onmouseover="ov(this)" onmouseout="bn(this)" class="but" onclick="displayText()" onclick="javascript:ara();return false">
									<a title="<?/*=$language[search3_title]*/?>" href="javascript:ara()">&nbsp;<?/*=$language[search];*/?>&nbsp;</a></td>

								<td onmousedown="md(this)" onmouseup="bn(this)" onmouseover="ov(this)" onmouseout="bn(this)" class="but" onclick="javascript:idara();return false">
									<a title="numaraya göre ara" href="javascript:idara()">&nbsp;#&nbsp;</a></td>
							</tr>
						</table>
					</td>
					<td style="">
						<?/* if ($languageuser == "en") $facelang = "en_US"; else if ($languageuser == "ar") $facelang = "ar_AR"; else $facelang = "tr_TR"; */?>
						<script  src="http://connect.facebook.net/<?/*=$facelang*/?>/all.js#xfbml=1"></script><fb:like href="<?/*=$language[facebook_url]*/?>" layout="button_count"></fb:like>

					</td>
				</tr>
			</table>
		</td>
		<td class="langtop" style="float:right" onclick=" top.left.location.href='left.php?t=tod';top.main.location.href='nedir.php'"><a href="sozluk.php?process=language&ln=en">中文</a></td>
		<td style="width:10px;float:right"> | </td>
		<td class="langtop" style="float:right"  onclick=" top.left.location.href='left.php?t=tod'; top.main.location.href='nedir.php'"><a  href="sozluk.php?process=language&ln=en">english</a></td>
		<td style="float:right;  margin-left:250px">
			<?/* $userquery =@ mysqli_query($baglan,"select * FROM user WHERE nick='$verified_user'");
			$userlist =@ mysqli_fetch_array($userquery);
			$user_id = $userlist["id"];*/?>


		</td>

	</tr>

</table>
-->