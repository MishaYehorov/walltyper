<?php
// En begenilen tanimlar
$act = @mysqli_query($baglan,"select k.baslik, m.yazar, entry_id, sum(o.oy) kacartioy
					from `oylar` o
					inner join mesajlar m on m.id = o.entry_id
					inner join konular k on k.id = m.sira
					where o.oy <> 0
					group by o.entry_id
					order by sum(o.oy) desc
					limit 0,20");
$i = 1;
?>
<div class="container-fluid">
	<div class="page-header">
		<h1><? echo $language[most_1]; ?></h1>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<?php
			if (@mysqli_num_rows($act) > 0): ?>
				<table class="table table-bordered table-striped">
					<?php
					while ($Rs = @mysqli_fetch_array($act)){
						$id = $Rs["id"];
						$kacartioy = $Rs["kacartioy"];
						$baslik = $Rs["baslik"];
						$entry_id = $Rs["entry_id"];
						?>
						<tr>
							<td><?=$i;?>.</td>
							<td><a href="post.php?eid=<?=$entry_id;?>"><?=$baslik;?> / #<?=$entry_id;?></a></td>
							<td><?=$kacartioy;?></td>
						</tr>
						<?php
						$i++;
					}
					?>
				</table>
			<?php else:
				echo "$language[message_noStats]";
			endif; ?>
		</div>
	</div>
</div>
