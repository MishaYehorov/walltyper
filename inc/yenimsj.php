<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<?php  header('content-type:text/html; charset=utf-8');

$ok = RequestUtil::Get("ok");
$gkime = RequestUtil::Get("gkime");
$gmesaj = RequestUtil::Got("gmesaj");
$gkonu = RequestUtil::Got("gkonu");
$cevap = RequestUtil::Got("cevap");
$eid = RequestUtil::Get("eid");

if ($ok and $gkime and $gmesaj and $gkonu)
{
	$gmesaj = str_replace("<","(",$gmesaj);
	$gmesaj = str_replace(">",")",$gmesaj);
	$gmesaj = str_replace("\n","<br>",$gmesaj);

	$gkonu = str_replace("<","(",$gkonu);
	$gkonu = str_replace(">",")",$gkonu);

	$tarih = tarihYarat("YmdHM");
	$gun = tarihYarat("d");
	$ay = tarihYarat("m");
	$yil = tarihYarat("Y");
	$saat = tarihYarat("HM");


	//latin varsa ufaltır yoksa ufaltmaz   
	$gkonu = mb_strtolower($gkonu, mb_detect_encoding($gkonu));
	$gmesaj = mb_strtolower($gmesaj, mb_detect_encoding($gmesaj));


	$sorgu = "SELECT nick FROM user WHERE `nick`='$gkime'";
	$sorgulama = mysqli_query($baglan,$sorgu);

	if (mysqli_num_rows($sorgulama) == 0)
	{
		echo "$gkime $language[notSuchMemberNick]";
		die;
	}

	if ($durum != "on")
	{
		echo "$language[author_NOT]";
		die;
	}

	$sorgu = "INSERT INTO privmsg ";
	$sorgu .= "(kime,konu,mesaj,gonderen,tarih,okundu,gun,ay,yil,saat)";
	$sorgu .= " VALUES ";
	$sorgu .= "('$gkime','$gkonu','$gmesaj','$verified_user','$tarih','2','$gun','$ay','$yil','$saat')";
	mysqli_query($baglan,$sorgu);
	echo "<center><br>$language[message_sent]</center>";
	echo "<br><br><div align=center><input type='button' onClick=top.main.location.href='nedir.php' value='".$language[OK]."'></div>";

	$sorguMail = mysqli_query($baglan,"select * from user where nick='$gkime'");
	$yazarMail =@ mysql_result($sorguMail,0,'email');
	$yazarDurum =@ mysql_result($sorguMail,0,'durum');

	$mailIcerik = "$language[hello3]\n\n $language[dictionaryMail]  $verified_user ,  $language[newmessage3]
	\n\n$language[newmessage3_1] \n\n $language[dictionaryUrl]/sozluk.php?process=privmsg ";

	if ($yazarDurum == 'on')
	{
		$mailkonu = "$language[dictionaryName] - $language[newmessage3_2]";
		mail($yazarMail, $mailkonu, $mailIcerik, "From: $language[dictionaryName] <$language[dictionaryMail]>\r\nContent-Type: text/plain; charset='iso-8859-9'\r\n");
	}
}
else
{
if ($cevap)
{
	$sorgu = "SELECT * FROM privmsg WHERE `kime` = '$verified_user' and id = '$cevap'";
	$sorgulama = mysqli_query($baglan,$sorgu);
	$kayit2=mysqli_fetch_array($sorgulama);
	$mmesaj=$kayit2["mesaj"];
	$gonderen=$kayit2["gonderen"];
	$gkonu=$kayit2["konu"];
	$gun=$kayit2["gun"];
	$ay=$kayit2["ay"];
	$kime=$kayit2["kime"];
	$yil=$kayit2["yil"];
	$saat=$kayit2["saat"];
	$mmesaj = "\n\n\n
-- $language[previous_message] --------------
$language[sender]: $gonderen
$language[reciever]: $kime
$language[msg_date]: $gun/$ay/$yil $saat
$language[msg_subject]: $gkonu
$mmesaj
----------------------------------";

	if (mysqli_num_rows($sorgulama) == 0)
	{
		echo "$language[message_error] ";
		die;
	}
}

if($verified_user)
{
	$cezaSQL="SELECT * FROM user WHERE nick='$verified_user'";
	$cezaSorgu=mysqli_query($baglan,$cezaSQL);
	$cezaKayit=mysqli_fetch_array($cezaSorgu);
	$cezatarihi=$cezaKayit["cezatarihi"];
	$cezasayisi=$cezaKayit["cezasayisi"];
	$cezasebep=$cezaKayit["cezasebep"];
}

$mmesaj = str_replace("<br>","\n",$mmesaj);

if ($eid == 0)
	$linkEid = "";
else
	$linkEid = "(#$eid) ";
?>
<form name='ok' method='post' action=''>
	<div class="form-group">
		<label><? echo $language[to]; ?></label>
		<input name="gkime" type="text" id="kime" class="form-control" value="<?=$gkime;?>" required>
	</div>
	<div class="form-group">
		<label><? echo $language[subject]; ?></label>
		<input name="gkonu" type="text" id="konu" class="form-control" value="<?=$gkonu;?>">
	</div>
	<div class="form-group">
		<label><? echo $language[message]; ?></label>
		<textarea class="form-control" id="aciklama" name="gmesaj" rows="10"><?=$mmesaj;?></textarea>
	</div>
	<?php
	if($cezatarihi<=tarihYarat("YmdHi-10dk")){ ?>
		<input class="btn btn-primary" id="kaydet" onclick="return ControlFields();" type=submit value="<?=$language[button_send_message]?>" name="ok">
	<?php }else{ echo "$language[punishment_message_alarm] ";} ?>
	<INPUT type="hidden" value="ok" name="ok">
	<?php } ?>
	<script type="text/javascript">

		function ControlFields()
		{
			var kime = document.getElementById("kime");
			var konu = document.getElementById("konu");
			var aciklama = document.getElementById("aciklama");

			if (kime.value == "" || konu.value == "" || aciklama.value == "" || aciklama.value == " ")
			{
				alert("<?=$language[empty_place_alarm]?>);
				return false;
			};
		}
	</script>
