<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/><?php

define("ALLOW_HTML_TAGS_IN_POST_GET_REQUESTS", false);
require_once('class.inputfilter.php');

class RequestUtil
{
public static function Get($elementName,$requestType="")
	{
		if (strtolower($requestType)=="get")
		{
			$data = $_GET[$elementName];
		}
		else if (strtolower($requestType)=="post")
		{
			$data = $_POST[$elementName];
		}
		else if (strtolower($requestType)=="cookie")
		{
			$data = $_COOKIE[$elementName];
		}
		else
		{
			$data = $_REQUEST[$elementName];
		}

		// If results is array.. as in check boxes etc.. return the array as is
		if (is_array($data))
		{
			return $data;
		}
		else
		{
			// DO SANITY CHECK HERE
			// Allow HTML from user input or not (flag set in application Constants)
			// do whatever else sanity checking here to prevent XSS and similar user injections
			//Kontrolden geçirelim
			$tags = array("'","em", "br");
			$attributes = array("title", "selected");
			$xss_auto = 1;
			$myFilter = new InputFilter($tags, $attributes, 1, $xss_auto);
			$data = stripslashes($data);
			$data = $myFilter->process($data);
			$data = escapeString($data);
			$data = XssControl($data);
			//$data = $myFilter->keephtml($data);
				
			$evil = "(delete)|(update)|(union)|(insert)|(drop)|(http)|(--)|(/*)|(select)";
			$data = str_replace($evil, "", $data);
			
			if (ALLOW_HTML_TAGS_IN_POST_GET_REQUESTS)
			{
				// Remove all HTML Tags from user input
				// including SCRIPT, APPLET, EMBED etc..
				$data = strip_tags($data);
			}

			// if magic quotes == on, return data as is
			if (get_magic_quotes_gpc()==1)
			{
				return $data;
			}
			// if magic quotes == off, then return data with slashes
			else
			{
				return addslashes($data);
			}
		}
	}

	public static  function Got($elementName,$requestType="")
	{
		if (strtolower($requestType)=="get")
		{
			$data = $_GET[$elementName];
		}
		else if (strtolower($requestType)=="post")
		{
			$data = $_POST[$elementName];
		}
		else if (strtolower($requestType)=="cookie")
		{
			$data = $_COOKIE[$elementName];
		}
		else
		{
			$data = $_REQUEST[$elementName];
		}

		// If results is array.. as in check boxes etc.. return the array as is
		if (is_array($data))
		{
			return $data;
		}
		else
		{
			// DO SANITY CHECK HERE
			// Allow HTML from user input or not (flag set in application Constants)
			// do whatever else sanity checking here to prevent XSS and similar user injections
			if (ALLOW_HTML_TAGS_IN_POST_GET_REQUESTS)
			{
				// Remove all HTML Tags from user input
				// including SCRIPT, APPLET, EMBED etc..
				$data = strip_tags($data);
			}

			// if magic quotes == on, return data as is
			if (get_magic_quotes_gpc()==1)
			{
				return $data;
			}
			// if magic quotes == off, then return data with slashes
			else
			{

				return addslashes($data);
			}
		}
	}
}
	function escapeString($string) 
	{   global $baglan;
		return mysqli_real_escape_string($baglan,$string);
	}
	
	function XssControl($veri)
	{
		$veri =str_replace("`","",$veri);
		$veri =str_replace("'or","",$veri);
		$veri =str_replace("=","",$veri);
		$veri =str_replace("&","",$veri);
		$veri =str_replace("%","",$veri);
		$veri =str_replace("!","",$veri);
		$veri =str_replace("#","",$veri);
		$veri =str_replace("SELECT","",$veri);
		$veri =str_replace("UNION","",$veri);
		$veri =str_replace("union","",$veri);
		$veri =str_replace("select","",$veri);
		$veri =str_replace("SeLeCt","",$veri);
		$veri =str_replace("sElEct","",$veri);
		$veri =str_replace("%","",$veri);
		$veri =str_replace("+","",$veri);
		$veri =str_replace("<?","",$veri);
		$veri =str_replace("?>","",$veri);
		$veri =str_replace("*","",$veri);
		$veri =str_replace("<%","",$veri);
		$veri =str_replace("%>","",$veri);
		$veri =str_replace("\/","",$veri);
		
		return $veri;
	}
	
	function KKM($veri)
	{
		$veri =str_replace("`","",$veri);
		$veri =str_replace("'or","",$veri);
		$veri =str_replace("=","",$veri);
		$veri =str_replace("&","",$veri);
		$veri =str_replace("%","",$veri);
		$veri =str_replace("!","",$veri);
		$veri =str_replace("#","",$veri);
		$veri =str_replace("SELECT","",$veri);
		$veri =str_replace("UNION","",$veri);
		$veri =str_replace("union","",$veri);
		$veri =str_replace("select","",$veri);
		$veri =str_replace("SeLeCt","",$veri);
		$veri =str_replace("sElEct","",$veri);
		$veri =str_replace("%","",$veri);
		$veri =str_replace("+","",$veri);
		$veri =str_replace("<?","",$veri);
		$veri =str_replace("?>","",$veri);
		$veri =str_replace("*","",$veri);
		$veri =str_replace("<%","",$veri);
		$veri =str_replace("%>","",$veri);
		//$veri =str_replace("/","",$veri);
		$veri =str_replace("\/","",$veri);
		return $veri;
	}
	
	function KKMwithoutSlASH($veri)
	{
		$veri =str_replace("`","",$veri);
		$veri =str_replace("'or","",$veri);
		$veri =str_replace("=","",$veri);
		$veri =str_replace("&","",$veri);
		$veri =str_replace("%","",$veri);
		$veri =str_replace("!","",$veri);
		$veri =str_replace("#","",$veri);
		$veri =str_replace("SELECT","",$veri);
		$veri =str_replace("UNION","",$veri);
		$veri =str_replace("union","",$veri);
		$veri =str_replace("select","",$veri);
		$veri =str_replace("SeLeCt","",$veri);
		$veri =str_replace("sElEct","",$veri);
		$veri =str_replace("%","",$veri);
		$veri =str_replace("+","",$veri);
		$veri =str_replace("<?","",$veri);
		$veri =str_replace("?>","",$veri);
		$veri =str_replace("*","",$veri);
		$veri =str_replace("<%","",$veri);
		$veri =str_replace("%>","",$veri);
		//$veri =str_replace("/","",$veri);
		$veri =str_replace("\/","",$veri);
		return $veri;
	}

?>