<link rel="stylesheet" type="text/css" media="all" href="inc/takvim/takvim.css">
<script type="text/javascript" src="inc/takvim/takvim.js"></script>
<script type="text/javascript" src="inc/takvim/takvim-tr.js"></script>
<script type="text/javascript" src="inc/takvim/takvim-setup.js"></script>

<table width="100%" cellpadding=0 cellspacing=0>
<tr><td>
<table cellpadding=0 cellspacing=0 width="100%"><tr>
<td class=tab><div><a href="sozluk.php?process=zirve"> <?=$language[zirvetor]?></a></div></td>
<td class=tab><div><a href="sozluk.php?process=zirveEkle"> <?=$language[zirvetor_add]?> </a></div></td>
<td class=tabsel><div> <?=$language[zirvetor_add_3]?> </div></td>
<td class=tab style="width:100%">&nbsp;</td></tr>
</table></td></tr>
<tr><td class=tabin>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/><?php 
if ($_GET['verified_user'] or $_GET['verified_kat'] or $_POST['verified_user'] or $_POST['verified_kat']) 
{
	die();
}

if (!$verified_user)
{ 
	echo "$language[author_NOT]";
	die;
}

$listedeMi = "SELECT * FROM zirve WHERE `id` = '$id' and `ekleyen` = '$verified_user'";
$listedeSorgu= @mysqli_query($baglan,$listedeMi);
$listedeMisin = @mysqli_num_rows($listedeSorgu);

if ($listedeMisin == 0 and $verified_kat != admin) 
{
echo "$language[zirvetor_wtf]
<br>$listedeMisin<br>
$verified_user";
die;
}

$ok = RequestUtil::Get("ok");
$id = RequestUtil::Get("id");
	
if($ok && $id)
{
	$kategori = RequestUtil::Get("kategori");
	$zirve = RequestUtil::Get("zirve");
	$organizator = RequestUtil::Get("organizator");
	$mekan = RequestUtil::Get("mekan");
	$tarih = RequestUtil::Get("tarih");
	$afis = RequestUtil::Get("afis");
	$durum = RequestUtil::Get("durum");
	$detaylar = RequestUtil::Get("detaylar");
	$detaylar = str_replace("&nbsp;\n","&nbsp;<br>",$detaylar);
	$detaylar = str_replace("\\\\r\\\\n"," <br> ",$detaylar);

	$sorgu = "UPDATE zirve SET 
				kategori = '$kategori',
				zirve_ismi = '$zirve',
				organizator = '$organizator',
				mekan = '$mekan',
				tarih = '$tarih',
				afis = '$afis',
				durum = '$durum',
				organizator = '$organizator',
				detaylar = '$detaylar'
			  WHERE id='$id'";

	mysqli_query($baglan,$sorgu);

	echo "<br><br><center>$language[zirvetor_added3].<br><br>
	<script language=\"javascript\">goUrl('sozluk.php?process=zirvedetay&id=$id','main');</script>
	<a href=\"sozluk.php?process=zirve\">&nbsp;zirvetör</a>$language[zirvetor_return3]</center>";
}
else
{
	$sorgu = "SELECT * FROM zirve WHERE `id` = '$id'";
	$sorgulama = mysqli_query($baglan,$sorgu);

	if (mysqli_num_rows($sorgulama) > 0)
	{
		while ($kayit = mysqli_fetch_array($sorgulama))
		{
			$id = $kayit["id"];
			$gun = $kayit["tarih"];
			$zirve = $kayit["zirve_ismi"];
			$kategori = $kayit["kategori"];
			$mekan = $kayit["mekan"];
			$organizator = $kayit["organizator"];
			$afis = $kayit["afis"];
			$durum = $kayit["durum"];
			$detaylar = $kayit["detaylar"];
			
			$detaylar = str_replace("<br>","\n",$detaylar);

			echo "
			<form METHOD=post action=>
			<table width=\"600\" border=\"0\">
			
			<tr><br>
		      <td><strong>$language[zirvetor_situation3]:</strong></td>
		      <td>:</td>
		      <td>
			  <SELECT name=durum>
		      <option value=\"\">$language[open] </option>
		      <option value=\"kapalı\">$language[closed] </option>
		      </SELECT>
			  </td>
			</tr>
			
			  <tr>
				<td width=\"116\">$language[irvetor_name3]</td>
				<td width=\"8\">:</td>
				<td width=\"341\"><input name=\"zirve\" size=60 type=\"text\" id=\"zirve\" value=\"$zirve\"></td>
			  </tr>
			  
			  <tr>
				<td width=\"116\">$language[zirvetor_locality]</td>
				<td width=\"8\">:</td>
				<td width=\"341\"><input name=\"mekan\" size=60 type=\"text\" id=\"mekan\" value=\"$mekan\"></td>
			  </tr>
			  <tr>
			   <td>kategori</td>
			   <td>:</td>
			   <td>
			   <SELECT name=kategori class=ksel id=kategori>
			   <option value=\"geleneksel sözlük zirvesi\">$language[zirvetor_category_1]</option>
			   <option value=\"toplantı/muhabbet\">$language[zirvetor_category_2]</option>
			   <option value=\"konser/festival\">$language[zirvetor_category_3]</option>
			   <option value=\"gezi\">$language[zirvetor_category_4]</option>
			   <option value=\"oyun\">$language[zirvetor_category_5]</option>
			   <option value=\"müzik/dans\">$language[zirvetor_category_6]</option>
			   <option value=\"izlence (sinema/tv/tiyatro)\">$language[zirvetor_category_7](sinema/tv/tiyatro)</option>
			   <option value=\"doğum günü\">$language[zirvetor_category_8]</option>
			   <option value=\"sportif\">$language[zirvetor_category_9]</option>
			   <option value=\"piknik\">$language[zirvetor_category_10]</option>
			   <option value=\"doğa gezisi\">$language[zirvetor_category_11]</option>
			   <option value=\"yardım/dayanışma\">$language[zirvetor_category_12]</option>
			   <option value=\"yemek\">$language[zirvetor_category_13]</option>
			   <option value=\"diğer\">$language[zirvetor_category_14]</option>
			   </SELECT>
			   </td>
			  </tr>
			  
			  <tr>
			   <td>tarih / saat</td>
			   <td>:</td>
			   <td>
				<input id=\"tarih\" name=\"tarih\" type=\"text\" class=\"edit\" value=\"$gun\"/>
				<button id=\"date\" class=\"but\">$language[zirvetor_choose3]</button>
				<script type = \"text/javascript\">
				Calendar.setup(
				{ 
					inputField: \"tarih\",       
					ifFormat: \"%d.%m.%Y %H:%M\", 
					button: \"date\",          
					firstDay: 1,
					weekNumbers: false,
					range: [2007, 2012],
					step: 1,
					showsTime: true,
					timeFormat: 24
				}
				);
				</script>
				</td>
			  </tr>
			  
			  <tr>
			   <td><br>organizatör(ler) <br><small>$language[zirvetor_comma3]</small></td>
			   <td><br>:</td>
			   <td><textarea cols=50 rows=4 name=organizator>$organizator </textarea></td>
			  </tr>
  
			  <tr>
			   <td>detaylar:</td>
			   <td>:</td>
			   <td><textarea cols=50 rows=8 name=detaylar>$detaylar</textarea></td>
			  </tr>
			  <tr>
			  <tr>
				<td width=\"116\">afiş</td>
				<td width=\"8\">:</td>
				<td width=\"341\"><input name=\"afis\" size=60 type=\"text\" id=\"afis\" value=\"$afis\"></td>
			  </tr>
			  
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<input TYPE=hidden name=ok value=ok>
				<td><input type=\"submit\" name=\"Submit\" value=\"$language[edit2]\"></td>
			  </tr>
			</table>
			</form>
			";
		}
	}
}

?>

</table>
</fieldset>