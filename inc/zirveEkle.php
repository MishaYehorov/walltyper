<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<div class="container-fluid">
    <?php
    if ($_GET['verified_user'] or $_GET['verified_kat'] or $_POST['verified_user'] or $_POST['verified_kat']) {
        echo "$language[author_NOT]";
        die();
    }

    if (!$verified_user)
    {
        echo "$language[author_NOT]";
        die;
    }

    $ok = RequestUtil::Get("ok");

    if ($ok)
    {
        $kategori =@ RequestUtil::Get("kategori");
        $zirve_ismi  =@ RequestUtil::Get("zirve");
        $organizator =@ RequestUtil::Get("organizator");
        $mekan =@ RequestUtil::Get("mekan");
        $tarih =@ RequestUtil::Get("tarih");
        $afis =@ RequestUtil::Get("afis");
        $detaylar =@ RequestUtil::Get("detaylar");
        //$detaylar = str_replace("&nbsp;\n","&nbsp;<br>",$detaylar);

        $detaylar = str_replace("\\\\r\\\\n"," <br> ",$detaylar);

        if ($zirve_ismi == "" or $kategori == "" or $organizator == "" or $mekan == "" or $tarih == "" or $detaylar == "" or $afis == "")
        {
            echo "
		<br><br>
		<center>
		<b>$language[message_fillEverywhere]</b>
		<br><br>
		<a href=\"sozluk.php?process=zirve\">&nbsp;$language[OK]&nbsp;</a><br>
		$language[activity_preview]:<hr>
		$language[activity_name]: $zirve_ismi <br>
		$language[activity_category]: $kategori <br>
		$language[activity_organizator]: $organizator <br>
		$language[activity_place]: $mekan <br>
		$language[activity_date]: $tarih <br>
		$language[activity_details]: $detaylar <br>
		</center>
		";
            exit;
        }
        $bugun = tarihYarat("tam");
        //Ekleyelim
        $sorgu = "INSERT INTO zirve ";
        $sorgu .= "(zirve_ismi,kategori,organizator,mekan,tarih,eklenmeTarihi,detaylar,ekleyen,afis)";
        $sorgu .= " VALUES ";
        $sorgu .= "('$zirve_ismi','$kategori','$organizator','$mekan','$tarih','$bugun','$detaylar','$verified_user','$afis')";

        mysqli_query($baglan,$sorgu);
        echo "<center> <br><br><h2> $zirve_ismi $language[activity_added].</h2></center>
	<br><br>
	<fieldset><legend>$language[activity_information]:</legend>
	<br><b>$language[activity_name]:</b> $zirve_ismi <br>
	<b>$language[activity_category]:</b> $kategori <br>
	<b>$language[activity_organizator]:</b> $organizator <br>
	<b>$language[activity_place]:</b> $mekan <br>
	<b>$language[activity_date]:</b> $tarih <br>
	<b>$language[activity_details]:</b> $detaylar <br><br>
	<br><br><center>
	</fieldset><a href=\"sozluk.php?process=zirve\">&nbsp;$language[button_back]</a></center>";
    }

    else
    {

        ?>
        <div class="row">
            <div class="col-xs-12">
                <a href="sozluk.php?process=zirve" class="btn btn-primary"><? echo $language[zirvetor]; ?></a>
                <a href="sozluk.php?process=zirveEkle" class="btn btn-primary"><? echo $language[zirvetor_add]; ?></a>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <script type="text/javascript" src="inc/takvim/takvim.js"></script>
                <script type="text/javascript" src="inc/takvim/takvim-tr.js"></script>
                <script type="text/javascript" src="inc/takvim/takvim-setup.js"></script>

                <form method="post" action="">
                    <div class="form-group">
                        <label><? echo $language[activity_name]; ?></label>
                        <input class="form-control" name="zirve" type="text" id="zirve">
                    </div>
                    <div class="form-group">
                        <label><? echo $language[activity_place]; ?></label>
                        <input class="form-control" name="mekan" type="text" id="mekan">
                    </div>
                    <div class="form-group">
                        <label><? echo $language[activity_category]; ?></label>
                        <select class="form-control" name="kategori" id="kategori">
                            <option value="<? echo $language[activity_category_conventional]; ?>"><? echo $language[activity_category_conventional]; ?></option>
                            <option value="<? echo $language[activity_category_chat]; ?>"><? echo $language[activity_category_chat]; ?></option>
                            <option value="<? echo $language[activity_category_concert]; ?>"><? echo $language[activity_category_concert]; ?></option>
                            <option value="<? echo $language[activity_category_trip]; ?>"><? echo $language[activity_category_trip]; ?></option>
                            <option value="<? echo $language[activity_category_game]; ?>"><? echo $language[activity_category_game]; ?></option>
                            <option value="<? echo $language[activity_category_music]; ?>"><? echo $language[activity_category_music]; ?></option>
                            <option value="<? echo $language[activity_category_screen]; ?>"><? echo $language[activity_category_screen]; ?></option>
                            <option value="<? echo $language[activity_category_birthdate]; ?>"><? echo $language[activity_category_birthdate]; ?></option>
                            <option value="<? echo $language[activity_category_sports]; ?>"><? echo $language[activity_category_sports]; ?></option>
                            <option value="<? echo $language[activity_category_help]; ?>"><? echo $language[activity_category_help]; ?></option>
                            <option value="<? echo $language[activity_category_eating]; ?>"><? echo $language[activity_category_eating]; ?></option>
                            <option value="<? echo $language[activity_category_other]; ?>"><? echo $language[activity_category_other]; ?></option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label><? echo $language[activity_date_hour]; ?></label>
                        <div class='input-group date' id='datetimepicker'>
                            <input id="tarih" name="tarih" type='text' class="form-control" />
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                        <script src="js/bootstrap-datepicker.js"></script>
                        <script type="text/javascript">
                            $('#datetimepicker').datepicker();
                        </script>
                    </div>
                    <div class="form-group">
                        <label><? echo $language[activity_organizators]; ?><br><small><? echo $language[activity_organizators_tips]; ?></small></label>
                        <textarea class="form-control" rows="4" name="organizator"><? echo $verified_user ?>, </textarea>
                    </div>
                    <div class="form-group">
                        <label><? echo $language[activity_details]; ?></label>
                        <textarea class="form-control" rows="8" id="d" name="detaylar"></textarea>
                        <br/>
                        <input type="button" class="btn btn-default btn-sm" onclick="hen('d','(<? echo $language[bkz] ?>: ',')')" value="<? echo $language[bkz] ?>" />
                        <input type="button" class="btn btn-default btn-sm" onclick="hen('d','(<? echo $language[gbkz] ?>: ',')')" value="<? echo $language[gbkz] ?>"/>
                        <input type="button" class="btn btn-default btn-sm" onclick="hen('d','(<? echo $language[u] ?>: ',')')" value="*"/>
                    </div>
                    <div class="form-group">
                        <label><? echo $language[activity_poster]; ?></label>
                        <input name="afis"class="form-control" type="text" id="afis" value="images/afis.jpg">
                        <small>
                            <? echo $language[activity_poster_tips]; ?>
                        </small>
                    </div>
                    <input type="hidden" name="ok" value="ok">
                    <input class="btn btn-primary" type="submit" name="Submit" value="<? echo $language[button_add_activity]; ?>">
                </form>
            </div>
        </div>
    <? } ?>
</div>