<?
$sorgu1 = "SELECT * FROM stat";
$sorgu2 = mysqli_query($baglan,$sorgu1);
mysqli_num_rows($sorgu2);
$kayit2=mysqli_fetch_array($sorgu2);
$baslik=$kayit2["baslik"];
$entry=$kayit2["entry"];
$silbaslik=$kayit2["silbaslik"];
$silentry=$kayit2["silentry"];
$hit=$kayit2["hit"];
$tekil=$kayit2["tekil"];
$yazar=$kayit2["yazar"];
$okur=$kayit2["okur"];
$mod=$kayit2["moderat"];
$op=$kayit2["op"];
$pilot=$kayit2["pilot"];
$admin=$kayit2["admin"];
$ortbaslik=$kayit2["ortbaslik"];
$ortentry=$kayit2["ortentry"];
$enhitbaslik=$kayit2["enhitbaslik"];
$tarih=$kayit2["tarih"];
$toplamnufus = $yazar + $okur + $mod + $gammaz + $admin;
$gammaz=@mysqli_num_rows(mysqli_query($baglan,"select * from user where yetki='gammaz'"));

$basliklink = str_replace(" ","+",$enhitbaslik);
?>
<div class="container-fluid">
    <div class="page-header">
        <h1><? echo $language[general_stats]; ?></h1>
        <small><? echo $language[general_stats_latest]; ?></small>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <table class="table table-striped table-bordered">
                <tr>
                    <td><?=$language[total_entry];?></td>
                    <td><?=$entry;?></td>
                </tr>
                <tr>
                    <td><?=$language[total_topic];?></td>
                    <td><?=$baslik;?></td>
                </tr>
                <tr>
                    <td><?=$language[total_population];?></td>
                    <td><?=$toplamnufus;?></td>
                </tr>
                <tr>
                    <td>..<?=$language[author];?></td>
                    <td><?=$yazar;?></td>
                </tr>
                <tr>
                    <td>..<?=$language[rookie];?></td>
                    <td><?=$okur;?></td>
                </tr>
                <tr>
                    <td>..<?=$language[spy];?></td>
                    <td><?=$gammaz;?></td>
                </tr>
                <tr>
                    <td>..<?=$language[moderator];?></td>
                    <td><?=$mod;?></td>
                </tr>
                <tr>
                    <td>..<?=$language[admin];?></td>
                    <td><?=$admin;?></td>
                </tr>
                <tr>
                    <td><?=$language[total_deleted_topic];?></td>
                    <td><?=$silbaslik;?></td>
                </tr>
                <tr>
                    <td><?=$language[total_deleted_entry];?></td>
                    <td><?=$silentry;?></td>
                </tr>
                <tr>
                    <td><?=$language[total_topic_views];?></td>
                    <td><?=$hit;?></td>
                </tr>
                <tr>
                    <td><?=$language[total_visitors];?></td>
                    <td><?=$tekil;?></td>
                </tr>
                <tr>
                    <td><?=$language[topic_per_author];?></td>
                    <td><?=$ortbaslik;?></td>
                </tr>
                <tr>
                    <td><?=$language[entry_per_author];?></td>
                    <td><?=$ortentry;?></td>
                </tr>
            </table>
        </div>
    </div>
</div>

