<?
$id = RequestUtil::Get("id");
$source = RequestUtil::Get("s");

if ($id)
{
    $konu = $language['contact_entry_about'] . "#$id";

    $is_sikayet = "selected";
}
?>
<div class="container-fluid">
    <div class="page-header">
        <h1><? echo $language[topbutton_contact]; ?></h1>
    </div>
    <div class="row">
        <div class="col-xs-6 col-xs-offset-2">
            <form class="form-horizontal" name="form" method="post" action="sozluk.php?process=iletisimok">
                <div class="form-group">
                    <label for="ad" class="col-xs-3 control-label"><?=$language[name];?></label>
                    <div class="col-xs-9">
                        <input type="text" class="form-control" id="ad" name="ad" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="soyisim" class="col-xs-3 control-label"><?=$language[surname];?></label>
                    <div class="col-xs-9">
                        <input type="text" class="form-control" id="soyisim" name="soyisim" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-xs-3 control-label"><?=$language[email];?></label>
                    <div class="col-xs-9">
                        <input type="text" class="form-control" id="email" name="email" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="telefon" class="col-xs-3 control-label"><?=$language[phone];?></label>
                    <div class="col-xs-9">
                        <input type="text" class="form-control" id="telefon" name="telefon">
                    </div>
                </div>
                <hr/>
                <div class="form-group">
                    <label for="konu" class="col-xs-3 control-label"><?=$language[subject];?></label>
                    <div class="col-xs-9">
                        <input type="text" class="form-control" id="konu" name="konu" value="<?=$konu?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="mesaj" class="col-xs-3 control-label"><?=$language[Message];?></label>
                    <div class="col-xs-9">
                        <textarea class="form-control" id="mesaj" name="mesaj" rows="10"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-offset-3 col-xs-9">
                        <div id='cpholder'>
                            <p><img id="siimage" src="captcha/securimage_show.php?sid=<?php echo md5(uniqid(time())); ?>"></p>
                            <div class="input-group">
                                <input type="text" name="code" id="code" class="form-control" />
                    <span class="input-group-btn">
        								<button class="btn btn-default" type="button" onclick="document.getElementById('siimage').src = 'captcha/securimage_show.php?sid=' + Math.random(); return false">
                                            Refresh
                                        </button>
      								</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-offset-3 col-xs-9">
                        <input class="btn btn-primary" type='submit' name='submit' value='<? echo $language[Send]; ?>' />
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>