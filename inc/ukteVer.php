<?

if (!$verified_user)
    die;

$okmsj = RequestUtil::Get('okmsj');
$baslik = RequestUtil::Get('baslik');

if (!$okmsj || $baslik == "")
{
    echo "what da!";
    exit;
}
else
{

    $mesaj = RequestUtil::Get('mesaj');

    if ($mesaj == "")
    {
        form($baslik);
        return;
    }
    else
    {

        $site = $_SERVER["HTTP_REFERER"];
        $site = explode("/", $site);
        $site = $site[2];

        $tarih = date("YmdHi");
        $gun = date("d");
        $ay = date("m");
        $yil = date("Y");
        $saat = date("H:i");
        $ip = getenv('REMOTE_ADDR');

        $yazar = $verified_user;

        $baslik = str_replace("I","ı",$baslik);

        if ($turkce_karakter == 0)
        {
            $baslik = str_replace("%u0131", "i", $baslik);
            $baslik = str_replace("%u011F", "g", $baslik);
            $baslik = str_replace("%u015F", "s", $baslik);
            $baslik = str_replace("ş", "s", $baslik);
            $baslik = str_replace("Ş", "S", $baslik);
            $baslik = str_replace("ç", "c", $baslik);
            $baslik = str_replace("Ç", "C", $baslik);
            $baslik = str_replace("ı", "i", $baslik);
            $baslik = str_replace("İ", "I", $baslik);
            $baslik = str_replace("ğ", "g", $baslik);
            $baslik = str_replace("Ğ", "G", $baslik);
            $baslik = str_replace("ö", "o", $baslik);
            $baslik = str_replace("Ö", "O", $baslik);
            $baslik = str_replace("ü", "u", $baslik);
            $baslik = str_replace("Ü", "U", $baslik);
            $baslik = str_replace("Ö", "O", $baslik);
        }

        $baslik = str_replace("\$", "s", $baslik);
        $baslik = str_replace("@", "a", $baslik);
        $baslik = str_replace("\,", " ", $baslik);
        $baslik = str_replace("\/", " ", $baslik);
        $baslik = str_replace("\[", " ", $baslik);
        $baslik = str_replace("\]", " ", $baslik);
        $baslik = str_replace("\'", " ", $baslik);
        $baslik = str_replace("\.", " ", $baslik);
        $baslik = str_replace("\-", " ", $baslik);
        $baslik = str_replace("  ", " ", $baslik);
        $baslik = str_replace("\?", " ", $baslik);
        $baslik = str_replace("\!", " ", $baslik);

        $baslik = strtolower($baslik);
        $mesaj = strtolower($mesaj);

        $baslik = substr($baslik, 0, 50);

        $mesaj = str_replace("<","(",$mesaj);
        $mesaj = str_replace(">",")",$mesaj);
        $mesaj = str_replace("\n","<br>",$mesaj);


        $sorgu = "SELECT id FROM ukte WHERE `baslik`='$baslik'";
        $sorgulama = mysqli_query($baglan,$sorgu);
        if (mysqli_num_rows($sorgulama)>0){
//kayıtları listele
            while ($kayit=mysqli_fetch_array($sorgulama)){
###################### var ##############################################
                $id=$kayit["id"];
                if ($id) {
                    echo "$language[wishAlreadyWaitingTopic]";
                    die;
                }
            }
        }

// db ye yaz
        $baslik = strtolower($baslik);
        $sorgu = "INSERT INTO ukte ";
        $sorgu .= "(baslik,aciklama,yazar,tarih)";
        $sorgu .= " VALUES ";
        $sorgu .= "('$baslik','$mesaj','$yazar','$tarih')";
        mysqli_query($baglan,$sorgu);


        echo "
<script language=\"javascript\">goUrl('sozluk.php?process=kuyu','left');</script>
<br>
<center>$language[wishAddedWishings]</center>
";

    }
}

function form($baslik){ ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <form method="post">
                    <div class="form-group">
                        <input class="inp form-control" maxLength="80" size="60" name="baslik" value=<?php if ($baslik) { echo "\"$baslik\" readonly"; } else echo "\"\""; ?>>
                    </div>
                    <div class="form-group">
                        <p><?=$language[ukte_note]; ?></p>
                        <textarea class="form-control" id="aciklama" name="mesaj" rows="2"></textarea>
                    </div>
                    <div class="form-group">
                        <input id="kaydet" class="btn btn-primary" type="submit" name="kaydet" value="ok">
                    </div>
                    <input type=hidden name="ok" value=ok>
                    <input type=hidden name="okmsj" value=ok>
                    <input type="hidden" name="gonder" value="<?=$language[throwup]?>">
                </form>
            </div>
        </div>
    </div>
    </body>
    </html>
    <?
}
?>