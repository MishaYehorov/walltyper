//////////////////////////////////////////////////////////////////////////////////////////////
//	Turkish Translation by Nuri AKMAN
//	Location: Ankara/TURKEY
//	e-mail	: nuriakman@hotmail.com
//	Date	: April, 9 2003
//
//	Note: if Turkish Characters does not shown on you screen
//		  please include falowing line your html code:
//
//		  <meta http-equiv="Content-Type" content="text/html; charset=windows-1254">
//
//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
//	Turkish translation fix Aykut Dizdar
//	Istanbul/TURKEY
//	aykutdizdar@yahoo.com
//	Date	: Feb, 11 2006
//////////////////////////////////////////////////////////////////////////////////////////////

// ** I18N
Calendar._DN = new Array
("Pazar",
 "Pazartesi",
 "Sal�",
 "�ar�amba",
 "Per�embe",
 "Cuma",
 "Cumartesi",
 "Pazar");
 Calendar._SDN = new Array
("Paz",
 "Pzt",
 "Sal",
 "�ar",
 "Per",
 "Cum",
 "Cts",
 "Paz");
 Calendar._MN = new Array
("Ocak",
 "�ubat",
 "Mart",
 "Nisan",
 "May�s",
 "Haziran",
 "Temmuz",
 "A�ustos",
 "Eyl�l",
 "Ekim",
 "Kas�m",
 "Aral�k");
 Calendar._FD = 1;
 Calendar._SMN = new Array
("Oca",
 "�ub",
 "Mar",
 "Nis",
 "May",
 "Haz",
 "Tem",
 "A�u",
 "Eyl",
 "Eki",
 "Kas",
 "Ara");

// tooltips
Calendar._TT = {};
Calendar._TT["INFO"] = "Takvim hakk�nda";

Calendar._TT["ABOUT"] =
"DHTML Date/Time Selector\n" +
"(c) dynarch.com 2002-2005 / Author: Mihai Bazon\n" + // don't translate this this ;-)
"En son versiyon i�in: http://www.dynarch.com/projects/calendar/\n" +
"GNU LGPL lisans� ile da��t�lmaktad�r.  Ayr�nt�lar i�in http://gnu.org/licenses/lgpl.html adresine g�z at�n�z." +
"\n\n" +
"Tarih se�imi:\n" +
"- \xab ve \xbb d��melerini y�l se�mek i�in\n" +
"- " + String.fromCharCode(0x2039) + " ve " + String.fromCharCode(0x203a) + " d��melerini ay se�mek i�in kullan�n�z.\n" +
"- H�zl� se�im yapmak i�in yukar�daki d��melere fareyle bas�l� tutunuz.";
Calendar._TT["ABOUT_TIME"] = "\n\n" +
"Saat se�imi:\n" +
"- Artt�rmak i�in zaman�n �zerine t�klay�n�z\n" +
"- Azaltmak i�in Shift ile t�klay�n�z\n" +
"- Daha h�zl� se�im i�in t�klay�p s�r�kleyiniz.";
Calendar._TT["TOGGLE"] = "Haftan�n ilk g�n�n� kayd�r";
Calendar._TT["PREV_YEAR"] = "�nceki Y�l (H�zl� se�im i�in bas�l� tutunuz)";
Calendar._TT["PREV_MONTH"] = "�nceki Ay (H�zl� se�im i�in bas�l� tutunuz)";
Calendar._TT["GO_TODAY"] = "Bug�n'e git";
Calendar._TT["NEXT_MONTH"] = "Sonraki Ay (H�zl� se�im i�in bas�l� tutunuz)";
Calendar._TT["NEXT_YEAR"] = "Sonraki Y�l (H�zl� se�im i�in bas�l� tutunuz)";
Calendar._TT["SEL_DATE"] = "Tarih se�iniz";
Calendar._TT["DRAG_TO_MOVE"] = "Ta��mak i�in s�r�kleyiniz";
Calendar._TT["PART_TODAY"] = " (bug�n)";
Calendar._TT["MON_FIRST"] = "Takvim Pazartesi g�n�nden ba�las�n";
Calendar._TT["SUN_FIRST"] = "Takvim Pazar g�n�nden ba�las�n";
Calendar._TT["DAY_FIRST"] = "%s g�n�n� ilk g�ster.";
Calendar._TT["WEEKEND"] = "0,6";
Calendar._TT["CLOSE"] = "Kapat";
Calendar._TT["TODAY"] = "Bug�n";
Calendar._TT["TIME_PART"] = "De�eri de�i�tirmek i�in t�klay�n�z.";
// date formats
Calendar._TT["DEF_DATE_FORMAT"] = "%Y-%m-%d";
Calendar._TT["TT_DATE_FORMAT"] = "%e %B %A";

Calendar._TT["WK"] = "Hafta";
Calendar._TT["TIME"] = "Saat:";
