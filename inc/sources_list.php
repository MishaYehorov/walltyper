<?php
session_start();

?>


<div class="container-fluid ">
    <?php
    $category_id = RequestUtil::Get('cat');
//
//    $query = "SELECT sources.*, user_sources.id_source as user_source FROM sources, user_sources
//    WHERE sources.category_id=".$category_id." and (user_sources.user_id=".$_SESSION['id_user']." and user_sources.id_source = sources.id)";

    $query = "SELECT * FROM sources WHERE category_id=".$category_id;
    $res = mysqli_query($baglan,$query);



    while($row = mysqli_fetch_array($res)){
//        var_dump($row)
    ?>
    <div class="col-xs-6">
            <div class='sources-block panel panel-default'>
                <div class="panel-body">
                    <div class="media">
                        <div class="media-left rss">
                            <a target="_blank" href="<?=$row['rss_link'] ?>">
                                <?php if(!empty($row["thumbnail"]) && file_exists('upload/sources/'.$row["thumbnail"])){ ?>
                                <img class="media-object rss-thumb" src='upload/sources/<?php echo $row['thumbnail'] ?>'>
                                <?php } else { ?>
                                <div class="media-object rss-thumb" style="background-color: #EEEEEE;height: 50px;"></div>
                                <?php } ?>
                            </a>
                        </div>
                        <div class="media-body">
                            <a target="_blank" href="<?=$row['rss_link'] ?>">
                                <h5 class="media-heading"><?=$row['title']?></h5>
                            </a>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-6 text-center">
                            <small>News number</small>
                            <b><small><?=$row['news_number'] ?></small></b>
                        </div>
                        <div class="col-xs-6 text-right">
                            <form method="post" action="">
                                <input type="text" value="<?=$row['id']?>" name="id_source" style="display: none">
                                <?
                                $query2 = "SELECT * FROM user_sources WHERE id_source =".$row['id']." and user_id=".$_SESSION['id_user'];
                                $res2 = mysqli_query($baglan,$query2);

                                if($row_subscribe = mysqli_fetch_array($res2)){
                                    ?>
                                    <div class="minus-source">
                                        <button class="unsubscribe subscribe-btn btn btn-default <?=$row['id'] ?>-source" data-source="<?=$row['id'] ?>" data-action="unsubscribe" type="button" name="delete" >-</button>
                                        <button class="subscribe subscribe-btn btn btn-success hide <?=$row['id'] ?>-source" data-source="<?=$row['id'] ?>" data-action="subscribe" type="button" name="subscribe">+</button>

                                    </div>

                                    <?php
                                }else{
                                    ?>
                                    <div class="plus-source">
                                        <button class="subscribe subscribe-btn btn btn-success <?=$row['id'] ?>-source" data-source="<?=$row['id'] ?>" data-action="subscribe" type="button" name="subscribe">+</button>
                                        <button class="unsubscribe subscribe-btn btn btn-default hide <?=$row['id'] ?>-source" data-source="<?=$row['id'] ?>" data-action="unsubscribe" type="button" name="delete" >-</button>
                                    </div>

                                    <?php
                                }
                                ?>


                            </form>
                        </div>
                    </div>

                </div>
            </div>
    </div>
    <?php }
//    if (isset($_POST["action"])) {
//        if ($_POST["action"] == 'subscribe') {
//            $create_user_source = mysqli_query($baglan,"CREATE TABLE user_sources (id INT AUTO_INCREMENT, user_id INT, id_source INT, PRIMARY KEY (id))");
//
//            if (!$create_user_source) {
//                echo 'error CREATE TABLE user_sources';
//            }
//
//            $insert_into_user_source = mysqli_query($baglan,"INSERT INTO user_sources (user_id, id_source)
//                                          VALUES (" . $_SESSION['id_user'] . ", '" . $_POST['id_source'] . "')");
//            if (!$insert_into_user_source) {
//                echo 'error INSERT INTO user_sources';
//            }
//            Header('Refresh: 0');
//        }
//
//        if ($_POST['action'] == 'unsubscribe') {
//
//            $delete_user_source = mysqli_query($baglan,"DELETE FROM user_sources WHERE user_id=" . $_SESSION['id_user'] . " and id_source =" . $_POST['id_source']);
//
//            if (!$delete_user_source) {
//                echo 'error DELETE FROM user_sources';
//            }
//            Header('Refresh: 0');
//        }
//    }
   ?>
    <script>
        $('.subscribe-btn').click(function(){
           var id_source = $(this).attr('data-source');
            $.ajax({

                type: "POST",
                url: "inc/send_sources_list.php",
                data: {
                    id_source: id_source,
                    action: $(this).attr('data-action')
                },
                dataType: "html",
                cache: false,
                success: function()
                {
                    $('.' + id_source + '-source').toggleClass('hide');
                }
            });
        });
    </script>
</div>