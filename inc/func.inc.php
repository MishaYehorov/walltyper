<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<?php
require_once('class.inputfilter.php');

function execute_scalar($sql, $def = "")
{
    global $baglan;
    $rs = mysqli_query($baglan, $sql) or die(mysqli_error($baglan) . $sql);
    if (mysqli_num_rows($rs)) {
        $r = mysqli_fetch_row($rs);
        mysqli_free_result($rs);
        return $r[0];
    }
    return $def;
}

function mysql_insert_array($table, $data, $password_field = "")
{ global $baglan;
    foreach ($data as $field => $value) {
        $fields[] = '`' . $field . '`';

        if ($field == $password_field) {
            $values[] = "PASSWORD('" . mysqli_real_escape_string($baglan,$value) . "')";
        } else {
            $values[] = "'" . mysqli_real_escape_string($baglan,$value) . "'";
        }
    }
    $field_list = join(',', $fields);
    $value_list = join(', ', $values);

    $query = "INSERT INTO `" . $table . "` (" . $field_list . ") VALUES (" . $value_list . ")";

    return $query;
}

function ControlMenu($process, $language)
{
    if ($process == "davet") $davet = "active"; else $davet = "";
    if ($process == "imha") $cop = "active"; else $cop = "";
    if ($process == "gorunum") $gorunum = "active"; else $gorunum = "";
    if ($process == "dusmanlarim") $dusmanlarim = "active"; else $dusmanlarim = "";
    if ($process == "arkadaslarim") $arkadaslarim = "active"; else $arkadaslarim = "";
    if ($process == "privmsg") $privmsg = "active"; else $privmsg = "";
    if ($process == "cp") $cp = "active"; else $cp = "";
    if ($process == "onlines") $onlines = "active"; else $onlines = "";

    return "<ul class=\"nav nav-tabs\">
                <li class=\"" . $onlines . "\"><a href=\"sozluk.php?process=onlines\">$language[tab_news]</a></li>
                <li class=\"" . $cp . "\"><a href=\"sozluk.php?process=cp\">$language[tab_setting]</a></li>
                <li class=\"" . $privmsg . "\"><a href=\"sozluk.php?process=privmsg\">$language[tab_message]</a></li>
                <li class=\"" . $arkadaslarim . "\"><a href=\"sozluk.php?process=arkadaslarim\">$language[tab_buddy]</a></li>
                <li class=\"" . $dusmanlarim . "\"><a href=\"sozluk.php?process=dusmanlarim\">$language[tab_blocked]</a></li>
                <li class=\"" . $cop . "\"><a href=\"sozluk.php?process=imha\">$language[tab_bin]</a></li>
                <li class=\"" . $davet . "\"><a href=\"sozluk.php?process=davet\">$language[tab_invite]</a></li>
              </ul>";
}

function GetBakinDur($title, $searchname)
{
    return '<select class="asl" style="width:102% !important;" onchange="orsch(this, \'' . $title . '\')">
				<option>..' . $searchname . '..</option>
				<option value="http://www.google.com/search?q=">google</option> 
				<option value="http://en.wikipedia.org/wiki/Special:Search?fulltext=Search&search=">wikipedia</option>
				<option value="http://www.google.com/search?tbm=isch&q=">google image</option>
				<option value="http://www.youtube.com/results?search_query=">youtube</option> 
				<option value="http://us.imdb.com/find?q=">imdb</option>
				</select>';
}


function EditText($mesaj)
{
    $mesaj = preg_replace("'\(quote]: (.*)\)'Ui", "- quote - \\1 - quote -", $mesaj);
    $mesaj = preg_replace("'\(hidden: (.*)\)'Ui", "\\1", $mesaj);
    $mesaj = preg_replace("'\(u: (.*)\)'Ui", "*\\1*", $mesaj);
    $mesaj = str_replace("<br>", " ", $mesaj);

    return $mesaj;
}

function ControlInput($q)
{
    $myFilter = new InputFilter();

    //Kontrolden geçirelim
    $q = stripslashes($q);
    //$q = $myFilter->process($q);
    $q = convert_smart_quotes($q);
    $q = str_replace("\$", " ", $q);
    $q = str_replace("@", "a", $q);
    $q = str_replace("İ", "i", $q);
    $q = str_replace("I", "ı", $q);
    $q = str_replace("\,", " ", $q);
    $q = str_replace("\/", " ", $q);
    $q = str_replace("\[", " ", $q);
    $q = str_replace("\]", " ", $q);
    $q = str_replace("\'", " ", $q);
    //$q = str_replace("\.", " ", $q);
    $q = str_replace("\+", " ", $q);
    $q = str_replace("\-", "-", $q);
    $q = str_replace("\?", " ", $q);
    $q = str_replace("\!", " ", $q);
    $q = str_replace("\<", " ", $q);
    $q = str_replace("\>", " ", $q);
    $q = str_replace("\"", " ", $q);
    $q = str_replace("\'", " ", $q);
    $q = str_replace("Ş", "ş", $q);
    $q = str_replace("Ç", "ç", $q);
    $q = str_replace("İ", "i", $q);
    $q = str_replace("Ğ", "ğ", $q);
    $q = str_replace("Ö", "ö", $q);
    $q = str_replace("Ü", "ü", $q);
    $q = str_replace("  ", " ", $q);

    return $q;
}

function convert_smart_quotes($string)
{
    $search = array(chr(145),
        chr(146),
        chr(147),
        chr(148),
        chr(151));

    $replace = array("'",
        "'",
        '"',
        '"',
        '-');

    $quotes = array(
        "\xC2\xAB" => '"', // « (U+00AB) in UTF-8
        "\xC2\xBB" => '"', // » (U+00BB) in UTF-8
        "\xE2\x80\x98" => "'", // ‘ (U+2018) in UTF-8
        "\xE2\x80\x99" => "'", // ’ (U+2019) in UTF-8
        "\xE2\x80\x9A" => "'", // ‚ (U+201A) in UTF-8
        "\xE2\x80\x9B" => "'", // ? (U+201B) in UTF-8
        "\xE2\x80\x9C" => '"', // “ (U+201C) in UTF-8
        "\xE2\x80\x9D" => '"', // ” (U+201D) in UTF-8
        "\xE2\x80\x9E" => '"', // „ (U+201E) in UTF-8
        "\xE2\x80\x9F" => '"', // ? (U+201F) in UTF-8
        "\xE2\x80\xB9" => "'", // ‹ (U+2039) in UTF-8
        "\xE2\x80\xBA" => "'", // › (U+203A) in UTF-8
    );
    $string = strtr($string, $quotes);


    return str_replace($search, $replace, $string);
}


function FileConvert($q)
{
    $q = TurkToLatin($q);
    $q = str_replace(" ", "", $q);

    return $q;
}

function Avatar($userID, $isThumb)
{
    $large_image_prefix = "resize_";
    $thumb_image_prefix = "thumbnail_";
    $upload_path = "uploads/";
    $large_image_location = $upload_path . $large_image_prefix . $userID . ".jpg";
    $thumb_image_location = $upload_path . $thumb_image_prefix . $userID . ".jpg";

    if (file_exists($large_image_location)) {
        if (file_exists($thumb_image_location)) {
            $thumb_photo_exists = $thumb_image_location;
        } else {
            $thumb_photo_exists = "";
        }
        $large_photo_exists = $large_image_location;
    } else {
        $large_photo_exists = "";
        $thumb_photo_exists = "";
    }

    if ($isThumb)
        return $thumb_photo_exists;
    else
        return $large_photo_exists;
}

function GetAvatar($userID)
{
    return Avatar($userID, true);
}

function GetBigAvatar($userID)
{
    return Avatar($userID, false);
}

function TurkToLatin($q)
{
    $q = str_replace("%u0131", "i", $q);
    $q = str_replace("%u011F", "g", $q);
    $q = str_replace("%u015F", "s", $q);
    $q = str_replace("ş", "s", $q);
    $q = str_replace("Ş", "S", $q);
    $q = str_replace("ç", "c", $q);
    $q = str_replace("Ç", "C", $q);
    $q = str_replace("ı", "i", $q);
    $q = str_replace("İ", "I", $q);
    $q = str_replace("ğ", "g", $q);
    $q = str_replace("Ğ", "G", $q);
    $q = str_replace("ö", "o", $q);
    $q = str_replace("Ö", "O", $q);
    $q = str_replace("ü", "u", $q);
    $q = str_replace("Ü", "U", $q);
    $q = str_replace("Ö", "O", $q);

    return $q;
}

function GetirBaslikIlkTanim($baslik_id)
{global $baglan;
    $result = mysqli_query($baglan,"SELECT mesaj FROM mesajlar WHERE sira=$baslik_id and statu='' ORDER BY id DESC LIMIT 1");

    if (@mysqli_num_rows($result) > 0) {
        $row = mysqli_fetch_array($result);

        return $row['mesaj'];
    } else
        return "";
}

function GetTitleLastEntryId($titleid)
{global $baglan;
    $result = mysqli_query($baglan,"SELECT id FROM mesajlar WHERE sira=$titleid and statu='' ORDER BY id DESC LIMIT 1");

    if (@mysqli_num_rows($result) > 0) {
        $row = mysqli_fetch_array($result);

        return $row['id'];
    } else
        return "";
}

function GetTitleEntryCount($baslik_id)
{global $baglan;
    $sor = mysqli_query($baglan,"select count(id) from mesajlar WHERE sira=$baslik_id and statu=''");
    $n = @ mysqli_fetch_row($sor);
    return $n[0];
}

function DictionaryString($string)
{
    $mesaj = $string;

    $mesaj = str_replace("‘", "'", $mesaj);
    $mesaj = str_replace("&#8216;", "'", $mesaj);
    $mesaj = str_replace("’", "'", $mesaj);
    $mesaj = str_replace("&#8217;", "'", $mesaj);
    $mesaj = str_replace("“", "''", $mesaj);
    $mesaj = str_replace("”", "''", $mesaj);
    $mesaj = str_replace("&#8220;", "''", $mesaj);
    $mesaj = str_replace("&#8221;", "''", $mesaj);

    $mesaj = preg_replace("'\($language[ara]: (.*)\)'Ui", "($language[ara]: <a href=\"sozluk.php?process=search&vampara_baslik=\\1\" target=left>\\1</a>)", $mesaj);
    $mesaj = preg_replace("'\($language[quote]: (.*)\)'Ui", "- <a href=\"nedir.php?&q=$language[quote]\">$language[quote]</a> - <br><br>\\1 <br><br>- <a href=\"nedir.php?&q=$language[quote]\">$language[quote]</a> -", $mesaj);
    $mesaj = preg_replace("'\($language[gbkz]: (.*)\)'Ui", "<a href=\"nedir.php?&q=\\1\">\\1</a>", $mesaj);
    $mesaj = preg_replace("'\($language[u]: (.*)\)'Ui", "<a href=\"nedir.php?&q=\\1\" title=\"\\1\">*</a>", $mesaj);
    $mesaj = preg_replace("'\(http://(.*)\)'Ui", "<a target=blank_ href=\"http\\3://\\1\">http\\3://\\1</a>", $mesaj);
    $mesaj = preg_replace("'\($language[bkz]: (.*)\)'Ui", "($language[bkz]: <a href=\"nedir.php?&q=\\1\">\\1</a>)", $mesaj);
    $mesaj = preg_replace("'\($language[image]: http://(.*)\)'Ui", "<br><a target=self_ href=\"http://\\1\"><img src=\"http://\\1\"></a>", $mesaj);
    $mesaj = preg_replace("'\(b: (.*)\)'Ui", "<b>\\1</b>", $mesaj);
    $mesaj = preg_replace("'\(i: (.*)\)'Ui", "<i>\\1</i>", $mesaj);
    $mesaj = preg_replace("'\#([0-9]{1,9})'", "<a href=post.php?eid=\\1><b>#\\1</b></a>", $mesaj);
    $mesaj = preg_replace("'\(youtube: (.*)\)'Ui", "<br><iframe title=\"YouTube video player\" class=\"youtube-player\" type=\"text/html\" width=\"640\" height=\"390\" src=\"http://www.youtube.com/embed/\\1?rel=0\" frameborder=\"0\"></iframe>", $mesaj);

    return $mesaj;
}

function dateTime($date)
{
    $z = explode(" ", $date);
    $tarih = $z[0];
    $saat = $z[1];
    $ilk = explode("-", $tarih);
    $son = explode(":", $saat);
    $temptime = mktime($son[0], $son[1], 0, $ilk[1], $ilk[2], $ilk[0]);

    $degisentrh = strftime('%d.%m.%Y %H:%M', $temptime);
    return $degisentrh;
}

function ToTime($date)
{
    $z = explode(" ", $date);
    $tarih = $z[0];
    $saat = $z[1];
    $ilk = explode("-", $tarih);
    $son = explode(":", $saat);
    $temptime = mktime($son[0], $son[1], 0, $ilk[1], $ilk[2], $ilk[0]);

    $degisentrh = strftime('%H:%M', $temptime);
    return $degisentrh;
}

function DateAdd($interval, $number, $date)
{

    $date_time_array = getdate($date);
    $hours = $date_time_array['hours'];
    $minutes = $date_time_array['minutes'];
    $seconds = $date_time_array['seconds'];
    $month = $date_time_array['mon'];
    $day = $date_time_array['mday'];
    $year = $date_time_array['year'];

    switch ($interval) {

        case 'yyyy':
            $year += $number;
            break;
        case 'q':
            $year += ($number * 3);
            break;
        case 'm':
            $month += $number;
            break;
        case 'y':
        case 'd':
        case 'w':
            $day += $number;
            break;
        case 'ww':
            $day += ($number * 7);
            break;
        case 'h':
            $hours += $number;
            break;
        case 'n':
            $minutes += $number;
            break;
        case 's':
            $seconds += $number;
            break;
    }
    $timestamp = mktime($hours, $minutes, $seconds, $month, $day, $year);
    return $timestamp;
}

//ceza için					-- ömer
function cezanezamana($cs, $goster)
{
    $dt = date("Y-m-d H:i");
    $z = explode(" ", $dt);
    $tarih = $z[0];
    $saat = $z[1];
    $ilk = explode("-", $tarih);
    $son = explode(":", $saat);
    $temptime = mktime($son[0], $son[1], 0, $ilk[1], $ilk[2], $ilk[0]);
    $temptime = DateAdd('h', saatFarkiAl(), $temptime);
    $temptime = DateAdd('d', $cs, $temptime);
    if ($goster == "kayit") {
        return strftime('%Y%m%d%H%M', $temptime);
    } else {
        return strftime('%d/%m/%Y %H:%M', $temptime);
    }
}

//parametrelerle geliştirildi			-- ömer
function tarihYarat($gunayyilformat)
{
    $dt = date("Y-m-d H:i");
    $z = explode(" ", $dt);
    $tarih = $z[0];
    $saat = $z[1];
    $ilk = explode("-", $tarih);
    $son = explode(":", $saat);
    $temptime = mktime($son[0], $son[1], 0, $ilk[1], $ilk[2], $ilk[0]);
    $temptime = DateAdd('h', saatFarkiAl(), $temptime);
    if ($gunayyilformat != "") {
        switch ($gunayyilformat) {
            case "d":
                $degisentrh = strftime('%d', $temptime);
                break;
            case "m":
                $degisentrh = strftime('%m', $temptime);
                break;
            case "Y":
                $degisentrh = strftime('%Y', $temptime);
                break;
            case "HM":
                $degisentrh = strftime('%H:%M', $temptime);
                break;
            case "Hi":
                $degisentrh = strftime('%H:%M', $temptime);
                break;
            case "H:i":
                $degisentrh = strftime('%H:%M', $temptime);
                break;
            case "d/m/Y":
                $degisentrh = strftime('%d/%m/%Y', $temptime);
                break;
            case "YmdHM":
                $degisentrh = strftime('%Y%m%d%H%M', $temptime);
                break;
            case "YmdHi":
                $degisentrh = strftime('%Y%m%d%H%M', $temptime);
                break;
            case "YmdHi-10dk":
                $temptime = DateAdd('n', -10, $temptime);
                $degisentrh = strftime('%Y%m%d%H%M', $temptime);
                break;
            case "YmdHi-1":
                $temptime = DateAdd('d', -1, $temptime);
                $degisentrh = strftime('%Y%m%d%H%M', $temptime);
                break;
            case "d/m/Y-1":
                $temptime = DateAdd('d', -1, $temptime);
                $degisentrh = strftime('%d/%m/%Y', $temptime);
                break;
            case "d-1":
                $temptime = DateAdd('d', -1, $temptime);
                $degisentrh = strftime('%d', $temptime);
                break;
            case "m-1":
                $temptime = DateAdd('d', -1, $temptime);
                $degisentrh = strftime('%m', $temptime);
                break;
            case "Y-1":
                $temptime = DateAdd('d', -1, $temptime);
                $degisentrh = strftime('%Y', $temptime);
                break;
            case "m-m":
                $temptime = DateAdd('m', -1, $temptime);
                $degisentrh = strftime('%m', $temptime);
                break;
            case "Y-m":
                $temptime = DateAdd('m', -1, $temptime);
                $degisentrh = strftime('%Y', $temptime);
                break;
            case "Y-m-d":
                $degisentrh = strftime('%Y-%m-%d', $temptime);
                break;
            case "Y/m/d G:i":
                $degisentrh = strftime('%Y/%m/%d %H:%M', $temptime);
                break;
            case "d/m/Y G:i":
                $degisentrh = strftime('%d/%m/%Y %H:%M', $temptime);
                break;
            case "tam":
                $degisentrh = strftime('%Y-%m-%d %H:%M:00', $temptime);
                break;
        }
    } else {
        $degisentrh = strftime('%Y-%m-%d- %H:%M:00', $temptime);
    }
    return $degisentrh;
}

function bugun()
{

    $dt = date("Y-m-d H:i");
    $z = explode(" ", $dt);
    $tarih = $z[0];
    $saat = $z[1];
    $ilk = explode("-", $tarih);
    $son = explode(":", $saat);
    $temptime = mktime($son[0], $son[1], 0, $ilk[1], $ilk[2], $ilk[0]);
    $temptime = DateAdd('h', saatFarkiAl(), $temptime);
    $degisentrh = strftime('%Y-%m-%d', $temptime);
    return $degisentrh;

}

function dun()
{

    $dt = date("Y-m-d H:i");
    $z = explode(" ", $dt);
    $tarih = $z[0];
    $saat = $z[1];
    $ilk = explode("-", $tarih);
    $son = explode(":", $saat);
    $temptime = mktime($son[0], $son[1], 0, $ilk[1], $ilk[2], $ilk[0]);
    $temptime = DateAdd('h', saatFarkiAl(), $temptime);
    $temptime = DateAdd('d', -1, $temptime);
    $degisentrh = strftime('%Y-%m-%d', $temptime);
    return $degisentrh;

}

function dunKisa()
{

    $dt = date("Y-m-d H:i");
    $z = explode(" ", $dt);
    $tarih = $z[0];
    $saat = $z[1];
    $ilk = explode("-", $tarih);
    $son = explode(":", $saat);
    $temptime = mktime($son[0], $son[1], 0, $ilk[1], $ilk[2], $ilk[0]);
    $temptime = DateAdd('h', saatFarkiAl(), $temptime);
    $temptime = DateAdd('d', -1, $temptime);
    $degisentrh = strftime('%d', $temptime);
    return $degisentrh;

}

function saatFarkiAl()
{

    global $baglan;
    $sql = "SELECT saatFarki FROM ayar WHERE id='1'";
    $d = mysqli_query($baglan, $sql);
    $fetch = @ mysqli_fetch_assoc($d);
    return $fetch['saatFarki'];
}
