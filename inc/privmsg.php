<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12">
			<?=ControlMenu($process, $language)?>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-3">
			<ul class="nav nav-pills nav-stacked">
				<li class="<?=tabname("yenimsj")?>"><a href="sozluk.php?process=privmsg&islem=yenimsj"><? echo $language[compose] ?></a></li>
				<li class="<?=tabname("privmsg")?>"><a href="sozluk.php?process=privmsg"><? echo $language[inbox] ?></a></li>
				<li><a href="sozluk.php?process=msjgiden"><? echo $language[outbox] ?></a></li>
			</ul>
		</div>
		<div class="col-xs-9">
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
			<?php

			$islem = RequestUtil::Get('islem');
			$gkime = RequestUtil::Get('gkime');

			function tabname($input)
			{
				$islem = RequestUtil::Get('islem');
				$retval = "";

				if ($islem != "" && $input == "yenimsj")
					$retval = "active";

				if ($islem == "" && $input == "privmsg")
					$retval = "active";

				return $retval;
			}

			if ($islem) {
				if (file_exists("inc/$islem.php"))
					include "inc/$islem.php";
				else if (file_exists("$islem.php"))
					include "inc/$islem.php";
				else
					echo "$language[message_diyarError]";
			}
			else {
				include "msjana.php";
			} ?>
		</div>
	</div>
</div>