<?php
require_once('inc/func.inc.php');
?>
<div class="container-fluid">
<?php
if(!empty($_POST)) 
{
	include("captcha/securimage.php");
	$img = new Securimage();
	$valid = $img->check($_POST['code']);

	$ad = RequestUtil::Get("ad");
	$soyisim = RequestUtil::Get("soyisim");
	$email = RequestUtil::Get("email");
	$telefon = RequestUtil::Get("telefon");
	$konu = RequestUtil::Get("konu");
	$mesaj = RequestUtil::Get("mesaj");

	if (!$valid)
	{
		echo "<br/><div class=\"alert alert-dismissible alert-danger\">
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>
                <strong>Oh snap!</strong> <a href=\"#\" class=\"alert-link\">".$language[register_code_alert]."</a>
              </div>";
		die();
		return;
	}

	$msg="";
	$msg.="$language[name]: $ad <br>";
	$msg.="$language[surname]: $soyisim <br>";
	$msg.="$language[email]: $email <br>";
	$msg.="$language[phone]: $telefon <br>";
	$msg.="$language[subject]: $konu <br>";
	$msg.="$language[Message]: $mesaj <br>";
	$msg.="\n";

	$subj = trim("$language[mail_header] "); //mail ba�l���

	$m_header = "From:$language[mail_header] <$language[dictionaryMail]>\nX-Sender: <$language[dictionaryMail]>\n"; // mailin gidece�i adres
	$m_header .= "X-Priority: 1\n"; // Acil stat�
	$m_header .= "Return-Path: <$language[dictionaryMail]>\n";  // mailin gidece�i adres
	$m_header .= "Content-type:text/html; charset=iso-8859-1\n";

	$Insert = array("email" => $email, "ad_soyad" => $ad, "telefon" => $telefon, "mesaj" => $mesaj,  "konu" => $konu, "credate" => tarihYarat("tam"), "IsActive" => 1);
	mysqli_query($baglan,(mysql_insert_array("iletisim_sikayet", $Insert)));

	if (mail("<$language[dictionaryMail]>", $subj, $mesaj, $m_header)) {}

	$errmsg="";
}
?>
	<div class="page-header">
		<h1><? echo $language['hello'].", ".$ad." ".$soyisim; ?></h1>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<span><? echo $language[mail_thanks]; ?></span>
		</div>
	</div>
</div>

<script type="text/javascript">
	function control()
	{
		var txtUsername = document.getElementById("nick");
		var regex = /^[abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_ ]*$/ ;//regex varaible
		
		if (document.getElementById("code").value == "")
		{
			alert("<?=$language[register_code_message]?>");
			document.getElementById("code").style.backgroundColor="#ffcccc";
			document.getElementById("code").focus();
			return false;
		}
	}
</script>
