<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12">
			<?=ControlMenu($process, $language)?>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-3">
			<ul class="nav nav-pills nav-stacked">
				<li><a href="sozluk.php?process=privmsg&islem=yenimsj"><? echo $language[compose] ?></a></li>
				<li><a href="sozluk.php?process=privmsg"><? echo $language[inbox] ?></a></li>
				<li class="active"><a href="sozluk.php?process=msjgiden"><? echo $language[outbox] ?></a></li>
			</ul>
		</div>
		<div class="col-xs-9">
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
			<?php
			$islem = RequestUtil::Get("islem");
			if ($islem)
			{
				if (file_exists("inc/$islem.php"))
					include "inc/$islem.php";
				else if (file_exists("$islem.php"))
					include "inc/$islem.php";
				else
					echo "$language[message_diyarError]";
			}
			else
				include "msjanagiden.php";
			?>
		</div>
	</div>
</div>
