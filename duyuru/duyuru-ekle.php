<script language="JavaScript" type="text/javascript">
function checkform()
{
  	if (document.getElementById("type").value <= 0) {
    alert( "Announcement type?" );
    document.getElementById("type").focus();
    return false ;
  }
	if (document.getElementById("subject").value == "") {
    alert( "Topic?" );
    document.getElementById("subject").focus();
    return false ;
  }
  
	if (document.getElementById("content").value == "") {
    alert( "Message?" );
    document.getElementById("content").focus();
    return false ;
  }
  
return true;

}
</script>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/><?php  header('content-type:text/html; charset=utf-8'); 
	function checkSpell($string){
		return $string = str_replace("'", "&#39;", $string);
	}
	
	if (RequestUtil::Get('submit'))
	{
		if ($verified_user && $durum == 'on'){
			$subject = checkSpell(RequestUtil::Get('subject'));
			$user_id = checkSpell($context['id']);
			$user = checkSpell($context['nick']);
			$subject = RequestUtil::Get('subject');
			$content = RequestUtil::Get('content');
			$type = checkSpell(RequestUtil::Get('type'));
			$date = tarihYarat("tam");
			
			$content = str_replace("<","&lt;",$content);
			$content = str_replace(">","&gt;",$content);
			$content = str_replace("\\\\r\\\\n"," <br> ",$content);

			$sql = "INSERT INTO duyuru (subject, type, content, date, user, user_id) VALUES ('$subject', '$type', '$content', '$date', '$user', $user_id)";
			mysqli_query($baglan,$sql);

			echo redirectPage(0, "index.php");
		}
	}
?>

<div class="content">
	<form name="duyuruekle" method="post" onsubmit="return checkform();">
	<table class="duyurular-single">
	<br>
		<tr>
			<td style="padding-left: 10px;"><?=$language[announcement_type]?>:</td>
			<td>
				<select name="type" id="type" style="width:50%">
				<?
					foreach ($options as $index => $value)
					{
						print '    <option value="' . $index . '"';
						
						if ($index == $option_to_preselect)
						{
							print ' selected ';
						}
						
						print '>' . $value . '</option>
					';
					}
				?>
				</select>
			</td>
			<td><div id="categorydescription"></div></td>
		</tr>
		<tr>
			<td style="padding-left: 10px;"><?=$language[announcement_topic]?>:</td>
			<td>
				<input type="text" name="subject" id="subject" style="width:50%">
			</td>
		</tr>
		<tr>
			<td style="padding-left: 10px;padding-top:120px; width: 80px;"><?=$language[announcement_news]?></td>
			<td style="padding-right: 50px;">
				<div style="text-align:right;white-space:nowrap">
					<input type="button" class="but" onclick="hen('content','(<? echo $language[bkz] ?>: ',')')" value="<? echo $language[bkz] ?>" />
					<input type="button" class="but" onclick="hen('content','(<? echo $language[gbkz] ?>: ',')')" value="<? echo $language[gbkz] ?>"/>
					<input type="button" class="but" onclick="hen('content','(<? echo $language[u] ?>: ',')')" value="*"/>
					<input type="button" class="but" onclick="hen('content','(<? echo $language[image] ?>: http://',')')" value="<? echo $language[image] ?>" />
					<input type="button" class="but" onclick="hen('content','(http://',')')" value="link" />
				</div>
				<textarea name="content" id="content" rows="10" cols="80"></textarea>
			</td>
		</tr>
		<tr>
		<td></td>
		<td><input type="submit" name="submit" value="<?=$language['Send']?>" class="but"></td>
		</tr>
	</table>

	<input type="hidden" name="uid" size="20" value="<?php echo $context['id']; ?>">
	</form>
</div>