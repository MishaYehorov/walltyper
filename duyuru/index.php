<?php
session_start();
ob_start();
function redirectPage($sure,$url){
	$deger = "<meta http-equiv=\"refresh\" content=\"$sure;url=$url\">\n";
	return $deger;
}

include "../inc/baglan.php";
include "../inc/func.inc.php";
include "../baslik.php";

$page = RequestUtil::Get('page');
$id = RequestUtil::Get('id');
$user = RequestUtil::Get('yazar');
$cevap = RequestUtil::Get('cevap');
$cevapsil = RequestUtil::Get('cevapsil');
$type = RequestUtil::Get('type');

if ($type)
	$option_to_preselect = $type;

$options = array
(
	0 => $language['announcement_all'],
	1 => $language[listenUp3],
	2 => $language[announcement_question],
	3 => $language[announcement_forsale]
);

if($verified_user){
	$get = mysqli_query($baglan,"select * from user where nick='$verified_user'");
	$context = mysqli_fetch_array($get);
	$tema = $context['tema'];
	$uid = $context['id'];
	$yazar = $context['nick'];
	$yetki = $context['yetki'];
	$durum = $context['durum'];

	if ($cevap)
	{
		$soru_id = RequestUtil::Get('did');
		$cevap = Msg(RequestUtil::Get('cevab'));
		$date = tarihYarat("tam");

		$sql = "INSERT INTO duyuru_cevap (soru_id, user_id, cevap, date, user) VALUES ($soru_id, $uid,'$cevap','$date', '$yazar')";
		mysqli_query($baglan,$sql);

		echo redirectPage(0, "index.php?id=$soru_id");
	}

	if ($cevapsil)
	{
		$cevap = RequestUtil::Get('cevap_id');

		$sql = "DELETE FROM duyuru_cevap WHERE cevap_id = $cevap";
		mysqli_query($baglan,$sql);

		echo redirectPage(0, "index.php");
	}
}

?>
<!--<style type="text/css">
	.buddy{width:100%; padding-left:150px;}

	.buddy .username {width: 270px; float:right; margin-right: 15%;}
	.content { border:1px solid #5c0000; background:#f6f6f6; color:black;  width:80%; height:auto; font-family:tahoma; font-size: 8pt; margin-top: 10px; }
	.single { border-bottom: 1px dotted #c0c0c0; margin: 5px 5px 5px 5px; color: black;}
	.duyurular-single {color: black; width:100%;}
	.highfive
	{
		font-family:tahoma; font-size: 8pt;
		padding: 10px;
		background: #F2D99E;
		color: black;
		border-bottom: 1px solid #000;
	}
</style>-->
<script language="JavaScript" type="text/javascript">
	function duyuruAc(id){

		if (document.getElementById(id).style.height=='auto'){
			document.getElementById(id).style.height='15px'
		}
		else{
			document.getElementById(id).style.height='auto';
		}
	}
	function cevapAc(id){

		if (document.getElementById(id).style.height=='auto'){
			document.getElementById(id).style.height='15px'
		}
		else{
			document.getElementById(id).style.height='auto';
		}
	}
</script>
<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12">
			<?php if($durum != 'off') { ?>
				<select name="type" class="form-control pull-right" id="duyuru" style="width:200px" onchange="if(this.selectedIndex>-1){window.location='index.php?type='+this.options[this.selectedIndex].value;}">
					<? foreach ($options as $index => $value) {
						print '    <option value="' . $index . '"';
						if ($index == $option_to_preselect) {
							print ' selected ';
						}
						print '>' . $value . '</option>';
					} ?>
				</select>
				<? if ($verified_user) {?>
					<a class="btn btn-primary" href="?p=submit"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> <?=$language[announcement_add]?></a>
				<? } ?>
				<?php
			}?>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<?php if(RequestUtil::Get('p') == 'index' || !RequestUtil::Get('p')){
				?>
				<div class="content">
					<?php
					$max = 20;
					$perpage=20;
					if (!$page)  { $page=1; }
					$alt = ($page - 1)  * $max;

					if ($id) $equery = "WHERE id = $id";

					if ($user) $yquery = "WHERE user = '$user'";

					if ($type) {
						$tquery = "WHERE type = $type";
					}

					$sql_full = "select * from duyuru";
					$sql = "select * from duyuru $equery $yquery $tquery ORDER BY id DESC limit $alt,$max";

					$getNews = mysqli_query($baglan,"$sql");
					$checkNews =@ mysqli_num_rows($getNews);

					$totalCount =@ mysqli_num_rows(mysqli_query($baglan,"$sql_full"));

					if($checkNews > 0){
						while($rowNews =@ mysqli_fetch_array($getNews)){
							$mesaj = $rowNews['content'];

							$sql = "select * from duyuru_cevap WHERE soru_id = $rowNews[id] ORDER BY cevap_id asc";
							$cevaplar = mysqli_query($baglan,"$sql");
							$cevapCount =@ mysqli_num_rows($cevaplar);

							?>
							<div class="single">
								<table cellspacing="0" border="0" class="duyurular-single">
									<tr>
										<td rowspan="3" width="75" align="center"><img src="../images/duyurular/<?php echo $rowNews['type'];?>.gif"></td>
										<td><b><a style="cursor:pointer" onClick="duyuruAc('tc<?php echo $rowNews['id']; ?>');"><?if ($cevapCount > 0)echo "($cevapCount)";?> <?php echo $rowNews['subject']; ?></a></b></td>
									</tr>
									<tr>
										<td>
											<div id="tc<?php echo $rowNews['id']; ?>" style="height: 15px; overflow:hidden;"><?=Msg($mesaj)?>
												<? if ($verified_user && $rowNews['type'] == "2") {?><br><br>
													<div id="d<?php echo $rowNews['id']; ?>" style="height: 15px; overflow:hidden;">
														<a href="#" onClick="cevapAc('d<?php echo $rowNews['id']; ?>');"> <?=$language[announcement_cevab]?></a><br>
														<form name="duyurusil" method="post" style="margin: 0; paddin: 0;">
															<textarea name="cevab" id="cevab" rows="6" cols="80"></textarea>
															<input type="hidden" value="<?=$rowNews['id']?>" name="did">
															<input type="submit" name="cevap" value="<?=$language[reply]?> " class="but">
														</form>
													</div>
												<? }	?>
												<br><hr>
												<?
												if ($cevapCount > 0)
													while($cevab =@ mysqli_fetch_array($cevaplar)){
														echo "<div class=highfive>".Msg($cevab['cevap'])."<br><div style='text-align:right'><b>$cevab[user]</b> (" . dateTime($cevab[date]) .")</div></div>";
														if($yetki=='admin' || $yetki == 'mod' || $rowNews['user'] == $context['nick'])
															echo '<table><tr><td><form name="cevapsil" method="post" style="margin: 0; paddin: 0;">
										<input type="hidden" value="', $cevab['cevap_id'], '" name="cevap_id">
										<input type="submit" name="cevapsil" value="$language[delete]" class="btn btn-default btn-sm" onclick="return confirm(\'Cevabı silmek istediğinize emin misiniz?\')">
										</form></td><td>
										<form name="cevapsil" action="?p=cedit"  method="post" style="margin: 0; paddin: 0;">
										<input type="hidden" value="', $cevab['cevap_id'], '" name="cevap_id">
										<input type="submit" name="cedit" value="$language[edit]" class="btn btn-default btn-sm">
										</form></td></tr></table>';
													}
												?>
											</div>
										</td>
									</tr>
									<tr>
										<td rowspan="4" align="right" width="100%"><a href="?id=<?=$rowNews['id']?>">#<?=$rowNews['id']?></a>
											(<a href="?yazar=<?=$rowNews['user']; ?>"><?=$rowNews['user']?></a>
											<? if ($verified_user) {?>
												<a href="javascript:od('../sozluk.php?process=ben&kim=<?php echo $rowNews['user']; ?>', '350','400')">?</a>
											<?}?>
											<?=dateTime($rowNews['date'])?>)
											<? if ($verified_user) {?>
												<input type="button" value="<?=$language[msg]?>" class="btn btn-default btn-sm" onclick="javascript:od('../sozluk.php?process=popupmsg&islem=yenimsj.popup&gkime=<?php echo $rowNews['user']; ?>&gkonu=<?=TurkToLatin($rowNews['subject'])?>','400','250');"></input>
											<?}?>
										</td>
										<td>
											<?php
											if($yetki=='admin' || $yetki == 'mod' || $rowNews['user'] == $context['nick'])
												echo "<form name='duyurusil' action='?p=del' method='post' style='margin: 0; paddin: 0;'>
							<input type='hidden' value='", $rowNews['id'], "' name='did'>
							<input type='hidden' value='", $rowNews['user'], "' name='user'>
							<input type='submit' value='$language[delete]' class='btn btn-default btn-sm' onclick='return confirm(\'Duyuruyu silmek istediğinizden emin misiniz?\')'>
							</form>";
											?>
										</td>
										<td>
											<?
											if($yetki=='admin' || $yetki == 'mod' || $rowNews['user'] == $context['nick'])
												echo "<form name='duyuruedit' action='?p=edit' method='post' style='margin: 0; paddin: 0;'>
							<input type='hidden' value='", $rowNews['id'], "' name='did'>
							<input type='hidden' value='", $rowNews['user'], "' name='user'>
							<input type='submit' value='$language[edit]' class='btn btn-default btn-sm'>
							</form>";
											?>
										</td>
									</tr>
								</table>
							</div>
							<?php
						}
					}
					else {
						echo "<br/> $language[noAnnouncementYet]
			<br/>";
					}
					?>
				</div>
				<?php
			}
			else if(RequestUtil::Get('p') == 'submit')
				include('duyuru-ekle.php');
			else if(RequestUtil::Get('p') == 'add')
				include('duyuru-gonder.php');
			else if(RequestUtil::Get('p') == 'del')
				include('duyuru-sil.php');
			else if(RequestUtil::Get('p') == 'edit')
				include('duyuru-duzenle.php');
			else if(RequestUtil::Get('p') == 'cedit')
				include('cedit.php');

			function Msg($string)
			{
				$mesaj = $string;

				$mesaj = str_replace("‘","'",$mesaj);
				$mesaj = str_replace("&#8216;","'",$mesaj);
				$mesaj = str_replace("’","'",$mesaj);
				$mesaj = str_replace("&#8217;","'",$mesaj);
				$mesaj = str_replace("“","''",$mesaj);
				$mesaj = str_replace("”","''",$mesaj);
				$mesaj = str_replace("&#8220;","''",$mesaj);
				$mesaj = str_replace("&#8221;","''",$mesaj);
				$mesaj = str_replace("\\\\r\\\\n"," <br> ",$mesaj);


				$mesaj = preg_replace("'\($language[ara]: (.*)\)'Ui","($language[ara]: <a href=\"sozluk.php?process=search&vampara_baslik=\\1\" target=left>\\1</a>)",$mesaj);
				$mesaj = preg_replace("'\($language[quote]: (.*)\)'Ui","- <a href=\"nedir.php?&q=$language[quote]\">$language[quote]</a> - <br><br>\\1 <br><br>- <a href=\"nedir.php?&q=$language[quote]\">$language[quote]</a> -",$mesaj);
				$mesaj = preg_replace("'\($language[gbkz]: (.*)\)'Ui","<a href=\"nedir.php?&q=\\1\">\\1</a>",$mesaj);
				$mesaj = preg_replace("'\($language[u]: (.*)\)'Ui","<a href=\"nedir.php?&q=\\1\" title=\"\\1\">*</a>",$mesaj);
				$mesaj = preg_replace("'\(http://(.*)\)'Ui","<a target=blank_ href=\"http\\3://\\1\">http\\3://\\1</a>",$mesaj);
				$mesaj = preg_replace("'\($language[bkz]: (.*)\)'Ui","($language[bkz]: <a href=\"nedir.php?&q=\\1\">\\1</a>)",$mesaj);
				$mesaj = preg_replace("'\($language[image]: http://(.*)\)'Ui","<br><a target=self_ href=\"http://\\1\"><img src=\"http://\\1\"></a>",$mesaj);
				$mesaj = preg_replace("'\(b: (.*)\)'Ui","<b>\\1</b>",$mesaj);
				$mesaj = preg_replace("'\(i: (.*)\)'Ui","<i>\\1</i>",$mesaj);
				$mesaj = preg_replace("'\#([0-9]{1,9})'","<a href=post.php?eid=\\1><b>#\\1</b></a>",$mesaj);
				$mesaj = preg_replace("'\(youtube: (.*)\)'Ui","<br><iframe title=\"YouTube video player\" class=\"youtube-player\" type=\"text/html\" width=\"640\" height=\"390\" src=\"http://www.youtube.com/embed/\\1?rel=0\" frameborder=\"0\"></iframe>",$mesaj);

				return $mesaj;
			}
			?>
		</div>

		<?php
		if($totalCount > $perpage) :
			echo ("<center>sayfa: ");
			$x = 2; // akrif sayfadan önceki/sonraki sayfa gösterim sayısı
			$lastP = ceil($totalCount/$perpage);

			// sayfa 1'i yazdır
			if($page==1) echo "<u>1</u>";
			else echo "<a href=\"?page=1\">1</a>";
			// "..." veya direkt 2
			if($page-$x > 2) {
				echo "...";
				$i = $page-$x;
			} else {
				$i = 2;
			}
			// +/- $x sayfaları yazdır
			for($i; $i<=$page+$x; $i++) {
				if($i==$page) echo "-<u>$i</u>";
				else echo "-<a href=\"?page=$i\">$i</a>";
				if($i==$lastP) break;
			}
			// "..." veya son sayfa
			if($page+$x < $lastP-1) {
				echo "...";
				echo "-<a href=\"?page=$lastP\">$lastP</a>";
			} elseif($page+$x == $lastP-1) {
				echo "-<a href=\"?page=$lastP/\">$lastP</a>";
			}
			echo "</center>";
		endif;
		?>
	</div>
</div>
