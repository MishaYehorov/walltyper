<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/><?php  header('content-type:text/html; charset=utf-8');
include "inc/baglan.php";
include "inc/func.inc.php";
require_once('settings.php');

$bugun = bugun();
$ay = tarihYarat("m");
$gmtdateiso = date("D, d M Y H:i:s O");		// bu özel

echo "<?xml version=\"1.0\" encoding=\"iso-8859-9\"?>\n";
echo "<rss version=\"2.0\">\n";
echo "	<channel>\n";
echo "		<title>$language[dictionaryName] - $language[last_entries]</title>\n";
echo "		<link>$language[dictionaryUrl]/</link>\n";
echo "		<description>$language[last_entries]</description>\n";
echo "		<language>en</language>\n";
echo "		<copyright>Copyright 2008 $language[dictionaryName]</copyright>\n";
echo "		<pubDate>$gmtdateiso</pubDate>\n";
echo "		<lastBuildDate>$gmtdateiso</lastBuildDate>\n";
echo "		<ttl>1</ttl>\n";

$listele = mysqli_query($baglan,"SELECT id, baslik, tarih, gun, statu FROM konular WHERE statu= '' ORDER BY tarih desc limit 0,25");
while($kayit =@ mysqli_fetch_array($listele))
{
	$id = $kayit["id"];
	$baslik = $kayit["baslik"];
	$link = str_replace(" ","+",$baslik);
	$tarih = $kayit["tarih"];
	
	echo "		<item>\n";
	echo "			<title>$baslik</title>\n";
	echo "			<link>$language[dictionaryUrl]/?title=$link</link>\n";
	echo "			<guid isPermaLink=\"true\">$language[dictionaryUrl]/?title=$link</guid>\n";
	echo "			<description>";
	echo 			EditText(substr(GetirBaslikIlkTanim($id),"",400)); 
	echo 			"...</description>\n";

	$gmtdatetime = mktime(substr($tarih,8,2),substr($tarih,10,2),0,substr($tarih,4,2),substr($tarih,6,2),substr($tarih,0,4));
	$gmtdatetimeformatted = strftime('%a, %d %b %Y %H:%M:%S +0300',$gmtdatetime);

	echo "			<pubDate>$gmtdatetimeformatted</pubDate>\n";
	echo "		</item>\n";
}

echo "	</channel>\n";
echo "</rss>\n";
?>