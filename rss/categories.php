<?php

include('header.php');
include "../inc/baglan.php";

$sql = "ALTER TABLE categories ADD images VARCHAR ";
$query = mysqli_query($baglan,$sql);


if(!empty($_GET['case'])) {

$case = make_safe($_GET['case']);	

} else {

$case = '';	

}

switch ($case) {

	case 'add';

		if (isset($_POST['submit']))

		{

			$category = make_safe(xss_clean($_POST['category']));

			if(isset($_POST['index_view']))

			{

				$index_view = intval(make_safe(xss_clean($_POST['index_view'])));

			}

			else{

				$index_view = 0;	

			}

			if(isset($_POST['menu_view']))

			{

				$menu_view = intval(make_safe(xss_clean($_POST['menu_view'])));

			}

			else

			{

				$menu_view = 0;	

			}

			$seo_keywords = make_safe(xss_clean($_POST['seo_keywords']));

			$seo_description = make_safe(xss_clean($_POST['seo_description']));



			if(empty($category))

			{

				$message = notification('warning','Insert Category Please.');

			}

			else

			{

				if (!empty($_FILES['thumbnail']['name'])) {
					$up = new fileDir('../upload/category/');
					$thumbnail = $up->upload($_FILES['thumbnail']);
				} else {
					$thumbnail = '';
				}

				$sql = "INSERT INTO categories (category,index_view,menu_view,seo_keywords,seo_description,images) VALUES ('$category','$index_view','$menu_view','$seo_keywords','$seo_description','$thumbnail')";

				echo $sql;

				$query = mysqli_query($baglan,$sql);

				if($query)
				{
					$message = notification('success','Category Added Successfully.');
				}
				else
				{
					$message = notification('danger','Error Happened.');
				}

			}

		}

?>			<div class="page-header page-heading">

				<h1>Add New Category

				<a href="categories.php" class="btn btn-default  pull-right"><span class="fa fa-arrow-right"></span></a>

				</h1>

			</div>

			<?php if (isset($message)) {echo $message;} ?>

		<form role="form" method="POST" action="" enctype="multipart/form-data">

		  <div class="form-group">

			<label for="category">Category <span>*</span></label>

			<input type="text" class="form-control" name="category" id="category" />

		  </div>

		<div class="form-group">

			<input type="checkbox" name="menu_view" id="menu_view" value="1" /> <span class="checkbox-label">Display Category In Menu</span>

		  </div>

		  <div class="form-group">

			<input type="checkbox" name="index_view" id="index_view" value="1" /> <span class="checkbox-label">Display Category In Home Page</span>

		  </div>

		  <div class="form-group">

			<label for="seo_keywords">SEO Keywords</label>

			<input type="text" class="form-control" name="seo_keywords" id="seo_keywords" />

		  </div>

		  <div class="form-group">

			<label for="seo_description">SEO Description</label>

			<textarea class="form-control" name="seo_description" id="seo_description" rows="3" ></textarea>

		  </div>

			<div class="form-group">
				<label for="category_id">Image</label>
				<div class="fileinput fileinput-new input-group" data-provides="fileinput">
					<div class="form-control" data-trigger="fileinput"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
					<span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" name="thumbnail"></span>
					<a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>

				</div>
			</div>

		  <button type="submit" name="submit" class="btn btn-primary">Save</button>

		</form>

<?php

break;

case 'edit';

$id = abs(intval(make_safe(xss_clean($_GET['id']))));

if (isset($_POST['submit'])) {

$category = make_safe(xss_clean($_POST['category']));

if (isset($_POST['index_view'])) {

$index_view = intval(make_safe(xss_clean($_POST['index_view'])));

} else {

$index_view = 0;	

}

if (isset($_POST['menu_view'])) {

$menu_view = intval(make_safe(xss_clean($_POST['menu_view'])));

} else {

$menu_view = 0;	

}

$seo_keywords = make_safe(xss_clean($_POST['seo_keywords']));

$seo_description = make_safe(xss_clean($_POST['seo_description']));

if (empty($category)) {

$message = notification('warning','Insert Category Please.');

} else {
	if (!empty($_FILES['thumbnail']['name'])) {
		mkdir("../upload/category/", 0777);
		$up = new fileDir('../upload/category/');
		$thumbnail = $up->upload($_FILES['thumbnail']);
		$up->delete("$_POST[old_thumbnail]");
	} else {
		$thumbnail = $_POST['old_thumbnail'];
	}
$sql = "UPDATE categories SET category='$category',index_view='$index_view',menu_view='$menu_view',seo_keywords='$seo_keywords',seo_description='$seo_description',images='$thumbnail' WHERE id='$id'";

$query = mysqli_query($baglan,$sql);

if ($query) {

$message = notification('success','Category Edited Successfully.');

} else {

$message = notification('danger','Error Happened.');

}

}

}

//$category = $general->category($id);

$sql = "SELECT * FROM categories WHERE id=".$id." LIMIT 1";

		$query = mysqli_query($baglan,$sql);

		if (mysqli_num_rows($query) == 0) {

			

		} else {

			$row = mysqli_fetch_assoc($query);

			

		}

$category = $row;		

?>

			<div class="page-header page-heading">

				<h1>Edit Category

				<a href="categories.php" class="btn btn-default  pull-right"><span class="fa fa-arrow-right"></span></a>

				</h1>

			</div>

			<?php if (isset($message)) {echo $message;} ?>

		<form role="form" method="POST" action="" enctype="multipart/form-data">

		  <div class="form-group">

			<label for="category">Category <span>*</span></label>

			<input type="text" class="form-control" name="category" id="category" value="<?php echo $category['category']; ?>" />

		  </div>

		<div class="form-group">

			<input type="checkbox" name="menu_view" id="menu_view" value="1" <?php if ($category['menu_view'] == 1) {echo 'CHECKED';} ?> /> <span class="checkbox-label">Display Category In Menu</span>

		  </div>

		  <div class="form-group">

			<input type="checkbox" name="index_view" id="index_view" value="1" <?php if ($category['index_view'] == 1) {echo 'CHECKED';} ?> /> <span class="checkbox-label">Display Category In Home Page</span>

		  </div>

		  <div class="form-group">

			<label for="seo_keywords">SEO Keywords</label>

			<input type="text" class="form-control" name="seo_keywords" id="seo_keywords" value="<?php echo $category['seo_keywords']; ?>" />

		  </div>

		  <div class="form-group">

			<label for="seo_description">SEO Description</label>

			<textarea class="form-control" name="seo_description" id="seo_description" rows="3" ><?php echo $category['seo_description']; ?></textarea>

		  </div>

			<div class="form-group">
				<label for="category_id">Image</label>
				<div class="fileinput fileinput-new input-group" data-provides="fileinput">
					<div class="form-control" data-trigger="fileinput"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
					<span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" name="thumbnail"></span>
					<a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
					<input type="hidden" name="old_thumbnail" value="<?php echo $category['images']; ?>" />
				</div>
			</div>
<!--			<p>--><?//=$category['images']?><!--</p>-->
			<?php if (!empty($category['images'])) { ?>
				<p><a href="javascript:void(0);" class="delete-category-image" id="<?php echo $category['id']; ?>" data-toggle="tooltip" data-placement="top" title="Delete Image"><span class="fa fa-close"></span></a> Current Image : <a href="javascript:void();" data-toggle="popover" data-placement="top" title="Current Image" data-content="<img src='../upload/category/<?php echo $category['images']; ?>' class='img-responsive' />"><?php echo $category['images']; ?></a></p>
			<?php } ?>
		  <button type="submit" name="submit" class="btn btn-primary">Save</button>

		</form>

<?php

break;

case 'delete';

	$id = abs(intval(make_safe(xss_clean($_GET['id']))));

	if (isset($_POST['move'])) {

	$new_category = make_safe(xss_clean(intval($_POST['category_id'])));

	if (empty($new_category)) {

	$message = notification('warning','Please Select a Category that you want to move the Sources to.');	

	} else {

	$sql = "SELECT * FROM sources WHERE category_id='$id'";

	$query = mysqli_query($baglan,$sql);

		if (mysqli_num_rows($query) > 0) {

			while ($row =mysqli_fetch_array($query)) {

			mysqli_query($baglan,"UPDATE news SET category_id='$new_category' WHERE source_id='$row[id]'");

			mysqli_query($baglan,"UPDATE sources SET category_id='$new_category' WHERE id='$row[id]'");

		}	

	}

	$delete = mysqli_query($baglan,"DELETE FROM categories WHERE id='$id'");

	if ($delete) {

	$message = notification('success','Sources Moved and Category Deleted Successfully.');

	$done = true;

	} else {

	$message = notification('danger','Error Happened.');

	}

	}

}

if (isset($_POST['delete'])) {

mysqli_query($baglan,"DELETE FROM news WHERE category_id='$id'");

mysqli_query($baglan,"DELETE FROM sources WHERE category_id='$id'");

$delete = mysqli_query($baglan,"DELETE FROM categories WHERE id='$id'");


if ($delete) {
	if (!empty($category['images']) AND file_exists('../upload/category/'.$category['images'])) {
		@unlink('../upload/category/'.$category['images']);
	}

$message = notification('success','Category and All related Sources and News Deleted Successfully.');

$done = true;

} else {

$message = notification('danger','Error Happened.');

}

}



//$tcategory = $general->category($id);

$sql = "SELECT * FROM categories WHERE id=".$id." LIMIT 1";

		$query = mysqli_query($baglan,$sql);

		if (mysqli_num_rows($query) == 0) {

			

		} else {

			$row = mysqli_fetch_assoc($query);

			

		}

$tcategory = $row;		



?>

			<div class="page-header page-heading">

				<h1>Delete Category

				<a href="categories.php" class="btn btn-default  pull-right"><span class="fa fa-arrow-right"></span></a>

				</h1>

			</div>

			<?php if (isset($message)) {echo $message;} ?>

		  <form role="form" method="POST" action="">

		  <?php if (get_category_sources($id) > 0) { ?>

			<div class="alert alert-warning">The Category <b><?php echo $tcategory['category']; ?></b> Contains <b><?php echo get_category_sources($id); ?></b> Source(s). Do You Want To Move Them to Another Category ?</div>

		<div class="form-group">

			<label for="seo_keywords">Choose a Category to Move The Source(s) To.</label>

		  <?php

		  $sql = "SELECT * FROM categories ORDER BY category_order ASC";

			$query = mysqli_query($baglan,$sql);

		  ?>

          

          <select class="form-control" name="category_id" id="category_id">

			<?php 

			//$categories = $general->categories('category_order ASC');

			

			

			

			while ($category = mysqli_fetch_array($query)) {

			if ($tcategory['id'] == $category['id']) {

				

			} else {

			?>

			<option value="<?php echo $category['id']; ?>"><?php echo $category['category']; ?></option>

			<?php			

			}

			}

			?>

			</select>

		</div>

		  <?php } ?>

		  <?php if (isset($done)) { ?>

		  <a href="categories.php" class="btn btn-default">Back To Categories</a>

		  <?php } else { ?>

		  <button type="submit" name="move" class="btn btn-warning">Move Then Delete</button>

		  <button type="submit" name="delete" class="btn btn-danger">Just Delete</button>

		  <?php } ?>

		</form>

<?php

break;

default;

?>

<div class="page-header page-heading">

	<h1><i class="fa fa-reorder"></i> Categories

	<a href="categories.php?case=add" class="btn btn-success  pull-right"><span class="fa fa-plus"></span></a>

	</h1>

</div>

<?php



//$categories = $general->categories('category_order ASC');	



	$sql = "SELECT * FROM categories ORDER BY category_order ASC";

	$query = mysqli_query($baglan,$sql);

		

//	echo mysqli_num_rows($query);





if (mysqli_num_rows($query) == 0) {

echo notification('warning','You didn\'t add any category. <a href="?case=add">Add new category</a>.');	

} else {

?>

<div class="categories-header">

<div class="col-xs-9 col-xs-5">Category</div>

<div class="col-xs-2 hidden-xs">Sources</div>

<div class="col-xs-2 hidden-xs">News</div>

<div class="col-xs-3"></div>

</div>

<div id="sort_category">

<ul>

<?php

while ($category = mysqli_fetch_array($query)) {

	

?>

<li id="records_<?php echo $category['id']; ?>" class="category_li" title="Drag To Re-Order">

<div class="col-xs-9 col-xs-5"><span class="fa fa-reorder"></span> <a href="news.php?case=category&id=<?php echo $category['id']; ?>"><?php echo $category['category']; ?></a></div>

<div class="col-xs-2 hidden-xs"><?php echo get_category_sources($category['id']); ?></div>

<div class="col-xs-2 hidden-xs"><?php echo get_category_news($category['id']); ?></div>

<div class="col-xs-3 text-right">

	<a href="categories.php?case=edit&id=<?php echo $category['id']; ?>" class="btn btn-xs btn-default"><span class="fa fa-edit"></span></a>

	<a href="categories.php?case=delete&id=<?php echo $category['id']; ?>" class="btn btn-xs btn-danger"><span class="fa fa-close"></span></a>

</div>

</li>

<?php	

}	

?>

</ul>

</div>

<?php

}

}

include('footer.php');

?>