<?php

session_start();

// check if the admin is logged, if not redirect it to login page

//if(!isset($_SESSION['rss_script_admin'])) {

//header("location:login.php");

//}

error_reporting(E_ALL); // hide notices and warnings and show only the real errors

// include database connection files and other neccessary classes and functions.

include("include/config.php");

include("include/connect.php");

include("include/functions.php");

//include("include/setting.php");

include("include/general.class.php");

include("include/upload.class.php");

include("include/pagination.php");

// define the general class

//$general = new General;

//$general->set_connection($mysqli);

// fetch the current url to get the page name

$parts = Explode('/', $_SERVER["PHP_SELF"]);

$currenttab = $parts[count($parts) - 1];

//echo '3';

?>

<!DOCTYPE html>

<html>

<head>

    <meta charset="UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">	

    <title>RSS News | Dashboard</title>

    <link rel="stylesheet" href="assets/css/bootstrap.min.css">

	<link rel="stylesheet" href="assets/css/bootstrap-theme.min.css">

	<link rel="stylesheet" href="assets/css/font-awesome.min.css">

	<link href="http://fonts.googleapis.com/css?family=Titillium+Web:700" rel="stylesheet" type="text/css">

	<link rel="stylesheet" href="assets/css/jasny-bootstrap.min.css">

	<link href="assets/js/plugins/morris/morris.css" rel="stylesheet">

    <link rel="stylesheet" href="assets/css/style.css">

	<script src="assets/js/jquery.min.js"></script>

	<script src="assets/js/jquery-ui.min.js"></script>

    <script src="assets/js/bootstrap.min.js"></script>

	<script src="assets/js/jasny-bootstrap.min.js"></script>

	<script src="assets/js/jquery_checkall.js"></script>

	<script src="assets/js/plugins/morris/raphael.min.js"></script>

	<script src="assets/js/plugins/morris/morris.min.js"></script>

	<script src="assets/js/plugins/tinymce/tinymce.min.js"></script>

	<script src="assets/js/plugins/tinymce/tinymce-function.js"></script>

	<script src="assets/js/functions.js"></script>



</head>

<body>

<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">

        <div class="container">

            <div class="navbar-header">

                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">

                    <span class="sr-only">Toggle navigation</span>

                    <span class="icon-bar"></span>

                    <span class="icon-bar"></span>

                    <span class="icon-bar"></span>

                </button>

                

            </div>



            <div class="collapse navbar-collapse">

                <ul class="nav navbar-nav">

                    <li <?php if ($currenttab == 'categories.php') { ?>class="active"<?php } ?>><a href="categories.php"><span class="fa fa-reorder"></span> Categories</a></li>

                    <li <?php if ($currenttab == 'sources.php') { ?>class="active"<?php } ?>><a href="sources.php"><span class="fa fa-rss"></span> Sources</a></li>

					<li <?php if ($currenttab == 'feed_news.php') { ?>class="active"<?php } ?>><a href="feed_news.php"><span class="fa fa-newspaper-o"></span> News</a></li>
					<li <?php if ($currenttab == 'news.php') { ?>class="active"<?php } ?>><a href="news.php"><span class="fa fa-newspaper-o"></span> Published News</a></li>

                    <li <?php if ($currenttab == 'unpublished_news.php') { ?>class="active"<?php } ?>><a href="unpublished_news.php"><span class="fa fa-newspaper-o"></span> Unpublished News</a></li>

                    <li <?php if ($currenttab == 'setting.php') { ?>class="active"<?php } ?>><a href="setting.php?case=general"><span class="fa fa-cog"></span> Setting</a></li>

                </ul>

				

            </div><!--.nav-collapse -->

        </div>

    </nav>

<div class="container">

