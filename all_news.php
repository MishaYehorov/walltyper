<?php
session_start();
ob_start();
error_reporting(E_ALL ^ E_NOTICE);

include "inc/baglan.php";
require_once('settings.php');
require_once('baslik.php');
require_once("inc/func.inc.php");


?>
<div class="container-fluid">
    <div class="news-title-table"><h3>ALL NEWS</h3></div>
    <table class="table table-hover table-striped table-news">


<?php
//$query = "SELECT news.title,news.category_id,categories.category FROM news, user_category,categories
//WHERE user_category.id_category = news.category_id AND user_category.user_id =". $_SESSION['id_user'];
/*$query = "SELECT news.title, news.datetime, news.permalink, news.source_id, sources.title as source_title
FROM news, sources
INNER JOIN user_sources on user_sources.user_id =".$_SESSION['id_user']."
WHERE user_sources.id_source = news.source_id and sources.id = user_sources.id_source and news.published=1 order by news.datetime DESC LIMIT 50 ";*/
$query = "SELECT DISTINCT feed_table.news_id, feed_table.title, feed_table.id, feed_table.p_datetime as datetime, feed_table.source as permalink, feed_table.source_id, sources.title as source_title
FROM feed_table, sources
INNER JOIN user_sources on user_sources.user_id =".$_SESSION['id_user']."
WHERE user_sources.id_source = feed_table.source_id and sources.id = user_sources.id_source order by feed_table.p_datetime DESC LIMIT 50 ";
$res = mysqli_query($baglan,$query);

if(mysqli_num_rows($res) === 0) {
    $query = "SELECT feed_table.id, feed_table.title, feed_table.p_datetime as datetime, feed_table.source as permalink, feed_table.source_id, sources.title as source_title
FROM feed_table, sources
WHERE sources.id = feed_table.source_id order by feed_table.p_datetime DESC LIMIT 50";
    $res = mysqli_query($baglan,$query);
}
while ($row = mysqli_fetch_array($res)) {
    ?>

    <tr>
        <td>
            <div class="news-category"><?= $row['source_title'] ?></div>
            <div class="news-date" id="news-time-<?=$row['id']?>"><?= date('d:m:Y', $row['datetime']) ?></div>
            <script>
                var time = "<?=$row['datetime'] ?>";
                $("#news-time-<?=$row['id']?>").html(moment(time).fromNow());
            </script>
            <a href="<?= $row['permalink'] ?>" target="_blank">
                <div class="news-title"> <?= $row['title'] ?></div>
            </a>
        </td>
    </tr>


    <?php
}
?>

    </table>
</div>