<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/><?php
/*
Sözlük Sistemi - 2012 - Sözlük v6
http://www.sozluksistemi.com
Destek: http://www.sozluksistemi.com/user

Bu dosya, tüm sözlük içindeki metinleri bir araya getirir. Çeviri yapacak olanlar için sistemi çevirmeyi kolaylaştırmayı amaçlamıştır.

- Çeviri Yaparken -

Örneğin: 
$language['dictionaryUrl'] = 'http://www.sozluksistemi.com';

Burada gördüğünüz $language['dictionaryUrl'] , bir değişkendir. İçine kesinlikle dokunulmamalıdır. Çeviri yapacak olanı sadece karşısındaki tırnak içindeki kelime ilgilendirir.
Gerekli çeviriyi tırnaklara dokunmadan yapar.

Örneğin:
$language['button_choose'] = 'seç'; (Orjinali)
$language['button_choose'] = 'choose'; (Çevrilmiş hali)

Kafanızı karıştıran bir durum olması durumunda sozluksistemi.com adresine girip müşteri panelinden destek alabilirsiniz.
*/
	$language = array();
	
	//General
	$language['dictionaryUrl'] = 'http://www.walltyper.com'; //sözlük urlsi (sonunda / koymayın)
	$language['dictionaryName'] = 'Walltyper'; //Sözlüğün ismi gerekli olan yerlerde bu isim kullanılır.
	$language['dictionarySlogan'] = 'Social Media of HKUST'; //sözlük sloganı
	$language['dictionaryYear'] = '2015'; //En alt satırdaki kuruluş yılı
	$language['creator'] = 'HKUST'; //düşünenin nickini yazınız
	$language['dictionaryKeywords'] = 'sözlük,entry'; //sözlük keywordleri
	$language['dictionaryDescription'] = 'sosyal bir ortamda başlıkların entry ile dolduğu yer'; //sözlüğün kısa açıklaması
	$language['googleAnalyticsCode'] = ''; //google analytics kodu
	$language['facebook_url'] = 'http://www.facebook.com'; //sözlüğün facebook adresi açıklaması
	$language['twitter_url'] = 'http://www.twitter.com'; //sözlüğün kısa açıklaması
	$language['software'] = ' <b><a href="http://www.sozluksistemi.com" target=\"_blank\">sözlük sistemi</a></b> ile geliştirilmiştir.'; //Bu kısmı kaldırmayınız, aksi takdirde teknik destek alamazsınız.
	$language['dictionaryMail'] = 'sozluk@sozluksistemi.com'; //Sözlüğün mail adresi
	$language['footerMessage'] = 'sözlük hiçbir kurumla bağlantılı olmayan birkaç kişi tarafından düşünülmüş bağımsız bir platformdur. 
	sözlük içerisindeki yazıların tüm sorumluluğu yazarlarına aiittir. sözlük bu yazıların doğru olduğu hakkında bir teminat vermez. 
	yazılan yazıların telifi bize ait değildir, çalınız çırpınız ama kaynak gösteriniz. '; //En alt satırdaki not
	
	//Generel Buttons
	$language['button_choose'] = 'seç';
	$language['button_back'] = 'geri dön';
	$language['button_bringIt'] = 'getir';
	$language['button_entries'] = 'tanımlar';
	$language['button_message'] = 'mesaj';
	$language['button_buddy'] = 'dost';
	$language['button_blocked'] = 'engelli';
	$language['button_report'] = 'şikayetçiyim!';
	$language['button_youSure'] = 'emin misiniz?';
	$language['button_yes'] = 'evet';
	$language['all'] = 'tümü';
	
	//Top.php
	$language['topbutton_dictionary'] = 'sozluk'; 
	$language['description_dictionary'] = 'sözlük nedir ne değildir?';
	$language['topbutton_sukela'] = 'şukela'; 
	$language['description_sukela'] = 'iyisinden olsun';
	$language['topbutton_mix'] = 'karıştır'; 
	$language['description_mix'] = 'karışık olsun';
	$language['topbutton_stats'] = 'istatistikler';
	$language['description_stats'] = 'rakamlarla sözlük';
	$language['topbutton_enter'] = 'yazar girişi';
	$language['description_enter'] = 'sözlük giriş kapısı';
	$language['topbutton_extras'] = 'aparat';
	$language['description_extras'] = 'sözlük aparatları';
	$language['topbutton_exit'] = 'çıkış';
	$language['description_exit'] = 'sözlükten çıkış';
	$language['message_exit'] = 'sözlükten çıkıyorsunuz?';
	$language['topbutton_me'] = 'ben';
	$language['description_me'] = 'evet sen';
	$language['topbutton_admin'] = 'yönetim';
	$language['description_admin'] = 'yönetim alanı';
	$language['topbutton_spy'] = 'ispiyon';
	$language['description_spy'] = 'casus ünitesi';
	$language['topbutton_sss'] = 'sss';
	$language['topbutton_sss_long'] = 'sözlük+hakkında+sık+sorulan+sorular';
	$language['description_sss'] = 'sözlük hakkında sıkça sorulan sorular';
	$language['topbutton_today'] = 'bugün';
	$language['description_today'] = 'bugün girilen tanımlar';
	$language['topbutton_yesterday'] = 'dün';
	$language['description_yesterday'] = 'dün girilen tanımlar';
	$language['topbutton_contact'] = 'iletişim';
	$language['description_contact'] = 'bize bulaşın';
	$language['topbutton_register'] = 'beni de alın!';
	$language['description_register'] = 'ben de yazar olmak istiyorum!';
	$language['topbutton_control'] = 'kontrol paneli';
	$language['description_control'] = 'kontrol edin';
	$language['topbutton_wish'] = 'ukde';
	$language['description_wish'] = 'ukde deposu';
	$language['topbutton_random'] = 'rastgele';
	$language['description_random'] = 'rastgele bir başlık getirir';
	$language['topbutton_ok'] = 'getir';
	$language['description_ok'] = 'hemen getir';
	$language['topbutton_search'] = 'ara';
	$language['description_search'] = 'benzer başlıkları bul';
	$language['topbutton_id'] = '#';
	$language['description_id'] = 'bu numaralı tanımı getir';
	
	// Word.php
	$language['topics'] = 'başlık';
	$language['entries'] = 'tanım';
	$language['general'] = 'genel';
	$language['OK'] = 'tamam';
	$language['or'] = 'ya da';
	$language['ADD'] = 'ekle';
	$language['page'] = 'sayfa';
	$language['message_edited'] = 'tanımınız düzenlendi';
	$language['deletedTopic'] = 'Bu başlık silinmiş durumdadır.';
	$language['deletedTopic_Mod'] = 'Bu başlık silinmiş durumdadır, yönetici olduğunuz için erişebiliyorsunuz.';
	$language['edit'] = 'Düzenle';
	$language['delete'] = 'Sil';
	$language['redirect'] = 'Yönlendir'; //Başlığı içindeki tüm verileri ile birlikte başka bir başlığa aktarır, yönlendirir.
	$language['stop'] = 'Dur'; 
	$language['pageViews'] = 'görüntülenme'; 
	$language['topicID'] = 'başlık no'; 
	$language['lockTopic'] = 'Kilitle';
	$language['unlockTopic'] = 'Kilidi kaldır';
	$language['messageAlert'] = 'mesajınız var';
	$language['noFriend'] = 'hiç dostunuz yok';
	$language['button_message'] = 'mesaj';
	$language['button_bin'] = 'çöp';
	$language['button_event'] = 'olay';
	$language['inn'] = 'içerisi';
	$language['mixthings'] = 'ortaya karışık';
	$language['searchintitle'] = 'başlık içinde ara';
	$language['searchintitle_error'] = 'aradım fakat bulamadım';
	$language['rookieconfirm_error'] = 'onay bekliyorsunuz';
	$language['search'] = 'ara';
	$language['jump'] = 'zıpla';
	$language['closeBanner'] = 'kapan!'; 	
	$language['howyoudoing'] = 'nasılsın?'; 	
	$language['redirecting'] = 'başlığa yönlendiriliyorsunuz..'; 
	$language['error_newTopic'] = 'başlığındaki karakter ya da karakterler sözlüğe başlık olarak açılamamaktadır. 
	Lütfen başlığınızı gözden geçirip işleminizi tekrar ediniz.'; 
	$language['noResult'] = 'aradık, lakin pek bir başlık bulamadık.'; 
	$language['relatedResults'] = 'fakat şöyle benzer şeyler bulduk:'; 
	$language['wishWaitingTopic'] = 'Bu başlık ukde olarak bırakılmış'; 
	$language['wishAlreadyWaitingTopic'] = 'Bu başlık zaten ukde olarak bırakılmış';
	$language['wishAddedWishings'] = 'başlık ansızın ukte oluverdi.';
	$language['wisher'] = 'ukdeyi veren'; 
	$language['wisherGiveComments'] = 'bir de ukteyi doludracak olana mesaj bırakın';
	$language['wisherComments'] = 'ukdecinin yorumları';
	$language['button_newTopic'] = 'ben açayım';
	$language['button_newWish'] = 'ukde olsun';
	$language['wishWaitingTopics'] = 'ukde';
	$language['wishWaitingTopics_delete'] = 'ukde sil';
	$language['title_solong'] = 'bu başlık 70 karakterden daha fazla. kırp birazcık...<br>';
	$language['warning_rookie'] = 'çaylak iken başlık açamazsınız, yukarıdan rastgele butonunu kullanınız.';
	$language['warning_fillComments'] = 'lütfen tanım giriniz.';
	$language['waitingForApproval'] = 'Tebrikler, gerekli olan tanımlarınızı girmiş gözüküyorsunuz. Yöneticilerimiz tanımlarınızı kontrol
	ettikten sonra sözlüğe kabul edileceksiniz, ya da edilmeyeceksiniz.';
	$language['warning_firstEntry'] = 'İlk tanımınızı girdiğinizi gördük, mutlu olduk. Tekrar hoşgediniz.';
	$language['warning_RookieEntryCounter'] = 'tanımınız! hadi az kaldı!';
	$language['entryInserted'] = 'bir tanım daha kazandık.';
	$language['viewEntry'] = 'bakayım.';
	$language['addnewtitle'] = 'başlığını sözlüğe ekleyelim.';
	$language['youaretheone'] = 'sen varya süpersin!';
	$language['bkz'] = 'bkz'; //bakınız
	$language['ara'] = 'ara'; 
	$language['spoiler'] = 'spoiler';
	$language['quote'] = 'alıntı';
	$language['gbkz'] = 'gbkz'; //gizli bakınız
	$language['u'] = 'u'; //akıllı bakınız
	$language['image'] = 'resim';
	$language['spy'] = 'ispiyonla';
	$language['move'] = 'Taşı';
	$language['editSmall'] = 'düzenle';
	$language['who'] = 'kim?';
	$language['message'] = 'mesaj';
	$language['destroy'] = 'Parçala';
	$language['makeAlive'] = 'Canlandir';
	$language['delete_reason'] = 'Sebep';
	$language['message_deletedEntry'] = 'Bu tanım silinmiş.';
	$language['message_deletedEntry_Edited'] = 'Bu tanım silinmiş, fakat yazar tarafından düzenlenip tekrar yönetici onayına sunulmuş.';
	$language['message_rookieEntry'] = 'çaylak tanım';
	$language['button_showAll'] = 'Tümünü Göster';
	$language['button_save'] = 'Kaydet';
	$language['message_penalty1'] = 'sebebiyle';
	$language['message_penalty2'] = 'cezanızı aldınız.';
	$language['message_rookieEnd'] = 'çaylaklıktan yazarlığa geçmek için gerekli olan tanımları girmişsiniz. onay aşamasındasınız.';
	
	//Bloklar
	$language['hottopics'] = 'Sıcak Başlıklar';
	$language['hardworkers'] = 'Azimli Yazarlar';
	$language['todaysmostloved'] = 'Günün <br>En Şahaneleri';
	$language['insiders'] = 'Kazandakiler';
	
	//Word.php metinleri bitti
	
	//Diğer
	$language['underConstruction'] = 'bir süreliğine bakımdayız.';
	$language['message_siteClosed'] = 'sözlük kapalı durumda.';
	$language['message_banned'] = 'sözlükten uzalaştırılmışsınız.';
	$language['message_wrongPassword'] = 'kullanıcı adınızı ya da şifrenizi yanlış girdiniz.';
	$language['message_diyarError'] = 'Bu bölüm geçici olarak servis dışı..';
	$language['message_logout'] = 'Çıkıp gittiniz ansızın.';
	$language['topic'] = 'başlık';
	$language['entry'] = 'tanım';
	$language['message_fillEverywhere'] = 'her yeri doldur arkadaşım..';
	$language['message_getTopic'] = 'bu başlık önceden açılmış, böyle bir hata alman dahi alkışlık.';
	$language['message_error'] = 'ansızın bir hata oluşuverdi.';
	$language['dictionary_gates'] = 'sözlük girişi';
	$language['username'] = 'kullanıcı adı';
	$language['button_enterSite'] = 'gireyim';
	$language['others'] = 'diğer';
	$language['register'] = 'ben de yazar olmalıyım!';
	$language['forget_pass'] = 'şifremi unuttum?';
	$language['vampara_no_topic'] = 'diye bir başlık yok?';
	$language['vampara_no_information'] = 'nasıl arayayım ki şimdi ben?';
	$language['about'] = 'hakkında';
	
	//inc altındakiler
	$language['add_message'] = 'başlığını hediye edelim :';
	$language['button_back'] = 'geri git';
	$language['message_noAuthor'] = 'böyle bir yazar mevcut değil';
	$language['message_selfEgo'] = 'kendi tanımınıza oy vermek mi?';
	$language['message_alreadyVoted'] = 'bu tanıma zaten oy vermişsiniz';
	$language['message_voteAdded'] = 'oyunuz eklendi';
	$language['message_OK'] = 'peki';
	$language['message_NOT'] = 'sözlükten olmadan burayı göremezsiniz';
	$language['author_NOT'] = 'yazar olmadan burayı göremezsiniz';
	
	//Sözlük Aparatları
	$language['dictionaryTools'] = 'Sözlük Aparatları';
	$language['zirvetor'] = 'Zirvetör';
	$language['zirvetor_description'] = 'sözlük içi aktivite şeysi';
	
	//ben.php  - ben sayfası
	$language['message_reportMessage'] = 'şikayetiniz alındı, yönetimimiz konuyla ilgilenecektir.';
	$language['message_reportMessageAgain'] = 'şikayetiniz kayıtlarmızda, tekrar tekrar şikayet etmenize gerek yok, sakin olun.';
	$language['authorBig'] = 'Yazar';
	$language['message_forgive'] = 'ceza kaldırıldı';
	$language['message_giveAppealRookie'] = 'sebebiyle çaylaklığa düşürüldünüz, gerekli tanımları girip yönetici onayı almayı bekleyiniz.';
	$language['message_giveAppealRookieHeader'] = 'azıcık mola';
	$language['message_giveAppealDelete'] = 'yazar sistemden uçuruldu';
	$language['message_giveAppealDay'] = 'kadar cezalı';
	$language['message_appealMessage1'] = 'sebebiyle';
	$language['message_appealMessage2'] = 'gün kadar ceza aldınız.';
	$language['appealDay'] = 'Kaç günlük ceza';
	$language['makeRookie'] = 'Çaylak olsun';
	$language['makeSuspended'] = 'Uçsun mu?';
	$language['giveAppeal'] = 'cezalandır';
	$language['forgive'] = 'affet';
	$language['ben_title'] = 'kim bu yazar?';
	$language['message_givenAppeal'] = 'bu yazar cezalı durumda';
	$language['admin'] = 'yönetici';
	$language['moderator'] = 'moderator';
	$language['spy'] = 'casus';
	$language['author'] = 'yazar';
	$language['rookie'] = 'çaylak';
	$language['rookieWA'] = 'onay bekleyen çaylak';
	$language['notSuchMember'] = 'böyle bir yazar yok';
	$language['notSuchMemberNick'] = 'isminde bir yazar yok';
	$language['totalEntry'] = 'toplam tanım';
	$language['today'] = 'bugün';
	$language['yesterday'] = 'dün';
	$language['thisMonth'] = 'bu ay';
	$language['lastMonth'] = 'geçen ay';
	$language['latestPositiveVotes'] = 'son alınan + oylar';
	$language['latestNegativeVotes'] = 'son alınan - oylar';
	$language['message_notYet'] = 'henüz yok.';
	$language['todaysEntries'] = 'bugün girdiği tanımlar';
	$language['yesterdaysEntries'] = 'dün girdiği tanımlar';
	$language['deeper'] = 'daha derin';
	$language['users_theme'] = 'kullandığı tema';
	$language['registration_day'] = 'kayıt tarihi';
	$language['votesGiven'] = 'verdiği oy';
	$language['votesTaken'] = 'aldığı oy';
	
	//tabs - kontrol paneli tabları
	$language['tab_news'] = 'olay';
	$language['tab_setting'] = 'ayarlar';
	$language['tab_message'] = 'mesaj';
	$language['tab_buddy'] = 'dostlarım';
	$language['tab_blocked'] = 'engelliler';
	$language['tab_themes'] = 'görünüm';
	$language['tab_bin'] = 'çöp';
	$language['tab_invite'] = 'davetiye';
	
	//cp.php - ayarlar sayfası
	$language['message_entryDeleted'] = 'numaralı tanım sözlükten silindi.';
	$language['message_giveMail'] = 'Lütfen mail adresinizi giriniz.';
	$language['your_mail'] = 'Mail adresiniz';
	$language['your_mail_changed'] = 'ile değiştirildi.';
	$language['my_password'] = 'şifrem';
	$language['new_password'] = 'şu olsun';
	$language['new_password_confirm'] = 'tekrar';
	$language['button_changePassword'] = 'değiştir';
	$language['your_name_surname'] = 'isim & soyisim';
	$language['city'] = 'şehir';
	$language['gender'] = 'cinsiyet';
	$language['male'] = 'erkek';
	$language['female'] = 'kadın';
	$language['my_mail'] = 'mail adresim';
	$language['change_my_mail'] = 'yeni adresim';
	$language['page_options'] = 'sayfa özellikleri';
	$language['message_entryListLimit'] = 'bir sayfada gösterilecek tanım sayısı';
	$language['message_ThemeNot'] = 'böyle bir tema yok';
	$language['message_succeed'] = 'işleminiz başarıyla gerçekleştirildi.';
	
	//diğer
	$language['blockedOnes'] = 'kişiyi engellemişsiniz';
	$language['buddyOnes'] = 'dostunuz var';
	$language['editingNotes'] = 'düzenleme notları';
	$language['notHave'] = 'böyle bir tanım yok';
	$language['plus'] = 'artı';
	$language['minus'] = 'eksi';
	$language['error_try_delete'] = 'başkalarının tanımını silmeye kalkışmak?';
	$language['message_deleted'] = 'silinenler listesine eklendi.';
	$language['button_sure'] = 'eminim';
	$language['message_move'] = 'numaralı başlığa taşı!';
	$language['message_move1'] = 'numaralı tanımı';
	$language['choose'] = 'seç';
	
	//İstatistikler
	$language['stats'] = 'istatistikler!';
	$language['general_stats'] = 'genel istatistikler';
	$language['most_1'] = 'en beğenilen tanımlar';
	$language['most_2'] = 'en kötülenen tanımlar';
	$language['most_3'] = 'geçen haftanın en beğenilen tanımları';
	$language['most_4'] = 'geçen haftanın en kötülenen tanımları';
	$language['most_5'] = 'en çok tanım girilmiş başlıklar';
	$language['most_6'] = 'en çok okunan başlıklar';
	$language['most_7'] = 'günün ilk tanımları';
	$language['most_8'] = 'en çok tanım giren yazarlar';
	$language['most_9'] = 'en az tanım giren yazarlar';
	$language['most_10'] = 'çöpü tıka basa olanlar';
	$language['most_11'] = 'mesaj butonunu eskitenler';
	$language['most_12'] = 'en beğenilen yazarlar';
	$language['most_13'] = 'en kötülenen yazarlar';
	$language['most_14'] = 'aylara göre tanım dağılımı';
	$language['most_15'] = 'mevsimlere göre tanım dağılımı';
	$language['most_16'] = 'sözlüğün cins dağılımı';
	$language['most_17'] = 'tema istatistikleri';
	$language['general_stats_latest'] = 'edinilen son bilgilere göre sözlük..';
	$language['total_entry'] = 'toplam tanım';
	$language['total_topic'] = 'toplam başlık';
	$language['total_population'] = 'toplam nüfus';
	$language['total_deleted_topic'] = 'silinen başlık sayısı';
	$language['total_deleted_entry'] = 'çöpteki tanım sayısı';
	$language['total_topic_views'] = 'toplam başlık gösterimi';
	$language['total_visitors'] = 'toplam ziyaretçi';
	$language['most_followed_topic'] = 'en çok rağbet gören başlık';
	$language['topic_per_author'] = 'yazar başına düşen başlık';
	$language['entry_per_author'] = 'yazar başına düşen tanım';
	$language['message_noStats'] = 'bu istatistik kategorisi henüz boş.';
	$language['spring'] = 'ilkbahar';
	$language['summer'] = 'yaz';
	$language['autumn'] = 'sonbahar';
	$language['winter'] = 'kış';
	
	//Mailler
	$language['forget_pass_subject'] = 'nickli kullanicinin bilgileri';
	$language['hello'] = 'Merhabalar';
	$language['gooddays'] = 'İyi günler dileriz.';
	$language['password'] = 'şifre';
	$language['forget_pass_info'] = 'talebiniz üzerine kullanıcı bilgilerinizi mail adresinize göndermiş bulunuyoruz. 
	bilgileriniz aşağıda belirtildiği gibidir.';
	$language['registeration_mail'] = 'Şifreniz';
	$language['registeration_username'] = 'Kullanıcı Adınız';
	
	//iletişim
	$language['contact'] = 'İletişim';
	$language['name'] = 'Ad';
	$language['surname'] = 'Soyad';
	$language['email'] = 'Email';
	$language['eposta'] = 'e-posta';
	$language['phone'] = 'Telefon';
	$language['subject'] = 'Konu';
	$language['Message'] = 'Mesaj';
	$language['Send'] = 'Gönder';
	$language['mail_header'] = 'Sözlükten mesaj var!';
	$language['mail_thanks'] = 'mesajınız bize ulaşmıştır. en kısa zamanda sizinle irtibata geçeceğiz.';
	
	//çöp
	$language['bin_empty'] = 'çöp kutunuz tertemiz';
	$language['bin_message'] = 'numaralı tanımın silinmesi üzerine';
	
	//ispiyon
	$language['spy_succeed'] = 'silindi ve onaylanmak üzere ispiyon listesine gönderildi.';
	$language['button_whisper'] = 'ispiyonla';
	
	//Mesaj 
	$language['inbox'] = 'gelenler';
	$language['outbox'] = 'gidenler';
	$language['compose'] = 'mesaj yaz';
	$language['to'] = 'kime';
	$language['sender'] = 'kimden';
	$language['reciever'] = 'alıcı';
	$language['msg_subject'] = 'konu';
	$language['msg_message'] = 'mesaj';
	$language['msg_date'] = 'tarih';
	$language['msg_new'] = 'Yeni';
	$language['delete_selected'] = 'Seçileni Sil';
	$language['delete_all'] = 'Tümünü Sil';
	$language['inbox_empty'] = 'kutunuz boş';
	$language['isnotReaded'] = 'okunmadı';
	$language['says'] = 'diyor ki';
	$language['reply'] = 'Cevapla';
	$language['message_sent'] = 'mesajınız gönderildi';
	$language['previous_message'] = 'önceki mesaj';
	$language['tips_subject'] = 'mesajınıza konu seçmezseniz mesajınız gönderilemez';
	$language['button_send_message'] = 'uçur mesajı';
	
	//Onlines.php
	$language['dictionary_news'] = 'Haberler';
	$language['dictionary_onlines'] = 'İçeridekiler';
	$language['dictionary_online_authors'] = 'aktif yazar';
	$language['dictionary_block'] = 'blokla';
	$language['error_notonline'] = 'içeride değilsin!';
	
	//Hata kontrol
	$language['error_password_fillEverywhere'] = 'tüm alanları doldurunuz';
	$language['error_password_wrong'] = 'yazdığınız şifre yanlış';
	$language['error_passwords_wrong'] = 'şifreleriniz uyuşmadı';
	$language['error_passwords_login'] = 'şifreniz başaryla değiştirildi, lütfen sözlüğe yeniden giriş yapınız.';
	$language['error_acceptRules'] = 'sözlüğe üye olabilmek için kuralları kabul etmelisiniz.';
	$language['error_password_wrong'] = 'yazdığınız şifre yanlış';
	$language['error_passwords_wrong'] = 'şifreleriniz uyuşmadı, lütfen tekrar deneyiniz.';
	$language['error_choose_password'] = 'lütfen bir şifre seçiniz';
	$language['error_passwords_login'] = 'şifreniz başaryla değiştirildi, lütfen sözlüğe yeniden giriş yapınız.';
	$language['error_username'] = 'kullanıcı adınız türkçe karakterler içeremez.';
	$language['error_mail'] = 'geçerli bir mail adresi veriniz ki, işleminizi tamamlayabilelim?';
	$language['error_already_mail'] = 'mail adresi diyarda mevcut. birden fazla hesabın yasak olduğunu, sözlükten atılabileceğinizi duymuş muydunuz?';
	$language['error_first_char'] = 'kullanıcı adının ilk karakterinde [boşluk] bırakılamaz.';
	$language['error_already_user'] = 'bu kullanıcı adını başkası kullanmakta.';
	
	//Sözlüğe Kayıt
	$language['dictionary_register'] = 'kayıt';
	$language['register_code_alert'] = 'kodu bilememişsiniz';
	$language['register_code'] = 'güvenlik kontrolü';
	$language['register_code_message'] = 'resimde ne görüyorsanız yazın, göremiyorsanız resmi yenileyin';
	$language['register_username_message'] = 'kullanıcı adınız özel karakter içeremez';
	$language['register_namesurname_message'] = 'ad? soyad?';
	$language['register_gender_message'] = 'cinsiyet?';
	$language['register_city_message'] = 'şehir?';
	$language['register_code_message'] = 'güvenlik kodu?';
	$language['dictionary_register_accept'] = 'kabul ediyorum';
	$language['button_ivecome'] = 'beni de alın!';
	$language['username_tips'] = 'sözlükte kullanacağınız ve bir daha değiştiremeyeceğiniz 
	nickiniz. türkçe karakter kullanmadan seçmeniz güzel olacaktır. boşluk bırakabilirsiniz. ';
	$language['confirm'] = 'onay';
	$language['password_tips'] = 'güvenliğiniz açısından gerekli kombinasyonlar yapılarak seçilmeli, 
	türkçe karakterden uzak durulmalıdır. lütfen seçtiğiniz şifreyi onayı ile birlikte giriniz.';
	$language['register_mail'] = 'mail adresi';
	$language['register_mail_tips'] = 'üyelik bilgilerinizi email adresinize göndereceğimizden bu kısmı doğru girmeniz sizin için faydalı olacaktır.
	adresiniz hotmail, mynet gibi ücretsiz sunucularda barınıyorsa göndereceğimiz onay maili spam olarak algılanabilir. bundandır ki 
	junk mail kısmına bakmanızı öneririz.';
	$language['register_name_tips'] = 'isim ve soyisim konusuna önem vermekteyiz. inandırıcı gelmeyen kimlik bilgileri 
	olan üyeleri kabul etmiyoruz.';
	$language['register_birthDate'] = 'doğum tarihi';
	$language['january'] = 'Ocak';
	$language['february'] = 'Şubat';
	$language['march'] = 'Mart';
	$language['april'] = 'Nisan';
	$language['may'] = 'Mayıs';
	$language['june'] = 'Haziran';
	$language['july'] = 'Temmuz';
	$language['august'] = 'Ağustos';
	$language['september'] = 'Eylül';
	$language['october'] = 'Ekim';
	$language['november'] = 'Kasım';
	$language['december'] = 'Aralık';
	$language['register_birth_tips'] = 'dünyaya adım atış tarihiniz.';
	$language['register_gender_tips'] = 'doğuştan sahip olduğunuz tür.';
	$language['register_city_tips'] = 'ikamet ettiğiniz şehir.';
	$language['button_makeMeAuthor'] = 'yazar olayım artık!';
	$language['button_giveup'] = 'vazgeçtim';
	$language['message_welcome_header'] = 'sözlüğe hoşgeldiniz!';
	$language['message_welcome_note'] = 'sözlüğe hoşgeldiniz. <br><br>şu anda sözlükte çaylak konumundasınız. bu konumdan çıkmak için
	10 tanım girmelisiniz. bu tanımlar yeterli görülürse yazarlığa yükseleceksiniz. şu aşamada tanımlarınız diğer yazarlar tarafından görülemez. 
	sadece yöneticiler tanımlarınızı görebilecekler.<br><br>bir an önce sözlüğe ısınıp bize güzel tanımlar kazandıracağınıza 
	inanıyoruz.<br><br>kalın sağlıcakla'; //buradaki <br> tagları satır atlamaya yarar. mesajınızda satır atlamak istiyorsanız iki defa <br> yazınız.
	$language['message_welcome_accepted'] = '<center><br><br>
	yazarlık başvurunuz kabul gördü.<br><br> kullanıcı bilgilerinizi belirttiğiniz mail adresine gönderdik. hemen bekliyoruz ;)</center>';

	//staff
	$language['staff_creator'] = 'düşünen';
	$language['admins'] = 'yöneticiler';
	$language['moderators'] = 'moderatörler';
	$language['spys'] = 'casuslar';
	$language['message_software'] = 'altyapı';
	
	//vampara
	$language['word'] = 'kelime';
	$language['how_sort'] = 'nasıl sıralayalım?';
	$language['sort_a_z'] = 'a\'dan z\'ye'; // ' işaretlerinden önce eğer kullanacaksanız bir adet / koymalısınız, örnekte olduğu gibi.
	$language['sort_z_a'] = 'z\'den a\'ya';
	$language['sort_new_old'] = 'yeni-eski';
	$language['time_interval'] = 'zaman aralığı';
	$language['from'] = 'şuradan';
	$language['there'] = 'şuraya';
	$language['detailed'] = 'detaylı';
	$language['fastly'] = 'çabuk';
	$language['from_beautiful'] = 'iyisinden';
	$language['vamp_ara'] = 'a r a'; //&nbsp; bir kare boşluktur
	$language['vamp_ara_button'] = 'hemen getir';
	
	//zirvetör
	$language['zirvetor'] = 'zirvetör';
	$language['zirvetor_add'] = 'zirve ekle';
	$language['new_activities'] = 'en yakın etkinlikler';
	$language['closed_activities'] = 'kapanmış etkinlikler';
	$language['activity_add_date'] = 'eklenme tarihi';
	$language['activity_date'] = 'zirve tarihi';
	$language['activity_name'] = 'zirve';
	$language['activity_category'] = 'kategori';
	$language['activity_place'] = 'mekan';
	$language['activity_organizator'] = 'organizatör';
	$language['activity_organizators'] = 'organizatör(ler)';
	$language['activity_organizators_tips'] = 'birden fazla organizatör varsa virgül ile ayırarak yazınız.';
	$language['activity_added'] = 'katılımcı listesine eklendiniz';
	$language['activity_comment_added'] = 'yorumunuz eklendi';
	$language['activity_comment_deleted'] = 'yorumunuz silindi';
	$language['activity_details'] = 'zirve detayları';
	$language['activity_date_hour'] = 'tarih-saat';
	$language['activity_attendance'] = 'katılımcılar';
	$language['activity_back_down'] = 'gelemiyorum :(';
	$language['activity_add_list'] = 'ben de geliyorum!';
	$language['activity_comments'] = 'yorumlar';
	$language['activity_preview'] = 'zirve önizleme';
	$language['activity_added'] = 'eklendi';
	$language['activity_deleted'] = 'numaralı zirve silindi';
	$language['activity_information'] = 'zirve bilgileri';
	$language['activity_poster'] = 'zirve afişi';
	$language['activity_poster_tips'] = '
	1- eklemek istediğiniz afişin "url"sini giriniz. <br>(ör: http://www.x.com/x.jpg , format bu şekilde olmalıdır.)
	<br>2- <a href="http://imageshack.us">http://imageshack.us</a> resimlerini upload edebilmeniz için uygun bir yerdir.
	<br>3- afişin boyutu 250x350 olmalıdır. değilse otomatik olarak küçülür.
	<br><b>4- birşey anlamadıysanız orjinali (images/afis.jpg) kalsın.</b>'; //taglara dokunmadan çevirinizi yapmaya özen gösteriniz.
	$language['button_add_activity'] = 'ortamlara akalım';
	
	//zirve kategorileri
	$language['activity_category_conventional'] = 'geleneksel sözlük zirvesi';
	$language['activity_category_chat'] = 'toplantı/muhabbet';
	$language['activity_category_concert'] = 'konser/festival';
	$language['activity_category_trip'] = 'gezi';
	$language['activity_category_game'] = 'oyun';
	$language['activity_category_music'] = 'müzik/dans';
	$language['activity_category_screen'] = 'izlence (sinema/tv/tiyatro)';
	$language['activity_category_birthdate'] = 'doğum günü';
	$language['activity_category_sports'] = 'sportif';
	$language['activity_category_screen'] = 'izlence (sinema/tv/tiyatro)';
	$language['activity_category_help'] = 'yardım/dayanışma';
	$language['activity_category_eating'] = 'yemek';
	$language['activity_category_other'] = 'diğer';
	
	//Admin Paneli
	
	//Admin Butonları
	$language['admin_menu_update'] = 'istatistikleri güncelle';
	$language['admin_menu_authors'] = 'yazar işlemleri';
	$language['admin_menu_dictionary'] = 'sözlük işlemleri';
	$language['admin_menu_mail'] = 'herkese mail';
	$language['admin_menu_room'] = 'yazar kabul odası';
	$language['admin_menu_ip'] = 'ip banla';
	$language['admin_menu_theme'] = 'tema paneli';
	$language['admin_menu_banner'] = 'reklam paneli';
	$language['admin_menu_seen_deleted_messages'] = 'silinen entryleri gösterme';
	$language['admin_menu_editted_entries'] = 'akıllanan tanımlar';
	$language['admin_menu_entries'] = 'tanım işlemleri';
	$language['admin_menu_deleted_entries'] = 'silinmiş tanımlar';
	$language['admin_menu_topics'] = 'başlık işlemleri';
	$language['admin_menu_deleted_topics'] = 'ölü başlıklar';
	$language['admin_menu_add_member'] = 'yazar ekle';
	$language['admin_menu_news'] = 'duyuru paneli';
	$language['admin_menu_admin_news'] = 'yönetici duyuruları';
	$language['admin_menu_reports'] = 'şikayetler';
	$language['admin_menu_spy'] = 'ispiyonlananlar';
	$language['admin_menu_admin_chat'] = 'yönetici sohbetleri';
	$language['admin_menu_spy_chat'] = 'casus sohbetleri';
	$language['admin_not_allowed'] = 'bu işlem için yeterli yetkiye sahip değilsiniz.';
	$language['admin_add_adminNews'] = 'Yönetime Seslen';
	$language['admin_news'] = 'haber';
	$language['admin_news_details'] = 'detaylar';
	$language['admin_news_spy'] = 'casuslar görsün mü?';
	$language['edit_activate'] = 'düzenle ve aktifleştir';
	$language['admin_bumber'] = 'patlatan';
	$language['admin_bumber_reason'] = 'patlatma sebebi';
	$language['change_topic'] = 'şu olsun';
	$language['unlocked'] = 'kilit kaldırıldı.';
	$language['locked'] = 'kilitlendi.';
	$language['deleted'] = 'silindi.';
	$language['deleted_allentries'] = 'başlığa bağlı olan tüm tanımlar silindi.';
	$language['problem_with_topic'] = 'başlığınız yanlış olmuş, karakter hatalarını düzeltip tekrar deneyiniz.';
	$language['topic_moved'] = 'başlığına taşındı.';
	$language['topic_where'] = 'taşınacak başlık';
	$language['listenUp'] = 'Duyuru!';
	$language['mail_sended'] = 'kişiye gönderildi.';
	$language['mail_everyone'] = 'herkes';
	$language['mail_admin'] = 'admin';
	$language['mail_mods'] = 'moderatörler';
	$language['mail_authors'] = 'yazarlar';
	$language['mail_readers'] = 'okurlar';
	$language['button_listenUp'] = 'duyduk duymadık demesinler';
	$language['button_bringAll'] = 'tüm tanımlarını getir';
	$language['latest_50entry'] = 'son 50 tanım listesi';
	$language['text'] = 'metin';
	$language['back_authorsroom'] = 'yazar kabul odasına dön';
	$language['entry_deleted'] = 'tanım silindi.';
	$language['add_new_news'] = 'yeni haber ekle';
	$language['ip_blocked'] = 'bloklandı.';
	$language['ip_add'] = 'ip banla';
	$language['wrong_spy'] = 'ispiyon reddedildi ve tanım aktifleştirildi.';
	$language['wrong_spy_subject'] = 'numaralı ispiyonladığınız tanım hakkında';
	$language['wrong_spy_message'] = 'numaralı tanım yanlış ispit olarak değerlendirilmiştir';
	$language['button_wrong_spy'] = 'hatalı ispiyon';
	$language['button_true_spy'] = 'onayla ve sil';
	$language['spy_reason'] = 'ispiyonlama sebebi';
	$language['delete_user_completely'] = 'sil ve tanımlarını yoket';
	$language['access_level'] = 'yetki';
	$language['suspended'] = 'uzaklaştırılmış';
	$language['button_edit_user'] = 'kullanıcı düzenle';
	$language['status'] = 'durum';
	$language['message_delete_user_entries'] = 'tanımları sessizce silindi.';
	$language['message_delete_user_comments'] = 'yorumları gürültüyle silindi.';
	$language['message_delete_user_votes'] = 'oyları gürültüyle silindi.';
	$language['message_delete_user'] = 'sessizce silindi.';
	$language['message_delete_user_sure'] = 'nickine ait tüm tanımlar silenecek, emin misiniz?';
	$language['message_you_are_accepted'] = 'bu mesajı okuyabildiğinize göre sözlüğe kabul edildiniz, uzun soluklu bir yazarlık dileriz. <br> 
	tekrar hoşgeldiniz.';
	$language['message_accepted_header'] = 'yazarlığınız kabul edildi';
	$language['message_you_are_accepted_mail'] = 'eğer bu maili okuyabiliyorsanız sözlüğe kabul edilmiş olmalısınız, uzun soluklu bir 
	yazarlık dileriz. <br> tekrar hoşgeldiniz.';
	$language['message_waitingApproval'] = 'onay bekleyen yazarlar';
	$language['button_acceptToDictionary'] = 'sözlüğe kabul et';
	$language['reactivated'] = 'yeniden aktifleştirildi.';
	$language['process'] = 'işlem';
	$language['activate'] = 'aktifleştir';
	$language['deleted_entries'] = 'silinmiş tanımlar';
	$language['latest_50_private_messages'] = 'son 50 özel mesaj';
	$language['report_declined'] = 'şikayet reddedildi.';
	$language['button_report_declined'] = 'olur böyle şeyler';
	$language['button_reporter'] = 'şikayetçi';
	$language['open'] = 'açık';
	$language['closed'] = 'kapalı';
	$language['construction'] = 'bakımda';
	$language['registration_mode'] = 'Kayıt Modu';
	$language['registration_mode_mail'] = 'mail onaylı';
	$language['registration_mode_Nomail'] = 'mail onaysız';
	$language['registration_tips'] = '<b> * </b>eğer email onaylı kayıt seçeneğini seçerseniz, kullanıcı kayıt olurken şifre giremez, 
    şifresi mail adresine gönderilir. biz email onaylı olmasını tavsiye ediyoruz.
    <br><b>*</b>eğer ki server saati ile türkiye saati arasında fark varsa buraya miktarı yazmanız yeterlidir. örneğin server amerikada 
    ise 8 yazarsanız saat 8 saat ileri alınır ve türkiye saatine denk olur.  ';
	$language['time_zone'] = 'Saat Farkı';
	$language['notes'] = 'notlar';
	$language['stats_update'] = 'Sözlüğünüzün istatistiklerini güncellediniz.';
	$language['new_theme'] = 'yeni tema ekle';
	$language['theme'] = 'tema';
	$language['theme_maker'] = 'yapımcı';
	$language['theme_updated'] = 'tema düzenlendi.';
	$language['theme_date_format'] = '(yıl/ay/gün)';
	$language['theme_add_tips'] = 'tema ismi, temanın /images klasörü içindeki css dosyasının ismi ile aynı olmalıdır. 
	<br>örneğin default.css temanızı sisteme eklerken tema adı olarak "default" yazmalısınız.';

	//sonradan
	$language['Subject'] = 'başlık';
	$language['total_theme'] = 'toplam tema';
	$language['entry_by_time'] = 'zamana göre tanım';
	$language['updated'] = 'güncellendi';
	$language['rememberpassword'] = 'Beni Hatırla';
	$language['karma'] = 'karma'; //Yazarın aldığı oyların sonucunda ortaya çıkan ortalama puanıdır
	$language['varma'] = 'varma'; //Yazarın verdiği oyların sonucunda ortaya çıkan ortalama puanıdır
	$language['last_appreciated'] = 'son beğenilen tanımları';
	$language['ben_most_appreciated'] = 'en beğenilen tanımları';
	$language['ben_most_disg'] = 'en kötülenen tanımları';
	$language['ben_last_disg'] = 'son kötülenen tanımları';
	$language['ben_latest_votes'] = 'son oylananlar';
	$language['profil'] = 'profil';
	
	//davet
	$language['invitation_dictionary'] = 'sözlüğe davet';
	$language['refer_text1'] = 'tarafından davet edilmişsiniz.';
	$language['refer_text2'] = 'kayıt sayfasına yönlendiriliyorsunuz';
	$language['refer_error'] = 'davet kodunu kafanızdan uyduramazsınız.';
	$language['invitations_sent'] = 'davetiyeler gönderildi';
	$language['invitations_slogan'] = 'bu adresi kopyalayıp arkadaşlarını sözlüğe davet et!';
	$language['invitations_desc_comma'] = 'mehmet@mehmet.com, ahmet@ahmet.com gibi virgüllerle ayırarak kendi mail listenizi oluşturun.';
	$language['invitation_message'] = 
	'selam yeni keşfettiğim sözlüğe arkadaşlarımı davet edeyim dedim sen geldin aklıma. sen de yazmak istersen, aşağıda bir url var. oradan gelip kayıt olabilirsin.';
	$language['invitation_message_footer'] = 'hadi görüşürüz.';
	$language['invitation_own'] = 'sözlüğe kazandırdığınız yazarlar';
	$language['invitation_own_message'] = 'kimseyi çağırmamışsınız hala?';
	
	//profil
	$language['change_picture'] = 'fotoğrafı değiştir';
	$language['upload_picture'] = 'fotoğraf gönder';
	$language['update_info'] = 'yazar profilini güncelle';
	$language['total'] = 'toplam';
	$language['total_plus'] = 'toplam +';
	$language['total_minus'] = 'toplam -';
	$language['whoami'] = 'kimim ben?';
	
		//en son eklenenler
	$language['listenUp2'] = 'duyuru';
	$language['panelsettings'] = 'Panel Ayarları';
	$language['picture'] = 'resim';
	$language['edit2'] = 'düzenle';
	$language['kick_from_list'] = 'listemden çıksın';
	$language['listenUp3'] = 'Duyuru';
	$language['announcement_platform'] = 'sözlük duyuru platformu';
	$language['city_cannot_empty'] = 'Şehir boş kalamaz';
	$language['namesurname_cannot_empty'] = 'Ad soyad boş kalamaz';
	$language['send_invitation'] = 'Duyduk Duymadik Demesinler';
	$language['impossible'] = 'olmadık işler bunlar';
	$language['save'] = 'kaydet';
	$language['closing'] = 'kapanıyor';
	$language['omg'] = 'Höst ulan';
	$language['isneakyou'] = 'ispitledim seni nihahaah :D';
	$language['kicked'] = 'sessizce uçuruldu.';
	$language['deleteee'] = 'PATLAT';
	$language['pwsentto'] = 'şifreniz kayıtlı e-posta adresine gönderildi.';
	$language['or2'] = 'ya da';
	$language['aboutyou'] = 'hakkında';
	$language['minimalauthor'] = '11';
	$language['minimalauthor'] = 'Beklenmeyen bir hata oluştu!.. Lütfen site yöneticisine başvurunuz..';
	$language['search'] = 'ara';
	$language['bring'] = 'getir';
	$language['bring_title'] = 'ogrenelim nedir';
	$language['search_title'] = 'ara bul';
	$language['ukte_note'] = 'ukteciye notlar';
	$language['throwup'] = 'fırlat gitsin';
	$language['nonauthor_warning'] = 'bu bölümü görebilmeniz için yazar olmaniz gerekmektedir.';
	$language['hello3'] = 'selam, naber?';
	$language['newmessage'] = 'sana bir mesaj göndermiş, git bir bak istersen.';
	$language['newmessage_1'] = 'aşağıdaki adresinden mesajlarına ulaşabilirsin.';
	$language['newmessage_2'] = 'Mesajınız var!';
	$language['punishment_message_alarm'] = 'cezalıyken mesaj gönderemezsiniz!';
	$language['empty_place_alarm'] = 'bütün alanları doldurunuz';
	$language['added_3'] = 'eklenmiş olmanız lazım.';
	$language['added_4'] = 'listeyeEkle';
	$language['added_5'] = 'listedenCik';
	$language['edit_comment3'] = 'yorumDuzenle';
	$language['delete_comment'] = 'yorumSil';
	$language['zirvetor_add2'] = 'zirve düzenle';
	$language['zirvetor_wtf'] = 'başkasının zirvesini düzenlemekteki amaç ne onu pek çözemedim işte.';
	$language['zirvetor_added'] = 'zirve düzenlendi';
	$language['zirvetor_return'] = 'e döneyim.';
	$language['zirvetor_situation'] = 'zirve durum ';
	$language['zirvetor_name'] = 'zirve durum ';
	$language['zirvetor_locality'] = 'zirve mekanı';
	$language['zirvetor_category_1'] = 'geleneksel sözlük zirvesi';
	$language['zirvetor_category_2'] = 'toplantı/muhabbet';
	$language['zirvetor_category_3'] = 'konser/festival';
	$language['zirvetor_category_4'] = 'gezi';
	$language['zirvetor_category_5'] = 'oyun';
	$language['zirvetor_category_6'] = 'müzik/dans';
	$language['zirvetor_category_7'] = 'izlence';
	$language['zirvetor_category_8'] = 'doğum günü';
	$language['zirvetor_category_9'] = 'sportif';
	$language['zirvetor_category_10'] = 'piknik';
	$language['zirvetor_category_11'] = 'doğa gezisi';
	$language['zirvetor_category_12'] = 'yardım/dayanışma';
	$language['zirvetor_category_13'] = 'yemek';
	$language['zirvetor_category_14'] = 'diğer';
	$language['zirvetor_choose'] = 'seçeyim';
	$language['zirvetor_comma'] = 'virgülle ayırarak yazınız.';
	$language['comment_edited'] = 'yorum düzenlendi.';
	$language['renew_image'] = 'resmi yenile';
	$language['announcement_comment'] = 'yorum';
	$language['announcement_question'] = 'Soru';
	$language['announcement_blood'] = 'Kan Aranıyor';
	$language['announcement_forsale'] = 'Alınık-Satılık';
	$language['announcement_type'] = 'Duyuru Tipi';
	$language['announcement_topic'] = 'Başlık';
	$language['announcement_news'] = 'Olay';
	$language['announcement_delete_5'] = 'yanlış işler peşindesin';
	$language['announcement_dict'] = 'Sözlük';
	$language['announcement_add'] = 'Duyuru Ekle';
	$language['announcement_cevab'] = 'el cevab';
	$language['announcement_enter'] = 'Giriş';
	$language['announcement_all'] = 'Tüm Duyurular';
	$language['youarekicked'] = 'şutlanmışsın yegen!';
	$language['go_back'] = 'geri git istersen';
	$language['sitemap'] = 'sitemap';
	$language['archive'] = 'arşiv';
	$language['last_entries'] = 'Son Tanımlar';
	$language['update_succsess'] = 'başarıyla kayıt güncellendi!';
	$language['upload_photo_crop'] = 'Profil Fotonu Kırp';
	$language['upload_photo_re'] = 'yeni fotonu göremiyorsan ctrl + f5 yap';
	$language['upload_photo_i_crop'] = 'Kırptım';
	$language['upload_picture2'] = 'Fotoğraf Gönder';
	$language['upload_photo_cmon'] = 'Gönder Bakalım';
	$language['upload_photo'] = 'Foto ';
	$language['top_ask'] = 'Sor ';
	$language['youtube_code'] = 'youtube kodu';
	$language['youtube_code_tip'] = '= den sonrasını giriniz';
	$language['image_url'] = 'resim url';
	$language['link_adress'] = 'gidilecek adres';
	$language['show_up_entries'] = 'Sol Frame';
	$language['rookie_status'] = 'Çaylaklık';
	$language['dict_operations'] = 'Sözlük İşlemleri';
	$language['Site'] = 'Site';
	$language['login_with_mail'] = 'Mail ile Login';
	$language['show_searchintitle'] = 'Başlıkiçi ara Gözüksün';
	$language['show_insiders'] = 'Kazandakiler Gözüksün';
	$language['show_profile_photo'] = 'Profil resmi Gözüksün';
	$language['show_inn'] = 'İçerisi Gözüksün';
	$language['show_howyoudoing'] = 'Nasılsın Gözüksün';
	$language['show_colorful_statistic_box'] = 'Renkli istatistik kutusu Gözüksün';
	$language['show_facebookandtwitter'] = 'Facebook&Twitter Gözüksün';
	$language['update_statistic'] = 'İstatistikleri Güncelle';
	$language['author_operations'] = 'Yazar İşlemleri';
	$language['dict_settings'] = 'Sözlük İşlemleri';
	$language['mail_to_everyone'] = 'Herkese Mail';
	$language['acceptance_room'] = 'Yazar Kabul Odası';
	$language['ip_ban'] = 'IP Banla';
	$language['theme_panel'] = 'Tema Paneli';
	$language['advertisement_panel'] = 'Reklam Paneli';
	$language['become_wiser_entries'] = 'Akıllanan Tanımlar';
	$language['entries_operations'] = 'Tanım İşlemleri';
	$language['deleted_entries'] = 'Silinmiş Tanımlar';
	$language['topic_operations'] = 'Başlık İşlemleri';
	$language['dead_topics'] = 'Ölü Başlıklar';
	$language['add_an_author'] = 'Yazar Ekle';
	$language['announcement_panel'] = 'Duyuru Paneli';
	$language['administrator_announcements'] = 'Yönetici Duyuruları';
	$language['complaints'] = 'Şikayetler';
	$language['entries_by_time'] = 'Zamana Göre tanım';
	$language['spieds'] = 'İspiyonlananlar';
	$language['admin_menu_add_desc'] = 'Bu bölümde sözlüğe en az bir entry girmiş çaylaklar gözükür. Sözlüğe yazar alımları bu alamdan yapılmalıdır. Kullanıcı düzenle kısmından yapılan alımlar sırasında entry kaybı olabilir.';
	$language['rookie_entries'] = 'Tanımları';
	$language['cancel_rookie'] = 'reddet';
	$language['entries_added'] = 'tanım girilmiş';
	$language['bring_most_hardworkers'] = 'en fazla yazanları getir';
	$language['add_new_theme'] = 'yeni tema ekleyeyim';
	$language['theme_control'] = 'kontrol';
	$language['select_as_maintheme'] = 'ana tema olsun';
	$language['total_themes'] = 'toplam tema';
	$language['maintheme'] = 'ana tema';
	$language['deleted_entries_returnback'] = 'Silinen entry tekrar basliga eklendi.';
	$language['regenerated'] = 'canlandirildi';
	// admin sidebar'a sonradan eklenenler
	$language['badwords'] = 'Bad Words';
	$language['forbidden_words'] = 'forbidden words';
	$language['add_word'] = 'add word';
	
	// yeni reklam paneli
	$language['add_new_advertisement'] = 'Add New Ad';
	$language['advertisement_code_ifadsense'] = 'enter code if its adsense.';
	$language['advertisement_Name'] = 'Ad Name';
	$language['photo_url'] = 'Photo Url';
	$language['Destination_url'] = 'Destination Url';
	$language['Advertisement_code'] = 'Ad Code';
	$language['Active_advertisements'] = 'Active Ads';
	$language['TheDate'] = 'Date';
	$language['Advertisement'] = 'Ad';
	$language['DestinationUrl'] = 'Destination  Url';
	$language['Advertisement_source'] = 'Ad Source';
	$language['Advertisement_admin'] = 'Ad Admin';
	$language['Active']= 'Active';
	
	//mafi son eklenenler
	$language['dictionaryMail_inv'] = 'Mafi Mushkila daveti';
	$language['entries_betweensum'] = 'arasında toplam';
	$language['close_complaint'] = 'şikayeti kapat';
	$language['answered'] = 'kapat';
	$language['take_it_back'] = 'geri al';
	$language['badwordwarning']= 'başlığınız yasaklı kelime içermektedir';
	$language['mailnick_together'] = 'please, insert nick and e-mail';
	$language['delete_reaseon'] = 'delete reason';
	$language['seen_byAdmin'] = 'seen by Admin';
	$language['turndefaultheme'] = 'Main Theme';
	$language['issetasmaintheme'] = 'is set as main theme';
	$language['addthistitletodict'] = 'add this title to';
	$language['lastupdate'] = 'last update';	
	
	//Photo upload
	$language['photo_extension_error'] = 'Permitted Extensions:';
	$language['photo_max_size'] = 'Max Photo File Size:';
	$language['photo_crop_error'] = 'Crop your photo!';
	$language['photo_error'] = 'Photo Upload Error!';
	$language['photo_go_profile'] = 'OK! Go to my profile';
	$language['photo_go_retry'] = 'Naaah! Let me try again';
	$language['contact_entry_about'] = 'About the entry: ';
	
	$language['position'] = 'position';
	$language['top'] = 'top';
	$language['right'] = 'right';
	$language['refresh'] = 'yenile';	
	
	$language['noAnnouncementYet'] = 'There is no Announcement Yet';
	$language['Announcement_Register'] = 'Register';
	$language['msg'] = 'msg';	
	$language['AnnouncementAnswer_suretoDelete'] = 'Are you sure to delete answer?';
	$language['Announcement_suretoDelete'] = 'Are you sure?';
	
	$language['buddyList'] = 'dostlar';
	$language['afterMe'] = 'after me';
?>