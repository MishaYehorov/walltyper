<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/><?php  
/*
       
Bu dosya, tüm sözlük içindeki metinleri bir araya getirir. Çeviri yapacak olanlar için sistemi çevirmeyi kolaylaştırmayı amaçlamıştır.

- Çeviri Yaparken -

Örneğin: 
$language['dictionaryUrl'] = 'http://www.sozluksistemi.com';

Burada gördüğünüz $language['dictionaryUrl'] , bir değişkendir. İçine kesinlikle dokunulmamalıdır. Çeviri yapacak olanı sadece karşısındaki tırnak içindeki kelime ilgilendirir.
Gerekli çeviriyi tırnaklara dokunmadan yapar.

Örneğin:
$language['button_choose'] = 'seç'; (Orjinali)

$language['button_choose'] = 'choose'; (Çevrilmiş hali)

Kafanızı karıştıran bir durum olması durumunda sozluksistemi.com adresine girip müşteri panelinden destek alabilirsiniz.
*/
	$language = array();
	
	//General
	$language['dictionaryUrl'] = 'http://www.walltyper.com'; //sözlük urlsi (sonunda / koymayın)
	$language['dictionaryName'] ='Walltyper'; //Sözlüğün ismi gerekli olan yerlerde bu isim kullanılır.
	$language['dictionarySlogan'] = 'Social Library of HKUST'; //sözlük sloganı
	$language['dictionaryYear'] = '2015'; //En alt satırdaki kuruluş yılı
	$language['creator'] = 'hkust ece students'; //düşünenin nickini yazınız
	$language['dictionaryKeywords'] = 'hkust,electronic and computer engineering department,hong kong,social media,china,walltyper,hkust library ,library,newspaper,portal,forum'; //sözlük keywordleri
	$language['dictionaryDescription'] = 'Virtual library,blog spot,diary,live dictionary,confession board,social portal,digital newspaper and much more'; //sözlüğün kısa açıklaması
	$language['googleAnalyticsCode'] = 'UA-27980506-1'; //google analytics kodu
	$language['facebook_url'] = 'http://www.google.com'; //sözlüğün facebook adresi açıklaması
	$language['twitter_url'] = 'https://twitter.com/#!/wall'; //sözlüğün kısa açıklaması
	$language['software'] =  'walltyper.com'; //Bu kısmı kaldırmayınız, aksi takdirde teknik destek alamazsınız.
	$language['dictionaryMail'] = 'walltyper@walltyper.com'; //Sözlüğün mail adresi
	$language['footerMessage'] = 'This site may contain information and language which can be inappropriate for children. The content of Walltyper.com may be inaccurate and fictional. Or maybe not! Who knows? Content of the entries are under the mere responsibility of their respective authors. Walltyper does not take any responsibility for what is written. Walltyper does not hold any copyrights for the entries written by the authors. Topics about vulgar, racist , harassing hateful, obscene, profane, political and religious discussions, promotion of political ideologies, flooding, spam, direct advertisement, invasive of a persons’s privacy are strictly forbidden in Walltyper. Any entry that may lead to violation of any law is prohibited. Respect the traditions, culture, law and censorship rules of Hong Kong .';
	//Generel Buttons
	$language['button_choose'] = 'Choose';
	$language['button_back'] = 'Back';
	$language['button_bringIt'] = 'Come here';
	$language['button_entries'] = 'Entries';
	$language['button_message'] = 'Message';
	$language['button_buddy'] = 'Buddy';
	$language['button_blocked'] = 'Blocked';
	$language['button_report'] = 'Report';
	$language['button_youSure'] = 'Confirm';
	$language['button_yes'] = 'Yes';
	$language['all'] = 'All';
	
	//Top.php
	$language['topbutton_dictionary'] = 'Walltyper'; 
	$language['description_dictionary'] = 'What’s Walltyper';
	$language['topbutton_sukela'] = 'Awesome'; 
	$language['description_sukela'] = 'Most liked entries';
	$language['topbutton_mix'] = 'Mix'; 
	$language['description_mix'] = 'Show random topics';
	$language['topbutton_stats'] = 'Statistics';
	$language['description_stats'] = 'Statistics of Walltyper';
	$language['topbutton_enter'] = 'Sign-in';
	$language['description_enter'] = 'Signin for Walltyper';
	$language['topbutton_extras'] = 'extras';
	$language['description_extras'] = 'additional features';
	$language['topbutton_exit'] = 'Exit';
	$language['description_exit'] = 'Sign out';
	$language['message_exit'] = 'Don’t leave if you are %100 sure!';
	$language['topbutton_me'] = 'me';
	$language['description_me'] = 'yes you';
	$language['topbutton_admin'] = 'admin';
	$language['description_admin'] = 'admin tools';
	$language['topbutton_spy'] = 'spy';
	$language['description_spy'] = 'spy tools';
	$language['topbutton_sss'] = 'FAQ';
	$language['topbutton_sss_long'] = 'frequently asked questions';
	$language['description_sss'] = 'frequently asked questions about Walltyper';
	$language['topbutton_today'] = 'Today';
	$language['description_today'] = 'Todays topics';
	$language['topbutton_yesterday'] = 'Yesterday';
	$language['description_yesterday'] = 'allmytroublesseemsofaraway';
	$language['topbutton_contact'] = 'Contact';
	$language['description_contact'] = 'Contact us';
	$language['topbutton_register'] = 'Register';
	$language['description_register'] = 'I want to be a writer';
	$language['topbutton_control'] = 'settings';
	$language['description_control'] = 'control panel';
	$language['topbutton_wish'] = 'I Wish';
	$language['description_wish'] = 'topics that you wish someone fill it for you';
	$language['topbutton_random'] = 'Random';
	$language['description_random'] = 'search for a random topic';
	$language['topbutton_ok'] = 'ok';
	$language['description_ok'] = 'find a specific topic';
	$language['topbutton_search'] = 'Search';
	$language['description_search'] = 'search for specific topic';
	$language['topbutton_id'] = '#';
	$language['description_id'] = 'bring a specific entry';
	
	// Word.php
	$language['topics'] = 'topics';
	$language['entries'] = 'entries';
	$language['general'] = 'general';
	$language['OK'] = 'ok';
	$language['or'] = 'or';
	$language['ADD'] = 'add';
	$language['page'] = 'page';
	$language['message_edited'] = 'Your entry has been edited.';
	$language['deletedTopic'] = 'This topic has been deleted.';
	$language['deletedTopic_Mod'] = 'This topic has been deleted, you can see it due to your moderator rights.';
	$language['edit'] = 'edit';
	$language['delete'] = 'delete';
	$language['redirect'] = 'redirect'; //Başlığı içindeki tüm verileri ile birlikte başka bir başlığa aktarır, yönlendirir.
	$language['stop'] = 'Stop'; 
	$language['pageViews'] = 'pageViews'; 
	$language['topicID'] = 'topicID'; 
	$language['lockTopic'] = 'lockTopic';
	$language['unlockTopic'] = 'unlockTopic';
	$language['messageAlert'] = 'you have message';
	$language['noFriend'] = 'you have nobuddy';
	$language['button_message'] = 'message';
	$language['button_bin'] = 'trash';
	$language['button_event'] = 'event';
	$language['inn'] = 'online writers';
	$language['mixthings'] = 'mix';
	$language['searchintitle'] = 'search in topic';
	$language['searchintitle_error'] = 'There is no topic opened with this name. You may add new topic or wish someone add this topic.';
	$language['rookieconfirm_error'] = 'you are awaiting approval';
	$language['search'] = 'Search';
	$language['jump'] = 'jump';
	$language['closeBanner'] = 'Close Banner!'; 	
	$language['howyoudoing'] = 'whatsayyou'; 	
	$language['redirecting'] = 'you are redicting to the topic'; 
	$language['error_newTopic'] = 'has some characters which avoids opening this topic. Please revise your topic and try again.'; 
	$language['noResult'] = 'There is no result matched with your search.'; 
	$language['relatedResults'] = 'Can it be something like these:'; 
	$language['wishWaitingTopic'] = 'This topic is waiting as an iwish'; 
	$language['wishAlreadyWaitingTopic'] = 'This topic is already an iwish topic';
	$language['wishAddedWishings'] = 'topic became iwish';
	$language['wisher'] = 'iwisher'; 
	$language['wisherGiveComments'] = 'drop a comment for your iwish';
	$language['wisherComments'] = 'iwisher comments';
	$language['button_newTopic'] = 'Create topic';
	$language['button_newWish'] = 'Add iwish';
	$language['wishWaitingTopics'] = 'iwish';
	$language['wishWaitingTopics_delete'] = 'delete iwish';
	$language['title_solong'] = 'this topic has more than 70 characters and needs some trim.';
	$language['warning_rookie'] = 'You are a rookie writer. You have to write 10 entries and after get approval in order to open a new topic.';
	$language['warning_fillComments'] = 'please fill comments';
	$language['waitingForApproval'] = 'Bravo, you are now on the waiting list to be a Walltyper writer. Our moderators will revise your entries and you will be either accepted to be a writer or not. Simple isn’t it? Ohh, by the way within this period you cannot post any more entries. Patience is the key to success…!';
	$language['warning_firstEntry'] = 'You have just entered your first entry. We are dancing with joy being a witness of this moment.';
	$language['warning_RookieEntryCounter'] = 'Nearly there!';
	$language['entryInserted'] = 'entry posted.';
	$language['viewEntry'] = 'view entry.';
	$language['addnewtitle'] = 'add new topic.';
	$language['youaretheone'] = 'You made it. Your entry is posted. What a glorious moment!';
	$language['bkz'] = 'ref'; //bakınız
	$language['ara'] = 'Search'; 
	$language['spoiler'] = 'spoiler';
	$language['quote'] = 'quote';
	$language['gbkz'] = 'sref'; //gizli bakınız
	$language['u'] = 'u'; //akıllı bakınız
	$language['image'] = 'image';
	$language['spy'] = 'spy';
	$language['move'] = 'move';
	$language['editSmall'] = 'edit';
	$language['who'] = 'who?';
	$language['message'] = 'message';
	$language['destroy'] = 'Delete';
	$language['makeAlive'] = 'resurrect';
	$language['delete_reason'] = 'reason?';
	$language['message_deletedEntry'] = 'this entry has been deleted';
	$language['message_deletedEntry_Edited'] = 'this entry has been deleted.Than writer gave a second chance,revised and waiting for admin approval';
	$language['message_rookieEntry'] = 'rookie entry';
	$language['button_showAll'] = 'Show All';
	$language['button_save'] = 'Send';
	$language['message_penalty1'] = 'because of';
	$language['message_penalty2'] = 'you are penalized';
	$language['message_rookieEnd'] = 'you submitted enough entries to become a writer. you are waiting for approval now. Within this period you cannot write any more entries. Take a deep breath and relax…';
	
	//Bloklar
	$language['hottopics'] = 'Hot Topics';
	$language['hardworkers'] = 'Hard Working Writers';
	$language['todaysmostloved'] = 'Todays Most Liked Topics';
	$language['insiders'] = 'Online Writers';
	
	//Word.php metinleri bitti
	
	//Diğer
	$language['underConstruction'] = 'We are currently under construction.';
	$language['message_siteClosed'] = 'Due to something we know but you don’t know, we are closed. Will be back soon.';
	$language['message_banned'] = 'You have been Banned!';
	$language['message_wrongPassword'] = 'Your username or password is incorrect.';
	$language['message_diyarError'] = 'This section is currently close.';
	$language['message_logout'] = 'You just logged out';
	$language['topic'] = 'topic';
	$language['entry'] = 'entry';
	$language['message_fillEverywhere'] = 'don’t forget to fill everywhere';
	$language['message_getTopic'] = 'There is a similar topic opened under same name.Wierd huh?';
	$language['message_error'] = 'Error happens';
	$language['dictionary_gates'] = 'Walltyper entry';
	$language['username'] = 'User Name';
	$language['button_enterSite'] = 'LetmeIn';
	$language['others'] = 'others';
	$language['register'] = 'Register';
	$language['forget_pass'] = 'forgot my password?';
	$language['vampara_no_topic'] = 'is not an available topic';
	$language['vampara_no_information'] = 'there is no information';
	$language['about'] = 'about';
	
	//inc altındakiler
	$language['add_message'] = 'add message:';
	$language['button_back'] = 'back';
	$language['message_noAuthor'] = 'no such writer';
	$language['message_selfEgo'] = 'are you voting your own entry, cmon…';
	$language['message_alreadyVoted'] = 'you already voted for this entry';
	$language['message_voteAdded'] = 'you successfully voted';
	$language['message_OK'] = 'ok';
	$language['message_NOT'] = 'you have to be a member in order to see the content';
	$language['author_NOT'] = 'you have to be a writer in order to see the content';
	
	//Sözlük Aparatları
	$language['dictionaryTools'] = 'Tool Box';
	$language['zirvetor'] = 'Activities';
	$language['zirvetor_description'] = 'Walltyper Activities';
	
	//ben.php  - ben sayfası
	$language['message_reportMessage'] = 'your complain has been received';
	$language['message_reportMessageAgain'] = 'you already complain about it, take a deep breath and relax.';
	$language['authorBig'] = 'Writer';
	$language['message_forgive'] = 'penalty removed';
	$language['message_giveAppealRookie'] = 'is the reason why you downgraded to rookie. Make necessary entries and wait for moderators approval.';
	$language['message_giveAppealRookieHeader'] = 'time out';
	$language['message_giveAppealDelete'] = 'writer has been deleted!';
	$language['message_giveAppealDay'] = 'penalty';
	$language['message_appealMessage1'] = 'is the reason';
	$language['message_appealMessage2'] = 'days of penalty you get.';
	$language['appealDay'] = 'How many days of penalty';
	$language['makeRookie'] = 'Approve to be rookie?';
	$language['makeSuspended'] = 'Suspended?';
	$language['giveAppeal'] = 'punish';
	$language['forgive'] = 'forgive';
	$language['ben_title'] = 'whois this?';
	$language['message_givenAppeal'] = 'this writer is penaltized';
	$language['admin'] = 'admin';
	$language['moderator'] = 'moderator';
	$language['spy'] = 'spy';
	$language['author'] = 'writer';
	$language['rookie'] = 'rookie';
	$language['rookieWA'] = 'rookie waiting for approval';
	$language['notSuchMember'] = 'there is no such writer';
	$language['notSuchMemberNick'] = 'is not a valid writer nick';
	$language['totalEntry'] = 'total entry';
	$language['today'] = 'today';
	$language['yesterday'] = 'yesterday';
	$language['thisMonth'] = 'this month';
	$language['lastMonth'] = 'last month';
	$language['latestPositiveVotes'] = 'latest + votes';
	$language['latestNegativeVotes'] = 'latest - votes';
	$language['message_notYet'] = 'no such thing yet';
	$language['todaysEntries'] = 'todays entries';
	$language['yesterdaysEntries'] = 'yesterdays entries';
	$language['deeper'] = 'deeper';
	$language['users_theme'] = 'users theme';
	$language['registration_day'] = 'registration day';
	$language['votesGiven'] = 'votes given';
	$language['votesTaken'] = 'votes taken';
	
	//tabs - kontrol paneli tabları
	$language['tab_news'] = 'news';
	$language['tab_setting'] = 'settings';
	$language['tab_message'] = 'message';
	$language['tab_buddy'] = 'buddy';
	$language['tab_blocked'] = 'blocked';
	$language['tab_themes'] = 'themes';
	$language['tab_bin'] = 'trash';
	$language['tab_invite'] = 'invite';
	
	//cp.php - ayarlar sayfası
	$language['message_entryDeleted'] = 'entry number has been deleted!';
	$language['message_giveMail'] = 'Please enter your mail adress';
	$language['your_mail'] = 'Your Mail Adress';
	$language['your_mail_changed'] = 'is the new mail adress';
	$language['my_password'] = 'password';
	$language['new_password'] = 'new password';
	$language['new_password_confirm'] = 're-enter';
	$language['button_changePassword'] = 'change';
	$language['your_name_surname'] = 'name & surname';
	$language['city'] = 'city';
	$language['gender'] = 'gender';
	$language['male'] = 'male';
	$language['female'] = 'female';
	$language['my_mail'] = 'my mail adress';
	$language['change_my_mail'] = 'my new mail adress';
	$language['page_options'] = 'page options';
	$language['message_entryListLimit'] = 'Entry list limit';
	$language['message_ThemeNot'] = 'there is no such theme';
	$language['message_succeed'] = 'Success!';
	
	//diğer
	$language['blockedOnes'] = 'Blocked ones';
	$language['buddyOnes'] = 'Buddy ones';
	$language['editingNotes'] = 'editing notes';
	$language['notHave'] = 'there is no such entry';
	$language['plus'] = 'plus';
	$language['minus'] = 'minus';
	$language['error_try_delete'] = 'you are trying to delete others entries…not good!';
	$language['message_deleted'] = 'added to the deleted scenes.';
	$language['button_sure'] = 'sure';
	$language['message_move'] = 'is the new topic number!';
	$language['message_move1'] = 'entry number';
	$language['choose'] = 'choose';
	
	//İstatistikler
	$language['stats'] = 'Statistics';
	$language['general_stats'] = 'general stats';
	$language['most_1'] = 'most liked entries';
	$language['most_2'] = 'most hated entries';
	$language['most_3'] = 'last weeks most liked entries';
	$language['most_4'] = 'last weeks most hated entries';
	$language['most_5'] = 'topics with most number of entries';
	$language['most_6'] = 'most read topics';
	$language['most_7'] = 'todays first entries';
	$language['most_8'] = 'most entry submitted writer';
	$language['most_9'] = 'least entry submitted writer';
	$language['most_10'] = 'writers with most deleted entries';
	$language['most_11'] = 'writers with most message sent';
	$language['most_12'] = 'most liked writers';
	$language['most_13'] = 'most hated writers';
	$language['most_14'] = 'entries with months';
	$language['most_15'] = 'entries with seasons';
	$language['most_16'] = 'gender analysis';
	$language['most_17'] = 'theme stats';
	$language['general_stats_latest'] = 'latest general stats';
	$language['total_entry'] = 'total entry';
	$language['total_topic'] = 'total topics';
	$language['total_population'] = 'total population';
	$language['total_deleted_topic'] = 'deleted topics';
	$language['total_deleted_entry'] = 'deleted entries';
	$language['total_topic_views'] = 'total topic views';
	$language['total_visitors'] = 'total visitors';
	$language['most_followed_topic'] = 'most followed topics';
	$language['topic_per_author'] = 'topics per writer';
	$language['entry_per_author'] = 'entries per writer';
	$language['message_noStats'] = 'there is no stats under this category';
	$language['spring'] = 'spring';
	$language['summer'] = 'summer';
	$language['autumn'] = 'autumn';
	$language['winter'] = 'winter';
	
	//Mailler
	$language['forget_pass_subject'] = 'user info';
	$language['hello'] = 'Hello';
	$language['hello_mail'] = 'Hello! You will find your username and password below. Please don\'t forget to change your password once you login. Walltyper is very happy to have you among us';
	$language['gooddays'] = 'Good days everyone!';
	$language['password'] = 'password';
	$language['forget_pass_info'] = 'upon your request, your user information has been sent to your mail address. 
	Your info is as listed below.';
	$language['registeration_mail'] = 'Mail';
	$language['registeration_username'] = 'User Name';
	
	//iletişim
	$language['contact'] = 'Contact';
	$language['name'] = 'Name';
	$language['surname'] = 'Surname';
	$language['email'] = 'Email';
	$language['eposta'] = 'Email';
	$language['phone'] = 'Phone Number';
	$language['subject'] = 'Subject';
	$language['Message'] = 'Message';
	$language['Send'] = 'Send';
	$language['mail_header'] = 'You have message from Walltyper!';
	$language['mail_thanks'] = 'We received your message and will contact you soon.';
	
	//çöp
	$language['bin_empty'] = 'empty trash';
	$language['bin_message'] = 'numbered entry deletion';
	
	//ispiyon
	$language['spy_succeed'] = 'deleted and sent to spy list for approval';
	$language['button_whisper'] = 'whisper';
	
	//Mesaj 
	$language['inbox'] = 'inbox';
	$language['outbox'] = 'outbox';
	$language['compose'] = 'compose';
	$language['to'] = 'to';
	$language['sender'] = 'from';
	$language['reciever'] = 'receiver';
	$language['msg_subject'] = 'subject';
	$language['msg_message'] = 'message';
	$language['msg_date'] = 'date';
	$language['msg_new'] = 'New';
	$language['delete_selected'] = 'Delete Selected';
	$language['delete_all'] = 'Delete All';
	$language['inbox_empty'] = 'inbox empty';
	$language['isnotReaded'] = 'is not readed';
	$language['says'] = 'says';
	$language['reply'] = 'Reply';
	$language['message_sent'] = 'message sent';
	$language['previous_message'] = 'previous message';
	$language['tips_subject'] = 'you have to write a subject to send your message';
	$language['button_send_message'] = 'send message';
	
	//Onlines.php
	$language['dictionary_news'] = 'News';
	$language['dictionary_onlines'] = 'Onlines';
	$language['dictionary_online_authors'] = 'Online writers';
	$language['dictionary_block'] = 'block';
	$language['error_notonline'] = 'you are not online!';
	
	//Hata kontrol
	$language['error_password_fillEverywhere'] = 'you have to fill everywhere';
	$language['error_password_wrong'] = 'your password is wrong';
	$language['error_passwords_wrong'] = 'your passwords are not matched';
	$language['error_passwords_login'] = 'you successfully changed your password, please re-login.';
	$language['error_acceptRules'] = 'you have to accept terms and conditions in order to register Walltyper';
	$language['error_password_wrong'] = 'your password is wrong';
	$language['error_passwords_wrong'] = 'your passwords are not matched, please try again.';
	$language['error_choose_password'] = 'please choose a password';
	$language['error_passwords_login'] = 'you successfully changed your password, please re-login.';
	$language['error_username'] = 'you entered an invalid character for your username.';
	$language['error_mail'] = 'you must give a valid mail address in order to complete the process.';
	$language['error_already_mail'] = 'your mail address is already registered for Walltyper.';
	$language['error_first_char'] = 'you cannot put [space] for the first letter of your user ID.';
	$language['error_already_user'] = 'this username is already taken by someone. Sorry Boss!';
	
	//Sözlüğe Kayıt
	$language['dictionary_register'] = 'register';
	$language['register_code_alert'] = 'code alert, wrong code';
	$language['register_code'] = 'register code';
	$language['register_code_message'] = 'enter whatever you see in the picture, if not clear you may refresh the picture';
	$language['register_username_message'] = 'your username cannot include special characters';
	$language['register_namesurname_message'] = 'name? surname?';
	$language['register_gender_message'] = 'gender?';
	$language['register_city_message'] = 'city?';
	$language['register_code_message'] = 'secret code?';
	$language['dictionary_register_accept'] = 'I accept';
	$language['button_ivecome'] = 'LetMeIn';
	$language['username_tips'] = 'Your nick which will represent you in Walltyper which you cannot change after which you can use space between words which we encourage to use English characters.End of whiches…';
	$language['confirm'] = 'confirm';
	$language['password_tips'] = 'for your account security, please use complicate combinations for your password. Please enter your password.';
	$language['register_mail'] = 'register mail';
	$language['register_mail_tips'] = 'your information will be send to your mail address. Depending on your mail service provider, this mail can go to spam or junk folder. We advice you to check those folders in case you don’t receive anything in your inbox.';
	$language['register_name_tips'] = 'Please write correct name and surname for your registration. Fake information may subject to account deletion.';
	$language['register_birthDate'] = 'birthdate';
	$language['january'] = 'January';
	$language['february'] = 'February';
	$language['march'] = 'March';
	$language['april'] = 'April';
	$language['may'] = 'May';
	$language['june'] = 'June';
	$language['july'] = 'July';
	$language['august'] = 'August';
	$language['september'] = 'September';
	$language['october'] = 'October';
	$language['november'] = 'November';
	$language['december'] = 'December';
	$language['register_birth_tips'] = 'The day you open your eyes to this wonderful world.';
	$language['register_gender_tips'] = 'you know…gender…not very complicate!';
	$language['register_city_tips'] = 'city you are living';
	$language['button_makeMeAuthor'] = 'make me writer';
	$language['button_giveup'] = 'IGiveUp';
	$language['message_welcome_header'] = 'welcome to Walltyper!';
	$language['message_welcome_note'] = 'welcome to Walltyper. <br><br>your status is rookie. You have to write 10 entries in order to be nominated as a writer. On the nomination period, your entries will be watched by our moderators and depending on the volume and quality you may promote to be a Walltyper writer. <br><br>Enjoy your time while roaming around Waltyper. Don’t be shy to write entries all around the site. Walltyper is as efficent as your contributions. <br><br>Take Care!'; //buradaki <br> tagları satır atlamaya yarar. mesajınızda satır atlamak istiyorsanız iki defa <br> yazınız.
	$language['message_welcome_accepted'] = '<center><br><br>
	Your application has been accepted.<br><br> your user information has been send to your mail address. Come back soon… </center>';

	//staff
	$language['staff_creator'] = 'Owner';
	$language['admins'] = 'Admins';
	$language['moderators'] = 'Moderators';
	$language['spys'] = 'Spies';
	$language['message_software'] = 'powered by'; 

	
	//vampara
	$language['word'] = 'word';
	$language['how_sort'] = 'sort it';
	$language['sort_a_z'] = 'from a to z'; // ' işaretlerinden önce eğer kullanacaksanız bir adet / koymalısınız, örnekte olduğu gibi.
	$language['sort_z_a'] = 'from z to a';
	$language['sort_new_old'] = 'new to old';
	$language['time_interval'] = 'time interval';
	$language['from'] = 'from';
	$language['there'] = 'there';
	$language['detailed'] = 'detailed';
	$language['fastly'] = 'quickly';
	$language['from_beautiful'] = 'goodOne';
	$language['vamp_ara'] = 's e a r c h'; //&nbsp; bir kare boşluktur
	$language['vamp_ara_button'] = 'search';
	
	//zirvetör
	$language['zirvetor'] = 'activities';
	$language['zirvetor_add'] = 'add activities';
	$language['new_activities'] = 'new activities';
	$language['closed_activities'] = 'closed activities';
	$language['activity_add_date'] = 'activity add date';
	$language['activity_date'] = 'activity date';
	$language['activity_name'] = 'name of activity';
	$language['activity_category'] = 'category';
	$language['activity_place'] = 'place';
	$language['activity_organizator'] = 'organiser';
	$language['activity_organizators'] = 'organisers';
	$language['activity_organizators_tips'] = 'if there is more than one organizer, please separate with comma.';
	$language['activity_added'] = 'you have been added to attendance list';
	$language['activity_comment_added'] = 'your comment has been added';
	$language['activity_comment_deleted'] = 'your comment has been deleted';
	$language['activity_details'] = 'activity details';
	$language['activity_date_hour'] = 'date-hour';
	$language['activity_attendance'] = 'attendance';
	$language['activity_back_down'] = 'I cannot participate';
	$language['activity_add_list'] = 'I am also coming';
	$language['activity_comments'] = 'comments';
	$language['activity_preview'] = 'activity previews';
	$language['activity_added'] = 'added';
	$language['activity_deleted'] = 'numbered activity has been deleted';
	$language['activity_information'] = 'activity information';
	$language['activity_poster'] = 'activity poster';
	$language['activity_poster_tips'] = '
	1- enter the "url" of the activirt. <br>(ex: http://www.abc.com/dfg.jpg , this should be the format.)
	<br>2- <a href="http://imageshack.us">http://imageshack.us</a> is good place to upload the photos.
	<br>3- poster size must be 250x350 . if not it will be adjusted automatically.
	<br><b>4- if you didn’t get it, keep the original(images/poster.jpg) </b>'; //taglara dokunmadan çevirinizi yapmaya özen gösteriniz.
	$language['button_add_activity'] = 'publish my activity';
	
	//zirve kategorileri
	$language['activity_category_conventional'] = 'Walltyper gathering';
	$language['activity_category_chat'] = 'meeting/chat';
	$language['activity_category_concert'] = 'concert';
	$language['activity_category_trip'] = 'trip';
	$language['activity_category_game'] = 'game';
	$language['activity_category_music'] = 'music/dance';
	$language['activity_category_screen'] = 'watch (cinema/tv/theater)';
	$language['activity_category_birthdate'] = 'birthday';
	$language['activity_category_sports'] = 'sports';
	$language['activity_category_screen'] = 'watch (cinema/tv/theater)';
	$language['activity_category_help'] = 'help/charity';
	$language['activity_category_eating'] = 'eating';
	$language['activity_category_other'] = 'others';
	
	//Admin Paneli
	
	//Admin Butonları
	$language['admin_menu_update'] = 'update stats';
	$language['admin_menu_authors'] = 'about writers';
	$language['admin_menu_dictionary'] = 'about Walltyper';
	$language['admin_menu_mail'] = 'mass mail';
	$language['admin_menu_room'] = 'writer acceptance room';
	$language['admin_menu_ip'] = 'ban ip';
	$language['admin_menu_theme'] = 'theme panel';
	$language['admin_menu_banner'] = 'ad panel';
	$language['admin_menu_seen_deleted_messages'] = 'don’t show deleted entries';
	$language['admin_menu_editted_entries'] = 'editted entries';
	$language['admin_menu_entries'] = 'about entries';
	$language['admin_menu_deleted_entries'] = 'deleted entries';
	$language['admin_menu_topics'] = 'about topics';
	$language['admin_menu_deleted_topics'] = 'deleted topics';
	$language['admin_menu_add_member'] = 'add member';
	$language['admin_menu_news'] = 'news';
	$language['admin_menu_admin_news'] = 'admin news';
	$language['admin_menu_reports'] = 'reports';
	$language['admin_menu_spy'] = 'spy';
	$language['admin_menu_admin_chat'] = 'admin chat';
	$language['admin_menu_spy_chat'] = 'spy chat';
	$language['admin_not_allowed'] = 'not authorized for this option.';
	$language['admin_add_adminNews'] = 'Admin announcements';
	$language['admin_news'] = 'news';
	$language['admin_news_details'] = 'details';
	$language['admin_news_spy'] = 'spies can view this announcement?';
	$language['edit_activate'] = 'activate';
	$language['admin_bumber'] = 'deleter';
	$language['admin_bumber_reason'] = 'reason of delete';
	$language['change_topic'] = 'change topic';
	$language['unlocked'] = 'unlocked';
	$language['locked'] = 'locked';
	$language['deleted'] = 'deleted';
	$language['deleted_allentries'] = 'all entries attached with this topic has been deleted.';
	$language['problem_with_topic'] = 'your topic is wrong, please correct the character mistakes and try again.';
	$language['topic_moved'] = 'topic moved.';
	$language['topic_where'] = 'topic to be moved';
	$language['listenUp'] = 'ListenUp!';
	$language['mail_sended'] = 'mail sended.';
	$language['mail_everyone'] = 'to everyone';
	$language['mail_admin'] = 'to admin';
	$language['mail_mods'] = 'to moderators';
	$language['mail_authors'] = 'to writers';
	$language['mail_readers'] = 'to readers';
	$language['button_listenUp'] = 'ListenUp';
	$language['button_bringAll'] = 'Bring all entries';
	$language['latest_50entry'] = 'latest 50 entry';
	$language['text'] = 'text';
	$language['back_authorsroom'] = 'back to writer approval room';
	$language['entry_deleted'] = 'entry deleted';
	$language['add_new_news'] = 'add news';
	$language['ip_blocked'] = 'ip blocked';
	$language['ip_add'] = 'ip banned';
	$language['wrong_spy'] = 'spy request denied and entry activated.';
	$language['wrong_spy_subject'] = 'numbered entry you spied';
	$language['wrong_spy_message'] = 'numbered entry considered as wrong spy';
	$language['button_wrong_spy'] = 'wrong spy';
	$language['button_true_spy'] = 'approve and delete';
	$language['spy_reason'] = 'spying reason';
	$language['delete_user_completely'] = 'purge the user and all entries';
	$language['access_level'] = 'access level';
	$language['suspended'] = 'suspended';
	$language['button_edit_user'] = 'edit user';
	$language['status'] = 'status';
	$language['message_delete_user_entries'] = 'delete user entries silently.';
	$language['message_delete_user_comments'] = 'delete user entries with comments.';
	$language['message_delete_user_votes'] = 'delete user votes.';
	$language['message_delete_user'] = 'delete user.';
	$language['message_delete_user_sure'] = 'entries will be deleted, are you sure?';
	$language['message_you_are_accepted'] = 'you have been accepted for Walltyper. We hope that you will enjoy your time roaming around and we are looking forward to read your entries. <br> 
	Welcome aboard.';
	$language['message_accepted_header'] = 'you have been accepted to Walltyper';
	$language['message_you_are_accepted_mail'] = 'You have been accepted for Walltyper as a writer. We hope that you will enjoy your time roaming around and we are looking forward to read your entries. <br> Welcome aboard.';
	$language['message_waitingApproval'] = 'writers waiting approval';
	$language['button_acceptToDictionary'] = 'accept to Walltyper';
	$language['reactivated'] = 'reactivated.';
	$language['process'] = 'process';
	$language['activate'] = 'activate';
	$language['deleted_entries'] = 'deleted entries';
	$language['latest_50_private_messages'] = 'last 50 private message';
	$language['report_declined'] = 'report declined';
	$language['button_report_declined'] = 'report declined';
	$language['button_reporter'] = 'reporter';
	$language['open'] = 'open';
	$language['closed'] = 'close';
	$language['construction'] = 'under construction';
	$language['registration_mode'] = 'Registration Mode';
	$language['registration_mode_mail'] = 'mail approved';
	$language['registration_mode_Nomail'] = 'mail not approved';
	$language['registration_tips'] = '<b> * </b>we highly advice you to choose e-mail approved option in order to register to Walltyper. Your password will be send to your e-mail separately.
    <br><b>*</b>if there is a time zone difference between your time and server time please mention it in here. ';
	$language['time_zone'] = 'Time Zone';
	$language['notes'] = 'notes';
	$language['stats_update'] = 'Update the statistics';
	$language['new_theme'] = 'add new theme';
	$language['theme'] = 'theme';
	$language['theme_maker'] = 'theme maker';
	$language['theme_updated'] = 'theme updated';
	$language['theme_date_format'] = '(day/month/year)';
	$language['theme_add_tips'] = 'theme name must be the same name with the css file inside /images folder. 
	<br>example: when you add sunset.css theme to the system, you have to write “sunset” as the theme name.';

	//sonradan
	$language['Subject'] = 'Subject';
	$language['total_theme'] = 'total theme';
	$language['entry_by_time'] = 'entries by time';
	$language['updated'] = 'updated';
	$language['rememberpassword'] = 'remember my password';
	$language['karma'] = 'average points you take'; //Yazarın aldığı oyların sonucunda ortaya çıkan ortalama puanıdır
	$language['varma'] = 'average points you give'; //Yazarın verdiği oyların sonucunda ortaya çıkan ortalama puanıdır
	$language['last_appreciated'] = 'last favorite entries';
	$language['ben_most_appreciated'] = 'most favorite entries';
	$language['ben_most_disg'] = 'most disliked entries';
	$language['ben_last_disg'] = 'last disliked entries';
	$language['ben_latest_votes'] = 'latest votes';
	$language['profil'] = 'profile';
	
	//davet
	$language['invitation_dictionary'] = 'Invite to Walltyper';
	$language['refer_text1'] = 'has invited you to join Walltyper.';
	$language['refer_text2'] = 'you are being redirected to';
	$language['refer_error'] = 'wrong invitation code.';
	$language['invitations_sent'] = 'invitations has been sent';
	$language['invitations_slogan'] = 'you can copy this address for inviting your friends to Walltyper!';
	$language['invitations_desc_comma'] = 'by using commas, you can create your own mail list.ex: wall@walltyper.com';
	$language['invitation_message'] = 
	'Walltyper is an online community where you can blog, share, discuss and seek information on almost any topics you can think of. Got something on your mind? Walltyper! Just register through the link below and start discovering the true meaning of life!';
	$language['invitation_message_footer'] = 'see you there.';
	$language['invitation_own'] = 'members joined through your invitation';
	$language['invitation_own_message'] = 'you didn’t send any invitations yet.everything ok boss?';
	
	//profil
	$language['change_picture'] = 'change photo';
	$language['upload_picture'] = 'upload photo';
	$language['update_info'] = 'update info';
	$language['total'] = 'total';
	$language['total_plus'] = 'total +';
	$language['total_minus'] = 'total -';
	$language['whoami'] = 'WhoAmI';
	
		//en son eklenenler
	$language['listenUp2'] = 'announcement';
	$language['panelsettings'] = 'Panel Settings';
	$language['picture'] = 'photo';
	$language['edit2'] = 'edit';
	$language['kick_from_list'] = 'kick from list';
	$language['listenUp3'] = 'Announcement';
	$language['announcement_platform'] = 'Announcement platform';
	$language['city_cannot_empty'] = 'cannot be empty';
	$language['namesurname_cannot_empty'] = 'name and surname cannot be empty';
	$language['send_invitation'] = 'send invitation';
	$language['impossible'] = 'impossible';
	$language['save'] = 'Send';
	$language['closing'] = 'closing';
	$language['omg'] = 'OMG!';
	$language['isneakyou'] = 'i see you!';
	$language['kicked'] = 'kicked';
	$language['deleteee'] = 'purge';
	$language['pwsentto'] = 'your password has been sent to your e-mail adress.';
	$language['or2'] = 'or';
	$language['aboutyou'] = 'about';
	$language['minimalauthor'] = '11';
	$language['minimalauthor'] = 'unexpected error happened!';
	$language['search'] = 'search';
	$language['bring'] = 'Create';
	$language['bring_title'] = 'bring title';
	$language['search_title'] = 'search title';
	$language['ukte_note'] = 'iwish notes';
	$language['throwup'] = 'fly me to the moon';
	$language['nonauthor_warning'] = 'you have to be writer in order to see the content.';
	$language['hello3'] = 'hello?';
	$language['newmessage'] = 'sent you a message.';
	$language['newmessage_1'] = 'you can reach your messages from the address below.';
	$language['newmessage_2'] = 'You got message!';
	$language['punishment_message_alarm'] = 'you cannot send message while penaltized!';
	$language['empty_place_alarm'] = 'fill the empty spaces';
	$language['added_3'] = 'you must be added';
	$language['added_4'] = 'add to list';
	$language['added_5'] = 'leave from list';
	$language['edit_comment3'] = 'edit comment';
	$language['delete_comment'] = 'delete comment';
	$language['zirvetor_add2'] = 'add activity';
	$language['zirvetor_wtf'] = 'does not compute!';
	$language['zirvetor_added'] = 'activity added';
	$language['zirvetor_return'] = 'return';
	$language['zirvetor_situation'] = 'activity situation';
	$language['zirvetor_name'] = 'activity name ';
	$language['zirvetor_locality'] = 'activity location';
	$language['zirvetor_category_1'] = 'Walltyper Offical Activity';
	$language['zirvetor_category_2'] = 'meeting/chat';
	$language['zirvetor_category_3'] = 'concert/festival';
	$language['zirvetor_category_4'] = 'trip';
	$language['zirvetor_category_5'] = 'game';
	$language['zirvetor_category_6'] = 'music/dance';
	$language['zirvetor_category_7'] = 'watch';
	$language['zirvetor_category_8'] = 'birthday';
	$language['zirvetor_category_9'] = 'sport';
	$language['zirvetor_category_10'] = 'picnic';
	$language['zirvetor_category_11'] = 'desert safari';
	$language['zirvetor_category_12'] = 'charity/support';
	$language['zirvetor_category_13'] = 'food';
	$language['zirvetor_category_14'] = 'other';
	$language['zirvetor_choose'] = 'LetMeChoose';
	$language['zirvetor_comma'] = 'use comma to seperate.';
	$language['comment_edited'] = 'comment edited.';
	$language['renew_image'] = 'renew image';
	$language['announcement_comment'] = 'Comment';
	$language['announcement_question'] = 'Question';
	$language['announcement_blood'] = '';
	$language['announcement_forsale'] = 'Buy-Sale';
	$language['announcement_type'] = 'Type';
	$language['announcement_topic'] = 'Topic';
	$language['announcement_news'] = 'News';
	$language['announcement_delete_5'] = 'wrong way';
	$language['announcement_dict'] = 'Walltyper';
	$language['announcement_add'] = 'Add';
	$language['announcement_cevab'] = 'Answer';
	$language['announcement_enter'] = 'Enter';
	$language['announcement_all'] = 'All announcements';
	$language['youarekicked'] = 'YouAreFired!';
	$language['go_back'] = 'go back';
	$language['sitemap'] = 'sitemap';
	$language['archive'] = 'archive';
	$language['last_entries'] = 'Last Entries';
	$language['update_succsess'] = 'succesfully updated!';
	$language['upload_photo_crop'] = 'crop your profil photo';
	$language['upload_photo_re'] = 'ctrl + f5 to refresh your photo';
	$language['upload_photo_i_crop'] = 'Crop';
	$language['upload_picture2'] = 'Upload Photo';
	$language['upload_photo_cmon'] = 'Send Photo';
	$language['upload_photo'] = 'Photo ';
	$language['top_ask'] = 'Ask ';
	$language['youtube_code'] = 'youtube code';
	$language['youtube_code_tip'] = 'write after = symbol';
	$language['image_url'] = 'image url';
	$language['link_adress'] = 'link address';
	$language['show_up_entries'] = 'show entries';
	$language['rookie_status'] = 'Rookie Status';
	$language['dict_operations'] = 'Walltyper Operations';
	$language['Site'] = 'Site';
	$language['login_with_mail'] = 'Login with Mail';
	$language['show_searchintitle'] = 'Search online writers Topic';
	$language['show_insiders'] = 'Show Online writers';
	$language['show_profile_photo'] = 'Show profile photo';
	$language['show_inn'] = 'Show Online writers';
	$language['show_howyoudoing'] = 'How you want it to be seen?';
	$language['show_colorful_statistic_box'] = 'Show colorfull statistic box';
	$language['show_facebookandtwitter'] = 'Show Facebook&Twitter';
	$language['update_statistic'] = 'Update Statistics';
	$language['author_operations'] = 'Writer Operations';
	$language['dict_settings'] = 'Walltyper Settings';
	$language['mail_to_everyone'] = 'send mail to everyone';
	$language['acceptance_room'] = 'writer acceptance room';
	$language['ip_ban'] = 'BanIP';
	$language['theme_panel'] = 'Theme Panel';
	$language['advertisement_panel'] = 'Ad Panel';
	$language['become_wiser_entries'] = 'Improving Entries';
	$language['entries_operations'] = 'Entry Operations';
	$language['deleted_entries'] = 'Deleted Entries';
	$language['topic_operations'] = 'Topic Operations';
	$language['dead_topics'] = 'Dead Topics';
	$language['add_an_author'] = 'Add Writer';
	$language['announcement_panel'] = 'Announcement Panel';
	$language['administrator_announcements'] = 'Admin Announcement';
	$language['complaints'] = 'Complains';
	$language['entries_by_time'] = 'Entries by time';
	$language['spieds'] = 'Spied ones';
	$language['admin_menu_add_desc'] = 'This section shows rookies that wrote at least one entry. Writer approvals must be done from here.';
	$language['rookie_entries'] = 'entries';
	$language['cancel_rookie'] = 'reject';
	$language['entries_added'] = 'entries added';
	$language['bring_most_hardworkers'] = 'bring most hard working writers';
	$language['add_new_theme'] = 'add new theme';
	$language['theme_control'] = 'theme control';
	$language['select_as_maintheme'] = 'select as main theme';
	$language['total_themes'] = 'total themes';
	$language['maintheme'] = 'main theme';
	$language['deleted_entries_returnback'] = 'add deleted entry to topic.';
	$language['regenerated'] = 'resurrect';
	// admin sidebar'a sonradan eklenenler
	$language['badwords'] = 'Bad Words';
	$language['forbidden_words'] = 'forbidden words';
	$language['add_word'] = 'add word';
	
	// yeni reklam paneli
	$language['add_new_advertisement'] = 'Add New Ad';
	$language['advertisement_code_ifadsense'] = 'enter code if its adsense.';
	$language['advertisement_Name'] = 'Ad Name';
	$language['photo_url'] = 'Photo Url';
	$language['Destination_url'] = 'Destination Url';
	$language['Advertisement_code'] = 'Ad Code';
	$language['Active_advertisements'] = 'Active Ads';
	$language['TheDate'] = 'Date';
	$language['Advertisement'] = 'Ad';
	$language['DestinationUrl'] = 'Destination  Url';
	$language['Advertisement_source'] = 'Ad Source';
	$language['Advertisement_admin'] = 'Ad Admin';
	$language['Active']= 'Active';
	
	//walltyper son eklenenler
	$language['dictionaryMail_inv'] = 'Walltyper daveti';
	$language['entries_betweensum'] = 'arasında toplam';
	$language['close_complaint'] = 'close complaint';
	$language['answered'] = 'answered';
	$language['take_it_back'] = 'reopen';
	$language['badwordwarning']= 'your title contains bad words';
	$language['mailnick_together'] = 'please, insert nick and e-mail';
	$language['delete_reaseon'] = 'delete reason';
	$language['seen_byAdmin'] = 'seen by Admin';
	$language['turndefaultheme'] = 'Main Theme';
	$language['issetasmaintheme'] = 'is set as main theme';
	$language['addthistitletodict'] = 'add this title to';
	$language['lastupdate'] = 'last update';	
	
	//Photo upload
	$language['photo_extension_error'] = 'Permitted Extensions:';
	$language['photo_max_size'] = 'Max Photo File Size:';
	$language['photo_crop_error'] = 'Crop your photo!';
	$language['photo_error'] = 'Photo Upload Error!';
	$language['photo_go_profile'] = 'OK! Go to my profile';
	$language['photo_go_retry'] = 'Naaah! Let me try again';
	$language['contact_entry_about'] = 'About the entry: ';
	
	$language['position'] = 'position';
	$language['top'] = 'top';
	$language['right'] = 'right';
	$language['refresh'] = 'refresh';
	
	$language['noAnnouncementYet'] = 'There is no Announcement Yet';
	$language['Announcement_Register'] = 'Register';
	$language['msg'] = 'msg';
	$language['AnnouncementAnswer_suretoDelete'] = 'Are you sure to delete answer?';
	$language['Announcement_suretoDelete'] = 'Are you sure?';
	
	$language['buddyList'] = 'buddies';
	$language['afterMe'] = 'after me';
?>