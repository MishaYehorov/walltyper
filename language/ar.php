﻿<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/><?php 
/*
Sözlük Sistemi - 2012 - Sözlük v6
http://www.sozluksistemi.com
Destek: http://www.sozluksistemi.com/user

Bu dosya, tüm sözlük içindeki metinleri bir araya getirir. Çeviri yapacak olanlar için sistemi çevirmeyi kolaylaştırmayı amaçlamıştır.

- Çeviri Yaparken -

Örneğin: 
$language['dictionaryUrl'] = 'http://www.sozluksistemi.com';

Burada gördüğünüz $language['dictionaryUrl'] , bir değişkendir. İçine kesinlikle dokunulmamalıdır. Çeviri yapacak olanı sadece karşısındaki tırnak içindeki kelime ilgilendirir.
Gerekli çeviriyi tırnaklara dokunmadan yapar.

Örneğin:
$language['button_choose'] = 'seç'; (Orjinali)
$language['button_choose'] = 'choose'; (Çevrilmiş hali)

Kafanızı karıştıran bir durum olması durumunda sozluksistemi.com adresine girip müşteri panelinden destek alabilirsiniz.
*/
	$language = array();
	
	//General
	$language['dictionaryUrl'] = 'http://www.onesourcer.com'; //sözlük urlsi (sonunda / koymayın)
	$language['dictionaryName'] = 'Walltyper'; //Sözlüğün ismi gerekli olan yerlerde bu isim kullanılır.
	$language['dictionarySlogan'] = '社会图书馆'; //sözlük sloganı
	$language['dictionaryYear'] = '2015'; //En alt satırdaki kuruluş yılı
	$language['creator'] = 'hkust ece students'; //düşünenin nickini yazınız
	$language['dictionaryKeywords'] = ' hkust'; //sözlük keywordleri
	$language['dictionaryDescription'] = ' المكتبة الظاهرية، بقعة بلوق، اليوميات، قاموس الحية، ومجلس الاعتراف، بوابة اجتماعية، الصحيفة الرقمية، وأكثر من ذلك بكثير'; //sözlüğün kısa açıklaması
	$language['googleAnalyticsCode'] = 'UA-27980506-1'; //google analytics kodu
	$language['facebook_url'] = 'http://www.google.com'; //sözlüğün facebook adresi açıklaması
	$language['twitter_url'] = 'https://twitter.com/#!/'; //sözlüğün kısa açıklaması
	$language['software'] = ' sozluksistemi.com '; //Bu kısmı kaldırmayınız, aksi takdirde teknik destek alamazsınız.
	$language['dictionaryMail'] = 'mafimushkila@mafimushkila.com'; //Sözlüğün mail adresi
	$language['footerMessage'] = ' قد يتضمن هذا الموقع معلومات واللغة التي يمكن أن تكون غير ملائمة للأطفال. قد يكون مضمون MafiMushkila.com تكون غير دقيقة وخيالية. أو ربما لا! من يدري؟ مضمون الإدخالات تحت مسؤولية مجرد المؤلفين كل منهما. MafiMushkila لا تتحمل أي مسؤولية عن ما هو مكتوب. MafiMushkila لا يحمل أي حقوق التأليف والنشر لمقالات مكتوبة من قبل المؤلفين. ويمنع منعا باتا المواضيع المبتذلة، والعنصرية، والتحرش، فاحش البغيضة، والمناقشات، السياسية و تدنيس الدينية، وتعزيز الايديولوجيات السياسية،والبريد التطفلي فيضان،الإعلان المباشرة، المجتاحة من خصوصية الأشخاص في MafiMushkila. ويحظر دخول أي التي قد تؤدي الى اي انتهاك للقانون. احترام قواعد التقاليد والثقافة والقانون والرقابة من الشرق الأوسط';
	//Generel Buttons
	$language['button_choose'] = 'اختر';
	$language['button_back'] = 'عودة';
	$language['button_bringIt'] = 'إلى هنا';
	$language['button_entries'] = 'الإدخالات';
	$language['button_message'] = 'رسالة';
	$language['button_buddy'] = 'رفيق';
	$language['button_blocked'] = 'حظر';
	$language['button_report'] = 'تقرير';
	$language['button_youSure'] = 'تأكيد';
	$language['button_yes'] = 'نعم';
	$language['all'] = '所有';
	
	//Top.php
	$language['topbutton_dictionary'] = 'Walltyper'; 
	$language['description_dictionary'] = ' ماذا يكونMafiMuskila؟';
	$language['topbutton_sukela'] = 'رهيبة'; 
	$language['description_sukela'] = 'أحب الإدخالات';
	$language['topbutton_mix'] = '混合'; 
	$language['description_mix'] = 'عرض مواضيع عشوائية';
	$language['topbutton_stats'] = '统计';
	$language['description_stats'] = ' إحصاءاتMafiMushkila';
	$language['topbutton_enter'] = 'تسجيل';
	$language['description_enter'] = ' تسجيلMafiMushkila';
	$language['topbutton_extras'] = 'إضافات';
	$language['description_extras'] = 'ميزات إضافية';
	$language['topbutton_exit'] = 'خروج';
	$language['description_exit'] = 'تسجيل الخروج';
	$language['message_exit'] = ' لا تترك إذا كنت متأكدا100٪!';
	$language['topbutton_me'] = 'أنا';
	$language['description_me'] = ' نعم أنت';
	$language['topbutton_admin'] = 'مشرف';
	$language['description_admin'] = 'مشرف أدوات';
	$language['topbutton_spy'] = 'الجاسوس';
	$language['description_spy'] = 'أدوات تجسس';
	$language['topbutton_sss'] = 'أسئلة وأجوبة';
	$language['topbutton_sss_long'] = 'الأسئلة المتداولة';
	$language['description_sss'] = ' الأسئلة المتداولة حول MafiMushkila';
	$language['topbutton_today'] = 'اليوم';
	$language['description_today'] = 'موضوعات اليوم';
	$language['topbutton_yesterday'] = 'أمس';
	$language['description_yesterday'] = 'كل ما عندي من المشاكل يبدو بعيدا جدا';
	$language['topbutton_contact'] = 'اتصل';
	$language['description_contact'] = 'الاتصال بنا';
	$language['topbutton_register'] = 'تسجيل';
	$language['description_register'] = 'أريد أن أكون كاتبا';
	$language['topbutton_control'] = 'إعدادات';
	$language['description_control'] = 'لوحة التحكم';
	$language['topbutton_wish'] = 'أتمنى';
	$language['description_wish'] = 'موضوعات اليوم التي ترغب شخص ملئه لك';
	$language['topbutton_random'] = 'عشوائية';
	$language['description_random'] = 'البحث عن موضوع عشوائي';
	$language['topbutton_ok'] = 'موافق';
	$language['description_ok'] = 'العثور على موضوع محدد';
	$language['topbutton_search'] = 'البحث';
	$language['description_search'] = 'البحث عن موضوع معين';
	$language['topbutton_id'] = '#';
	$language['description_id'] = 'جلب إدخال محدد';
	
	// Word.php
	$language['topics'] = 'موضوعات اليوم';
	$language['entries'] = 'الإدخالات';
	$language['general'] = 'عام';
	$language['OK'] = 'موافق';
	$language['or'] = 'أو';
	$language['ADD'] = 'إضافة';
	$language['page'] = 'صفحة';
	$language['message_edited'] = ' تم تحرير الإدخال.';
	$language['deletedTopic'] = ' لقد تم حذف هذا الموضوع.';
	$language['deletedTopic_Mod'] = ' لقد تم حذف هذا الموضوع، يمكنك ان ترى أنها نتيجة لحقوقك وسيط.';
	$language['edit'] = 'تحرير';
	$language['delete'] = 'حذف';
	$language['redirect'] = 'إعادة توجيه'; //Başlığı içindeki tüm verileri ile birlikte başka bir başlığa aktarır, yönlendirir.
	$language['stop'] = 'توقف'; 
	$language['pageViews'] = 'صفحة وجهات النظر'; 
	$language['topicID'] = 'المواضيع معرف'; 
	$language['lockTopic'] = 'قفل الموضوع';
	$language['unlockTopic'] = 'فتح الموضوع';
	$language['messageAlert'] = 'لديك رسالة';
	$language['noFriend'] = 'لا يوجد لديك رفيق';
	$language['button_message'] = 'رسالة';
	$language['button_bin'] = 'القمامة';
	$language['button_event'] = 'حدث';
	$language['inn'] = 'الكتاب على الانترنت';
	$language['mixthings'] = 'مزيج';
	$language['searchintitle'] = 'البحث في المواضيع';
	$language['searchintitle_error'] = ' ليس هناك المواضيع فتح مع هذا الاسم. يمكنك إضافة موضوع جديد أو يرغب شخص ما إضافة لهذا الموضوع.';
	$language['rookieconfirm_error'] = 'كنت تنتظر الموافقة';
	$language['search'] = 'البحث';
	$language['jump'] = 'قفز';
	$language['closeBanner'] = ' أغلق يا سمسم!'; 	
	$language['howyoudoing'] = 'ماذا تقول '; 	
	$language['redirecting'] = 'تتم إعادة توجيهك إلى هذا الموضوع'; 
	$language['error_newTopic'] = ' لديه بعض الشخصيات التي تتجنب فتح هذا الموضوع. الرجاء مراجعة الموضوع الخاص بك وحاول مرة أخرى.'; 
	$language['noResult'] = ' ليس هناك نتيجة مطابقة مع بحثك.'; 
	$language['relatedResults'] = ' يمكن أن يكون شيء من هذا القبيل:'; 
	$language['wishWaitingTopic'] = 'هذا الموضوع ينتظر باعتباره أتمنى'; 
	$language['wishAlreadyWaitingTopic'] = 'هذا الموضوع هو بالفعل أتمنى المواضيع';
	$language['wishAddedWishings'] = 'وأصبح المواضيع أتمنى';
	$language['wisher'] = 'أنا راغب'; 
	$language['wisherGiveComments'] = 'ترك تعليقا على أنا أتمنى لكم';
	$language['wisherComments'] = 'أنا راغب تعليق';
	$language['button_newTopic'] = 'إضافة المواضيع';
	$language['button_newWish'] = 'إضافة أتمنى';
	$language['wishWaitingTopics'] = 'أتمنى';
	$language['wishWaitingTopics_delete'] = 'أتمنى حذف';
	$language['title_solong'] = ' هذا الموضوع له أكثر من 70 حرفا، ويحتاج إلى بعض خفض.';
	$language['warning_rookie'] = ' كنت كاتبا المبتدئ. لديك لكتابة الإدخالات والحصول على 10 في حال الموافقة عليها من أجل فتح موضوع جديد.';
	$language['warning_fillComments'] = 'يرجى ملء تعليقات';
	$language['waitingForApproval'] = ' أحسنت، أنت الآن على قائمة الانتظار إلى أن تكون كاتبا MafiMushkila. سيكون لدينا المشرفين تعديل المداخل الخاصة بك وسيتم كم قبلت إما أن يكون كاتبا أو لا. بسيطة أليس كذلك؟ أوه، بالمناسبة خلال هذه الفترة لا تستطيع كتابة الإدخالات أي أكثر. الصبر هو مفتاح النجاح ...!';
	$language['warning_firstEntry'] = ' كنت قد دخلت للتو دخول الخاص بك أولا. نحن ترقص فرحا كونه الشاهد من هذه اللحظة.';
	$language['warning_RookieEntryCounter'] = ' ما يقرب من هناك!';
	$language['entryInserted'] = ' سجلت دخول.';
	$language['viewEntry'] = ' عرض دخول.';
	$language['addnewtitle'] = ' إضافة موضوع جديد.';
	$language['youaretheone'] = ' ولكم جعله. يتم نشر الإدخال. ما لحظة المجيدة';
	$language['bkz'] = 'ref'; //bakınız
	$language['ara'] = 'search'; 
	$language['spoiler'] = 'spoiler';
	$language['quote'] = 'quote';
	$language['gbkz'] = 'sref'; //gizli bakınız
	$language['u'] = 'u'; //akıllı bakınız
	$language['image'] = 'image';
	$language['spy'] = 'الجاسوس';
	$language['move'] = 'تحرك';
	$language['editSmall'] = 'تحرير';
	$language['who'] = 'من؟';
	$language['message'] = 'رسالة';
	$language['destroy'] = 'حذف';
	$language['makeAlive'] = 'بعث';
	$language['delete_reason'] = 'السبب؟';
	$language['message_deletedEntry'] = 'لقد تم حذف هذا البند';
	$language['message_deletedEntry_Edited'] = ' لقد تم حذف هذا الإدخال. من أعطى الكاتب فرصة ثانية، منقحة وانتظار موافقة مشرف';
	$language['message_rookieEntry'] = 'المبتدئ دخول';
	$language['button_showAll'] = 'عرض جميع';
	$language['button_save'] = 'إرسال';
	$language['message_penalty1'] = 'بسبب';
	$language['message_penalty2'] = 'ويعاقب كم';
	$language['message_rookieEnd'] = ' قدمت كم الإدخالات بما فيه الكفاية لأن يصبح كاتبا. كنت في انتظار موافقة الآن. خلال هذه الفترة لا يمكنك كتابة إدخالات أي أكثر. خذ نفسا عميقا واسترخ ...';
	
	//Bloklar
	$language['hottopics'] = 'مواضيع ساخنة';
	$language['hardworkers'] = 'الكاتب الشغول';
	$language['todaysmostloved'] = 'مواضيع أحببت اليوم';
	$language['insiders'] = 'الكتاب على الانترنت';
	
	//Word.php metinleri bitti
	
	//Diğer
	$language['underConstruction'] = 'نحن الآن قيد الإنشاء.';
	$language['message_siteClosed'] = ' بسبب ما نعرفه ولكن لا تعلمون، لقد أغلقنا. سوف يعود قريبا.';
	$language['message_banned'] = 'وقد تم حظر لك!';
	$language['message_wrongPassword'] = ' اسم المستخدم أو كلمة المرور غير صحيحة.';
	$language['message_diyarError'] = 'هذا القسم هو أغلق.';
	$language['message_logout'] = 'قمت بتسجيل للتو';
	$language['topic'] = 'الموضوع';
	$language['entry'] = 'الإدخال';
	$language['message_fillEverywhere'] = 'لا تنسى أن تملأ كل مكان';
	$language['message_getTopic'] = ' هناك موضوع مماثل افتتح تحت نفس الاسم. غريب أليس كذلك؟';
	$language['message_error'] = 'الأخطاء يحدث';
	$language['dictionary_gates'] = 'MafiMushkilaالإدخال';
	$language['username'] = 'اسم المستخدم';
	$language['button_enterSite'] = 'واسمحوا لي في دخول';
	$language['others'] = 'غيرها';
	$language['register'] = 'تسجيل';
	$language['forget_pass'] = 'نسيت كلمة السر؟';
	$language['vampara_no_topic'] = 'ليس الموضوع متاح';
	$language['vampara_no_information'] = 'لا توجد معلومات';
	$language['about'] = 'عن';
	
	//inc altındakiler
	$language['add_message'] = 'إضافة الرسالة:';
	$language['button_back'] = 'عودة';
	$language['message_noAuthor'] = 'لا يوجد مثل هذا الكاتب';
	$language['message_selfEgo'] = 'صوتوا لصالح الإدخال الخاصة بك، لا';
	$language['message_alreadyVoted'] = 'صوتت مسبقا لهذا الدخول';
	$language['message_voteAdded'] = 'صوتت بنجاح';
	$language['message_OK'] = 'موافق';
	$language['message_NOT'] = 'عليك أن تكون عضوا من أجل رؤية المحتوى';
	$language['author_NOT'] = 'عليك أن تكون كاتبا من أجل رؤية المحتوى';
	
	//Sözlük Aparatları
	$language['dictionaryTools'] = 'صندوق الادوات';
	$language['zirvetor'] = 'أنشطة';
	$language['zirvetor_description'] = 'MafiMushkilaأنشطة';
	
	//ben.php  - ben sayfası
	$language['message_reportMessage'] = 'تلقي تشكو الخاص بك';
	$language['message_reportMessageAgain'] = ' اشتكى أنت مسبقا عن ذلك، خذ نفسا عميقا واسترخ.';
	$language['authorBig'] = 'كاتب';
	$language['message_forgive'] = 'تتم إزالة عقوبة';
	$language['message_giveAppealRookie'] = ' هو السبب لماذا خفضت إلى المبتدئ. جعل القيود اللازمة وتنتظر موافقة المراقبين.';
	$language['message_giveAppealRookieHeader'] = 'مهلة';
	$language['message_giveAppealDelete'] = 'لقد تم حذف الكاتب!';
	$language['message_giveAppealDay'] = 'عقوبة';
	$language['message_appealMessage1'] = 'هو السبب';
	$language['message_appealMessage2'] = 'أيام من ركلة جزاء تحصل عليها.';
	$language['appealDay'] = 'كم يوما من العقوبة';
	$language['makeRookie'] = 'الموافقة على أن يكون المبتدئ؟';
	$language['makeSuspended'] = 'تعليق؟';
	$language['giveAppeal'] = 'معاقبة';
	$language['forgive'] = 'سامح';
	$language['ben_title'] = 'من هو هذا؟';
	$language['message_givenAppeal'] = 'يعاقب هذا الكاتب';
	$language['admin'] = 'المشرف';
	$language['moderator'] = 'المنسق';
	$language['spy'] = 'الجاسوس';
	$language['author'] = 'كاتب';
	$language['rookie'] = 'المبتدئ';
	$language['rookieWA'] = 'المبتدئ في انتظار موافقة';
	$language['notSuchMember'] = 'ليس هناك كاتب مثل مثل هذا';
	$language['notSuchMemberNick'] = 'هذه ليست كنية الكاتب صحيحة';
	$language['totalEntry'] = 'مجموع الإدخال';
	$language['today'] = 'اليوم';
	$language['yesterday'] = 'أمس';
	$language['thisMonth'] = 'هذا الشهر';
	$language['lastMonth'] = 'الشهر الماضي';
	$language['latestPositiveVotes'] = ' صوت آخر+';
	$language['latestNegativeVotes'] = ' آخر- اصوات';
	$language['message_notYet'] = 'لا شيء من هذا القبيل حتى الآن';
	$language['todaysEntries'] = 'الإدخالات اليوم';
	$language['yesterdaysEntries'] = 'الإدخالات ليوم امس';
	$language['deeper'] = 'أعمق';
	$language['users_theme'] = 'المستخدمين الفكرة';
	$language['registration_day'] = 'تسجيل اليوم';
	$language['votesGiven'] = 'أصوات إعطاء';
	$language['votesTaken'] = 'الأصوات التي اتخذت';
	
	//tabs - kontrol paneli tabları
	$language['tab_news'] = 'أخبار';
	$language['tab_setting'] = 'إعدادات';
	$language['tab_message'] = 'رسالة';
	$language['tab_buddy'] = 'رفيق';
	$language['tab_blocked'] = 'حظر';
	$language['tab_themes'] = 'المواضيع';
	$language['tab_bin'] = 'القمامة';
	$language['tab_invite'] = 'دعوة';
	
	//cp.php - ayarlar sayfası
	$language['message_entryDeleted'] = ' تم حذف عدد الإدخال!';
	$language['message_giveMail'] = 'الرجاء إدخال العنوان الإلكتروني الخاص بك';
	$language['your_mail'] = 'العنوان الإلكتروني الخاص بك';
	$language['your_mail_changed'] = 'هو العنوان الإلكتروني الجديد';
	$language['my_password'] = 'كلمة السر';
	$language['new_password'] = 'كلمة مرور جديدة';
	$language['new_password_confirm'] = 'إعادة إدخال';
	$language['button_changePassword'] = 'تغيير';
	$language['your_name_surname'] = 'اسم ولقب';
	$language['city'] = 'مدينة';
	$language['gender'] = 'النوع الاجتماعي';
	$language['male'] = 'ذكر';
	$language['female'] = 'أنثى';
	$language['my_mail'] = 'العنوان بريدي';
	$language['change_my_mail'] = 'العنوان الإلكتروني الجديد';
	$language['page_options'] = 'صفحة الخيارات';
	$language['message_entryListLimit'] = 'الحد الأقصى من دخول قائمة';
	$language['message_ThemeNot'] = 'لا يوجد أي موضوع من هذا القبيل';
	$language['message_succeed'] = ' النجاح!';
	
	//diğer
	$language['blockedOnes'] = 'المحظورة منها';
	$language['buddyOnes'] = 'الأصدقاء هم';
	$language['editingNotes'] = 'تحرير الملاحظات';
	$language['notHave'] = 'لا يوجد مثل الإدخال';
	$language['plus'] = 'زائد';
	$language['minus'] = 'ناقص';
	$language['error_try_delete'] = ' تحاول حذف أخرى الإدخالات ... ليست جيدة!';
	$language['message_deleted'] = ' إضافة إلى حذف المشاهد.';
	$language['button_sure'] = 'بالتأكيد';
	$language['message_move'] = ' هو رقم موضوع جديد!';
	$language['message_move1'] = 'الإدخال عدد';
	$language['choose'] = 'اختر';
	
	//İstatistikler
	$language['stats'] = 'إحصائيات';
	$language['general_stats'] = 'الإحصاءات العامة';
	$language['most_1'] = 'الأكثر الإدخالات';
	$language['most_2'] = 'ابغض الإدخالات';
	$language['most_3'] = 'الأسابيع الماضية الأكثر الإدخالات';
	$language['most_4'] = 'الأسابيع الأخيرة ابغض الإدخالات';
	$language['most_5'] = 'في أكبر عدد من الإدخالات';
	$language['most_6'] = 'موضوعات اليوم الأكثر قراءة ';
	$language['most_7'] = 'اليوم الإدخالات الأولى';
	$language['most_8'] = 'الأكثر الكاتب الإدخال قدمت';
	$language['most_9'] = 'قدمت الاقل الإدخال الكاتب';
	$language['most_10'] = 'الكتاب في الإدخالات الأكثر حذف';
	$language['most_11'] = 'الكتاب في الأكثر رسالة أرسلت';
	$language['most_12'] = 'الأكثر الكتاب';
	$language['most_13'] = 'ابغض الكتاب';
	$language['most_14'] = 'الإدخالات في أشهر';
	$language['most_15'] = 'الإدخالات في مواسم';
	$language['most_16'] = 'النوع الاجتماعي';
	$language['most_17'] = 'موضوع الإحصائيات';
	$language['general_stats_latest'] = 'آخر الإحصائيات';
	$language['total_entry'] = ' إجمالي الإدخال';
	$language['total_topic'] = ' إجمالي المواضيع';
	$language['total_population'] = 'إجمالي';
	$language['total_deleted_topic'] = 'حذف';
	$language['total_deleted_entry'] = 'حذف الإدخالات';
	$language['total_topic_views'] = 'مجموع وجهات النظر إجمالي';
	$language['total_visitors'] = 'إجمالي الزوار';
	$language['most_followed_topic'] = 'الموضوع الأكثر اتباعا';
	$language['topic_per_author'] = 'المعدل اليومي للمواضيع كاتبا';
	$language['entry_per_author'] = 'الإدخالات لكل كاتب';
	$language['message_noStats'] = 'لا توجد إحصائيات في إطار هذه الفئة';
	$language['spring'] = 'ربيع';
	$language['summer'] = 'الصيف';
	$language['autumn'] = 'خريف';
	$language['winter'] = 'شتاء';
	
	//Mailler
	$language['forget_pass_subject'] = 'معلومات المستخدم';
	$language['hello'] = ' مرحبا! سوف تجد اسم المستخدم وكلمة المرور أدناه. من فضلك لا ننسى تغيير كلمة السر الخاصة بك بمجرد تسجيل الدخول. مافي Mushkila سعيد جدا أن تكونوا بيننا';
	$language['gooddays'] = ' يوم جيد للجميع!';
	$language['password'] = 'كلمة السر';
	$language['forget_pass_info'] = ' بناء على طلبك، تم ارسال معلومات المستخدم الخاص بك إلى عنوان البريد الإلكتروني الخاص بك. يتم سرد المعلومات الخاصة بك أدناه.';
	$language['registeration_mail'] = 'بريد';
	$language['registeration_username'] = 'اسم المستخدم';
	
	//iletişim
	$language['contact'] = 'اتصل';
	$language['name'] = 'اسم';
	$language['surname'] = 'كنية';
	$language['email'] = 'البريد الإلكتروني';
	$language['eposta'] = 'البريد الإلكتروني';
	$language['phone'] = 'رقم الهاتف';
	$language['subject'] = 'موضوع';
	$language['Message'] = 'رسالة';
	$language['Send'] = 'إرسال';
	$language['mail_header'] = ' لديك الرسالة منMafiMushkila!';
	$language['mail_thanks'] = ' تلقينا رسالتك وسوف نتصل بك قريبا.';
	
	//çöp
	$language['bin_empty'] = 'إفراغ سلة المهملات';
	$language['bin_message'] = 'حذف الإدخال المرقمة';
	
	//ispiyon
	$language['spy_succeed'] = 'حذفت وأرسلت للموافقة عليها';
	$language['button_whisper'] = 'همس';
	
	//Mesaj 
	$language['inbox'] = 'البريد الوارد';
	$language['outbox'] = 'علبة الصادر';
	$language['compose'] = 'إنشاء';
	$language['to'] = 'إلى';
	$language['sender'] = 'من';
	$language['reciever'] = 'المتلقي';
	$language['msg_subject'] = 'الموضوع';
	$language['msg_message'] = 'الرسالة';
	$language['msg_date'] = 'التاريخ';
	$language['msg_new'] = 'جديد';
	$language['delete_selected'] = 'حذف المختارات';
	$language['delete_all'] = 'حذف الكل';
	$language['inbox_empty'] = 'البريد الوارد فارغ';
	$language['isnotReaded'] = 'لم يقرأ';
	$language['says'] = 'يقول';
	$language['reply'] = 'رد';
	$language['message_sent'] = 'أرسلت الرسالة';
	$language['previous_message'] = 'الرسالة السابقة';
	$language['tips_subject'] = 'عليك أن تكتب موضوعا لإرسال رسالتك';
	$language['button_send_message'] = 'إرسال الرسالة';
	
	//Onlines.php
	$language['dictionary_news'] = 'الأخبار';
	$language['dictionary_onlines'] = 'المتواجدين حاليا';
	$language['dictionary_online_authors'] = 'الكتاب على الانترنت';
	$language['dictionary_block'] = 'منع';
	$language['error_notonline'] = 'ليس على الانترنت';
	
	//Hata kontrol
	$language['error_password_fillEverywhere'] = 'عليك ملء كل مكان';
	$language['error_password_wrong'] = 'كلمة السر غير صحيحة';
	$language['error_passwords_wrong'] = 'لا تطابق كلمة السر الخاصة بك';
	$language['error_passwords_login'] = ' قمت بتغيير كلمة السر الخاصة بك بنجاح، الرجاء إعادة تسجيل الدخول.';
	$language['error_acceptRules'] = ' عليك أن تقبل الشروط والأحكام من أجل التسجيل MafiMushkila';
	$language['error_password_wrong'] = 'كلمة السر غير صحيحة';
	$language['error_passwords_wrong'] = ' لا تطابق كلمة السر الخاصة بك، يرجى المحاولة مرة أخرى.';
	$language['error_choose_password'] = 'الرجاء اختيار كلمة مرور';
	$language['error_passwords_login'] = ' قمت بتغيير كلمة السر الخاصة بك بنجاح، الرجاء إعادة تسجيل الدخول.';
	$language['error_username'] = ' قمت إدخال حرف غير صالح لاسم المستخدم.';
	$language['error_mail'] = ' يجب اعطاء عنوان إلكتروني صحيح من أجل إتمام العملية.';
	$language['error_already_mail'] = ' مسجل مسبقا عنوانك الإلكتروني لMafiMushkila.';
	$language['error_first_char'] = ' لا يمكنك وضع [مساحة] لأول حرف من هوية المستخدم الخاصة بك.';
	$language['error_already_user'] = ' أخذ اسم المستخدم من قبل شخص ما. عفوا الزعيم!';
	
	//Sözlüğe Kayıt
	$language['dictionary_register'] = 'التسجيل';
	$language['register_code_alert'] = 'كود التنبيه رمز الخطأ';
	$language['register_code'] = 'التسجيل رمز';
	$language['register_code_message'] = ' أدخل كل ما تراه في الصورة، إن لم يكن واضحا قم بتحديث صور';
	$language['register_username_message'] = ' يمكن أن اسم المستخدم الخاص بك لا تتضمن الأحرف الخاصة';
	$language['register_namesurname_message'] = 'الاسم؟ اللقب؟';
	$language['register_gender_message'] = 'الجنس؟';
	$language['register_city_message'] = 'المدينة؟';
	$language['register_code_message'] = 'بالرمز السري؟';
	$language['dictionary_register_accept'] = 'أقبل';
	$language['button_ivecome'] = 'اسمحوا لي في الداخل';
	$language['username_tips'] = ' سيكوناسم الشهرة الخاص بك الذي يمثلك في MafiMushkila التي لا يمكنك تغيير بعد والتي يمكنك استخدامها مسافة بين الكلمات التي نحن نشجع على استخدام الأحرف الإنجليزية. نهاية';
	$language['confirm'] = 'تأكيد';
	$language['password_tips'] = ' لأمن حسابك، يرجى استخدام تعقيد تركيبات كلمة المرور الخاصة بك. الرجاء إدخال كلمة المرور.';
	$language['register_mail'] = 'التسجيل إلكتروني';
	$language['register_mail_tips'] = ' وسوف يتم إرسال المعلومات الخاصة بك إلى عنوان البريد الإلكتروني الخاص بك. اعتمادا على البريد الخاص بك مزود الخدمة، يمكن أن يذهب إلى إلكتروني المزعج أو مجلد غير المرغوب فيه. نحن ننصحك بأن تحقق هذه المجلدات في حال كنت لا تتلقى أي شيء في البريد الوارد الخاص بك.';
	$language['register_name_tips'] = ' يرجى كتابة الاسم الصحيح واللقب للتسجيل الخاص بك. قد تكون معلومات وهمية الموضوع إلى حذف الحساب.';
	$language['register_birthDate'] = 'تاريخ الميلاد';
	$language['january'] = 'يناير';
	$language['february'] = 'فبراير';
	$language['march'] = 'مارس';
	$language['april'] = 'أبريل';
	$language['may'] = 'مايو';
	$language['june'] = 'يونيو';
	$language['july'] = 'يوليو';
	$language['august'] = 'أغسطس';
	$language['september'] = 'سبتمبر';
	$language['october'] = 'تشرين الأول';
	$language['november'] = 'تشرين الثاني';
	$language['december'] = 'ديسمبر';
	$language['register_birth_tips'] = ' في اليوم الذي تفتح عينيك على هذا العالم الرائع.';
	$language['register_gender_tips'] = ' تعلمون ... نوع الجنس ... لا تعقد للغاية!';
	$language['register_city_tips'] = 'المدينة انت تعيش';
	$language['button_makeMeAuthor'] = 'اجعلني كاتبا';
	$language['button_giveup'] = 'أتراجع';
	$language['message_welcome_header'] = ' مرحبا بكم فيMafiMushkila!';
	$language['message_welcome_note'] = 'مرحبا بكم فيMafiMushkila. وضعك هو المبتدئ. عليك أن تكتب 10 إدخالات لكي يتم ترشيح ككاتب. في فترة الترشيح سوف مراقبتهم المداخل الخاصة بك من قبل المشرفين لدينا وهذا يتوقف على حجم ونوعية قد تشجع على أن يكون كاتبا MafiMushkila. تمتع وقتك أثناء التجوال في جميع أنحاء MafiMushkila. لا تخجل من كتابة الإدخالات في جميع أنحاء الموقع. MafiMushkila هو يتسم بفعالية بقدر مساهماتكم. يحترس!'; //buradaki <br> tagları satır atlamaya yarar. mesajınızda satır atlamak istiyorsanız iki defa <br> yazınız.
	$language['message_welcome_accepted'] = ' وقد تم قبول طلبك معلومات المستخدم قد تم إرسالها إلى عنوان البريد الإلكتروني الخاص بك. نعود قريبا ...';

	//staff
	$language['staff_creator'] = 'المالك';
	$language['admins'] = 'المسؤولين';
	$language['moderators'] = 'المشرفين';
	$language['spys'] = 'جواسيس';
	$language['message_software'] = 'بدعم من'; 

	
	//vampara
	$language['word'] = 'كلمة';
	$language['how_sort'] = 'ترتيب';
	$language['sort_a_z'] = 'من الألف إلى الياء'; // ' işaretlerinden önce eğer kullanacaksanız bir adet / koymalısınız, örnekte olduğu gibi.
	$language['sort_z_a'] = 'من ي إلى أ';
	$language['sort_new_old'] = 'الجديد إلى القديم';
	$language['time_interval'] = 'الفترة';
	$language['from'] = 'من';
	$language['there'] = 'هناك';
	$language['detailed'] = 'مفصل';
	$language['fastly'] = 'بسرعة';
	$language['from_beautiful'] = 'جيدة';
	$language['vamp_ara'] = 'البحث'; //&nbsp; bir kare boşluktur
	$language['vamp_ara_button'] = 'البحث';
	
	//zirvetör
	$language['zirvetor'] = 'الأنشطة';
	$language['zirvetor_add'] = 'إضافة أنشطة';
	$language['new_activities'] = 'أنشطة جديدة';
	$language['closed_activities'] = 'مغلق الأنشطة';
	$language['activity_add_date'] = 'إضافة نشاط التاريخ';
	$language['activity_date'] = 'تاريخ النشاط';
	$language['activity_name'] = 'اسم النشاط';
	$language['activity_category'] = 'الفئة';
	$language['activity_place'] = 'المكان';
	$language['activity_organizator'] = 'المنظم';
	$language['activity_organizators'] = 'المنظمون';
	$language['activity_organizators_tips'] = ' إذا كان هناك أكثر من منظم، يرجى تقسيم مع فاصلة.';
	$language['activity_added'] = 'قد تم إضافتك إلى الحضور';
	$language['activity_comment_added'] = 'تم إضافة تعليقك';
	$language['activity_comment_deleted'] = 'لقد تم حذف تعليقك';
	$language['activity_details'] = 'تفاصيل الأنشطة';
	$language['activity_date_hour'] = 'تاريخ ساعة';
	$language['activity_attendance'] = 'الحضور';
	$language['activity_back_down'] = 'لا استطيع المشاركة';
	$language['activity_add_list'] = 'أنا قادم أيضا';
	$language['activity_comments'] = 'تعليقات';
	$language['activity_preview'] = 'الأنشطة معاينة';
	$language['activity_added'] = 'إضافة';
	$language['activity_deleted'] = 'لقد تم حذف الأنشطة المرقمة';
	$language['activity_information'] = 'معلومات الأنشطة';
	$language['activity_poster'] = 'ملصق الأنشطة';
	$language['activity_poster_tips'] = '
	1 -دخول "للتسجيل" للنشاط. (مثلا: http://www.abc.com/dfg.jpg، ينبغي أن يكون هذا التنسيق.)
2 - <a href="http://imageshack.us"> http://imageshack.us </ أ> هو مكان جيد لتحميل الصور.
3 - يجب أن يكون حجم الملصق 250x350.وإلا سيتم تعديله تلقائيا.
4 - إذا لم تحصل عليه. تبقي الأصلي (صور /poster.jpg)'; //taglara dokunmadan çevirinizi yapmaya özen gösteriniz.
	$language['button_add_activity'] = 'نشر نشاطي';
	
	//zirve kategorileri
	$language['activity_category_conventional'] = 'MafiMushkila تجمع';
	$language['activity_category_chat'] = ' اجتماع/الدردشة';
	$language['activity_category_concert'] = 'حفلة موسيقية';
	$language['activity_category_trip'] = 'رحلة';
	$language['activity_category_game'] = 'لعبة';
	$language['activity_category_music'] = ' موسيقى/ رقص';
	$language['activity_category_screen'] = ' مشاهدة(السينما / التلفزيون / المسرح)';
	$language['activity_category_birthdate'] = 'عيد ميلاد';
	$language['activity_category_sports'] = 'الرياضة';
	$language['activity_category_screen'] = ' مشاهدة(السينما /التلفزيون /المسرح)';
	$language['activity_category_help'] = ' مساعدة/مؤسسة خيرية';
	$language['activity_category_eating'] = 'الأكل';
	$language['activity_category_other'] = 'آخرون';
	
	//Admin Paneli
	
	//Admin Butonları
	$language['admin_menu_update'] = 'تحديث الإحصائيات';
	$language['admin_menu_authors'] = 'حول الكتاب';
	$language['admin_menu_dictionary'] = ' حولMafiMushkila';
	$language['admin_menu_mail'] = 'كتلة البريد الإلكتروني';
	$language['admin_menu_room'] = 'غرفة قبول الكاتب';
	$language['admin_menu_ip'] = ' حظرIP';
	$language['admin_menu_theme'] = 'لوحة الموضوع';
	$language['admin_menu_banner'] = 'لوحة إعلانية';
	$language['admin_menu_seen_deleted_messages'] = 'لا تظهر الإدخالات المحذوفة';
	$language['admin_menu_editted_entries'] = 'تحرير مقالات';
	$language['admin_menu_entries'] = 'حول إدخالات';
	$language['admin_menu_deleted_entries'] = 'حذف الإدخالات';
	$language['admin_menu_topics'] = 'حول المواضيع';
	$language['admin_menu_deleted_topics'] = 'حذف المواضيع';
	$language['admin_menu_add_member'] = 'أضف عضو';
	$language['admin_menu_news'] = 'أخبار';
	$language['admin_menu_admin_news'] = 'أخبار المشرف';
	$language['admin_menu_reports'] = 'تقارير';
	$language['admin_menu_spy'] = 'جاسوس';
	$language['admin_menu_admin_chat'] = 'مشرف الدردشة';
	$language['admin_menu_spy_chat'] = 'جاسوس الدردشة';
	$language['admin_not_allowed'] = ' غير مسموح لهذا الخيار.';
	$language['admin_add_adminNews'] = 'مشرف الإعلانات';
	$language['admin_news'] = 'أخبار';
	$language['admin_news_details'] = 'تفاصيل';
	$language['admin_news_spy'] = 'يمكن عرض هذا الاعلان من قبل جواسيس؟';
	$language['edit_activate'] = 'تفعيل';
	$language['admin_bumber'] = 'حذف';
	$language['admin_bumber_reason'] = 'سبب الحذف';
	$language['change_topic'] = ' تغيير الموضوع';
	$language['unlocked'] = 'غير مقفلة';
	$language['locked'] = 'مقفل';
	$language['deleted'] = 'حذف';
	$language['deleted_allentries'] = ' لقد تم حذف كافة الإدخالات المرفقة مع هذا الموضوع.';
	$language['problem_with_topic'] = ' الموضوع الخاص بك خاطئ، يرجى تصحيح اخطاء الطابع وحاول مرة أخرى.';
	$language['topic_moved'] = ' انتقل الموضوع.';
	$language['topic_where'] = ' سيتم نقل الموضوع';
	$language['listenUp'] = 'استمع';
	$language['mail_sended'] = 'إرسال البريد';
	$language['mail_everyone'] = 'إلى الجميع';
	$language['mail_admin'] = 'إلى المشرف';
	$language['mail_mods'] = 'إلى المشرفين';
	$language['mail_authors'] = 'للكتاب';
	$language['mail_readers'] = 'إلى القراء';
	$language['button_listenUp'] = 'استمع';
	$language['button_bringAll'] = 'جلب كافة الإدخالات';
	$language['latest_50entry'] = ' آخر50 إدخال';
	$language['text'] = 'نص';
	$language['back_authorsroom'] = 'العودة إلى غرفة موافقة الكاتب';
	$language['entry_deleted'] = 'تم حذف الإدخال';
	$language['add_new_news'] = 'أضف أخبار';
	$language['ip_blocked'] = 'IP غير مسموح';
	$language['ip_add'] = 'IP المحظورة';
	$language['wrong_spy'] = ' رفض طلب الجاسوس يمكنهم إدخال.';
	$language['wrong_spy_subject'] = 'مرقمة إدخال أنت تجسست';
	$language['wrong_spy_message'] = 'إدخال عدد اعتبار الجاسوس خاطئ';
	$language['button_wrong_spy'] = 'الجاسوس خاطئ';
	$language['button_true_spy'] = 'الموافقة على وحذف';
	$language['spy_reason'] = 'السبب التجسس';
	$language['delete_user_completely'] = 'تطهير المستخدم وجميع الإدخالات';
	$language['access_level'] = 'مستوى الوصول';
	$language['suspended'] = 'علقت';
	$language['button_edit_user'] = 'تعديل المستخدم';
	$language['status'] = 'الوضع';
	$language['message_delete_user_entries'] = ' حذف إدخالات المستخدم بصمت.';
	$language['message_delete_user_comments'] = ' حذف إدخالات المستخدم مع تعليق.';
	$language['message_delete_user_votes'] = ' حذف صوت المستخدم.';
	$language['message_delete_user'] = ' حذف المستخدم.';
	$language['message_delete_user_sure'] = ' سيتم حذف الإدخالات، هل أنت متأكد؟';
	$language['message_you_are_accepted'] =' وقد قبلت منكMafiMushkila. نأمل أن تستمتع وقتك تتجول ونحن نتطلع إلى قراءة المداخل الخاصة بك. مرحبا بكم على متن.';
	$language['message_accepted_header'] = ' وقد قبلت لكMafiMushkila';
	$language['message_you_are_accepted_mail'] = ' وقد قبلت منكMafiMushkila ككاتب. نأمل أن تستمتع وقتك تتجول ونحن نتطلع إلى قراءة المداخل الخاصة بك. مرحبا بكم على متن.';
	$language['message_waitingApproval'] = 'الكتاب في انتظار موافقة';
	$language['button_acceptToDictionary'] = ' تقبل فيMafiMushkila';
	$language['reactivated'] = ' تنشيط.';
	$language['process'] = 'العملية';
	$language['activate'] = 'تفعيل';
	$language['deleted_entries'] = 'حذف الإدخالات';
	$language['latest_50_private_messages'] = 'آخر 50 رسالة خاصة';
	$language['report_declined'] = 'رفض التقرير';
	$language['button_report_declined'] = 'رفض التقرير';
	$language['button_reporter'] = 'صحافي';
	$language['open'] = 'افتح';
	$language['closed'] = 'أغلق';
	$language['construction'] = 'تحت الانشاء';
	$language['registration_mode'] = 'طريقة التسجيل';
	$language['registration_mode_mail'] = 'تمت الموافقة على البريد الإلكتروني الخاص بك';
	$language['registration_mode_Nomail'] = 'لم تتم الموافقة البريد الإلكتروني';
	$language['registration_tips'] = ' نحن ننصحك بشدة باختيار البريد الإلكتروني خيار الموافقة من أجل التسجيل لMafiMushkila. وسوف يتم إرسال كلمة السر الخاصة بك إلى البريد الإلكتروني الخاص بك بشكل منفصل. إذا كان هناك فارق التوقيت بين وقتك ووقت الخادم يرجى ذكر ذلك في هنا.';
	$language['time_zone'] = 'منطقة التوقيت';
	$language['notes'] = 'ملاحظات';
	$language['stats_update'] = 'تحديث الإحصائيات';
	$language['new_theme'] = 'إضافة موضوع جديد';
	$language['theme'] = 'موضوع';
	$language['theme_maker'] = 'صانع موضوع';
	$language['theme_updated'] = 'موضوع محدث';
	$language['theme_date_format'] = '( اليوم / الشهر / السنة)';
	$language['theme_add_tips'] = ' يجب أن يكون اسم موضوع الذي يحمل نفس الاسم مع الملف المغلق داخل / صور مجلد.
على سبيل المثال: عند إضافة غروب الشمس. موضوع المغلق للنظام، لديك لكتابة "غروب الشمس" كاسم موضوع.';

	//sonradan
	$language['Subject'] = 'موضوع';
	$language['total_theme'] = 'مجموع موضوع';
	$language['entry_by_time'] = 'مقالات من قبل وقت';
	$language['updated'] = 'محدث';
	$language['rememberpassword'] = 'تذكر كلمة السر';
	$language['karma'] = 'متوسط ​​النقاط التي تتخذ'; //Yazarın aldığı oyların sonucunda ortaya çıkan ortalama puanıdır
	$language['varma'] = 'متوسط ​​النقاط التي تعطي'; //Yazarın verdiği oyların sonucunda ortaya çıkan ortalama puanıdır
	$language['last_appreciated'] = 'المفضلة مداخل مشاركة';
	$language['ben_most_appreciated'] = 'المفضل لمعظم مقالات';
	$language['ben_most_disg'] = 'الأكثر كرها مقالات';
	$language['ben_last_disg'] = 'كره مشاركة مقالات';
	$language['ben_latest_votes'] = 'أحدث تصويت';
	$language['profil'] = 'الملف الشخصي';
	
	//davet
	$language['invitation_dictionary'] = ' دعوة إلىMafiMushkila';
	$language['refer_text1'] = ' لقد تلقيت دعوة للانضمام الىMafiMushkila.';
	$language['refer_text2'] = 'ويجري حاليا إعادة توجيهك إلى';
	$language['refer_error'] = 'رمز الدعوة خاطئ';
	$language['invitations_sent'] = 'تم ارسال الدعوات';
	$language['invitations_slogan'] = ' يمكنك نسخ هذا العنوان لدعوة أصدقائك إلى MafiMushkila!';
	$language['invitations_desc_comma'] = ' باستخدام الفواصل، يمكنك إنشاء قائمة البريد الخاصة. مثلا: mafi@mushkila.com، mushkila@mafi.com';
	$language['invitation_message'] = 
	'MafiMushkila هو مجتمع دولي على الانترنت حيث يمكنك بلوق، وتبادل ومناقشة والحصول على معلومات حول موضوعات أي تقريبا يمكن ان يخطر لك. حصل شيء يدور في ذهنك؟ MafiMushkila! فقط سجل من خلال الرابط أدناه والبدء في اكتشاف المعنى الحقيقي للحياة!';
	$language['invitation_message_footer'] = ' أراك هناك.';
	$language['invitation_own'] = 'انضم أعضاء من خلال دعوتك';
	$language['invitation_own_message'] = ' أنت لم ترسل أي دعوات حتى الآن.موافق؟';
	
	//profil
	$language['change_picture'] = 'تغيير الصورة';
	$language['upload_picture'] = 'رفع صور';
	$language['update_info'] = 'تحديث معلومات';
	$language['total'] = 'مجموع';
	$language['total_plus'] = ' مجموع +';
	$language['total_minus'] = ' المجموع-';
	$language['whoami'] = 'من أنا';
	
		//en son eklenenler
	$language['listenUp2'] = 'إعلان';
	$language['panelsettings'] = 'ضبط لوحة';
	$language['picture'] = 'صور';
	$language['edit2'] = 'تحرير';
	$language['kick_from_list'] = 'ركلة من قائمة';
	$language['listenUp3'] = 'إعلان';
	$language['announcement_platform'] = 'إعلان منصة';
	$language['city_cannot_empty'] = 'لا يمكن أن تكون فارغة';
	$language['namesurname_cannot_empty'] = 'يمكن أن الاسم واللقب لا يكون فارغا';
	$language['send_invitation'] = 'إرسال دعوة';
	$language['impossible'] = 'مستحيل';
	$language['save'] = 'إرسال';
	$language['closing'] = 'إغلاق';
	$language['omg'] = 'OMG!';
	$language['isneakyou'] = ' أراك!';
	$language['kicked'] = 'طرد';
	$language['deleteee'] = 'تطهير';
	$language['pwsentto'] = ' لقد تم إرسال كلمة السر الخاصة بك إلى العنوان البريد الإلكتروني الخاص بك.';
	$language['or2'] = 'أو';
	$language['aboutyou'] = 'حول';
	$language['minimalauthor'] = '11';
	$language['minimalauthor'] = ' حدث خطأ غير متوقع!';
	$language['search'] = 'البحث';
	$language['bring'] = 'إحضار';
	$language['bring_title'] = 'إحضار اللقب';
	$language['search_title'] = 'البحث عن لقب';
	$language['ukte_note'] = ' الملاحظاتiwish';
	$language['throwup'] = 'حلق بي الى القمر';
	$language['nonauthor_warning'] = ' عليك أن تكون كاتبا من أجل رؤية المحتوى.';
	$language['hello3'] = 'مرحبا؟';
	$language['newmessage'] = ' أرسلت لك رسالة.';
	$language['newmessage_1'] = ' يمكنك الوصول إلى الرسائل الخاصة بك من خلال العنوان التالي.';
	$language['newmessage_2'] = ' حصلت على الرسالة!';
	$language['punishment_message_alarm'] = ' لا يمكنك إرسال رسالة في حين تتم معاقبة لك!';
	$language['empty_place_alarm'] = 'ملء المساحات الفارغة';
	$language['added_3'] = 'يجب أولا أن تضاف';
	$language['added_4'] = 'إضافة إلى قائمة';
	$language['added_5'] = 'مغادرة من قائمة';
	$language['edit_comment3'] = 'تحرير تعليق';
	$language['delete_comment'] = 'حذف التعليق';
	$language['zirvetor_add2'] = 'إضافة نشاط';
	$language['zirvetor_wtf'] = ' لا تحسب!';
	$language['zirvetor_added'] = 'واضاف هذا النشاط';
	$language['zirvetor_return'] = 'عودة';
	$language['zirvetor_situation'] = 'وضع النشاط';
	$language['zirvetor_name'] = 'اسم النشاط';
	$language['zirvetor_locality'] = 'نشاط الموقع';
	$language['zirvetor_category_1'] = 'MafiMushkila نشاط رابطة';
	$language['zirvetor_category_2'] = ' اجتماع/ الدردشة';
	$language['zirvetor_category_3'] = ' حفلة موسيقية/ مهرجان';
	$language['zirvetor_category_4'] = 'رحلة';
	$language['zirvetor_category_5'] = 'لعبة';
	$language['zirvetor_category_6'] = ' موسيقى/ رقص';
	$language['zirvetor_category_7'] = 'مشاهدة';
	$language['zirvetor_category_8'] = 'عيد ميلاد';
	$language['zirvetor_category_9'] = 'رياضة';
	$language['zirvetor_category_10'] = 'تنزه';
	$language['zirvetor_category_11'] = 'رحلات السفاري الصحراوية';
	$language['zirvetor_category_12'] = ' مؤسسة خيرية/ دعم';
	$language['zirvetor_category_13'] = 'طعام';
	$language['zirvetor_category_14'] = 'آخر';
	$language['zirvetor_choose'] = 'اسمحوا لي ان اختار';
	$language['zirvetor_comma'] = 'استخدم فاصلة للتفريق';
	$language['comment_edited'] = 'تحرير التعليق';
	$language['renew_image'] = 'تجديد صورة';
	$language['announcement_comment'] = 'تعليق';
	$language['announcement_question'] = 'سؤال';
	$language['announcement_blood'] = '';
	$language['announcement_forsale'] = 'الشراء البيع';
	$language['announcement_type'] = 'النوع';
	$language['announcement_topic'] = 'موضوع';
	$language['announcement_news'] = 'أخبار';
	$language['announcement_delete_5'] = 'طريق خاطئ';
	$language['announcement_dict'] = 'MafiMushkila';
	$language['announcement_add'] = 'إضافة';
	$language['announcement_cevab'] = 'إجابة';
	$language['announcement_enter'] = 'أدخل';
	$language['announcement_all'] = 'كل الإعلانات';
	$language['youarekicked'] = 'مطرود';
	$language['go_back'] = 'العودة';
	$language['sitemap'] = 'خريطة الموقع';
	$language['archive'] = 'الأرشيف';
	$language['last_entries'] = 'آخر مقالات';
	$language['update_succsess'] = ' تحديث بنجاح!';
	$language['upload_photo_crop'] = 'إصلاح صورتك الشخصية';
	$language['upload_photo_re'] = 'CTRL + F5 لتحديث صورتك';
	$language['upload_photo_i_crop'] = 'iCrop';
	$language['upload_picture2'] = 'تحميل الصور';
	$language['upload_photo_cmon'] = 'إرسال صورة';
	$language['upload_photo'] = 'صور';
	$language['top_ask'] = 'اسأل';
	$language['youtube_code'] = 'youtube code';
	$language['youtube_code_tip'] = ' بعد كتابة رمز=';
	$language['image_url'] = ' صورةURL';
	$language['link_adress'] = 'link address';
	$language['show_up_entries'] = 'عرض الإدخالات';
	$language['rookie_status'] = 'وضع المبتدئ';
	$language['dict_operations'] = ' عملياتMafiMushkila';
	$language['Site'] = 'الموقع';
	$language['login_with_mail'] = 'الدخول مع البريد الالكتروني';
	$language['show_searchintitle'] = 'البحث على الانترنت الكتاب موضوع';
	$language['show_insiders'] = 'إظهار الكتاب على الانترنت';
	$language['show_profile_photo'] = 'إظهار الصورة';
	$language['show_inn'] = 'إظهار الكتاب على الانترنت';
	$language['show_howyoudoing'] = 'كيف تريد أن يرى؟';
	$language['show_colorful_statistic_box'] = 'إظهار مربع الإحصائية الملونة';
	$language['show_facebookandtwitter'] = 'إظهار الفيسبوك وتويتر';
	$language['update_statistic'] = 'تحديث الإحصائيات';
	$language['author_operations'] = 'كاتب عمليات';
	$language['dict_settings'] = 'MafiMushkila إعدادات';
	$language['mail_to_everyone'] = 'إرسال بريد إلى الجميع';
	$language['acceptance_room'] = 'غرفة كاتبا القبول';
	$language['ip_ban'] = 'حظر IP';
	$language['theme_panel'] = 'موضوع لوحة';
	$language['advertisement_panel'] = 'لوحة الإعلانات';
	$language['become_wiser_entries'] = 'تحسين المقالات';
	$language['entries_operations'] = 'الإدخال العمليات';
	$language['deleted_entries'] = 'حذف المقالات';
	$language['topic_operations'] = 'موضوع عمليات';
	$language['dead_topics'] = 'المواضيع الميت';
	$language['add_an_author'] = 'إضافة كاتب';
	$language['announcement_panel'] = 'لوحة الإعلان';
	$language['administrator_announcements'] = 'مشرف الإعلان';
	$language['complaints'] = 'والشكاوى';
	$language['entries_by_time'] = 'المقالات مرتبة حسب الوقت';
	$language['spieds'] = 'تلك التي تجسست';
	$language['admin_menu_add_desc'] = ' هذا الجزء يظهر الناشئين الذي كتب ما لا يقل عن إدخال واحد. ويجب أن يتم الموافقة على الكاتب من هنا.';
	$language['rookie_entries'] = 'الإدخالات';
	$language['cancel_rookie'] = 'رفض';
	$language['entries_added'] = 'إضافة إدخالات';
	$language['bring_most_hardworkers'] = 'استخراج أكثر الكتاب تعمل بجد';
	$language['add_new_theme'] = 'إضافة الموضوع جديد';
	$language['theme_control'] = 'مراقبة الموضوع';
	$language['select_as_maintheme'] = 'اختار هذا كموضوع رئيسي';
	$language['total_themes'] = 'إجمالي المواضيع';
	$language['maintheme'] = 'الموضوع الرئيسي';
	$language['deleted_entries_returnback'] = ' إضافة إدخال حذف لهذا الموضوع.';
	$language['regenerated'] = 'بعث';
	// admin sidebar'a sonradan eklenenler
	$language['badwords'] = 'كلمات سيئة';
	$language['forbidden_words'] = 'كلمة ممنوعة';
	$language['add_word'] = 'إضافة كلمة';	
	
	// yeni reklam paneli
	$language['add_new_advertisement'] = 'إضافة إعلان جديد';
	$language['advertisement_code_ifadsense'] = ' إدخال رمز إذا ادسينس لها.';
	$language['advertisement_Name'] = 'اسم إعلان';
	$language['photo_url'] = ' عنوان URL للصورة';
	$language['Destination_url'] = ' عنوان URL المقصود';
	$language['Advertisement_code'] = 'شفرة الإعلان';
	$language['Active_advertisements'] = 'الإعلانات النشطة';
	$language['TheDate'] = 'تاريخ';
	$language['Advertisement'] = 'الإعلانات';
	$language['DestinationUrl'] = ' عنوان URL المقصود';
	$language['Advertisement_source'] = 'الإعلانات المصدر';
	$language['Advertisement_admin'] = 'الإعلانات الادارية';
	$language['Active']= 'النشطة';
	
	//mafi son eklenenler
	$language['dictionaryMail_inv'] = 'MafiMushkila دعوة';
	$language['entries_betweensum'] = 'الإدخالات بين';
	$language['close_complaint'] = 'close complaint';
	$language['answered'] = 'إغلاق';
	$language['take_it_back'] = 'إعادته';
	$language['badwordwarning']= 'هناك كلمات محظورة داخل الإدخال';	
	$language['mailnick_together'] = 'please, insert nick and e-mail';
	$language['delete_reaseon'] = 'delete reason';
	$language['seen_byAdmin'] = 'seen by Admin';	
	$language['turndefaultheme'] = 'Main Theme';
	$language['issetasmaintheme'] = 'is set as main theme';
	$language['addthistitletodict'] = 'add this title to';
	$language['lastupdate'] = 'last update';
	
	//Photo upload
	$language['photo_extension_error'] = 'Permitted Extensions:';
	$language['photo_max_size'] = 'Max Photo File Size:';
	$language['photo_crop_error'] = 'Crop your photo!';
	$language['photo_error'] = 'Photo Upload Error!';
	$language['photo_go_profile'] = 'OK! Go to my profile';
	$language['photo_go_retry'] = 'Naaah! Let me try again';
	$language['contact_entry_about'] = 'About the entry: ';
	
	$language['position'] = 'position';
	$language['top'] = 'top';
	$language['right'] = 'right';
	$language['refresh'] = 'refresh';
	
	$language['noAnnouncementYet'] = 'There is no Announcement Yet';
	$language['Announcement_Register'] = 'Register';
	$language['msg'] = 'msg';	
	$language['AnnouncementAnswer_suretoDelete'] = 'Are you sure to delete answer?';
	$language['Announcement_suretoDelete'] = 'Are you sure?';
	
	$language['buddyList'] = 'buddies';
	$language['afterMe'] = 'after me';
?>