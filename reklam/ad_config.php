<?php
include "../language/tr.php";
include "../inc/baglan.php";
require_once('../inc/requestUtils.class.php'); 

$banner_table = "reklam_bannerlar";
$client_table = "reklam_musteriler"; 
$pos_table = "reklam_pozisyonlar"; 


// Admin �ifresi
$admin_password = "test";

// Resim klas�r�
$img_dir = "images/";

// ad_click.php nin bulundu�u yer
$redir = "$language[dictionaryUrl]/reklam/ad_click.php";

// Bir hata olu�mas� durumunda g�sterilecek banner
$default_banner = "$language[dictionaryUrl]/reklam/images/logo.jpg";
$default_url = "$language[dictionaryUrl]/reklam/";

// Webmaster maili:
$webmaster = "$language[dictionaryMail]";

// Hata olu�mas� durumunda y�nlendirilecek url:
$redir_site = "$language[dictionaryUrl]/reklam/";

// Bannerlar�n alt�nda ��kacak mesaj (bir�ey ��kmamas� i�in bo� b�rak�n)
$message = "";

/* ##########################################################
 * A�a��daki ayarlarla oynamay�n
 * ########################################################## */

function connect_to_db() {
	global $host, $user, $password, $database;
	global $baglan;
	if(!($mysql = mysqli_connect($host, $user, $password)))
		return 0;

	if(!($db	 = mysqli_select_db($baglan,$database)))
		return 0;

	return $mysql;
}

?>