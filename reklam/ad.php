<?php

require "ad_config.php";

function show_ad($group = "", $width = 0, $height = 0) {
	global $banner_table, $pos_table, $message, $redir;

	$db = connect_to_db();
	if($db == 0)
		return error_msg();
	// else

	// First query: check position
	$query = mysql_query("SELECT * FROM $pos_table WHERE title = '$group';");
	if(!$query)
		return error_msg();
	// else

	if(mysql_num_rows($query) == 0) {
		$pos["title"] = $group;
		$pos["position"] = -1;
		$create_row = 1;
	}
	else
		$pos = mysql_fetch_array($query);

	if($group == "")
		$sql_group = "";
	else
		$sql_group = "AND banner_group = '$group'";

	// Get total number of banners in the group
	$query = mysql_query("SELECT 1 FROM $banner_table WHERE status = 'ok' $sql_group;");
	if(!$query)
		return error_msg();
	// else
	$total_banners = mysql_num_rows($query);
	if($total_banners == 0)
		error_msg();

	$pos["position"]++;
	if($total_banners < ($pos["position"]+1))
		$pos["position"] = 0;

	// select the banner we want
	$query = mysql_query("SELECT id, image_url, alt_text, displays_life, expiration_displays, expiration_date, width, height, CURDATE() as c_date FROM $banner_table WHERE status = 'ok' $sql_group LIMIT ".$pos["position"].", 1;");
	if(!$query)
		return error_msg();
	// else
	$banner = mysql_fetch_array($query);
	$id = $banner["id"];

	$ad = "";
	if(isset($message) && ($message != ""))
		$ad .= "<TABLE BORDER=\"0\" CELLSPACING=\"0\" CELLPADDING=\"0\">\n<TR>\n<TD ALIGN=\"CENTER\" VALIGN=\"TOP\">";

	$ad .= "<A target=\"_blank\" HREF =\"$redir?id=$id\">";
	if($banner["alt_text"] != "")
		$ad .= stripslashes($banner["alt_text"]);
	else {
		$ad .="<IMG SRC=\"".$banner["image_url"]."\"";
		if($width != 0)
			$ad .=" WIDTH=\"$width\"";
		elseif(($width == 0) && ($banner["width"] != ""))
			$ad .=" WIDTH=\"".$banner["width"]."\"";

		if($height != 0)
			$ad .=" HEIGHT=\"$height\"";
		elseif(($height == 0) && ($banner["height"] != ""))
			$ad .=" HEIGHT=\"".$banner["height"]."\"";

		$ad .= " BORDER=\"0\">";
	}

	$ad .= "</A>";

	if(isset($message) && ($message != ""))
		$ad .= "</TD></TR>\n<TR>\n<TD ALIGN=\"CENTER\" VALIGN=\"TOP\">$message</TD></TR></TABLE>";

	if(($banner["c_date"] >= $banner["expiration_date"]) &&
						($banner["expiration_date"] != "") && ($banner["expiration_date"] != "0000-00-00")) {
		$query = mysql_query("UPDATE $banner_table SET status = 'expired' WHERE id = $id;");
	}

	$query = mysql_query("UPDATE $banner_table SET displays_life = displays_life + 1, displays_day = displays_day + 1 WHERE id = $id;");
	if(!$query)
		echo "Hen�z bir reklam yok. Dilerseniz buray� doldurabilirsiniz.";
	if(($banner["expiration_displays"] != 0) && ($banner["expiration_displays"] <= $banner["displays_life"]))
		$query = mysql_query("UPDATE $banner_table SET status = 'expired' WHERE id = $id;");

	// Update position table
	$query = mysql_query("REPLACE INTO $pos_table VALUES ('".$pos["title"]."', ".$pos["position"].");");
	if(!$query)
		echo "Hen�z bir reklam yok. Dilerseniz buray� doldurabilirsiniz.";

	return $ad;
}


function show_specific_ad($id, $count = 0, $width = 0, $height = 0) {
	global $banner_table, $message, $redir;

	$db = connect_to_db();
	if($db == 0)
		return error_msg();
	else {

		// First query: get image data
		$query = mysql_query("SELECT status, image_url, alt_text, displays_life, expiration_displays, expiration_date, width, height, CURDATE() as c_date FROM $banner_table WHERE id = $id;");

		if(!$query)
			return error_msg();
		else {
			$banner = mysql_fetch_array($query);

			// Check expired, if count > 0
			if(($banner["status"] == "expired") && ($count > 0))
				return "";

			// Check expiration date
			if(($count > 0) && ($banner["c_date"] <= $banner["expiration_date"]) &&
								($banner["expiration_date"] != "") && ($banner["expiration_date"] != "0000-00-00")) {
				$query = mysql_query("UPDATE $banner_table SET status = 'expired' WHERE id = $id;");
				return "";
			}

			$ad = "";
		if(isset($message) && ($message != ""))
			$ad .= "<TABLE BORDER=\"0\" CELLSPACING=\"0\" CELLPADDING=\"0\">\n<TR>\n<TD ALIGN=\"CENTER\" VALIGN=\"TOP\">";

			$ad .= "<A HREF=\"$redir?id=$id&count=$count\">";
			if($banner["alt_text"] != "")
				$ad .= stripslashes($banner["alt_text"]);
			else {
				$ad .="<IMG SRC=\"".$banner["image_url"]."\"";
				if($width != 0)
					$ad .=" WIDTH=\"$width\"";
				elseif(($width == 0) && ($banner["width"] != ""))
					$ad .=" WIDTH=\"".$banner["width"]."\"";

				if($height != 0)
					$ad .=" HEIGHT=\"$height\"";
				elseif(($height == 0) && ($banner["height"] != ""))
					$ad .=" HEIGHT=\"".$banner["height"]."\"";

				$ad .= " BORDER=\"0\">";
			}

			$ad .= "</A>";

		if(isset($message) && ($message != ""))
			$ad .= "</TD></TR>\n<TR>\n<TD ALIGN=\"CENTER\" VALIGN=\"TOP\">$message</TD></TR></TABLE>";

		// If $nocount > 0, displays life = displays life + 1
		if($count > 0) {
			$query = mysql_query("UPDATE $banner_table SET displays_life = displays_life + 1, displays_day = displays_day + 1 WHERE id = $id;");
			if(!$query)
				echo "Error in updating database, please contact the <A HREF=\"mailto:$webmaster\">webmaster</A> about this.";
			if(($banner["expiration_displays"] != 0) && ($banner["expiration_displays"] <= $banner["displays_life"]))
				$query = mysql_query("UPDATE $banner_table SET status = 'expired' WHERE id = $id;");
		}

		}
	}

	return $ad;
}

function error_msg() {
	global $message, $webmaster, $default_banner, $default_url;

	$aderror = "";
	if(isset($message) && ($message != ""))
		$aderror .= "<TABLE BORDER=\"0\" CELLSPACING=\"0\" CELLPADDING=\"0\">\n<TR>\n<TD ALIGN=\"CENTER\" VALIGN=\"TOP\">";
	$aderror .= "<A HREF=\"$default_url\"><IMG SRC=\"$default_banner\" BORDER=\"0\"></A>";
	if(isset($message) && ($message != ""))
		$aderror .= "</TD></TR>\n<TR>\n<TD ALIGN=\"CENTER\" VALIGN=\"TOP\">$message</TD></TR></TABLE>";

	return $aderror;
}

?>