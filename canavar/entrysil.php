<?
if ($_GET['verified_user'] or $_GET['verified_kat'] or $_POST['verified_user'] or $_POST['verified_kat']) 
{
	die();
}
if ($entry != 1) 
{
	echo "$language[admin_not_allowed]";
	die;
}

$id = RequestUtil::Get("id");
$sebep = RequestUtil::Get("sebep");
$sira = RequestUtil::Get("sira");
$yazar = RequestUtil::Get("yazar");

if ($id and $sebep and $yazar and $sira) 
{
	$aciklama = RequestUtil::Get("aciklama");

	$sorgu1 = "SELECT baslik,id FROM konular WHERE `id` = '$sira'";
	$sorgu2 = mysqli_query($baglan,$sorgu1);
	$kayit2=mysqli_fetch_array($sorgu2);
	$baslik=$kayit2["baslik"];

	$sorgu1 = "SELECT mesaj,id FROM mesajlar WHERE `sira` = '$sira'";
	$sorgu2 = mysqli_query($baglan,$sorgu1);
	$kayit2=mysqli_fetch_array($sorgu2);
	$mesaj=$kayit2["mesaj"];

	$tarih = date("YmdHi");
	$gun = date("d");
	$ay = date("m");
	$yil = date("Y");
	$saat = date("H:i");

	$sebep = strtolower($sebep);
	$olay = "$language[entry_deleted]";
	$mesaj = "$baslik -> <i>$mesaj</i> <b>[$yazar]</b> $language[delete_reason]: <b>$sebep</b>";

	$sorgu = "INSERT INTO `history` ";
	$sorgu .= "(`olay` , `mesaj` , `mod` , `tarih` , `gun` , `ay` , `yil` , `saat`)";
	$sorgu .= " VALUES ";
	$sorgu .= "('$olay','$mesaj','$verified_user','$tarih','$gun','$ay','$yil','$saat')";
	mysqli_query($baglan,$sorgu);

	$sorgu = "UPDATE mesajlar SET `statu` = 'silindi' WHERE id='$id'";
	mysqli_query($baglan,$sorgu);
	echo "$id $language[message_deleted]";
}
else if ($id) 
{
	$sorgu = "SELECT id,mesaj,yazar,sira FROM mesajlar WHERE id = '$id'";
	$sorgulama = mysqli_query($baglan,$sorgu);
	
	if (mysqli_num_rows($sorgulama)>0)
	{
		while ($kayit=mysqli_fetch_array($sorgulama))
		{
			$id=$kayit["id"];
			$sira=$kayit["sira"];
			$mesaj=$kayit["mesaj"];
			$yazar=$kayit["yazar"];
			$mesaj = substr($mesaj, 0, 60);
			echo "
			      <FORM name=mesform method=post action=>
			      <TABLE class=dash cellSpacing=0 cellPadding=3 width=\"100%\" align=center
			      border=0>
			        <TBODY>
			        <TR>
			          <TD height=\"18\">&nbsp;</TD>
			          <TD>$mesaj... ($language[author]: $yazar)</TD>
			        </TR>
			        <TR>
			          <TD width=\"11%\" height=\"18\"><p>$language[delete_reason]</p>
			            </TD>
			          <TD width=\"89%\"><INPUT class=inp maxLength=50 size=35 name=sebep>
			          </TD>
			            <input type=hidden name=yazar value=$yazar>
			            <input type=hidden name=sira value=$sira>
			            <input type=hidden name=id value=$id>
			        </TR>
			          <TD>&nbsp;</TD>
			          <TD><INPUT type=hidden value=gonder name=gonder> <INPUT class=buton id=kaydet type=submit value=$language[delete] name=kaydet>
			          </TD></TR></TBODY></TABLE>
			      </FORM>
			";
		}
	}
}
?>