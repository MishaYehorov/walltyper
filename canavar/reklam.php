<?
if (!$_GET['verified_user'] && $verified_kat != "admin") die();

$submit = RequestUtil::Get('submit');

if ($submit)
{
	$id = RequestUtil::Get('ID');
	$name = RequestUtil::Get('name');
	$fotourl = Control($_POST['fotourl']);
	$targeturl = Control($_POST['targeturl']);
	$position = RequestUtil::Get('cmbPosition');
	$source = $_POST['source'];
	$source = addslashes($source);

	if (!$id)
	{
		$sorgu = "
		INSERT INTO reklamlar 
		(name,target_url,image_url,reklam_source,isactive,position)
		VALUES 
		('$name','$targeturl','$fotourl','$source','1','$position')";
	
	}
	else
	{
		$sorgu = "
		UPDATE reklamlar 
		SET 
			name = '$name',
			target_url = '$targeturl',
			image_url = '$fotourl',
			reklam_source = '$source',
			position = '$position'
		WHERE 
			id = $id";	
			
	}

	mysqli_query($baglan,$sorgu) or die(mysql_error($baglan).$sorgu);;
}

function Control($veri)
{
	$veri =str_replace("`","",$veri);
	$veri =str_replace("'or","",$veri);
	$veri =str_replace("!","",$veri);
	$veri =str_replace("SELECT","",$veri);
	$veri =str_replace("UNION","",$veri);
	$veri =str_replace("union","",$veri);
	$veri =str_replace("select","",$veri);
	$veri =str_replace("SeLeCt","",$veri);
	$veri =str_replace("sElEct","",$veri);
	$veri =str_replace("<?","",$veri);
	$veri =str_replace("?>","",$veri);
	$veri =str_replace("*","",$veri);
	$veri =str_replace("<%","",$veri);
	$veri =str_replace("%>","",$veri);

	return $veri;
}

function removeLinebreaks($string) {
	$string	=	str_replace('<br>','<br/>',nl2br($string));			//	convert linebreaks to valid BR tags (textarea issue)
	$string	=	preg_replace("/[\r|\n|\r\n]+/",'<br/>',$string);		//	convert successive linebreaks to BR tags
	$string	=	preg_replace("/(<br\s*\/?>\s*)+/","<br/>",$string);		//	convert successive BR tags to single BR tag.
	$string = 	preg_replace('/<br\s*?\/?>\s*$/', '',$string);			//	convert trailing BR tags to empty string.
	$string	=	trim($string);							//	trim this string.
	return $string;
}

if (isset($_POST['delete']))
{
	$ID = $_POST['ID'];
	
	$sorgu = "UPDATE reklamlar SET isactive = 0 WHERE id = $ID";

	mysqli_query($baglan,$sorgu) or die(mysql_error($baglan).$sql);
}

if (isset($_POST['activate']))
{
	$ID = $_POST['ID'];
	
	$sorgu = "UPDATE reklamlar SET isactive = 1 WHERE id = $ID";

	mysqli_query($baglan,$sorgu) or die(mysql_error($baglan).$sql);
}

if (isset($_POST['update']))
{
	$ID = $_POST['ID'];
	
	$sql = "SELECT name, target_url,image_url,reklam_source,isactive,position FROM reklamlar WHERE id = $ID";					
	$data =@ mysqli_fetch_array(mysqli_query($baglan,$sql));
	
	$reklamname = $data['name'];
	$target_url = $data['target_url'];
	$image_url = $data['image_url'];
	$reklam_source = $data['reklam_source'];
	$selectedposition = $data['position'];
	
	if ($selectedposition == "right")
		$rigtposition = "selected";
		
	if ($selectedposition == "top")
		$topposition = "selected";
}

?>

<form method="post">
<h2><?=$language[advertisement_panel]?></h2>

<b><?=$language[add_new_advertisement]?></b>
<br><br>
<small><?=$language[advertisement_code_ifadsense]?></small>
<table>
	<input name="ID" type="hidden" value="<?=$ID?>">
	<tr>
		<td width="100"><?=$language['position']?></td>
		<td width="10">:</td>
		<td width="350">
			<select name="cmbPosition" id="cmbPosition">
			  <option value="top" <?=$topposition?>><?=$language['top']?></option>
			  <option value="right" <?=$rigtposition?>><?=$language['right']?></option>
			</select>
		</td>
	</tr>
	<tr>
		<td width="100"><?=$language[advertisement_Name]?></td>
		<td width="10">:</td>
		<td width="350">
			<input name="name" size="60px" type="text" id=" " value="<?=$reklamname?>">
		</td>
	</tr>
	<tr>
		<td width="100"><?=$language[photo_url]?></td>
		<td width="10">:</td>
		<td width="350">
			<input name="fotourl" size="60px" type="text" id="rer/tr" value="<?=$image_url?>">
		</td>
	</tr>
	<tr>
		<td width="100"><?=$language[Destination_url]?></td>
		<td width="10">:</td>
		<td width="350">
			<input name="targeturl" size="60px" type="text" id="targeturl" value="<?=$target_url?>">
		</td>
	</tr>
	<tr>
		<td><?=$language[Advertisement_code]?></td>
		<td>:</td>
		<td>
			<textarea rows="6" name="source" id="source"><?=$reklam_source?></textarea>
		</td>
	</tr>
	<tr>
		<td></td>
		<td></td>
		<td>
			<input type="submit" onClick="return control();" name="submit" value="Ekle" style="width:120px; float: right">
		</td>
	</tr>

</form>
<h2><?=$language[Active_advertisements]?></h2>
<table width="100%" class="highlight">
  <tr style="background-color:#CCCCCC;">
    <td width="106"><strong><?=$language[TheDate]?></strong></td>
    <td width="120"><strong><?=$language[Advertisement]?> </strong></td>
    <td width="134"><strong><?=$language[DestinationUrl]?></strong></td>
    <td width="134"><strong><?=$language[photo_url]?></strong></td>
    <td width="150"><strong>pozisyon</strong></td>
    <td width="150"><strong><?=$language[Active]?></strong></td>
    <td width="134"><strong><?=$language[Advertisement_admin]?></strong></td>
  </tr>
<?
$sorgu = "SELECT * FROM reklamlar ORDER by `date` DESC";
$sorgulama = mysqli_query($baglan,$sorgu);

if (@mysqli_num_rows($sorgulama) > 0)
{
	while ($kayit=@mysqli_fetch_array($sorgulama))
	{
		$ID = $kayit["id"];
		$name = $kayit["name"];
		$target_url = $kayit["target_url"];
		$image_url = $kayit["image_url"];
		$reklam_source = $kayit["reklam_source"];
		$isactive = $kayit["isactive"];
		$date = $kayit["date"];
		$position = $kayit["position"];

		echo "
		<form id=\"buttonForm\" method=\"post\">
		<input name=\"ID\" type=\"hidden\" value=\"$ID\">
		
		<tr>
			<td>".dateTime($date)."</td>
			<td>$name</td>
			<td>$target_url</td>
			<td>$image_url</td>
			<td>$position</td>
			<td>$isactive</td>
			<td>";
			
		if ($isactive == 0)
			echo "<input name=\"activate\" type=\"submit\" value=\"$language[activate]\">";
		else
			echo "<input name=\"delete\" type=\"submit\" value=\"$language[delete]\">";
		
		echo "<input name=\"update\" type=\"submit\" value=\"...\";>
				</td>
			</tr>
		</form>";
	}
}

?>
</table>

<script type="text/javascript">
function control()
{
	if (document.getElementById("name").value == "")
	{
		alert("reklam ismi?");
		document.getElementById("name").style.backgroundColor="#ffcccc";
		document.getElementById("name").focus();
		return false;
	}
	
	if (document.getElementById("fotourl").value == "" && document.getElementById("source").value == "")
	{
		alert("foto url ya da reklam kodu girmelisiniz");
		return false;
	}
}
</script>
