<?
if ($_GET['verified_user'] or $_GET['verified_kat'] or $_POST['verified_user'] or $_POST['verified_kat']) 
	die();
	
if ($sozluk != 1) 
	die;

if ($verified_kat!="admin" and $verified_kat!="mod") 
	die();
	
$site = RequestUtil::Get("site");
$entries = RequestUtil::Get("entries");
$ok = RequestUtil::Get("ok");
$reg = RequestUtil::Get("reg");
$saat = RequestUtil::Get("saat");
$defaultentry = RequestUtil::Get("defaultentry");
$chkrookie = RequestUtil::Get("chkrookie");
$chkbaslikici = RequestUtil::Get("chkbaslikici");
$chkMail = RequestUtil::Get("chkMail");
$chkkazandakiler = RequestUtil::Get("chkkazandakiler");
$chkicerisi = RequestUtil::Get("chkicerisi");
$chknasilsin = RequestUtil::Get("chknasilsin");
$chkistatistikkutusu = RequestUtil::Get("chkistatistikkutusu");
$chkprofilresmi = RequestUtil::Get("chkprofilresmi");
$chkfacetwit = RequestUtil::Get("chkfacetwit");

if ($ok and $site and $reg and $entries) 
{	
    if ($chkrookie == "on") $chkrookie = 1; else $chkrookie = 0;
	if ($chkMail == "on") $chkMail = 1; else $chkMail = 0;
	if ($chkbaslikici == "on") $chkbaslikici = 1; else $chkbaslikici = 0;
	if ($chkkazandakiler == "on") $chkkazandakiler = 1; else $chkkazandakiler = 0;
	if ($chkprofilresmi == "on") $chkprofilresmi = 1; else $chkprofilresmi = 0;
	if ($chkicerisi == "on") $chkicerisi = 1; else $chkicerisi = 0;
	if ($chknasilsin == "on") $chknasilsin = 1; else $chknasilsin = 0;
	if ($chkistatistikkutusu == "on") $chkistatistikkutusu = 1; else $chkistatistikkutusu = 0;
	if ($chkfacetwit == "on") $chkfacetwit = 1; else $chkfacetwit = 0;	
	
	if(is_numeric($saat)) 
	{
		$sorgu = "UPDATE ayar SET site = '$site' , reg='$reg' , entries='$entries' ,saatFarki='$saat', rookie=$chkrookie,facetwit=$chkfacetwit, profilresmi=$chkprofilresmi, 
				 istatistikkutusu=$chkistatistikkutusu, nasilsin=$chknasilsin, icerisi=$chkicerisi, kazandakiler=$chkkazandakiler, baslikiciara=$chkbaslikici, 
				 maillogin=$chkMail,defaultentry='$defaultentry' WHERE id='1'";
				 
		mysqli_query($baglan,$sorgu) or die(mysqli_error($baglan).$sorgu);

		echo "$language[message_succeed]";
	}  
	else 
		echo "$language[message_succeed]";	
}

else 
{
	$sq = "SELECT * FROM ayar WHERE id='1'";
	$d = mysqli_query($baglan,$sq);
	$st = mysqli_fetch_assoc($d);

	$saat = $st['saatFarki'];
	$defaultentry = $st['defaultentry'];
	$site = $st['site'];
	$entries = $st['entries'];
	$reg = $st['reg'];
	$seo = $st['seo'];
	$trChar = $st['turkce_karakter'];
	$icerisiChar = $st['icerisi'];
	$nasilsinChar = $st['nasilsin'];
	$istatistikkutusuChar = $st['istatistikkutusu'];
	$baslikChar = $st['baslikiciara'];
	$profilresmiChar = $st['profilresmi'];
	$facetwitChar = $st['facetwit'];
	$maillogin = $st['maillogin'];
	$rookieChar = $st['rookie'];
	$anket = $st['anket'];
	
	if ($reg == "on")
		$reg_on = "selected";
	else if ($reg == "off")
		$reg_off = "selected";
	else if ($reg == "tech")
		$reg_tech = "selected";
		
	if ($site == "on")
		$site_on = "selected";
	else
		$site_off = "selected";
		
	if ($entries == "all")
		$entries_all = "selected";
	else
		$entries_today = "selected";	
		
	if ($maillogin == 1)
	{
		$mailSelected =  "checked=\"yes\"";
	}
	
	if ($rookie == 1)
	{
		$rookieSelected =  "checked=\"yes\"";
	}
	
	if ($baslikChar == 1)
	{
		$baslikiciSelected = "checked=\"yes\"";
		$baslikChar = "true";
	}
	else
	{
		$baslikChar = "false";
		$baslikiciSelected = "";
	}
	
		if ($kazandakiler == 1)
	{
		$kazandakilerSelected =  "checked=\"yes\"";
	}
		if ($profilresmi == 1)
	{
		$profilresmiSelected =  "checked=\"yes\"";
	}		
		if ($icerisi == 1)
	{
		$icerisiSelected =  "checked=\"yes\"";
	}
		if ($istatistikkutusu == 1)
	{
		$istatistikkutusuSelected =  "checked=\"yes\"";
	}		
		if ($nasilsin == 1)
	{
		$nasilsinSelected =  "checked=\"yes\"";
	}		
	if ($facetwit == 1)
	{
		$facetwitSelected =  "checked=\"yes\"";
	}
	
?>

<form method="post"> <h2><?=$language[dict_operations]?></h2>
<table width="100%" border="0">
  <tr>
    <td width="135"><?=$language[Site]?></td>
    <td width="8">:</td>
    <td width="263">
		<select name="site" id="site">
		  <option value="on" <?= $site_on?>><? echo $language[open]; ?></option>
		  <option value="off" <?= $site_off?>><? echo $language[closed]; ?></option>
		  <option value="tech" <?= $site_tech?>><? echo $language[construction]; ?></option>
		</select>
	</td>
  </tr>
  
  <tr>
	<td><? echo $language[registration_mode]; ?> </td>
		<td>:</td>
		<td>
			<select name="reg" id="reg">
				<option value="on" <?= $reg_on?>><? echo $language[registration_mode_mail]; ?> </option>
				<option value="off" <?= $reg_off?>><? echo $language[registration_mode_Nomail]; ?> </option>
			</select>
		</td>
	</tr>
	
		<td><? echo $language[show_up_entries]; ?> </td>
		<td>:</td>
		<td>
			<select name="entries" id="entries">
				<option value="all" <?= $entries_all?>><? echo $language[all]; ?> </option>
				<option value="today" <?= $entries_today?>><? echo $language[today]; ?> </option>
			</select>
		</td>
	</tr>
  
	<tr>
	    <td><? echo $language[time_zone]; ?></td>
	    <td>:</td>
	    <td><input type=text name=saat size="2" value="<?=$saat;?>"></td>
	</tr>
	
	<tr>
	    <td><?=$language[login_with_mail]?></td>
	    <td>:</td>
	    <td><input id="chkMail" name="chkMail" type="checkbox" <?=$mailSelected;?>></td>
	</tr>
	
	<tr>
	    <td><? echo $language[rookie_status]; ?></td>
	    <td>:</td>
	    <td><input id="chkrookie" name="chkrookie" type="checkbox" <?=$rookieSelected;?>></td>
	</tr>
	
	<tr>
	    <td><? echo $language[defaultentry_id]; ?></td>
	    <td>:</td>
	    <td>#<input type="text" name="defaultentry" size="10" value="<?=$defaultentry;?>"><small>empty box gets random entry</small></td>
	</tr>
	
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td><input type="submit" name="Submit" value="<? echo $language[OK]; ?>"></td>
		<input type="hidden" name="ok" value="ok">
	</tr>
	
<hr>

</table>

<table width="46%" border="0">
<br><br>
<h2><?=$language[panelsettings]?></h2> 
<hr>
	<tr>
	    <td><?=$language[show_searchintitle]?></td>
	    <td>:</td>
	    <td><input id="chkbaslikici" name="chkbaslikici" type="checkbox" <?=$baslikiciSelected;?>></td>
	</tr>	
	<tr>
	    <td><?=$language[show_insiders]?></td>
	    <td>:</td>
	    <td><input id="chkkazandakiler" name="chkkazandakiler" type="checkbox" <?=$kazandakilerSelected;?>></td>
	</tr>	
	<tr>
	    <td><?=$language[show_profile_photo]?></td>
	    <td>:</td>
	    <td><input id="chkprofilresmi" name="chkprofilresmi" type="checkbox" <?=$profilresmiSelected;?>></td>
	</tr>	
	<tr>
	    <td><?=$language[show_inn]?></td>
	    <td>:</td>
	    <td><input id="chkicerisi" name="chkicerisi" type="checkbox" <?=$icerisiSelected;?>></td>
	</tr>	
	<tr>
	    <td><?=$language[show_howyoudoing]?></td>
	    <td>:</td>
	    <td><input id="chknasilsin" name="chknasilsin" type="checkbox" <?=$nasilsinSelected;?>></td>
	</tr>	
	<tr>
	    <td><?=$language[show_colorful_statistic_box]?></td>
	    <td>:</td>
	    <td><input id="chkistatistikkutusu" name="chkistatistikkutusu" type="checkbox" <?=$istatistikkutusuSelected;?>></td>
	</tr>	
	<tr>
	    <td><?=$language[show_facebookandtwitter]?></td>
	    <td>:</td>
	    <td><input id="chkfacetwit" name="chkfacetwit" type="checkbox" <?=$facetwitSelected;?>></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td><input type="submit" name="Submit" value="<? echo $language[OK]; ?>"></td>
		<input type="hidden" name="ok" value="ok">
	</tr>
	
</table> 
<br><br>
<fieldset><legend><b><? echo $language[notes]; ?></b></legend>

<? echo $language[registration_tips]; ?>
</fieldset>
</form>
	


<? } ?>