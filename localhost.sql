-- phpMyAdmin SQL Dump
-- version 3.5.8.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 14, 2015 at 08:17 PM
-- Server version: 5.5.42-37.1-log
-- PHP Version: 5.4.23

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `walltype_baris`
--
CREATE DATABASE `walltype_baris` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `walltype_baris`;

-- --------------------------------------------------------

--
-- Table structure for table `ahaberler`
--

DROP TABLE IF EXISTS `ahaberler`;
CREATE TABLE IF NOT EXISTS `ahaberler` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `konu` text COLLATE utf8_bin NOT NULL,
  `aciklama` text COLLATE utf8_bin NOT NULL,
  `tarih` text COLLATE utf8_bin NOT NULL,
  `gun` text COLLATE utf8_bin NOT NULL,
  `ay` text COLLATE utf8_bin NOT NULL,
  `yil` text COLLATE utf8_bin NOT NULL,
  `saat` text COLLATE utf8_bin NOT NULL,
  `yazar` text COLLATE utf8_bin NOT NULL,
  `sovalyeduyuru` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `praetorduyuru` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `id_2` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=6 ;

--
-- Dumping data for table `ahaberler`
--

INSERT INTO `ahaberler` (`id`, `konu`, `aciklama`, `tarih`, `gun`, `ay`, `yil`, `saat`, `yazar`, `sovalyeduyuru`, `praetorduyuru`) VALUES
(2, 'hoooop', 'naber', '201106141556', '14', '06', '2011', '15:56', 'admin', '', ''),
(3, 'ddddddd', 'ddda\r<br>dsds\r<br>sd\r<br>sd\r<br>sd\r<br>ds\r<br>sd\r<br>sd\r<br>', '201106141558', '14', '06', '2011', '15:58', 'admin', '', ''),
(4, 'dddddddddddddddddddddd', 'dddddda\r<br>sda\r<br>sda\r<br>sda', '201106141605', '14', '06', '2011', '16:05', 'admin', '', ''),
(5, 'أيك" و', 'أيك" و', '201201301333', '30', '01', '2012', '13:33', 'admin', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `ayar`
--

DROP TABLE IF EXISTS `ayar`;
CREATE TABLE IF NOT EXISTS `ayar` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `site` varchar(20) COLLATE utf8_bin NOT NULL,
  `reg` varchar(10) COLLATE utf8_bin NOT NULL,
  `hit` varchar(15) COLLATE utf8_bin NOT NULL,
  `saatFarki` tinyint(4) NOT NULL DEFAULT '0',
  `dil` varchar(5) COLLATE utf8_bin NOT NULL,
  `anatema` varchar(50) COLLATE utf8_bin NOT NULL DEFAULT '',
  `seo` tinyint(1) NOT NULL DEFAULT '0',
  `turkce_karakter` tinyint(1) NOT NULL DEFAULT '1',
  `maillogin` tinyint(4) NOT NULL,
  `facetwit` int(4) DEFAULT NULL,
  `nasilsin` int(4) DEFAULT NULL,
  `icerisi` int(4) DEFAULT NULL,
  `kazandakiler` int(4) DEFAULT NULL,
  `baslikiciara` int(4) DEFAULT NULL,
  `seendeletedmeessages` int(4) DEFAULT NULL,
  `istatistikkutusu` int(4) DEFAULT NULL,
  `profilresmi` int(4) DEFAULT NULL,
  `entries` varchar(5) COLLATE utf8_bin NOT NULL,
  `rookie` int(2) DEFAULT NULL,
  `defaultentry` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=2 ;

--
-- Dumping data for table `ayar`
--

INSERT INTO `ayar` (`id`, `site`, `reg`, `hit`, `saatFarki`, `dil`, `anatema`, `seo`, `turkce_karakter`, `maillogin`, `facetwit`, `nasilsin`, `icerisi`, `kazandakiler`, `baslikiciara`, `seendeletedmeessages`, `istatistikkutusu`, `profilresmi`, `entries`, `rookie`, `defaultentry`) VALUES
(1, 'on', 'off', '352', 0, 'en', 'new', 0, 1, 0, 1, 1, 1, 1, 1, NULL, 1, 1, 'all', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `badwords`
--

DROP TABLE IF EXISTS `badwords`;
CREATE TABLE IF NOT EXISTS `badwords` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `badword` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `badwords`
--

INSERT INTO `badwords` (`id`, `badword`) VALUES
(1, 'fuck'),
(2, 'shit');

-- --------------------------------------------------------

--
-- Table structure for table `duyuru`
--

DROP TABLE IF EXISTS `duyuru`;
CREATE TABLE IF NOT EXISTS `duyuru` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `content` text CHARACTER SET utf8 NOT NULL,
  `date` datetime NOT NULL,
  `subject` varchar(200) CHARACTER SET utf8 NOT NULL,
  `type` varchar(100) CHARACTER SET utf8 NOT NULL,
  `user` varchar(200) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=7 ;

--
-- Dumping data for table `duyuru`
--

INSERT INTO `duyuru` (`id`, `user_id`, `content`, `date`, `subject`, `type`, `user`) VALUES
(6, 4, '(sref: gh)', '2012-03-26 20:48:00', 'gh', '2', 'admin'),
(4, 4, 'غرق ساعة كاملة على موق <br> غرق ساعة كاملة على موق <br> غرق ساعة كاملة على موق <br> غرق ساعة كاملة على موق <br> غرق ساعة كاملة على موق <br> غرق ساعة كاملة على موق <br> ', '2012-01-31 10:36:00', 'غرق ساعة كاملة على موق', '2', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `duyuru_cevap`
--

DROP TABLE IF EXISTS `duyuru_cevap`;
CREATE TABLE IF NOT EXISTS `duyuru_cevap` (
  `cevap_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `soru_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `cevap` text CHARACTER SET utf8 NOT NULL,
  `date` datetime NOT NULL,
  `user` varchar(200) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`cevap_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=10 ;

--
-- Dumping data for table `duyuru_cevap`
--

INSERT INTO `duyuru_cevap` (`cevap_id`, `soru_id`, `user_id`, `cevap`, `date`, `user`) VALUES
(7, 4, 4, 'ة كاملة   ة كاملة   ة كاملة   ة كاملة   كاملة <br> كاملة <br> كاملة <br> كاملة <br> كاملة <br> ', '2012-01-31 10:43:00', 'admin'),
(8, 4, 4, 'كاملةكاملة <br> كاملة <br> كاملة <br> كاملة <br> كاملة <br> ', '2012-01-31 10:44:00', 'admin'),
(9, 4, 4, 'على مو <br> على مو <br> على مو <br> على مو <br> على مو <br> ', '2012-01-31 10:45:00', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `eniyiler`
--

DROP TABLE IF EXISTS `eniyiler`;
CREATE TABLE IF NOT EXISTS `eniyiler` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `eid` text COLLATE utf8_bin NOT NULL,
  `oysayisi` text COLLATE utf8_bin NOT NULL,
  `yazar` text COLLATE utf8_bin NOT NULL,
  `baslik` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `id_2` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `haberler`
--

DROP TABLE IF EXISTS `haberler`;
CREATE TABLE IF NOT EXISTS `haberler` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `konu` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `aciklama` text COLLATE utf8_bin NOT NULL,
  `tarih` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `gun` int(11) NOT NULL DEFAULT '0',
  `ay` int(11) NOT NULL DEFAULT '0',
  `yil` int(11) NOT NULL DEFAULT '0',
  `saat` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `yazar` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `id_2` (`id`),
  KEY `saat` (`saat`),
  KEY `gun` (`gun`),
  KEY `ay` (`ay`),
  KEY `yil` (`yil`),
  KEY `konu` (`konu`),
  KEY `yazar` (`yazar`),
  KEY `tarih` (`tarih`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `history`
--

DROP TABLE IF EXISTS `history`;
CREATE TABLE IF NOT EXISTS `history` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `olay` text COLLATE utf8_bin NOT NULL,
  `mesaj` text COLLATE utf8_bin NOT NULL,
  `moderat` text COLLATE utf8_bin NOT NULL,
  `tarih` text COLLATE utf8_bin NOT NULL,
  `gun` text COLLATE utf8_bin NOT NULL,
  `ay` text COLLATE utf8_bin NOT NULL,
  `yil` text COLLATE utf8_bin NOT NULL,
  `saat` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `hmtstat`
--

DROP TABLE IF EXISTS `hmtstat`;
CREATE TABLE IF NOT EXISTS `hmtstat` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(250) COLLATE utf8_bin NOT NULL DEFAULT '',
  `degree` varchar(250) COLLATE utf8_bin NOT NULL DEFAULT '',
  `value` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `iletisim_sikayet`
--

DROP TABLE IF EXISTS `iletisim_sikayet`;
CREATE TABLE IF NOT EXISTS `iletisim_sikayet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_bin NOT NULL,
  `ad_soyad` varchar(255) COLLATE utf8_bin NOT NULL,
  `telefon` varchar(255) COLLATE utf8_bin NOT NULL,
  `mesaj` varchar(255) COLLATE utf8_bin NOT NULL,
  `konu` varchar(255) COLLATE utf8_bin NOT NULL,
  `credate` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `IsActive` int(2) NOT NULL,
  `IsReplied` int(3) NOT NULL,
  `ReplyDate` varchar(255) COLLATE utf8_bin NOT NULL,
  `ReplyUser` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=13 ;

--
-- Dumping data for table `iletisim_sikayet`
--

INSERT INTO `iletisim_sikayet` (`id`, `email`, `ad_soyad`, `telefon`, `mesaj`, `konu`, `credate`, `IsActive`, `IsReplied`, `ReplyDate`, `ReplyUser`) VALUES
(12, 'aaaaaaa', 'aaaaaaaaaaaaa', 'aaaaaa', 'aaaaaaaa', 'About the entry: 15', '2012-03-01 20:32:00', 1, 0, '2012-03-01 20:38:00', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `ipban`
--

DROP TABLE IF EXISTS `ipban`;
CREATE TABLE IF NOT EXISTS `ipban` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `ip` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `id_2` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `konular`
--

DROP TABLE IF EXISTS `konular`;
CREATE TABLE IF NOT EXISTS `konular` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `sira` bigint(11) NOT NULL DEFAULT '0',
  `baslik` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `ip` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `tarih` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `duntarih` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `gun` int(11) NOT NULL DEFAULT '0',
  `ay` int(11) NOT NULL DEFAULT '0',
  `yil` int(11) NOT NULL DEFAULT '0',
  `saat` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `hit` bigint(11) NOT NULL DEFAULT '0',
  `statu` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `tasi` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `silmod` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `siltarih` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `silsebep` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `baslikDurum` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `bugun` date NOT NULL DEFAULT '0000-00-00',
  `dun` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`id`),
  KEY `baslik` (`baslik`),
  KEY `ay` (`ay`),
  KEY `sira` (`sira`),
  KEY `statu` (`statu`),
  KEY `gun` (`gun`),
  KEY `tarih` (`tarih`),
  KEY `yil` (`yil`),
  KEY `baslikDurum` (`baslikDurum`),
  KEY `ip` (`ip`),
  KEY `saat` (`saat`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=73 ;

--
-- Dumping data for table `konular`
--

INSERT INTO `konular` (`id`, `sira`, `baslik`, `ip`, `tarih`, `duntarih`, `gun`, `ay`, `yil`, `saat`, `hit`, `statu`, `tasi`, `silmod`, `siltarih`, `silsebep`, `baslikDurum`, `bugun`, `dun`) VALUES
(69, 0, 'gibson lam  ', '14.136.248.10', '201505291013', '201505291013', 29, 5, 2015, '10:13', 73, '', '', '', '', '', '', '2015-05-29', '0000-00-00'),
(70, 0, 'david rossiter  ', '14.136.248.10', '201505291033', '201505291014', 29, 5, 2015, '10:14', 75, '', '', '', '', '', '', '2015-05-29', '0000-00-00'),
(68, 0, 'astri  ', '14.136.248.10', '201505270942', '201505270942', 27, 5, 2015, '09:42', 56, '', '', '', '', '', '', '2015-05-27', '0000-00-00'),
(67, 0, '27.5.2015 eesm5700 final exam  ', '14.136.248.10', '201505290917', '201505261124', 29, 5, 2015, '11:24', 98, '', '', '', '', '', '', '2015-05-29', '2015-05-26'),
(64, 0, 'hkust-swimming pool', '221.221.234.29', '201505310518', '201505260917', 31, 5, 2015, '09:17', 91, '', '', '', '', '', '', '2015-05-31', '0000-00-00'),
(65, 0, 'job  ', '1.36.23.23', '201505260942', '201505260942', 26, 5, 2015, '09:42', 52, '', '', '', '', '', '', '2015-05-26', '0000-00-00'),
(66, 0, 'albert chi-shing chung  ', '14.136.248.10', '201505261121', '201505261121', 26, 5, 2015, '11:21', 72, '', '', '', '', '', '', '2015-05-26', '0000-00-00'),
(55, 0, 'khaled ben letaief (李 德 富)  ', '14.136.248.10', '201505310515', '201505260807', 31, 5, 2015, '08:06', 141, '', '', '', '', '', '', '2015-05-31', '2015-05-26'),
(56, 0, 'amine bermak  ', '14.136.248.10', '201506010812', '201505310505', 1, 6, 2015, '08:08', 155, '', '', '', '', '', '', '2015-06-01', '2015-05-31'),
(57, 0, 'vincent lau (劉 堅 能)  ', '14.136.248.10', '201505301202', '201505290916', 30, 5, 2015, '08:09', 136, '', '', '', '', '', '', '2015-05-30', '2015-05-29'),
(58, 0, 'vladimir g. chigrinov  ', '14.136.248.10', '201505310509', '201505260811', 31, 5, 2015, '08:11', 129, '', '', '', '', '', '', '2015-05-31', '0000-00-00'),
(59, 0, 'bertram shi (施 毅 明)  ', '14.136.248.10', '201505301128', '201505260951', 30, 5, 2015, '08:33', 159, '', '', '', '', '', '', '2015-05-30', '2015-05-26'),
(60, 0, 'kei may lau (劉 紀 美)  ', '14.136.248.10', '201505310451', '201505301204', 31, 5, 2015, '08:34', 132, '', '', '', '', '', '', '2015-05-31', '2015-05-30'),
(61, 0, 'andrew wing-on poon (潘 永 安)  ', '14.136.248.10', '201505310500', '201505260950', 31, 5, 2015, '08:36', 165, '', '', '', '', '', '', '2015-05-31', '2015-05-26'),
(62, 0, 'chi-ying tsui ( - 英)  ', '14.136.248.10', '201505260858', '201505260856', 26, 5, 2015, '08:56', 90, '', '', '', '', '', '', '2015-05-26', '0000-00-00'),
(49, 0, 'frequently asked questions  ', '202.40.139.130', '201504260239', '201504260239', 26, 4, 2015, '02:39', 138, '', '', '', '', '', '', '2015-04-26', '0000-00-00'),
(52, 0, 'hong kong university of science and technology  ', '202.40.139.130', '201504260243', '201504260243', 26, 4, 2015, '02:43', 19, '', '', '', '', '', '', '2015-04-26', '0000-00-00'),
(53, 0, 'papilion  ', '14.136.248.10', '201505250048', '201505250048', 25, 5, 2015, '00:48', 68, '', '', '', '', '', '', '2015-05-25', '0000-00-00'),
(63, 0, 'walltyper  ', '221.221.234.29', '201507072250', '201505301120', 7, 7, 2015, '09:14', 845, '', '', '', '', '', '', '2015-07-07', '2015-05-30'),
(54, 0, 'ross d. murch  ', '14.136.248.10', '201505301155', '201505290917', 30, 5, 2015, '08:05', 108, '', '', '', '', '', '', '2015-05-30', '2015-05-29'),
(71, 0, 'ming liu (劉 明)  ', '202.140.78.214', '201505301156', '201505301156', 30, 5, 2015, '11:56', 59, '', '', '', '', '', '', '2015-05-30', '0000-00-00'),
(72, 0, 'philip k.t. mok (莫 國 泰)  ', '202.140.72.90', '201505310516', '201505301158', 31, 5, 2015, '11:58', 80, '', '', '', '', '', '', '2015-05-31', '2015-05-30');

-- --------------------------------------------------------

--
-- Table structure for table `mesajlar`
--

DROP TABLE IF EXISTS `mesajlar`;
CREATE TABLE IF NOT EXISTS `mesajlar` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `sira` bigint(11) NOT NULL DEFAULT '0',
  `mesaj` text COLLATE utf8_bin NOT NULL,
  `yazar` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `ip` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `tarih` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `gun` int(11) NOT NULL DEFAULT '0',
  `ay` int(11) NOT NULL DEFAULT '0',
  `yil` int(11) NOT NULL DEFAULT '0',
  `saat` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `oy` bigint(11) NOT NULL DEFAULT '0',
  `update2` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `updatetarih` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `updater` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `updatesebep` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `statu` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `silen` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `silsebep` text COLLATE utf8_bin NOT NULL,
  `dava` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `eser` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `eserci` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `esertarih` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `nedeneser` text COLLATE utf8_bin NOT NULL,
  `kacartioy` bigint(11) NOT NULL DEFAULT '0',
  `kaceksioy` bigint(11) NOT NULL DEFAULT '0',
  `tarih2` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `oy` (`oy`),
  KEY `kacartioy` (`kacartioy`),
  KEY `yazar` (`yazar`),
  KEY `ip` (`ip`),
  KEY `yil` (`yil`),
  KEY `kaceksioy` (`kaceksioy`),
  KEY `sira` (`sira`),
  KEY `gun` (`gun`),
  KEY `saat` (`saat`),
  KEY `tarih` (`tarih`),
  KEY `statu` (`statu`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=175 ;

--
-- Dumping data for table `mesajlar`
--

INSERT INTO `mesajlar` (`id`, `sira`, `mesaj`, `yazar`, `ip`, `tarih`, `gun`, `ay`, `yil`, `saat`, `oy`, `update2`, `updatetarih`, `updater`, `updatesebep`, `statu`, `silen`, `silsebep`, `dava`, `eser`, `eserci`, `esertarih`, `nedeneser`, `kacartioy`, `kaceksioy`, `tarih2`) VALUES
(3, 2, 'على', 'admin', '127.0.0.1', '201201301034', 30, 1, 2012, '10:34', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2012-01-30 10:34:00'),
(5, 1, '448', 'admin', '127.0.0.1', '201201301340', 30, 1, 2012, '13:40', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2012-01-30 13:40:00'),
(6, 2, '474\r<br>', 'admin', '127.0.0.1', '201201301340', 30, 1, 2012, '13:40', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2012-01-30 13:40:00'),
(98, 56, 'Prof. Amine Bermak received the Masters and PhD degrees, both in electrical and electronic engineering (microelectronics and Microsystems), from Paul Sabatier University, Toulouse, France in 1994 and 1998, respectively. During his PhD, he was part of the Microsystems and Microstructures Research Group at a major French National Research Centre LAAS-CNRS, where he developed a 3D VLSI chip for artificial neural network classification and detection applications in a project funded by Motorola. While finalizing his PhD, he was offered a Post-doc position at the Advanced Computer Architecture group at York University – England, to work on VLSI implementation of CMM neural network for vision applications in a project funded by British Aerospace and the Yorkshire Post-office. In November 1998, he joined Edith Cowan University, Perth, Australia as a research fellow working on smart vision sensors. In January 2000, he was appointed Lecturer and promoted to Senior Lecturer in December 2001. In July 2002, he joined the Electronic and Computer Engineering Department of Hong Kong University of Science and Technology (HKUST), where he is currently a full Professor and ECE Associate Head for postgraduate education and research. He has also been serving as the Director of Computer Engineering as well as the Director of the Master Program in IC Design. He is also the founder and the leader of the Smart Sensory Integrated Systems Research Lab at HKUST. He was a member of the senate committee at HKUST. \r<br>\r<br>Over the last decade, Prof. Bermak has acquired a significant academic and industrial experience. He has taught 18 different courses at the undergraduate and post-graduate levels. For his excellence and outstanding contribution to teaching, he was nominated for the 2013 Hong Kong UGC best teacher award (for all HK Universities). He is the recipient of the 2011 University Michael G. Gale Medal for distinguished teaching (Highest University-wide Teaching Award). This gold medal is established to recognize excellence in teaching and only one recipient/year is honoured for his/her contribution. Prof. Bermak is also a two-time recipient of the “Engineering School Teaching Excellence Award" in HKUST for 2004 and 2009, respectively.\r<br>\r<br>Prof. Bermak has been very actively involved in many research and industrial projects in collaboration with Motorola-France, 3D plus- France, British Aerospace-UK, Intelligent Pixel Inc. – USA. His research interests are related to VLSI circuits and systems for signal, image processing, sensors and microsystems applications.\r<br>\r<br>Prof. Bermak has received five distinguished awards, including the “Best student paper award” at IEEE International Symposium on Circuits and systems ISCAS 2010 (the conference featured 1081 papers and the paper received the first prize); the 2004 “IEEE Chester Sall Award” from IEEE Consumer electronics society; the IEEE Service Award from IEEE Computer Society and the “Best Paper Award” at the 2005 International Workshop on System-On-Chip for Real-Time Applications. Prof. Bermak is a member of technical program committees of a number of international conferences including the IEEE Custom Integrated Circuit Conference CICC’2006, CICC’2007, the European Solid-State Conference 2010-11, the IEEE Consumer Electronics Conference CEC’ 2007-11, Design Automation and Test in Europe DATE2007-08. He has published over 250 articles in journals, book chapters and conference proceedings and designed over 30 chips. He is a member of the IEEE Circuits and Systems Society committee on Sensory Systems as well as BIOCAS. He has served on the editorial board of IEEE Transactions on Very Large Scale Integration (VLSI) Systems and the Sensors Journal. He is also currently serving on the editorial board of IEEE Transactions on Biomedical Circuits and Systems and IEEE Transactions on Circuits and Systems II. He is the guest editor of the November 2010 special issue in IEEE Transactions on Biomedical Circuits and Systems. Prof. Bermak is a Fellow of IEEE and IEEE distinguished Lecturer.', 'admin', '14.136.248.10', '201505260808', 26, 5, 2015, '08:08', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-26 08:08:00'),
(46, 27, 'gt', 'admin', '127.0.0.1', '201203092010', 9, 3, 2012, '20:10', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2012-03-09 20:10:00'),
(97, 55, 'Professor Ben Letaief received the BS degree with distinction, MS and Ph.D. degrees from Purdue University, USA. From 1990 to 1993, he was a faculty member at the University of Melbourne, Australia. Since 1993, he has been at HKUST where he is currently Chair Professor and Dean of Engineering. \r<br> \r<br>Dr. Letaief has contributed to the advances of wireless technologies, including several major developments in OFDM and MIMO along with multiuser resource allocation, which are considered as the core technologies for 4G cellular systems and other advanced wireless networks.   In his field, he has over 460 journal and conference papers and given invited keynote talks as well as courses all over the world.  He has 2 China patents and 11 US patents and made 6 technical contributions to IEEE standards.\r<br>\r<br>He is the recipient of many prestigious awards including IEEE Marconi Prize Award in Wireless Communications; IEEE Harold Sobol Award; IEEE ComSoc Publications Exemplary Award; Purdue University Outstanding Electrical and Computer Engineer Award (2010); IEEE Wireless Communications Technical Committee Recognition Award (2011); 10 IEEE Best Paper Awards, and 6 Teaching Awards.  \r<br> \r<br>He served in many IEEE leadership positions including IEEE ComSoc Vice-President for Conferences; elected member of IEEE Publication Services and Products Board, and member of IEEE Fellow Evaluation Committee.  He is the founding Editor-in-Chief of the prestigious IEEE Transactions on Wireless Communications and has served as General/Technical Program Chair of IEEE flagship conferences (e.g., WCNC’07, ICC’08; ICC’10).\r<br> \r<br>He is an IEEE Fellow and ISI Highly Cited Author.', 'admin', '14.136.248.10', '201505260807', 26, 5, 2015, '08:07', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-26 08:07:00'),
(96, 55, 'Khaled BEN LETAIEF (李 德 富)', 'admin', '14.136.248.10', '201505260806', 26, 5, 2015, '08:06', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-26 08:06:00'),
(64, 38, '--! (spoiler) !--\r<br>\r<br>dsad\r<br>\r<br>--! (gbkz: spoiler) !--\r<br>', 'admin', '127.0.0.1', '201203142119', 14, 3, 2012, '21:19', 0, '', '', '', '', 'silindi', '', '', '', '', '', '', '', 0, 0, '2012-03-14 21:19:00'),
(69, 39, '(youtube: 4wGR4-SeuJ0)', 'admin', '127.0.0.1', '201203142130', 14, 3, 2012, '21:30', 0, '', '14/03/2012 21:36', 'admin', '', 'silindi', '', '', '', '', '', '', '', 0, 0, '2012-03-14 21:30:00'),
(70, 40, 'الصحف', 'admin', '127.0.0.1', '201203261441', 26, 3, 2012, '14:41', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2012-03-26 14:41:00'),
(71, 41, '\r<br>العربية ', 'admin', '127.0.0.1', '201203261505', 26, 3, 2012, '15:05', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2012-03-26 15:05:00'),
(72, 42, 'ا ', 'admin', '127.0.0.1', '201203261955', 26, 3, 2012, '19:55', 0, '', '', '', '', 'silindi', '', '', '', '', '', '', '', 0, 0, '2012-03-26 19:55:00'),
(73, 43, 'jedi scum', 'vader', '127.0.0.1', '201204021047', 2, 4, 2012, '10:47', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2012-04-02 10:47:00'),
(74, 43, 'haha', 'admin', '127.0.0.1', '201204021056', 2, 4, 2012, '10:56', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2012-04-02 10:56:00'),
(75, 44, 'yodayım ben hıh!', 'yoda', '127.0.0.1', '201204021316', 2, 4, 2012, '13:16', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2012-04-02 13:16:00'),
(76, 45, 'güzel oyundur.', 'vader', '::1', '201204021426', 2, 4, 2012, '14:26', 0, '', '', '', '', 'silindi', '', '', '', '', '', '', '', 0, 0, '2012-04-02 14:26:00'),
(77, 46, 'süperdir.', 'vader', '::1', '201204021427', 2, 4, 2012, '14:27', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2012-04-02 14:27:00'),
(78, 46, 'deneme.', 'admin', '::1', '201204021427', 2, 4, 2012, '14:27', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2012-04-02 14:27:00'),
(79, 45, 'buna inanamıyorum.', 'admin', '::1', '201204021427', 2, 4, 2012, '14:27', 0, '', '', '', '', 'silindi', '', '', '', '', '', '', '', 0, 0, '2012-04-02 14:27:00'),
(80, 33, '(image: http://lh5.googleusercontent.com/-DVHdLH31tDY/AAAAAAAAAAI/AAAAAAAAAAA/Y71fEMMDZmk/s46-c-k/photo.jpg)', 'admin', '::1', '201204041038', 4, 4, 2012, '10:38', 0, '', '04/04/2012 10:38', 'admin', '', 'silindi', '', '', '', '', '', '', '', 0, 0, '2012-04-04 10:38:00'),
(81, 11, 'pek değil.', 'vader', '::1', '201204041657', 4, 4, 2012, '16:57', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2012-04-04 16:57:00'),
(82, 47, 'پنجابی', 'admin', '::1', '201503041257', 4, 3, 2015, '12:57', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-03-04 12:57:00'),
(83, 48, 'Австра́лия (англ. Australia, МФА: [əˈstreɪljə], от лат. austrālis — «южный»), официально Австрали́йский Сою́з (англ. Commonwealth of Australia[4], МФА: [ˈkɒm.ənˌwɛlθ əv əˈstreɪljə]) — государство в Южном полушарии, занимающее материк Австралия, остров Тасмания и несколько других островов Индийского и Тихого океанов[* 1]; является шестым государством по площади в мире. К северу от Австралийского Союза расположены Восточный Тимор, Индонезия и Папуа — Новая Гвинея, к северо-востоку — Вануату, Новая Каледония и Соломоновы Острова, к юго-востоку — Новая Зеландия. Кратчайшее расстояние между главным островом Папуа — Новой Гвинеи и материковой частью Австралийского Союза составляет всего 145 км[6], а расстояние от австралийского острова Боигу до Папуа — Новой Гвинеи — всего 5 километров. Население на 4 марта 2015 составляет 23 845 725, большинство из которых проживает в городах на восточном побережье[7].\r<br>\r<br>Австралия является одной из развитых стран, являясь тринадцатой по размеру экономикой в мире, и имеет шестое место в мире по ВВП в расчёте на душу населения. Военные расходы Австралии являются двенадцатыми по размеру в мире. Со вторым по величине индексом развития человеческого потенциала Австралия занимает высокое место во многих сферах, таких как качество жизни, здоровье, образование, экономическая свобода, защита гражданских свобод и политических прав[8]. Австралия является членом G20, ОЭСР, ВТО, АТЭС, ООН, Содружества наций, АНЗЮСа и Форума тихоокеанских островов.', 'admin', '::1', '201503041303', 4, 3, 2015, '13:03', 0, '', '', '', '', 'silindi', '', '', '', '', '', '', '', 0, 0, '2015-03-04 13:03:00'),
(84, 48, 'Термин «Австралия» (англ. Australia, [əˈstɹæɪljə, -liə] в австралийском английском[9]) происходит от лат. austrālis («южный»). В разговорной речи австралийцев для обозначения Австралии используется слово Oz. Для обозначения прилагательного «австралийский» австралийцами используется слово Aussie ([ˈɒzi]).\r<br>\r<br>Легенды о Неведомой Южной земле (лат. Terra Australis Incognita) — «неизвестной земле на юге» — восходят к временам Римской империи и были обычным явлением в средневековой географии несмотря на то, что не базировались на каких-либо знаниях о самом континенте.\r<br>\r<br>Самое раннее задокументированное сведение об использовании в английском языке слова «Australia» были написанные в 1625 году «Сведения об Аустралиа-дель-Эспириту-Санту, записанные мастером Халклайтом» (англ. A note of Australia del Espíritu Santo, written by Master Hakluyt) и опубликованные Самуэлем Пурчасом[en] в Hakluytus Posthumus, где испанское название Аустралиа-дель-Эспириту-Санту (исп. Australia del Espíritu Santo), данное острову в архипелаге Новые Гебриды, было искажено до «Australia»[10]. Прилагательное «Australische» также использовалось голландскими чиновниками Батавии (современная Джакарта) для обозначения всех новооткрытых с 1638 года южных земель[11]. Слово «Australia» было использовано в переведённой на английский язык книге французского писателя-утописта Габриэля де Фуаньи «Приключения Жака Садера, его путешествие и открытие Астральной Земли» (фр. Les Aventures de Jacques Sadeur dans la Découverte et le Voyage de la Terre Australe; 1676)[12]. По отношению ко всей южной части Тихого Океана этот термин использует Александр Далримпл, шотландский географ, в своей книге «Историческая коллекция путешествий и открытий в южной части Тихого океана» (англ. An Historical Collection of Voyages and Discoveries in the South Pacific Ocean; 1771). В конце XVIII века термин используется ботаниками Джорджем Шоу и Джеймсом Эдвардом Смитом для обозначения австралийского континента в их книге «Зоология и ботаника Новой Голландии» (англ. Zoology and Botany of New Holland; 1793)[13], а также на карте 1799 года, принадлежавшей Джеймсу Уилсону[en][14].\r<br>\r<br>Название «Australia» стало популярным после опубликования в 1814 году «Путешествия в Terra Australis» капитана Мэтью Флиндерса, который является первым человеком, обогнувшим Австралийский континент. При её подготовке Флиндерс убедил своего патрона, Джозефа Бэнкса, использовать термин Terra Australis, как более известный публике. Флиндерс сделал это, указав:\r<br>\r<br>Если бы я позволил себе любое новшество, то это было бы преобразование названия континента в «Australia», так как оно и более приятное для уха, и сочетается с именами других великих частей света.[15]\r<br>\r<br>Оригинальный текст  (англ.)  [показать]\r<br>Это единственное употребление слова «Australia» в тексте; но в Приложении III книги Роберта Броуна «Общие сведения, географические и систематические, о ботанике Терра Аустралис» (англ. General remarks, geographical and systematical, on the botany of Terra Australis; 1814) повсеместно используется прилагательное «Australian»[16] и эта книга является первым задокументированным использованием этого слова[17]. Несмотря на распространенное заблуждение, книга не сыграла особой роли в принятии слова «Australia» для названия континента — это название было принято в течение последующих десяти лет после выхода книги[18]. Лаклан Маккуори, губернатор Нового Южного Уэльса[en], использовал это название в официальных посланиях в Англию, а 12 декабря 1817 года рекомендовал Министерству по делам колоний Британской империи официально принять его[19]. В 1824 году Британское адмиралтейство окончательно утвердило это название континента[20].', 'admin', '::1', '201503041303', 4, 3, 2015, '13:03', 0, '', '', '', '', 'silindi', '', '', '', '', '', '', '', 0, 0, '2015-03-04 13:03:00'),
(85, 40, '河數列，海內習用，方', 'admin', '124.158.204.105', '201503060920', 6, 3, 2015, '09:20', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-03-06 09:20:00'),
(86, 33, '分布规律、物质结构、发展历史和开发利用的科学', 'admin', '58.152.168.74', '201504180320', 18, 4, 2015, '03:20', 0, '', '', '', '', 'silindi', '', '', '', '', '', '', '', 0, 0, '2015-04-18 03:20:00'),
(87, 46, 'Deneme 2\r<br>', 'baris', '202.40.139.130', '201504240528', 24, 4, 2015, '05:28', 0, '', '', '', '', 'wait', '', '', '', '', '', '', '', 0, 0, '2015-04-24 05:28:00'),
(88, 46, 'Deneme 2\r<br>', 'baris', '202.40.139.130', '201504240528', 24, 4, 2015, '05:28', 0, '', '', '', '', 'wait', '', '', '', '', '', '', '', 0, 0, '2015-04-24 05:28:00'),
(89, 46, 'Torik', 'baris', '202.140.108.55', '201504241127', 24, 4, 2015, '11:27', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-04-24 11:27:00'),
(90, 49, 'head', 'admin', '202.40.139.130', '201504260239', 26, 4, 2015, '02:39', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-04-26 02:39:00'),
(91, 50, 'sometih', 'admin', '202.40.139.130', '201504260240', 26, 4, 2015, '02:40', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-04-26 02:40:00'),
(92, 51, 'Hong Kong University of Science and Technology', 'admin', '202.40.139.130', '201504260241', 26, 4, 2015, '02:41', 0, '', '', '', '', 'silindi', 'admin', '', '', '', '', '', '', 0, 0, '2015-04-26 02:41:00'),
(93, 52, 'my university', 'admin', '202.40.139.130', '201504260243', 26, 4, 2015, '02:43', 0, '', '', '', '', 'silindi', 'admin', '', '', '', '', '', '', 0, 0, '2015-04-26 02:43:00'),
(94, 53, 'some', 'admin', '14.136.248.10', '201505250048', 25, 5, 2015, '00:48', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-25 00:48:00'),
(95, 54, 'Prof. Ross MURCH is a Chair Professor of Electronic and Computer Engineering at the Hong Kong University of Science and Technology and an IEEE Fellow. His current research interests include Wireless Communications with a focus on novel Multiple Input Multiple output (MIMO) Wireless Systems, Compact MIMO Antenna design, Cognitive Radio, Cooperative Wireless systems, WLAN and LTE-Advanced. His unique expertise lies in his combination of knowledge from both wireless communication systems and electromagnetics. He has contributed both to the design of compact multiple antennas for wireless communications and also physical layer algorithms for enhancing the bit rates of wireless systems with over 200 publications and patents in the area of wireless communications that have attracted over 9000 citations. He also has industrial experience in the wireless communication area through sabbaticals at Allgon (a Swedish antenna supplier for Nokia), AT&T (USA based wireless service provider) and consulting for various organizations including the Wireless Access Group of ASTRI (Hong Kong Applied Science and Technology Research Institute). In addition he is the Publication Editor for IEEE Transactions on Wireless Communications, was an editor for IEEE Journal on Selected Areas in Communications: Wireless Series and acts as a reviewer for several journals. He was also the technical program chair of the IEEE Wireless Communications and Networking Conference in 2007 and was the technical program chair for the Advanced Wireless Communication Systems Symposium at the 2002 IEEE International Conference on Communication (ICC 2002) in New York. In July 2005, he was invited to the School of Engineering Science, Simon Fraser University, Canada, as the David Bensted Fellow. In July 2004 he visited Southampton University, UK as an HKTIIT fellow. He was also a keynote speaker at IEEE APWCS 2008/GCOE, IEEE GCC 2007 and IEEE WiCOM 2007. His wireless research contributions include over 200 publications and patents on MIMO systems and antennas, OFDM, propagation and broadband wireless systems and these have attracted over 9000 citations.', 'admin', '14.136.248.10', '201505260805', 26, 5, 2015, '08:05', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-26 08:05:00'),
(99, 57, 'Vincent obtained his B.Eng (Distinction 1st Hons – ranked 2nd) from the department of EEE, University of Hong Kong in 1992. He joined the Hong Kong Telecom (PCCW) after graduation for 3 years as system engineer. He obtained the Sir Edward Youde Memorial Fellowship, Rotoract Scholarship and the Croucher Foundation in 1995 and study for Ph.D. at the University of Cambridge. He completed the Ph.D. degree in 2 years and joined the Bell Labs - Lucent Technologies, New Jersey in 1997 as Member of Technical Staff. He has been working on various advanced wireless technologies such as IS95, 3G1X, UMTS as well as wideband CDMA base station ASIC Design and Post 3G Technologies such as MIMO and HSDPA. He joined the department of ECE, HKUST as Associate Professor in August 2004 and was promoted to Professor in July 2010 and Chair Professor in July 2014. He has also been the technology advisor and consultant for a number of companies such as ZTE and Huawei, ASTRI, leading several R&D projects on B3G, WiMAX and Cognitive Radio. He is the founder and co-director of Huawei-HKUST Innovation Lab.\r<br>\r<br> \r<br>Vincent has published over 200 papers, including around 100 IEEE Transaction papers (IEEE Transactions on Information Theoy, IEEE JSAC, IEEE Transactions on Wireless Communications, IEEE Transactions on Communications, IEEE Transactions on Vehicular Technologies,..etc), 100 IEEE conference papers, 15 Bell Labs Technical Memorandum and contributed to 28 US patents on wireless systems. In addition, Vincent is also the key contributor of 4 IEEE standard contributions (IEEE 802.22 WRAN / Cognitive Radio) which are accepted into the IEEE 802.22 specification. He has received three best paper awards from IEEE ICC 2008, IEEE ChinaCOM 2008 and IEEE CICC 2007 and is a Fellow of IEEE. He is currently an Area Editor of IEEE Transactions on Wireless Communications, a guest editor of IEEE Journal on Selected Areas on Communications (JSAC) – Special Issue on Limited Feedback, a guest editor of IEEE Special Topics on Signal Processing, IEEE System Journal, a book-series editor of the Information and Communications Technologies (ICT) book series for John Wiley and Sons as well as an editor of the EURASIP Journal on Wireless Communications and Networking.', 'admin', '14.136.248.10', '201505260809', 26, 5, 2015, '08:09', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-26 08:09:00'),
(100, 58, 'Professor Chigrinov graduated from Faculty of Applied Mathematics, Moscow Electronics Institute, the Diploma of Engineer - Mathematician (MPhil) in 1973. In 1978, he obtained PhD degree in Solid State Physics (Liquid Crystals) in the Institute of Crystallography , USSR Academy of Sciences. In 1988, he becomes a Doctor of Physical and Mathematical Science and obtained a degree of a Professor in 1998. Since 1973, he was a Senior, Leading Researcher, and then Chief of Department in Organic Intermediates & Dyes Institute (NIOPIK). Since 1996 he was working as a Leading Scientist in the Institute of Crystallography , Russian Academy of Sciences and join HKUST in 1999, as an Associate Professor. He was a coauthor of the first LC materials and devices based on Electrically Controlled Birefringence, Twisted Nematic and Supertwisted Nematic and Ferroelectric LC materials, working at understanding the fundamental aspects of LC physics and technology, including electrooptical effects in liquid crystals and optimization of LC device configurations. Some new LC Electrooptical Modes, such as Orientational Instability in Cholesteric LC, Deformed Helix Effect in Ferroelectric LC, and Total Internal Reflection, Surface Gliding Effect and Surface Induced Orientational Transition in Nematic LC were first described by him and confirmed in experiment. The classification of the Domain Structures in LC was made based on his theoretical predictions and simulation results. Efficient Modeling Universal System of LC Electrooptics software was developed with his direct participation and supervision. He was a coauthor of a pioneering work in LC Photoaligning Technology, which has about 500 citations in scientific and technical journals. He is an Expert in Flat Panel Technology in Russia , recognized by World Technology Evaluation Centre, 1994, a Senior Member of the Society of Information Display (SID) since 2004 and become a fellow of SID since Jan 2008. Since 1997 he is a Vice-President of Russian Chapter of SID. He is a member of Editorial Board of "Liquid Crystals Today" since 1996 and Associate Editor of Journal of SID since 2005. He is an author of 2 books, 15 reviews and book chapters, 133 journal papers, 286 Conference presentations and 50 patents and patent applications in the field of liquid crystals since 1974. 7 PhD students defended their degrees under his supervision. He has outstanding poster paper award in IDW''03 and IDW''06, which are the largest display annual Conferences in Japan . He is a Member of International Advisory Committee for Advanced Display Technology Conferences in Russia , Ukraine and Belarus since 1999, European SID Program Committee since 2004, International Advisory Board of International Liquid Crystal Conference since 2006.', 'admin', '14.136.248.10', '201505260811', 26, 5, 2015, '08:11', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-26 08:11:00'),
(101, 59, 'Shi received the BS and MS degrees in Electrical Engineering from Stanford University. He received his PhD degree in Electrical Engineering and Computer Science from the University of California at Berkeley. He joined the ECE Department at HKUST in 1994. He has been as an Associate Editor for the IEEE Transactions on Circuits and Systems-I: Fundamental Theory and Applications twice, and was a Distinguished Lecturer for the IEEE Circuits and Systems Society. He is a Fellow of the IEEE and Chair of the IEEE Circuits and Systems Society Technical Committee on Cellular Neural Networks and Array Computing.', 'admin', '14.136.248.10', '201505260833', 26, 5, 2015, '08:33', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-26 08:33:00'),
(102, 60, 'Professor Kei May Lau was born in Hong Kong and received all her pre-college education (K-12) from the Pui Ching Middle School.  She received the B.S. and M.S. degrees in physics from University of Minnesota, Minneapolis, in 1976 and 1977 respectively, and the Ph.D. Degree in Electrical Engineering from Rice University, Houston, Texas, in 1981.\r<br> \r<br>From 1980 to 1982, she was a Senior Engineer at M/A-COM Gallium Arsenide Products, Inc., where she worked on epitaxial growth of GaAs for microwave devices, development of high-efficiency and mm-wave IMPATT diodes, and multi-wafer epitaxy by the chloride transport process. In the fall of 1982, she joined the faculty of the Electrical and Computer Engineering Department at the University of Massachusetts/Amherst, where she became a full professor in 1993. She initiated metalorganic chemical vapor deposition (MOCVD), compound semiconductor materials and devices programs at UMass. Her research group performed studies on heterostructures, quantum wells, strained-layers, III-V selective epitaxy, as well as high-frequency and photonic devices. Professor Lau spent her first sabbatical leave in 1989 at the MIT Lincoln Laboratory and worked with the Electro-optical Devices Group. She developed acoustic sensors at the DuPont Central Research and Development Laboratory in Wilmington, Delaware during her second sabbatical leave (''95-''96). In the fall of 1998, she was a visiting professor at the Hong Kong University of Science and Technology (HKUST), where she joined the regular faculty since the summer of 2000. She established the Photonics Technology Center for R&D effort in III-V and wide band-gap semiconductor materials and devices.\r<br>\r<br>Professor Lau is a Fellow of the IEEE, a recipient of the US National Science Foundation (NSF) Faculty Awards for Women (FAW) Scientists and Engineers (1991) and Hong Kong Croucher Senior Research Fellowship (2008). She served on the IEEE Electron Devices Society Administrative Committee and was an Editor of the IEEE Transactions on Electron Devices (1996-2002). She also served as an Associate Editor for the Journal of Crystal Growth.', 'admin', '14.136.248.10', '201505260834', 26, 5, 2015, '08:34', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-26 08:34:00'),
(103, 61, 'Andrew W. O. Poon received his B.A. (Hons.) degree from The University of Chicago, Illinois, USA in 1995, and his M. Phil and Ph. D. degrees from Yale University, Connecticut, USA, in 1998 and 2001, all in Physics. In 2001, he joined the Department of Electronic and Computer Engineering, The Hong Kong University of Science and Technology, as an assistant professor. He is currently a professor. His research group focuses on experiments and modeling of optical microresonators and silicon photonics.\r<br> 	 	\r<br>Website:	\r<br>http://www.ece.ust.hk/~eeawpoon', 'admin', '14.136.248.10', '201505260836', 26, 5, 2015, '08:36', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-26 08:36:00'),
(104, 58, 'good ', 'allenwang', '221.221.234.29', '201505260842', 26, 5, 2015, '08:42', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-26 08:42:00'),
(105, 58, 'like him', 'allenwang', '221.221.234.29', '201505260843', 26, 5, 2015, '08:43', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-26 08:43:00'),
(106, 58, 'so charming', 'allenwang', '221.221.234.29', '201505260844', 26, 5, 2015, '08:44', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-26 08:44:00'),
(107, 58, 'miss him so much', 'allenwang', '221.221.234.29', '201505260846', 26, 5, 2015, '08:46', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-26 08:46:00'),
(108, 58, 'his courses are so difficult', 'allenwang', '221.221.234.29', '201505260848', 26, 5, 2015, '08:48', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-26 08:48:00'),
(109, 61, 'he is a good professor', 'allen xu', '14.18.29.105', '201505260849', 26, 5, 2015, '08:49', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-26 08:49:00'),
(110, 56, 'first commentor, so lucky', 'allenwang', '221.221.234.29', '201505260851', 26, 5, 2015, '08:51', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-26 08:51:00'),
(111, 56, 'lilke him very much', 'allenwang', '221.221.234.29', '201505260851', 26, 5, 2015, '08:51', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-26 08:51:00'),
(112, 56, 'he has a mini car, very fancy', 'allenwang', '221.221.234.29', '201505260852', 26, 5, 2015, '08:52', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-26 08:52:00'),
(113, 56, 'his courses are so interesting ', 'allenwang', '221.221.234.29', '201505260855', 26, 5, 2015, '08:55', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-26 08:55:00'),
(114, 62, 'He obtained his BS degree in Electrical Engineering from University of Hong Kong, and MS and PhD degrees in Computer Engineering from University of Southern California. Professor Tsui received the IEEE Transactions on VLSI System Best Paper Award in 1995. He also supervised the best student paper award of IEEE ISCAS in 1999. He has reviewed several IEEE transactions and journals, and he served on the organization committee of ASP-DAC’99 and program committees of IEEE ISLPE, VLSI, ASP-DAC and other conferences. He also consulted for local and multinational companies. He is an IEEE member.\r<br>\r<br>He is Associate Dean of Engineering (Undergraduate Studies) now in HKUST...', 'admin', '14.136.248.10', '201505260856', 26, 5, 2015, '08:56', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-26 08:56:00'),
(115, 59, 'first commentor, yeah', 'allenwang', '221.221.234.29', '201505260856', 26, 5, 2015, '08:56', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-26 08:56:00'),
(116, 59, 'professor Shi is so fit', 'allenwang', '221.221.234.29', '201505260857', 26, 5, 2015, '08:57', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-26 08:57:00'),
(117, 59, 'he is such a good climber', 'allenwang', '221.221.234.29', '201505260857', 26, 5, 2015, '08:57', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-26 08:57:00'),
(118, 62, 'he is adorable', 'allenwang', '221.221.234.29', '201505260858', 26, 5, 2015, '08:58', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-26 08:58:00'),
(119, 54, 'so charasmatic ', 'allenwang', '221.221.234.29', '201505260900', 26, 5, 2015, '09:00', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-26 09:00:00'),
(120, 63, 'swimming pool', 'allenwang', '221.221.234.29', '201505260914', 26, 5, 2015, '09:14', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-26 09:14:00'),
(121, 64, 'the swimming pool in HKUST is so great, don''t you love it?', 'allenwang', '221.221.234.29', '201505260917', 26, 5, 2015, '09:17', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-26 09:17:00'),
(122, 59, 'I was very impressed by his lecture which is called pattern recognition.\r<br>He made good analogy and let those complex concept understoodable', 'allen xu', '14.18.29.105', '201505260919', 26, 5, 2015, '09:19', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-26 09:19:00'),
(123, 57, 'How possible to finish Ph.D. in 2 years at Cambridge ?', 'admin', '14.136.248.10', '201505260923', 26, 5, 2015, '09:23', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-26 09:23:00'),
(124, 65, 'What are you going to do after you graduate? Let''s share!', 'shing', '1.36.23.23', '201505260942', 26, 5, 2015, '09:42', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-26 09:42:00'),
(125, 57, 'Also how possible for him to be promoted from professor to chair professor in 4 years? ', 'shing', '1.36.23.23', '201505260949', 26, 5, 2015, '09:49', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-26 09:49:00'),
(126, 61, 'Someone said he is very handsome', 'shing', '1.36.23.23', '201505260950', 26, 5, 2015, '09:50', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-26 09:50:00'),
(127, 59, 'Good professor but difficult course', 'shing', '1.36.23.23', '201505260951', 26, 5, 2015, '09:51', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-26 09:51:00'),
(128, 64, 'Outdoor swimming pool already open, much better than the indoor', 'shing', '1.36.23.23', '201505260959', 26, 5, 2015, '09:59', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-26 09:59:00'),
(129, 58, 'LCD is the best!!', 'shing', '1.36.23.23', '201505261000', 26, 5, 2015, '10:00', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-26 10:00:00'),
(130, 66, 'Prof Chung received his BEng degree (First-Class Honours) in Computer Engineering from the University of Hong Kong in 1995. He was an exchange student at the University of British Columbia, Canada from 1992-93 with the Cathay Pacific Visiting Student Award. He joined The Hong Kong University of Science and Technology (HKUST) in 1996, and received his MPhil degree in Computer Science in 1998. He then joined the Medical Vision Laboratory, Robotics Research Group at the University of Oxford, U.K. as a doctoral research student in 1998, and graduated in 2001 with D.Phil. degree in Engineering Science. Prof Chung was a Croucher scholar (1998 – 2001) with the Croucher Foundation Scholarship. He was a visiting scientist at the Artificial Intelligence Laboratory, Massachusetts Institute of Technology, U.S.A. (Sept 2001 – Feb 2002).\r<br>\r<br>Prof Chung was honored to receive the British Machine Vision Association Sullivan Thesis Award for the best doctoral thesis submitted to a United Kingdom university in the field of computer vision or natural vision in 2002; and the Top Ten Lecturers Award at HKUST in 2010.\r<br>\r<br>Prof Chung founded the Lo Kwee-Seong Medical Image Analysis Laboratory in 2005 with a generous donation from the K.S. Lo foundation, Hong Kong. The laboratory focuses on interdisciplinary research aiming at creating and applying engineering techniques in the area of medical image analysis, with an ultimate goal to improving the quality of healthcare in Hong Kong, China and other regions.', 'admin', '14.136.248.10', '201505261121', 26, 5, 2015, '11:21', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-26 11:21:00'),
(131, 67, 'eesm5700 (Image and Video Signal Processing)', 'admin', '14.136.248.10', '201505261124', 26, 5, 2015, '11:24', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-26 11:24:00'),
(132, 56, 'Prof. Amine Bermak of ECE was interviewed by the South China Morning Post to discuss the new research project on which he is currently working with(sref:chi-ying tsui ( - 英)  ) Prof. CY Tsui, Prof. George Yuan and other professors. This project on the development of a multifunctional sensor which can monitor the air in offices has received a financial support at HK$9.8 million from the government''s Innovation and Technology Fund. The article "Sensor aims to keep an eye on your office" was published on 4 May 2015.', 'admin', '14.136.248.10', '201505261125', 26, 5, 2015, '11:25', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-26 11:25:00'),
(133, 56, 'He is French and have good English!', 'xtang', '14.136.248.10', '201505270926', 27, 5, 2015, '09:26', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-27 09:26:00'),
(134, 61, 'Sure he is handsome :)..', 'xtang', '14.136.248.10', '201505270928', 27, 5, 2015, '09:28', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-27 09:28:00'),
(135, 60, 'Such a nano-technology.', 'xtang', '14.136.248.10', '201505270929', 27, 5, 2015, '09:29', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-27 09:29:00'),
(136, 55, 'Dean.', 'xtang', '14.136.248.10', '201505270929', 27, 5, 2015, '09:29', 0, '', '', '', '', 'silindi', 'xtang', '', '', '', '', '', '', 0, 0, '2015-05-27 09:29:00'),
(137, 68, 'So many HKUST graduates are working there, kinds of tech company.', 'admin', '14.136.248.10', '201505270942', 27, 5, 2015, '09:42', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-27 09:42:00'),
(138, 67, 'Difficult,but the last one for this program ', 'kelvin caleb', '220.246.201.171', '201505271229', 27, 5, 2015, '12:29', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-27 12:29:00'),
(139, 56, 'I like him', 'kelvin caleb', '220.246.201.171', '201505271230', 27, 5, 2015, '12:30', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-27 12:30:00'),
(140, 59, 'very good prof and lectures pattern recognition', 'denny', '203.218.58.116', '201505272112', 27, 5, 2015, '21:12', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-27 21:12:00'),
(141, 61, 'so handsome！！ Really good professor', 'zmy07007', '218.102.199.141', '201505290912', 29, 5, 2015, '09:12', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-29 09:12:00'),
(142, 56, 'learn much in his class', 'zmy07007', '218.102.199.141', '201505290914', 29, 5, 2015, '09:14', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-29 09:14:00'),
(143, 57, 'i have heard he is so famous and talented', 'zmy07007', '218.102.199.141', '201505290916', 29, 5, 2015, '09:16', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-29 09:16:00'),
(144, 67, 'hehe', 'zmy07007', '218.102.199.141', '201505290917', 29, 5, 2015, '09:17', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-29 09:17:00'),
(145, 54, 'so nice', 'zmy07007', '218.102.199.141', '201505290917', 29, 5, 2015, '09:17', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-29 09:17:00'),
(146, 69, 'Lecturer in the Department of Computer Science and Engineering of the Hong Kong University of Science and Technology. I received my PhD degree in Computer Science and Engineering from HKUST in 2012.\r<br>My research interests include Multimedia Web Services, Internet Technologies, IT in Education and Computer Graphics. I am the co-creator of the Gong Project and the NanoGong Voice Applet.', 'admin', '14.136.248.10', '201505291013', 29, 5, 2015, '10:13', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-29 10:13:00'),
(147, 70, 'COMP303 Internet Computing', 'admin', '14.136.248.10', '201505291014', 29, 5, 2015, '10:14', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-29 10:14:00'),
(148, 70, '1. met him in COMP1022Q in 2014-2015 Fall Semester. He''s a good professor :)', 'kc', '42.3.172.218', '201505291033', 29, 5, 2015, '10:33', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-29 10:33:00'),
(149, 63, 'hello!!~~~I am coming~~', 'guang', '116.77.50.12', '201505291901', 29, 5, 2015, '19:01', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-29 19:01:00'),
(150, 63, 'Hi there! ', 'tiko', '1.36.224.30', '201505301032', 30, 5, 2015, '10:32', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-30 10:32:00'),
(151, 63, 'Hello!', 'ruisi chen', '183.178.51.182', '201505301120', 30, 5, 2015, '11:20', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-30 11:20:00'),
(152, 59, 'Very difficult course, do not choose his course!', 'ruisi chen', '183.178.51.182', '201505301128', 30, 5, 2015, '11:28', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-30 11:28:00'),
(153, 54, 'He is Department Head of Electronic and Computer Engineering at HKUST. Under his management , ECE department is significantly rising at rankings. ', 'xtang', '202.140.78.229', '201505301155', 30, 5, 2015, '11:55', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-30 11:55:00'),
(154, 71, 'Research Interests : Mobile Robotics, Mapping and Navigation, Computer Vision, Machine Learning, Non-parametric Modelling, Multi-robot System, Elderly-care robotics.', 'xtang', '202.140.78.214', '201505301156', 30, 5, 2015, '11:56', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-30 11:56:00'),
(155, 72, 'Professor Mok received his B.A.Sc., M.A.Sc. and Ph.D. degrees in Electrical and Computer Engineering  from the University of Toronto , Ontario , Canada , in 1986, 1989 and 1995, respectively.', 'admin', '202.140.72.90', '201505301158', 30, 5, 2015, '11:58', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-30 11:58:00'),
(156, 57, ' I think he must be really good engineer and more than an ordinary professor or a instructor. His teaching style is also different, he always try to make student look topic from an engineering perspective. Really really good prof, will miss him .', 'xtang', '202.140.72.34', '201505301202', 30, 5, 2015, '12:02', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-30 12:02:00'),
(157, 60, '(youtube: https://www.youtube.com/watch?v=vva5Q5kS6Is)', 'xtang', '202.140.78.211', '201505301204', 30, 5, 2015, '12:04', 0, '', '', '', '', 'silindi', 'xtang', '', '', '', '', '', '', 0, 0, '2015-05-30 12:04:00'),
(158, 60, '(youtube: https://www.youtube.com/watch?v=vva5Q5kS6Is)', 'xtang', '202.140.108.122', '201505310447', 31, 5, 2015, '04:47', 0, '', '', '', '', 'silindi', 'xtang', '', '', '', '', '', '', 0, 0, '2015-05-31 04:47:00'),
(159, 60, '(http://www.ust.hk/press_release_archive/eng/news/photos/20080327-595_1z.JPG)', 'xtang', '202.140.108.122', '201505310450', 31, 5, 2015, '04:50', 0, '', '', '', '', 'silindi', 'xtang', '', '', '', '', '', '', 0, 0, '2015-05-31 04:50:00'),
(160, 60, 'http://www.ust.hk/press_release_archive/eng/news/photos/20080327-595_1z.JPG', 'xtang', '202.140.101.38', '201505310451', 31, 5, 2015, '04:51', 0, '', '', '', '', 'silindi', 'xtang', '', '', '', '', '', '', 0, 0, '2015-05-31 04:51:00'),
(161, 61, 'He is giving Photonic Course. His teaching so intense and topic is so wide but he can teach details as well. Students should not miss due date for any homework or shouldn''t ask any extension, he is so strict about rules...Course is not easy too !!!', 'xtang', '202.140.108.122', '201505310500', 31, 5, 2015, '05:00', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-31 05:00:00'),
(162, 56, 'For CMOS lab need to spend so much time and start course project as soon as possible. Project cannot complete in two days unless you are taking design from someone ! Most important part of course is lab. Personally everyone like him :).', 'xtang', '202.140.108.55', '201505310505', 31, 5, 2015, '05:05', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-31 05:05:00'),
(163, 72, 'His course was easy :)... Hard to find easy course in UST...', 'xtang', '202.140.101.46', '201505310506', 31, 5, 2015, '05:06', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-31 05:06:00'),
(164, 58, 'His LCD Display course is like hell ! So difficult !! But he is so nice professor, I can say that he is pure scientist and author of almost all LCD books hahahaha :)', 'xtang', '202.140.108.64', '201505310509', 31, 5, 2015, '05:09', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-31 05:09:00'),
(165, 55, '(https://scholar.google.com.hk/citations?user=6WLhtHgAAAAJ&hl=en) This explain why he is dean.', 'xtang', '202.140.108.64', '201505310512', 31, 5, 2015, '05:12', 0, '', '', '', '', 'silindi', 'xtang', '', '', '', '', '', '', 0, 0, '2015-05-31 05:12:00'),
(166, 55, 'https://scholar.google.com.hk/citations?user=6WLhtHgAAAAJ&hl=en this explain why he is dean.', 'xtang', '202.140.108.107', '201505310513', 31, 5, 2015, '05:13', 0, '', '', '', '', 'silindi', 'xtang', '', '', '', '', '', '', 0, 0, '2015-05-31 05:13:00'),
(167, 55, '(https://scholar.google.com.hk/citations?user=6WLhtHgAAAAJ&hl=en)', 'xtang', '202.140.108.106', '201505310514', 31, 5, 2015, '05:14', 0, '', '', '', '', 'silindi', 'xtang', '', '', '', '', '', '', 0, 0, '2015-05-31 05:14:00'),
(168, 55, '\r<br>Citation indices	All	Since 2010\r<br>Citations	17737	9543\r<br>h-index	61	43\r<br>i10-index	228	154\r<br>\r<br>That is why he is dean.', 'xtang', '202.140.108.122', '201505310515', 31, 5, 2015, '05:15', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-31 05:15:00'),
(169, 72, '(image: http://pasaporti.blogspot.hk/2015/04/apple-logo.html)', 'xtang', '202.140.108.122', '201505310516', 31, 5, 2015, '05:16', 0, '', '', '', '', 'silindi', 'xtang', '', '', '', '', '', '', 0, 0, '2015-05-31 05:16:00'),
(170, 64, 'Best Place in UST. (image: https://www.easyuni.com/media/institution/photo/2013/12/13/HKUST_Swimming_Pool.jpg.1200x1200_q80_crop-smart.jpg)', 'xtang', '202.140.108.107', '201505310518', 31, 5, 2015, '05:18', 0, '', '', '', '', 'silindi', 'xtang', '', '', '', '', '', '', 0, 0, '2015-05-31 05:18:00');
INSERT INTO `mesajlar` (`id`, `sira`, `mesaj`, `yazar`, `ip`, `tarih`, `gun`, `ay`, `yil`, `saat`, `oy`, `update2`, `updatetarih`, `updater`, `updatesebep`, `statu`, `silen`, `silsebep`, `dava`, `eser`, `eserci`, `esertarih`, `nedeneser`, `kacartioy`, `kaceksioy`, `tarih2`) VALUES
(171, 64, '(image: https://www.easyuni.com/media/institution/photo/2013/12/13/HKUST_Swimming_Pool.jpg.1200x1200_q80_crop-smart.jpg)', 'xtang', '202.140.101.45', '201505310518', 31, 5, 2015, '05:18', 0, '', '', '', '', 'silindi', 'xtang', '', '', '', '', '', '', 0, 0, '2015-05-31 05:18:00'),
(172, 63, 'I like it :)', 'xtang', '202.140.101.45', '201505310519', 31, 5, 2015, '05:19', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-05-31 05:19:00'),
(173, 56, 'Prof. Bermak is an awesome professor more importantly he is an amazing person.He is actually from Algeria , not from France.', 'inferno', '157.14.237.118', '201506010812', 1, 6, 2015, '08:12', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-06-01 08:12:00'),
(174, 63, 'this is good, yet just some basic functions, is it gonna be an SNS? like MMUbee?', 'vincent', '183.62.147.34', '201507072250', 7, 7, 2015, '22:50', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2015-07-07 22:50:00');

-- --------------------------------------------------------

--
-- Table structure for table `online`
--

DROP TABLE IF EXISTS `online`;
CREATE TABLE IF NOT EXISTS `online` (
  `nick` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `islem_zamani` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `ip` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `ondurum` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  KEY `nick` (`nick`),
  KEY `islem_zamani` (`islem_zamani`),
  KEY `ip` (`ip`),
  KEY `ondurum` (`ondurum`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `online`
--

INSERT INTO `online` (`nick`, `islem_zamani`, `ip`, `ondurum`) VALUES
('vincent', '1436510099', '183.62.147.34', 'on');

-- --------------------------------------------------------

--
-- Table structure for table `oylar`
--

DROP TABLE IF EXISTS `oylar`;
CREATE TABLE IF NOT EXISTS `oylar` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `entry_id` bigint(11) NOT NULL DEFAULT '0',
  `nick` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `entry_sahibi` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `oy` bigint(11) NOT NULL DEFAULT '0',
  `gostert` int(1) NOT NULL,
  `tarih` datetime NOT NULL,
  `ip` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`),
  KEY `entry_id` (`entry_id`),
  KEY `nick` (`nick`),
  KEY `oy` (`oy`),
  KEY `entry_sahibi` (`entry_sahibi`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=7 ;

--
-- Dumping data for table `oylar`
--

INSERT INTO `oylar` (`id`, `entry_id`, `nick`, `entry_sahibi`, `oy`, `gostert`, `tarih`, `ip`) VALUES
(1, 147, 'admin', 'nuri', 0, 0, '2011-06-10 17:36:38', '127.0.0.1'),
(2, 165, 'nuri', 'admin', 0, 0, '2011-06-11 11:08:08', '127.0.0.1'),
(3, 166, 'nuri', 'admin', 1, 0, '2011-06-11 11:08:12', '127.0.0.1'),
(4, 22, 'admin', 'seyfi', 1, 0, '2012-03-14 20:20:51', '127.0.0.1'),
(5, 112, 'admin', 'allenwang', 1, 0, '2015-05-29 10:34:58', '14.136.248.10'),
(6, 173, 'admin', 'inferno', 1, 0, '2015-06-01 08:59:00', '14.136.248.10');

-- --------------------------------------------------------

--
-- Table structure for table `privmsg`
--

DROP TABLE IF EXISTS `privmsg`;
CREATE TABLE IF NOT EXISTS `privmsg` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `konu` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `mesaj` text COLLATE utf8_bin NOT NULL,
  `kime` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `gonderen` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `tarih` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `okundu` int(11) NOT NULL DEFAULT '0',
  `gun` int(11) NOT NULL DEFAULT '0',
  `ay` int(11) NOT NULL DEFAULT '0',
  `yil` int(11) NOT NULL DEFAULT '0',
  `saat` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `silkime` tinyint(1) NOT NULL DEFAULT '0',
  `silgonderen` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `saat` (`saat`),
  KEY `gonderen` (`gonderen`),
  KEY `gun` (`gun`),
  KEY `ay` (`ay`),
  KEY `yil` (`yil`),
  KEY `konu` (`konu`),
  KEY `tarih` (`tarih`),
  KEY `kime` (`kime`),
  KEY `okundu` (`okundu`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=67 ;

--
-- Dumping data for table `privmsg`
--

INSERT INTO `privmsg` (`id`, `konu`, `mesaj`, `kime`, `gonderen`, `tarih`, `okundu`, `gun`, `ay`, `yil`, `saat`, `silkime`, `silgonderen`) VALUES
(4, 'sözlüğe hoşgeldiniz!', '<br>	Merhabalar sam sam,<br>sözlüğe hoşgeldiniz. <br><br>şu anda sözlükte çaylak konumundasınız. bu konumdan çıkmak için\r<br>	10 tanım girmelisiniz. bu tanımlar yeterli görülürse yazarlığa yükseleceksiniz. şu aşamada tanımlarınız diğer yazarlar tarafından görülemez. \r<br>	sadece yöneticiler tanımlarınızı görebilecekler.<br><br>bir an önce sözlüğe ısınıp bize güzel tanımlar kazandıracağınıza \r<br>	inanıyoruz.<br><br>kalın sağlıcakla', 'admin', 'sözlük sistemi', '201106011703', 1, 1, 6, 2011, '17:03', 1, 0),
(6, 'sözlüğe hoşgeldiniz!', '<br>	Merhabalar dd dd,<br>sözlüğe hoşgeldiniz. <br><br>şu anda sözlükte çaylak konumundasınız. bu konumdan çıkmak için\r<br>	10 tanım girmelisiniz. bu tanımlar yeterli görülürse yazarlığa yükseleceksiniz. şu aşamada tanımlarınız diğer yazarlar tarafından görülemez. \r<br>	sadece yöneticiler tanımlarınızı görebilecekler.<br><br>bir an önce sözlüğe ısınıp bize güzel tanımlar kazandıracağınıza \r<br>	inanıyoruz.<br><br>kalın sağlıcakla', 'canimmanticekti microsoft kardes', 'sözlük sistemi', '201106021814', 1, 2, 6, 2011, '18:14', 0, 0),
(5, '', '', 'admin', '', '', 1, 0, 0, 0, '', 1, 0),
(7, '', '', 'canimmanticekti microsoft kardes', '', '', 1, 0, 0, 0, '', 0, 0),
(8, 'sözlüğe hoşgeldiniz!', '<br>	Merhabalar nuri,<br>sözlüğe hoşgeldiniz. <br><br>şu anda sözlükte çaylak konumundasınız. bu konumdan çıkmak için\r<br>	10 tanım girmelisiniz. bu tanımlar yeterli görülürse yazarlığa yükseleceksiniz. şu aşamada tanımlarınız diğer yazarlar tarafından görülemez. \r<br>	sadece yöneticiler tanımlarınızı görebilecekler.<br><br>bir an önce sözlüğe ısınıp bize güzel tanımlar kazandıracağınıza \r<br>	inanıyoruz.<br><br>kalın sağlıcakla', 'nuri', 'sözlük sistemi', '201106031500', 1, 3, 6, 2011, '15:00', 0, 0),
(9, '', '', 'nuri', '', '', 1, 0, 0, 0, '', 0, 0),
(10, 'sözlüğe hoşgeldiniz!', '<br>	Merhabalar sonny,<br>sözlüğe hoşgeldiniz. <br><br>şu anda sözlükte çaylak konumundasınız. bu konumdan çıkmak için\r<br>	10 tanım girmelisiniz. bu tanımlar yeterli görülürse yazarlığa yükseleceksiniz. şu aşamada tanımlarınız diğer yazarlar tarafından görülemez. \r<br>	sadece yöneticiler tanımlarınızı görebilecekler.<br><br>bir an önce sözlüğe ısınıp bize güzel tanımlar kazandıracağınıza \r<br>	inanıyoruz.<br><br>kalın sağlıcakla', 'sony', 'sözlük sistemi', '201106131413', 1, 13, 6, 2011, '14:13', 0, 0),
(11, 'sözlüğe hoşgeldiniz!', '<br>	Merhabalar gamgam,<br>sözlüğe hoşgeldiniz. <br><br>şu anda sözlükte çaylak konumundasınız. bu konumdan çıkmak için\r<br>	10 tanım girmelisiniz. bu tanımlar yeterli görülürse yazarlığa yükseleceksiniz. şu aşamada tanımlarınız diğer yazarlar tarafından görülemez. \r<br>	sadece yöneticiler tanımlarınızı görebilecekler.<br><br>bir an önce sözlüğe ısınıp bize güzel tanımlar kazandıracağınıza \r<br>	inanıyoruz.<br><br>kalın sağlıcakla', 'gammaz', 'sözlük sistemi', '201106141353', 1, 14, 6, 2011, '13:53', 0, 0),
(12, '', '', 'gammaz', '', '', 1, 0, 0, 0, '', 0, 0),
(13, 'sözlüğe hoşgeldiniz!', '<br>	Merhabalar sonny,<br>sözlüğe hoşgeldiniz. <br><br>şu anda sözlükte çaylak konumundasınız. bu konumdan çıkmak için\r<br>	10 tanım girmelisiniz. bu tanımlar yeterli görülürse yazarlığa yükseleceksiniz. şu aşamada tanımlarınız diğer yazarlar tarafından görülemez. \r<br>	sadece yöneticiler tanımlarınızı görebilecekler.<br><br>bir an önce sözlüğe ısınıp bize güzel tanımlar kazandıracağınıza \r<br>	inanıyoruz.<br><br>kalın sağlıcakla', 'admi\\''n', 'sözlük sistemi', '201106200841', 1, 20, 6, 2011, '08:41', 0, 0),
(14, 'sözlüğe hoşgeldiniz!', '<br>	Merhabalar fd,<br>sözlüğe hoşgeldiniz. <br><br>şu anda sözlükte çaylak konumundasınız. bu konumdan çıkmak için\r<br>	10 tanım girmelisiniz. bu tanımlar yeterli görülürse yazarlığa yükseleceksiniz. şu aşamada tanımlarınız diğer yazarlar tarafından görülemez. \r<br>	sadece yöneticiler tanımlarınızı görebilecekler.<br><br>bir an önce sözlüğe ısınıp bize güzel tanımlar kazandıracağınıza \r<br>	inanıyoruz.<br><br>kalın sağlıcakla', 'ad\\''min', 'sözlük sistemi', '201106200842', 1, 20, 6, 2011, '08:42', 0, 0),
(15, 'sözlüğe hoşgeldiniz!', '<br>	Merhabalar fsd,<br>sözlüğe hoşgeldiniz. <br><br>şu anda sözlükte çaylak konumundasınız. bu konumdan çıkmak için\r<br>	10 tanım girmelisiniz. bu tanımlar yeterli görülürse yazarlığa yükseleceksiniz. şu aşamada tanımlarınız diğer yazarlar tarafından görülemez. \r<br>	sadece yöneticiler tanımlarınızı görebilecekler.<br><br>bir an önce sözlüğe ısınıp bize güzel tanımlar kazandıracağınıza \r<br>	inanıyoruz.<br><br>kalın sağlıcakla', 'isma\\il', 'sözlük sistemi', '201106200850', 1, 20, 6, 2011, '08:50', 0, 0),
(16, 'sözlüğe hoşgeldiniz!', '<br>	Merhabalar isma,<br>sözlüğe hoşgeldiniz. <br><br>şu anda sözlükte çaylak konumundasınız. bu konumdan çıkmak için\r<br>	10 tanım girmelisiniz. bu tanımlar yeterli görülürse yazarlığa yükseleceksiniz. şu aşamada tanımlarınız diğer yazarlar tarafından görülemez. \r<br>	sadece yöneticiler tanımlarınızı görebilecekler.<br><br>bir an önce sözlüğe ısınıp bize güzel tanımlar kazandıracağınıza \r<br>	inanıyoruz.<br><br>kalın sağlıcakla', 'is\\mail', 'sözlük sistemi', '201106200853', 1, 20, 6, 2011, '08:53', 0, 0),
(17, 'sözlüğe hoşgeldiniz!', '<br>	Merhabalar dasdsa,<br>sözlüğe hoşgeldiniz. <br><br>şu anda sözlükte çaylak konumundasınız. bu konumdan çıkmak için\r<br>	10 tanım girmelisiniz. bu tanımlar yeterli görülürse yazarlığa yükseleceksiniz. şu aşamada tanımlarınız diğer yazarlar tarafından görülemez. \r<br>	sadece yöneticiler tanımlarınızı görebilecekler.<br><br>bir an önce sözlüğe ısınıp bize güzel tanımlar kazandıracağınıza \r<br>	inanıyoruz.<br><br>kalın sağlıcakla', 'ismail abi', 'sözlük sistemi', '201106200901', 1, 20, 6, 2011, '09:01', 0, 0),
(18, 'sözlüğe hoşgeldiniz!', '<br>	Merhabalar sam sam,<br>sözlüğe hoşgeldiniz. <br><br>şu anda sözlükte çaylak konumundasınız. bu konumdan çıkmak için\r<br>	10 tanım girmelisiniz. bu tanımlar yeterli görülürse yazarlığa yükseleceksiniz. şu aşamada tanımlarınız diğer yazarlar tarafından görülemez. \r<br>	sadece yöneticiler tanımlarınızı görebilecekler.<br><br>bir an önce sözlüğe ısınıp bize güzel tanımlar kazandıracağınıza \r<br>	inanıyoruz.<br><br>kalın sağlıcakla', 'ismail-abi', 'sözlük sistemi', '201106200905', 1, 20, 6, 2011, '09:05', 0, 0),
(19, 'sözlüğe hoşgeldiniz!', '<br>	Merhabalar dasdas,<br>sözlüğe hoşgeldiniz. <br><br>şu anda sözlükte çaylak konumundasınız. bu konumdan çıkmak için\r<br>	10 tanım girmelisiniz. bu tanımlar yeterli görülürse yazarlığa yükseleceksiniz. şu aşamada tanımlarınız diğer yazarlar tarafından görülemez. \r<br>	sadece yöneticiler tanımlarınızı görebilecekler.<br><br>bir an önce sözlüğe ısınıp bize güzel tanımlar kazandıracağınıza \r<br>	inanıyoruz.<br><br>kalın sağlıcakla', 'ismail_abi', 'sözlük sistemi', '201106200907', 1, 20, 6, 2011, '09:07', 0, 0),
(20, 'sözlüğe hoşgeldiniz!', '<br>	Merhabalar dsad,<br>sözlüğe hoşgeldiniz. <br><br>şu anda sözlükte çaylak konumundasınız. bu konumdan çıkmak için\r<br>	10 tanım girmelisiniz. bu tanımlar yeterli görülürse yazarlığa yükseleceksiniz. şu aşamada tanımlarınız diğer yazarlar tarafından görülemez. \r<br>	sadece yöneticiler tanımlarınızı görebilecekler.<br><br>bir an önce sözlüğe ısınıp bize güzel tanımlar kazandıracağınıza \r<br>	inanıyoruz.<br><br>kalın sağlıcakla', 'mecnun', 'sözlük sistemi', '201106200913', 1, 20, 6, 2011, '09:13', 0, 0),
(21, 'sözlüğe hoşgeldiniz!', '<br>	Merhabalar dasdas,<br>sözlüğe hoşgeldiniz. <br><br>şu anda sözlükte çaylak konumundasınız. bu konumdan çıkmak için\r<br>	10 tanım girmelisiniz. bu tanımlar yeterli görülürse yazarlığa yükseleceksiniz. şu aşamada tanımlarınız diğer yazarlar tarafından görülemez. \r<br>	sadece yöneticiler tanımlarınızı görebilecekler.<br><br>bir an önce sözlüğe ısınıp bize güzel tanımlar kazandıracağınıza \r<br>	inanıyoruz.<br><br>kalın sağlıcakla', 'sam\\et', 'sözlük sistemi', '201106200920', 1, 20, 6, 2011, '09:20', 0, 0),
(22, 'sözlüğe hoşgeldiniz!', '<br>	Merhabalar sam sam,<br>sözlüğe hoşgeldiniz. <br><br>şu anda sözlükte çaylak konumundasınız. bu konumdan çıkmak için\r<br>	10 tanım girmelisiniz. bu tanımlar yeterli görülürse yazarlığa yükseleceksiniz. şu aşamada tanımlarınız diğer yazarlar tarafından görülemez. \r<br>	sadece yöneticiler tanımlarınızı görebilecekler.<br><br>bir an önce sözlüğe ısınıp bize güzel tanımlar kazandıracağınıza \r<br>	inanıyoruz.<br><br>kalın sağlıcakla', 'samet sa', 'sözlük sistemi', '201106200923', 1, 20, 6, 2011, '09:23', 0, 0),
(23, 'sözlüğe hoşgeldiniz!', '<br>	Merhabalar ismailleylamecnun iskenderalifıratmehmet,<br>sözlüğe hoşgeldiniz. <br><br>şu anda sözlükte çaylak konumundasınız. bu konumdan çıkmak için\r<br>	10 tanım girmelisiniz. bu tanımlar yeterli görülürse yazarlığa yükseleceksiniz. şu aşamada tanımlarınız diğer yazarlar tarafından görülemez. \r<br>	sadece yöneticiler tanımlarınızı görebilecekler.<br><br>bir an önce sözlüğe ısınıp bize güzel tanımlar kazandıracağınıza \r<br>	inanıyoruz.<br><br>kalın sağlıcakla', 'domatesliboreklipasta cok guzel olmus', 'sözlük sistemi', '201107011319', 1, 1, 7, 2011, '13:19', 0, 0),
(24, 'sözlüğe hoşgeldiniz!', '<br>	Merhabalar denemnee,<br>sözlüğe hoşgeldiniz. <br><br>şu anda sözlükte çaylak konumundasınız. bu konumdan çıkmak için\r<br>	10 tanım girmelisiniz. bu tanımlar yeterli görülürse yazarlığa yükseleceksiniz. şu aşamada tanımlarınız diğer yazarlar tarafından görülemez. \r<br>	sadece yöneticiler tanımlarınızı görebilecekler.<br><br>bir an önce sözlüğe ısınıp bize güzel tanımlar kazandıracağınıza \r<br>	inanıyoruz.<br><br>kalın sağlıcakla', 'deneme', 'sözlük sistemi', '201107012054', 1, 1, 7, 2011, '20:54', 0, 0),
(25, 'sözlüğe hoşgeldiniz!', '<br>	Merhabalar abu,<br>sözlüğe hoşgeldiniz. <br><br>şu anda sözlükte çaylak konumundasınız. bu konumdan çıkmak için\r<br>	10 tanım girmelisiniz. bu tanımlar yeterli görülürse yazarlığa yükseleceksiniz. şu aşamada tanımlarınız diğer yazarlar tarafından görülemez. \r<br>	sadece yöneticiler tanımlarınızı görebilecekler.<br><br>bir an önce sözlüğe ısınıp bize güzel tanımlar kazandıracağınıza \r<br>	inanıyoruz.<br><br>kalın sağlıcakla', 'abu', 'sözlük sistemi', '201107231656', 1, 23, 7, 2011, '16:56', 0, 0),
(26, 'sözlüğe hoşgeldiniz!', '<br>	Merhabalar ads,<br>sözlüğe hoşgeldiniz. <br><br>şu anda sözlükte çaylak konumundasınız. bu konumdan çıkmak için\r<br>	10 tanım girmelisiniz. bu tanımlar yeterli görülürse yazarlığa yükseleceksiniz. şu aşamada tanımlarınız diğer yazarlar tarafından görülemez. \r<br>	sadece yöneticiler tanımlarınızı görebilecekler.<br><br>bir an önce sözlüğe ısınıp bize güzel tanımlar kazandıracağınıza \r<br>	inanıyoruz.<br><br>kalın sağlıcakla', 'abu2', 'sözlük sistemi', '201107231706', 1, 23, 7, 2011, '17:06', 0, 0),
(27, 'sözlüğe hoşgeldiniz!', '<br>	Merhabalar sada,<br>sözlüğe hoşgeldiniz. <br><br>şu anda sözlükte çaylak konumundasınız. bu konumdan çıkmak için\r<br>	10 tanım girmelisiniz. bu tanımlar yeterli görülürse yazarlığa yükseleceksiniz. şu aşamada tanımlarınız diğer yazarlar tarafından görülemez. \r<br>	sadece yöneticiler tanımlarınızı görebilecekler.<br><br>bir an önce sözlüğe ısınıp bize güzel tanımlar kazandıracağınıza \r<br>	inanıyoruz.<br><br>kalın sağlıcakla', 'abu3', 'sözlük sistemi', '201107231715', 1, 23, 7, 2011, '17:15', 0, 0),
(28, 'sözlüğe hoşgeldiniz!', '<br>	Merhabalar asda,<br>sözlüğe hoşgeldiniz. <br><br>şu anda sözlükte çaylak konumundasınız. bu konumdan çıkmak için\r<br>	10 tanım girmelisiniz. bu tanımlar yeterli görülürse yazarlığa yükseleceksiniz. şu aşamada tanımlarınız diğer yazarlar tarafından görülemez. \r<br>	sadece yöneticiler tanımlarınızı görebilecekler.<br><br>bir an önce sözlüğe ısınıp bize güzel tanımlar kazandıracağınıza \r<br>	inanıyoruz.<br><br>kalın sağlıcakla', 'abu4', 'sözlük sistemi', '201107231718', 1, 23, 7, 2011, '17:18', 0, 0),
(29, 'sözlüğe hoşgeldiniz!', '<br>	Merhabalar hansolooo,<br>sözlüğe hoşgeldiniz. <br><br>şu anda sözlükte çaylak konumundasınız. bu konumdan çıkmak için\r<br>	10 tanım girmelisiniz. bu tanımlar yeterli görülürse yazarlığa yükseleceksiniz. şu aşamada tanımlarınız diğer yazarlar tarafından görülemez. \r<br>	sadece yöneticiler tanımlarınızı görebilecekler.<br><br>bir an önce sözlüğe ısınıp bize güzel tanımlar kazandıracağınıza \r<br>	inanıyoruz.<br><br>kalın sağlıcakla', 'han solo', 'sözlük sistemi', '201107291604', 1, 29, 7, 2011, '16:04', 0, 0),
(30, '&#1585; &#1571;&#1610; &#1588;&#1610;&#1569; &#1604;&#1571;&#1606;&#1606;&#1575; &#1604;&#1575; &#1606;&#1606;&#1578;&#1607;&#1603; &#1571;&#1610; &#1575;&#1578;&#1601;', '&#1585; &#1571;&#1610; &#1588;&#1610;&#1569; &#1604;&#1571;&#1606;&#1606;&#1575; &#1604;&#1575; &#1606;&#1606;&#1578;&#1607;&#1603; &#1571;&#1610; &#1575;&#1578;&#1601;', 'admin', 'admin', '201201192119', 0, 19, 1, 2012, '21:19', 1, 0),
(31, 'sözlüğe hoşgeldiniz!', '<br>	Merhabalar 4,<br>sözlüğe hoşgeldiniz. <br><br>şu anda sözlükte çaylak konumundasınız. bu konumdan çıkmak için\r<br>	10 tanım girmelisiniz. bu tanımlar yeterli görülürse yazarlığa yükseleceksiniz. şu aşamada tanımlarınız diğer yazarlar tarafından görülemez. \r<br>	sadece yöneticiler tanımlarınızı görebilecekler.<br><br>bir an önce sözlüğe ısınıp bize güzel tanımlar kazandıracağınıza \r<br>	inanıyoruz.<br><br>kalın sağlıcakla', '4', 'sözlük sistemi', '201201211604', 1, 21, 1, 2012, '16:04', 0, 0),
(32, 'sözlüğe hoşgeldiniz!', '<br>	Merhabalar 3,<br>sözlüğe hoşgeldiniz. <br><br>şu anda sözlükte çaylak konumundasınız. bu konumdan çıkmak için\r<br>	10 tanım girmelisiniz. bu tanımlar yeterli görülürse yazarlığa yükseleceksiniz. şu aşamada tanımlarınız diğer yazarlar tarafından görülemez. \r<br>	sadece yöneticiler tanımlarınızı görebilecekler.<br><br>bir an önce sözlüğe ısınıp bize güzel tanımlar kazandıracağınıza \r<br>	inanıyoruz.<br><br>kalın sağlıcakla', '3', 'sözlük sistemi', '201201211604', 1, 21, 1, 2012, '16:04', 0, 0),
(33, 'sözlüğe hoşgeldiniz!', '<br>		Merhabalar samsama,<br>sözlüğe hoşgeldiniz. <br><br>şu anda sözlükte çaylak konumundasınız. bu konumdan çıkmak için<br>	10 tanım girmelisiniz. bu tanımlar yeterli görülürse yazarlığa yükseleceksiniz. şu aşamada tanımlarınız diğer yazarlar tarafından görülemez. <br>	sadece yöneticiler tanımlarınızı görebilecekler.<br><br>bir an önce sözlüğe ısınıp bize güzel tanımlar kazandıracağınıza <br>	inanıyoruz.<br><br>kalın sağlıcakla', 'hansolo', 'sözlük sistemi', '201201301035', 1, 30, 1, 2012, '10:35', 0, 0),
(47, 'أيك" و', 'أيك" و', 'hansolo', 'hansolo', '201201301330', 0, 30, 1, 2012, '13:30', 0, 0),
(46, '', '', 'hansolo', 'hansolo', '201201301313', 1, 30, 1, 2012, '13:13', 0, 0),
(45, '', '', 'hansolo', 'hansolo', '201201301308', 0, 30, 1, 2012, '13:08', 0, 0),
(44, '92512 ', '', 'hansolo', 'admin', '201201301302', 0, 30, 1, 2012, '13:02', 0, 0),
(43, 'sadas  ', 'sdas ', 'hansolo', 'admin', '201201301134', 0, 30, 1, 2012, '11:34', 0, 0),
(48, 'welcome to MafiMushkila!', '<br>		welcome to MafiMushkila. <br><br>your status is rookie. You have to write 10 entries in order to be nominated as a writer. On the nomination period, your entries will be watched by our moderators and depending on the volume and quality you may promote to be a MafiMushkila writer. <br><br>Enjoy your time while roaming around MafiMushkila. Don’t be shy to write entries all around the site. MafiMushkila is as efficent as your contributions. <br><br>Take Care!', 'aq', 'Mafi Mushkila', '201203011911', 1, 1, 3, 2012, '19:11', 0, 0),
(49, 'welcome to MafiMushkila!', '<br>		welcome to MafiMushkila. <br><br>your status is rookie. You have to write 10 entries in order to be nominated as a writer. On the nomination period, your entries will be watched by our moderators and depending on the volume and quality you may promote to be a MafiMushkila writer. <br><br>Enjoy your time while roaming around MafiMushkila. Don’t be shy to write entries all around the site. MafiMushkila is as efficent as your contributions. <br><br>Take Care!', 'vader', 'Mafi Mushkila', '201203021800', 1, 2, 3, 2012, '18:00', 0, 0),
(50, 'welcome to MafiMushkila!', '<br>		welcome to MafiMushkila. <br><br>your status is rookie. You have to write 10 entries in order to be nominated as a writer. On the nomination period, your entries will be watched by our moderators and depending on the volume and quality you may promote to be a MafiMushkila writer. <br><br>Enjoy your time while roaming around MafiMushkila. Don’t be shy to write entries all around the site. MafiMushkila is as efficent as your contributions. <br><br>Take Care!', 'dsd', 'Mafi Mushkila', '201203021927', 1, 2, 3, 2012, '19:27', 0, 0),
(51, 'welcome to MafiMushkila!', '<br>		welcome to MafiMushkila. <br><br>your status is rookie. You have to write 10 entries in order to be nominated as a writer. On the nomination period, your entries will be watched by our moderators and depending on the volume and quality you may promote to be a MafiMushkila writer. <br><br>Enjoy your time while roaming around MafiMushkila. Don’t be shy to write entries all around the site. MafiMushkila is as efficent as your contributions. <br><br>Take Care!', 'ali', 'Mafi Mushkila', '201203021927', 1, 2, 3, 2012, '19:27', 0, 0),
(52, 'welcome to MafiMushkila!', '<br>		welcome to MafiMushkila. <br><br>your status is rookie. You have to write 10 entries in order to be nominated as a writer. On the nomination period, your entries will be watched by our moderators and depending on the volume and quality you may promote to be a MafiMushkila writer. <br><br>Enjoy your time while roaming around MafiMushkila. Don’t be shy to write entries all around the site. MafiMushkila is as efficent as your contributions. <br><br>Take Care!', 'seyfi', 'Mafi Mushkila', '201203091927', 1, 9, 3, 2012, '19:27', 0, 0),
(53, '', '', 'seyfi', '', '', 1, 0, 0, 0, '', 0, 0),
(54, 'welcome to MafiMushkila!', '<br>		welcome to MafiMushkila. <br><br>your status is rookie. You have to write 10 entries in order to be nominated as a writer. On the nomination period, your entries will be watched by our moderators and depending on the volume and quality you may promote to be a MafiMushkila writer. <br><br>Enjoy your time while roaming around MafiMushkila. Don’t be shy to write entries all around the site. MafiMushkila is as efficent as your contributions. <br><br>Take Care!', 'lol', 'Mafi Mushkila', '201203151708', 1, 15, 3, 2012, '17:08', 0, 0),
(55, 'ر أي شيء لأننا لا ننتهك أي اتف', '\r<br>\r<br>gfd\r<br>-- previous message --------------\r<br>from: admin\r<br>receiver: admin\r<br>date: 19/1/2012 21:19\r<br>subject: ر أي شيء لأننا لا ننتهك أي اتف\r<br>ر أي شيء لأننا لا ننتهك أي اتف\r<br>----------------------------------', 'admin', 'admin', '201203262015', 1, 26, 3, 2012, '20:15', 1, 0),
(56, '3', '3', 'admin', 'admin', '201203262017', 1, 26, 3, 2012, '20:17', 0, 0),
(57, 're3r', 're', 'admin', 'admin', '201203262018', 1, 26, 3, 2012, '20:18', 0, 0),
(58, '', '', 'vader', '', '', 1, 0, 0, 0, '', 0, 0),
(59, 'welcome to MafiMushkila!', '<br>		welcome to MafiMushkila. <br><br>your status is rookie. You have to write 10 entries in order to be nominated as a writer. On the nomination period, your entries will be watched by our moderators and depending on the volume and quality you may promote to be a MafiMushkila writer. <br><br>Enjoy your time while roaming around MafiMushkila. Don’t be shy to write entries all around the site. MafiMushkila is as efficent as your contributions. <br><br>Take Care!', 'yoda', 'Mafi Mushkila', '201204021314', 1, 2, 4, 2012, '13:14', 0, 0),
(60, '', '', 'yoda', '', '', 1, 0, 0, 0, '', 0, 0),
(61, 'welcome to MafiMushkila!', '<br>		welcome to MafiMushkila. <br><br>your status is rookie. You have to write 10 entries in order to be nominated as a writer. On the nomination period, your entries will be watched by our moderators and depending on the volume and quality you may promote to be a MafiMushkila writer. <br><br>Enjoy your time while roaming around MafiMushkila. Don’t be shy to write entries all around the site. MafiMushkila is as efficent as your contributions. <br><br>Take Care!', 'baris', 'One Sourcer', '201504240524', 1, 24, 4, 2015, '05:24', 0, 0),
(62, 'welcome to Walltyper!', '<br>		welcome to Walltyper. <br><br>your status is rookie. You have to write 10 entries in order to be nominated as a writer. On the nomination period, your entries will be watched by our moderators and depending on the volume and quality you may promote to be a Walltyper writer. <br><br>Enjoy your time while roaming around Waltyper. Don’t be shy to write entries all around the site. Walltyper is as efficent as your contributions. <br><br>Take Care!', 'shing', 'Walltyper', '201505250741', 1, 25, 5, 2015, '07:41', 0, 0),
(63, 'welcome to Walltyper!', '<br>		welcome to Walltyper. <br><br>your status is rookie. You have to write 10 entries in order to be nominated as a writer. On the nomination period, your entries will be watched by our moderators and depending on the volume and quality you may promote to be a Walltyper writer. <br><br>Enjoy your time while roaming around Waltyper. Don’t be shy to write entries all around the site. Walltyper is as efficent as your contributions. <br><br>Take Care!', 'allenwang', 'Walltyper', '201505260825', 1, 26, 5, 2015, '08:25', 0, 0),
(64, 'welcome to Walltyper!', '<br>		welcome to Walltyper. <br><br>your status is rookie. You have to write 10 entries in order to be nominated as a writer. On the nomination period, your entries will be watched by our moderators and depending on the volume and quality you may promote to be a Walltyper writer. <br><br>Enjoy your time while roaming around Waltyper. Don’t be shy to write entries all around the site. Walltyper is as efficent as your contributions. <br><br>Take Care!', 'kelvin caleb', 'Walltyper', '201505260835', 1, 26, 5, 2015, '08:35', 0, 0),
(65, 'welcome to Walltyper!', '<br>		welcome to Walltyper. <br><br>your status is rookie. You have to write 10 entries in order to be nominated as a writer. On the nomination period, your entries will be watched by our moderators and depending on the volume and quality you may promote to be a Walltyper writer. <br><br>Enjoy your time while roaming around Waltyper. Don’t be shy to write entries all around the site. Walltyper is as efficent as your contributions. <br><br>Take Care!', 'allen xu', 'Walltyper', '201505260846', 1, 26, 5, 2015, '08:46', 0, 0),
(66, 'hi dude', 'hi dude \r<br>\r<br>this is a test message.\r<br>\r<br>cheers\r<br>baris', 'shing', 'admin', '201505290911', 2, 29, 5, 2015, '09:11', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `rehber`
--

DROP TABLE IF EXISTS `rehber`;
CREATE TABLE IF NOT EXISTS `rehber` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `kim` varchar(250) COLLATE utf8_bin NOT NULL DEFAULT '',
  `kimin` varchar(250) COLLATE utf8_bin NOT NULL DEFAULT '',
  `num` char(1) COLLATE utf8_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=4 ;

--
-- Dumping data for table `rehber`
--

INSERT INTO `rehber` (`id`, `kim`, `kimin`, `num`) VALUES
(2, 'admin', 'hansolo', '1'),
(3, 'admin', 'vader', '0');

-- --------------------------------------------------------

--
-- Table structure for table `reklam_bannerlar`
--

DROP TABLE IF EXISTS `reklam_bannerlar`;
CREATE TABLE IF NOT EXISTS `reklam_bannerlar` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `status` enum('ok','expired') COLLATE utf8_bin DEFAULT NULL,
  `title` varchar(80) COLLATE utf8_bin DEFAULT NULL,
  `url` varchar(160) COLLATE utf8_bin NOT NULL DEFAULT '',
  `image_url` varchar(160) COLLATE utf8_bin NOT NULL DEFAULT '',
  `alt_text` text COLLATE utf8_bin,
  `banner_group` varchar(80) COLLATE utf8_bin DEFAULT NULL,
  `client_id` bigint(11) NOT NULL DEFAULT '0',
  `width` varchar(5) COLLATE utf8_bin DEFAULT NULL,
  `height` varchar(5) COLLATE utf8_bin DEFAULT NULL,
  `expiration_date` date DEFAULT NULL,
  `expiration_displays` bigint(10) unsigned DEFAULT NULL,
  `expiration_clicks` bigint(10) unsigned DEFAULT NULL,
  `displays_life` bigint(20) unsigned NOT NULL DEFAULT '0',
  `displays_day` bigint(10) unsigned NOT NULL DEFAULT '0',
  `clicks_life` bigint(20) unsigned NOT NULL DEFAULT '0',
  `clicks_day` bigint(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=3 ;

--
-- Dumping data for table `reklam_bannerlar`
--

INSERT INTO `reklam_bannerlar` (`id`, `status`, `title`, `url`, `image_url`, `alt_text`, `banner_group`, `client_id`, `width`, `height`, `expiration_date`, `expiration_displays`, `expiration_clicks`, `displays_life`, `displays_day`, `clicks_life`, `clicks_day`) VALUES
(2, 'ok', 'AAA', 'http://www.sozluksistemi.com', 'http://www.sozluksistemi.com/css/images/sozluk.jpg', '', '', 1, '', '', '0000-00-00', 0, 0, 2573, 2573, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `reklam_musteriler`
--

DROP TABLE IF EXISTS `reklam_musteriler`;
CREATE TABLE IF NOT EXISTS `reklam_musteriler` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) COLLATE utf8_bin DEFAULT NULL,
  `email` varchar(80) COLLATE utf8_bin DEFAULT NULL,
  `login` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `passwd` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `info` text COLLATE utf8_bin,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=2 ;

--
-- Dumping data for table `reklam_musteriler`
--

INSERT INTO `reklam_musteriler` (`id`, `name`, `email`, `login`, `passwd`, `info`) VALUES
(1, 'TEST', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `reklam_pozisyonlar`
--

DROP TABLE IF EXISTS `reklam_pozisyonlar`;
CREATE TABLE IF NOT EXISTS `reklam_pozisyonlar` (
  `title` char(80) COLLATE utf8_bin NOT NULL DEFAULT '',
  `position` bigint(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`title`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `reklam_pozisyonlar`
--

INSERT INTO `reklam_pozisyonlar` (`title`, `position`) VALUES
('', 0);

-- --------------------------------------------------------

--
-- Table structure for table `reklamlar`
--

DROP TABLE IF EXISTS `reklamlar`;
CREATE TABLE IF NOT EXISTS `reklamlar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `target_url` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `image_url` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `reklam_source` text COLLATE utf8_bin,
  `isactive` bit(1) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `position` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=8 ;

--
-- Dumping data for table `reklamlar`
--

INSERT INTO `reklamlar` (`id`, `name`, `target_url`, `image_url`, `reklam_source`, `isactive`, `date`, `position`) VALUES
(4, 'yy///yyyy//yyyy/yy', 'http://www.ece.ust.hk/ece.php', 'http://s22.postimg.org/auxvb11e7/ezgif_com_gif_maker.gif', '', b'1', '2012-03-17 10:32:58', 'top'),
(5, 'dddd', 'http://www.ece.ust.hk/ece.php', 'http://s22.postimg.org/auxvb11e7/ezgif_com_gif_maker.gif', '', b'1', '2012-03-17 10:33:15', 'top'),
(6, '\\\\9ıkık', 'http://www.ece.ust.hk/ece.php', 'http://s22.postimg.org/auxvb11e7/ezgif_com_gif_maker.gif', '', b'1', '2012-03-17 10:48:15', 'top'),
(7, 'lokkjkj', 'http://www.ece.ust.hk/ece.php', 'http://gifmaker.cc/PlayFrameAnimation.php?folder=2015052422rTb2z8CRTC1517KsWXSphEhttp://s22.postimg.org/auxvb11e7/ezgif_com_gif_maker.gif', '', b'1', '2012-03-17 10:48:31', 'top');

-- --------------------------------------------------------

--
-- Table structure for table `sikayet`
--

DROP TABLE IF EXISTS `sikayet`;
CREATE TABLE IF NOT EXISTS `sikayet` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `sikayetci` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `zanli` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `sebep` text COLLATE utf8_bin NOT NULL,
  `tarih` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `statu` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=2 ;

--
-- Dumping data for table `sikayet`
--

INSERT INTO `sikayet` (`id`, `sikayetci`, `zanli`, `sebep`, `tarih`, `statu`) VALUES
(1, 'admin', 'hansolo', 'hgf', '27/02/2012 11:11', '');

-- --------------------------------------------------------

--
-- Table structure for table `sorular`
--

DROP TABLE IF EXISTS `sorular`;
CREATE TABLE IF NOT EXISTS `sorular` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `baslik` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `id_2` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `stat`
--

DROP TABLE IF EXISTS `stat`;
CREATE TABLE IF NOT EXISTS `stat` (
  `baslik` text COLLATE utf8_bin NOT NULL,
  `entry` text COLLATE utf8_bin NOT NULL,
  `silbaslik` text COLLATE utf8_bin NOT NULL,
  `silentry` text COLLATE utf8_bin NOT NULL,
  `hit` text COLLATE utf8_bin NOT NULL,
  `tekil` text COLLATE utf8_bin NOT NULL,
  `yazar` text COLLATE utf8_bin NOT NULL,
  `okur` text COLLATE utf8_bin NOT NULL,
  `moderat` text COLLATE utf8_bin NOT NULL,
  `op` text COLLATE utf8_bin NOT NULL,
  `admin` text COLLATE utf8_bin NOT NULL,
  `ortbaslik` text COLLATE utf8_bin NOT NULL,
  `ortentry` text COLLATE utf8_bin NOT NULL,
  `tarih` text COLLATE utf8_bin NOT NULL,
  `enhitbaslik` text COLLATE utf8_bin NOT NULL,
  `gun` text COLLATE utf8_bin NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `stat`
--

INSERT INTO `stat` (`baslik`, `entry`, `silbaslik`, `silentry`, `hit`, `tekil`, `yazar`, `okur`, `moderat`, `op`, `admin`, `ortbaslik`, `ortentry`, `tarih`, `enhitbaslik`, `gun`) VALUES
('67', '132', '0', '11', '380', '342', '8', '0', '0', '', '2', '9', '17', '27/05/2015 09:18', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `statmonth`
--

DROP TABLE IF EXISTS `statmonth`;
CREATE TABLE IF NOT EXISTS `statmonth` (
  `baslik` text COLLATE utf8_bin NOT NULL,
  `entry` text COLLATE utf8_bin NOT NULL,
  `silbaslik` text COLLATE utf8_bin NOT NULL,
  `silentry` text COLLATE utf8_bin NOT NULL,
  `hit` text COLLATE utf8_bin NOT NULL,
  `tekil` text COLLATE utf8_bin NOT NULL,
  `ortbaslik` text COLLATE utf8_bin NOT NULL,
  `ortentry` text COLLATE utf8_bin NOT NULL,
  `tarih` text COLLATE utf8_bin NOT NULL,
  `enhitbaslik` text COLLATE utf8_bin NOT NULL,
  `gun` text COLLATE utf8_bin NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `temalar`
--

DROP TABLE IF EXISTS `temalar`;
CREATE TABLE IF NOT EXISTS `temalar` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `tema` text COLLATE utf8_bin NOT NULL,
  `kimyapmis` text COLLATE utf8_bin NOT NULL,
  `tarih` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `id_2` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=18 ;

--
-- Dumping data for table `temalar`
--

INSERT INTO `temalar` (`id`, `tema`, `kimyapmis`, `tarih`) VALUES
(17, 'new', 'admin', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `ukte`
--

DROP TABLE IF EXISTS `ukte`;
CREATE TABLE IF NOT EXISTS `ukte` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `baslik` text COLLATE utf8_bin NOT NULL,
  `aciklama` text COLLATE utf8_bin NOT NULL,
  `yazar` text COLLATE utf8_bin NOT NULL,
  `tarih` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=8 ;

--
-- Dumping data for table `ukte`
--

INSERT INTO `ukte` (`id`, `baslik`, `aciklama`, `yazar`, `tarih`) VALUES
(4, 'rrr4', 'rrrrr4', 'admin', '201203191330'),
(5, 'lol', 'lol', 'lol', ''),
(6, 'r3r34ds', 'rwr4r4', 'admin', '201203191338'),
(7, 'r3r34dsr43', 'fsdffe', 'admin', '201203191339');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `isim` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `nick` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `sifre` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `yetki` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `email` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `durum` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `dt` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `cinsiyet` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `sehir` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `regip` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `regtarih` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `online` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `tema` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `kacentry` bigint(30) NOT NULL DEFAULT '0',
  `sayfaTanim` bigint(8) NOT NULL DEFAULT '0',
  `olay_tarih` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `son_online` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `cezasayisi` bigint(10) NOT NULL DEFAULT '0',
  `cezatarihi` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `cezasebep` text COLLATE utf8_bin NOT NULL,
  `ileti` text COLLATE utf8_bin NOT NULL,
  `ileti_time` datetime NOT NULL,
  `referans` int(10) DEFAULT NULL,
  `credate` datetime NOT NULL,
  `chatSonOnline` datetime NOT NULL,
  `ip` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `facebook` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `twitter` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `description` varchar(280) COLLATE utf8_bin DEFAULT NULL,
  `dil` varchar(5) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`),
  KEY `dt` (`dt`),
  KEY `tema` (`tema`),
  KEY `sifre` (`sifre`),
  KEY `isim` (`isim`),
  KEY `cinsiyet` (`cinsiyet`),
  KEY `email` (`email`),
  KEY `kacentry` (`kacentry`),
  KEY `nick` (`nick`),
  KEY `durum` (`durum`),
  KEY `yetki` (`yetki`),
  FULLTEXT KEY `isim_2` (`isim`),
  FULLTEXT KEY `isim_3` (`isim`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=55 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `isim`, `nick`, `sifre`, `yetki`, `email`, `durum`, `dt`, `cinsiyet`, `sehir`, `regip`, `regtarih`, `online`, `tema`, `kacentry`, `sayfaTanim`, `olay_tarih`, `son_online`, `cezasayisi`, `cezatarihi`, `cezasebep`, `ileti`, `ileti_time`, `referans`, `credate`, `chatSonOnline`, `ip`, `facebook`, `twitter`, `description`, `dil`) VALUES
(4, 'sam sam', 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'admin', 'barissonmezee@gmail.com', 'on', '371975', 'm', '', '127.0.0.1', '2011/06/01 17:03', '', 'new', 301, 25, '201505291147', '201506020816', 0, '', '', 'Hi all', '2011-06-09 18:42:00', 0, '2011-06-01 17:03:00', '0000-00-00 00:00:00', NULL, '', '', ' ', 'en'),
(41, 'Zhang', 'james zhang', 'd7a75177d41b660dfbe6f135f70a9f00467a8181', 'user', 'zhangzhonghan007@gmail.com', 'on', '28/11/1991', 'm', 'HongKong ', '219.77.2.103', '2015/05/26 10:38', '', 'new', 0, 0, '', '', 0, '', '', '', '0000-00-00 00:00:00', 0, '2015-05-26 10:38:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, ''),
(42, 'LI Siqi', 'lisiqi', '793930e372a703ab4f66287214b8976c585ce786', 'user', 'cathylihk@hotmail.com', 'on', '28/7/1991', 'f', 'Hong Kong', '14.136.245.193', '2015/05/27 00:00', '', 'new', 0, 0, '', '201505270001', 0, '', '', '', '0000-00-00 00:00:00', 0, '2015-05-27 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, ''),
(43, 'Na be', 'xtang', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'user', 'fdfafs@cv.com', 'on', '1/3/1992', 'm', 'HK', '14.136.248.10', '2015/05/27 09:24', '', 'new', 11, 0, '201505310309', '201505310506', 0, '', '', '', '0000-00-00 00:00:00', 0, '2015-05-27 09:24:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, ''),
(44, 'LIU Tongyu', 'swlty', '6804eeff1f19f4e1c80bf7223524b0525cb4484d', 'user', 'tliuak@connect.ust.hk', 'on', '28/10/1991', 'f', 'Hongkong', '202.40.139.131', '2015/05/27 09:29', '', 'new', 0, 0, '', '201505270947', 0, '', '', '', '0000-00-00 00:00:00', 0, '2015-05-27 09:29:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, ''),
(40, 'xu', 'allen xu', '57937c290cb836de266c76169e297dcc93328b7c', 'user', '503808684@qq.com', 'on', '2/12/1991', 'm', '', '14.18.29.105', '2015/05/26 08:46', '', 'new', 2, 0, '', '201505260848', 0, '', '', '', '0000-00-00 00:00:00', 0, '2015-05-26 08:46:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, ''),
(38, 'Ruiqiao Wang', 'allenwang', '6efc0026b0ccaa699f8e132940d8ed7e02f5e1cb', 'user', 'wangrq068@163.com', 'on', '8/6/1990', 'm', '', '221.221.234.29', '2015/05/26 08:25', '', 'new', 14, 0, '201505260835', '201505260923', 0, '', '', '', '0000-00-00 00:00:00', 0, '2015-05-26 08:25:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'en'),
(39, 'Xiao ', 'kelvin caleb', '78cfaa23254ba41019f735e2ee31755f7109135e', 'user', '654778790@qq.com', 'on', '11/8/1991', 'm', 'hk', '220.246.201.171', '2015/05/26 08:35', '', 'new', 2, 0, '', '201505271228', 0, '', '', '', '0000-00-00 00:00:00', 0, '2015-05-26 08:35:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, ''),
(36, 'Baris sonmez', 'baris', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'admin', 'barissonmezee@yahoo.com.tr', 'on', '6/5/1987', 'm', '', '202.40.139.130', '2015/04/24 05:24', '0', 'new', 3, 0, '201504240748', '201505260904', 0, '', '', '', '0000-00-00 00:00:00', 0, '2015-04-24 05:24:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, ''),
(37, 'Hok Shing Chan', 'shing', '74d546c1ddff577f284faa14b9368cef2ad7c060', 'user', 'hshingc@gmail.com', 'on', '21/6/1985', 'm', 'HK', '1.36.81.152', '2015/05/25 07:41', '', 'new', 5, 0, '201505250747', '201505260939', 0, '', '', '', '0000-00-00 00:00:00', 0, '2015-05-25 07:41:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'en'),
(45, 'HWANG', 'denny', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'user', 'jaych.2008@163.com', 'on', '15/11/1990', 'm', 'HK', '203.218.58.116', '2015/05/27 21:07', '', 'new', 1, 0, '', '201505272109', 0, '', '', '', '0000-00-00 00:00:00', 0, '2015-05-27 21:07:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, ''),
(46, 'zmy07007', 'zmy07007', 'b909ef0821f88f8e68f65ecf08576c29111a2070', 'user', 'zmy07007@126.com', 'on', '2/6/1992', 'f', 'hongkong', '218.102.199.141', '2015/05/29 09:10', '', 'new', 5, 0, '201505290919', '201505290911', 0, '', '', '', '0000-00-00 00:00:00', 0, '2015-05-29 09:10:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, ''),
(47, 'Kelvin Cheung', 'kc', '6392ad9f50930aae75f73d5622f131700fb63fd1', 'user', 'whcheungah@connect.ust.hk', 'on', '4/8/1993', 'm', 'Hong Kong', '42.3.172.218', '2015/05/29 10:30', '', 'new', 1, 0, '', '201505291030', 0, '', '', '', '0000-00-00 00:00:00', 0, '2015-05-29 10:30:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, ''),
(48, 'Allen Guang', 'guang', '7c222fb2927d828af22f592134e8932480637c0d', 'user', 'feiboguang@sina.com', 'on', '12/8/1990', 'm', 'ShenZhen', '116.77.50.12', '2015/05/29 18:51', '', 'new', 1, 0, '201506250248', '201506250248', 0, '', '', '', '0000-00-00 00:00:00', 0, '2015-05-29 18:51:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'en'),
(49, 'Farhan', 'amftm', '9c557960f329c3ff91e1c3a82daa912dfbe12169', 'user', 'musthafafarhan@gmail.com', 'on', '30/10/1989', 'm', 'kowloon', '202.40.139.131', '2015/05/30 09:22', '', 'new', 0, 0, '', '', 0, '', '', '', '0000-00-00 00:00:00', 0, '2015-05-30 09:22:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, ''),
(50, 'LONG Tinggao', 'tiko', '71f5430e2a803d4a0daf9130a54a839ea01be5a0', 'user', 'tinggao.long@gmail.com', 'on', '4/9/1990', 'm', 'HK', '1.36.224.30', '2015/05/30 10:31', '', 'new', 1, 0, '', '201505301032', 0, '', '', '', '0000-00-00 00:00:00', 0, '2015-05-30 10:31:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, ''),
(51, 'Ruisi Chen', 'ruisi chen', '3280b417d2a0b78329fbcd55115ed7d6aa5c460f', 'user', 'chenruisi123@outlook.com', 'on', '29/1/1992', 'm', 'Hong Kong', '183.178.51.182', '2015/05/30 11:12', '', 'new', 2, 0, '201505301139', '201505301135', 0, '', '', '', '0000-00-00 00:00:00', 0, '2015-05-30 11:12:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, ''),
(52, 'Naseer Ahmed Kamal', 'naseer ahmed kamal', 'd1a33c7fbfd22254c3559cf5e07a2ecb9e1086de', 'user', 'nakamal@connect.ust.hk', 'on', '1/1/1979', 'm', 'Kowloon', '202.40.139.131', '2015/05/30 11:18', '', 'new', 0, 0, '', '', 0, '', '', '', '0000-00-00 00:00:00', 0, '2015-05-30 11:18:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, ''),
(53, 'Shrey Malik', 'inferno', 'efd4784b29767e32af2523b52f6e92e797b40894', 'user', 'smalik@connect.ust.hk', 'on', '20/11/1991', 'm', 'Tokyo', '157.14.237.118', '2015/06/01 08:05', '', 'new', 1, 0, '', '201506010808', 0, '', '', '', '0000-00-00 00:00:00', 0, '2015-06-01 08:05:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, ''),
(54, 'wenqian Li', 'vincent', '4233137d1c510f2e55ba5cb220b864b11033f156', 'user', 'hkustliwenqian@hotmail.com', 'on', '14/5/1991', 'm', 'ShenZhen', '183.62.147.34', '2015/07/07 22:45', '', 'new', 1, 0, '', '201507100034', 0, '', '', '', '0000-00-00 00:00:00', 0, '2015-07-07 22:45:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'en');

-- --------------------------------------------------------

--
-- Table structure for table `yorum`
--

DROP TABLE IF EXISTS `yorum`;
CREATE TABLE IF NOT EXISTS `yorum` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `kime` varchar(250) COLLATE utf8_bin NOT NULL DEFAULT '',
  `kimden` varchar(250) COLLATE utf8_bin NOT NULL DEFAULT '',
  `yorum` text COLLATE utf8_bin NOT NULL,
  `num` varchar(250) COLLATE utf8_bin NOT NULL DEFAULT '',
  `tarih` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `okundu` tinyint(1) NOT NULL DEFAULT '0',
  `silkime` tinyint(1) NOT NULL DEFAULT '0',
  `silgonderen` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `okundu` (`okundu`),
  FULLTEXT KEY `kime` (`kime`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `zirve`
--

DROP TABLE IF EXISTS `zirve`;
CREATE TABLE IF NOT EXISTS `zirve` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `zirve_ismi` varchar(100) COLLATE utf8_bin NOT NULL DEFAULT '',
  `kategori` varchar(100) COLLATE utf8_bin NOT NULL DEFAULT '',
  `organizator` text COLLATE utf8_bin NOT NULL,
  `mekan` varchar(100) COLLATE utf8_bin NOT NULL DEFAULT '',
  `tarih` varchar(100) COLLATE utf8_bin NOT NULL DEFAULT '',
  `eklenmeTarihi` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `detaylar` text COLLATE utf8_bin NOT NULL,
  `ekleyen` varchar(100) COLLATE utf8_bin NOT NULL DEFAULT '',
  `afis` varchar(200) COLLATE utf8_bin NOT NULL DEFAULT 'images/afis.jpg',
  `durum` varchar(20) COLLATE utf8_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=25 ;

--
-- Dumping data for table `zirve`
--

INSERT INTO `zirve` (`id`, `zirve_ismi`, `kategori`, `organizator`, `mekan`, `tarih`, `eklenmeTarihi`, `detaylar`, `ekleyen`, `afis`, `durum`) VALUES
(24, 'قاء استغرق سا', 'geleneksel sözlük zirvesi', 'admin,   ', 'قاء استغرق سا', 'seç', '2012-01-31 09:51:00', 'قاء استغرق سا <br> قاء استغرق سا <br> قاء استغرق ساقاء استغرق سا <br> قاء استغرق ساقاء استغرق سا <br> قاء استغرق سا <br> قاء استغرق سا', 'admin', 'imagesafis.jpg', '');

-- --------------------------------------------------------

--
-- Table structure for table `zirve_yorum`
--

DROP TABLE IF EXISTS `zirve_yorum`;
CREATE TABLE IF NOT EXISTS `zirve_yorum` (
  `id` bigint(8) NOT NULL AUTO_INCREMENT,
  `zirve_id` bigint(8) NOT NULL DEFAULT '0',
  `yorum` text COLLATE utf8_bin NOT NULL,
  `yorumlayan` varchar(100) COLLATE utf8_bin NOT NULL DEFAULT '',
  `tarih` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=21 ;

--
-- Dumping data for table `zirve_yorum`
--

INSERT INTO `zirve_yorum` (`id`, `zirve_id`, `yorum`, `yorumlayan`, `tarih`) VALUES
(18, 24, 'ستغرق سا \\r\\nستغرق سا \\r\\nستغرق سا \\r\\n', 'admin', '2012-01-31 10:13:00'),
(17, 24, 'غرق \\r\\nغرق \\r\\nغرق ', 'admin', '2012-01-31 10:10:00'),
(16, 24, 'غر <br> غر <br> غر <br> ', 'admin', '2012-01-31 10:02:00'),
(15, 14, 'مواقع قبيلة في\\r\\nمواقع قبيلة في', 'admin', '2012-01-31 09:18:00'),
(14, 14, 'مواقع قبيلة في\\r\\nمواقع قبيلة في', 'admin', '2012-01-31 09:17:00'),
(13, 14, 'مواقع قبيلة في\\r\\nمواقع قبيلة في', 'admin', '2012-01-31 09:17:00'),
(11, 14, 'مواقع قبيلة في\\r\\nمواقع قبيلة في', 'admin', '2012-01-31 09:16:00'),
(12, 14, 'مواقع قبيلة في\\r\\nمواقع قبيلة في', 'admin', '2012-01-31 09:16:00'),
(19, 24, 'قاء استغرق ساقاء ا\\r\\nقاء استغرق ساقاء ا\\r\\nقاء استغرق ساقاء ا', 'admin', '2012-01-31 10:21:00'),
(20, 24, 'قاء استغر  <br>  قاء استغر  قاء استغر  ', 'admin', '2012-01-31 10:24:00');

-- --------------------------------------------------------

--
-- Table structure for table `zirveuser`
--

DROP TABLE IF EXISTS `zirveuser`;
CREATE TABLE IF NOT EXISTS `zirveuser` (
  `id` bigint(8) NOT NULL AUTO_INCREMENT,
  `zirve_id` bigint(8) NOT NULL DEFAULT '0',
  `katilimci` varchar(100) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `kacKisi` bigint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
