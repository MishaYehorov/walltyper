<?
session_start();
include "inc/baglan.php";
include_once('baslik.php');
require_once("inc/func.inc.php");
require_once('settings.php');
if ($verified_user) {
	$userquery = @ mysqli_query($baglan,"select * FROM user WHERE nick='$verified_user'");
	$userlist = @ mysqli_fetch_array($userquery);
	$user_id = $userlist["id"];
	$useravatar = GetBigAvatar($user_id);
}
?>
<body class="bgleft">
<div class="container-fluid">
	<?php if($verified_user): ?>
		<div class="row">
			<div class="col-xs-12 profile">
				<div class="pull-left">
					<?php
					if ($profilresmi == "1")
					{
						if ($useravatar){
							echo "<a target=\"main\" href=\"profil.php?u=".urlencode($verified_user)."\"><div class=\"image\" style=\"background-image: url('".$useravatar."')\"></div>";
						} else {
							echo "<a target=\"main\" href=\"profil.php?u=".urlencode($verified_user)."\"><div class=\"image\" style=\"background-image: url('images/no_profile.jpg');\"></div></a><a target=\"main\" href=\"profil.php?u=".urlencode($verified_user)."\">";
						}
					} ?>
				</div>
				<div class="pull-left info">
					<b><?=$userlist["isim"];?></b><br/>
					<small class="text-muted">(<?=$userlist["nick"];?>)</small>
				</div>
				</a>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 text-center">
				<div class="btn-group btn-group-xs" role="group">
					<a href="sozluk.php?process=privmsg" target="main" class="btn btn-primary">&nbsp;<span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>&nbsp;<?=$language[button_message]; ?>&nbsp;</a>
					<a href="sozluk.php?process=imha" target="main" class="btn btn-primary">&nbsp;<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>&nbsp;<?=$language[button_bin]; ?>&nbsp;</a>
					<a href="sozluk.php?process=onlines" target="main" class="btn btn-primary">&nbsp;<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>&nbsp;<?=$language[button_event]; ?>&nbsp;</a>
				</div>
			</div>
		</div>
	<?php endif;
	include "inc/vampara.php";

$listele = mysqli_query($baglan,"SELECT k.id konuid,max(m.id) id,k.baslik FROM mesajlar m
				INNER JOIN konular k on k.id = m.sira
				WHERE yazar = '$verified_user' and k.statu = '' and m.statu = ''
				group by k.id
				order by m.tarih2 desc
				limit 0,100");

echo '<ul class="row nav nav-left">';
while ($kayit =@ mysqli_fetch_array($listele))
{
	//son entry
	$id = $kayit["id"];
	$konuid = $kayit["konuid"];

	$result = mysqli_query($baglan,"SELECT id,yazar FROM mesajlar WHERE sira=$konuid and statu='' ORDER BY id DESC LIMIT 1");
	
	if (@mysqli_num_rows($result) > 0)
	{
		$row = mysqli_fetch_array($result);
		
		$le_id = $row['id'];
		$le_yazar = $row['yazar'];
	}

	//eğer başlığa son girilen entry bizimki değilse
	if ($le_id != $id && $verified_user != $le_yazar)
	{
		$baslik = $kayit["baslik"];
		$statu = $kayit["statu"];
		$link = str_replace(" ","+",$baslik);	
		$toplamkac = execute_scalar("select count(id) from mesajlar where sira ='$konuid' and statu ='' and id > $id ");
		
		if ($toplamkac < 2) $toplamkac = ""; else  $toplamkac = "($toplamkac)";
		
		$saydir++;

		echo "<li><a href=\"nedir.php?q=$baslik&afterme=1\" target='main'><div class='pull-right'>$toplamkac</div>#$baslik</a></li>";
	}
}
echo "</ul>";

?>
	</div>
