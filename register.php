<? session_start();

require_once('inc/baglan.php');
require_once('baslik.php');
require_once('settings.php');
require_once('inc/func.inc.php');



$self = $_SERVER['PHP_SELF'];
$result = 'Bir bakalım...';
$clr = '#C0C0FF';

$sorgu1 = "SELECT * FROM ayar";
$sorgu2 = mysqli_query($baglan, $sorgu1);
mysqli_num_rows($sorgu2);
$kayit2=mysqli_fetch_array($sorgu2);
$reg=$kayit2["reg"];
$anaTema=$kayit2["anatema"];

if (RequestUtil::Get('ref') != "")
	$referans = RequestUtil::Get('ref');
else $referans = 0;



$ip = getenv('REMOTE_ADDR');
$site = $_SERVER["HTTP_REFERER"];
$site = explode("/", $site);
$site = $site[2];
$now=tarihYarat("tam");

function secure($str)
{
	$str = escapeString($str);
	$str = strip_tags($str);
	$str = trim($str);

	$str = stripslashes($str); // sql inject clean

	return $str;
}

if(!empty($_POST))
{
	include("captcha/securimage.php");
	$img = new Securimage();
	$valid = $img->check($_POST['code']);
	//$good = $captcha->CheckPassed();	

	$nick = secure($_POST['nick']);
	$email = $_POST['email'];
	$sehir = $_POST['sehir'];
	$isim = $_POST['isim'];
	$day = $_POST['day'];
	$month = $_POST['month'];
	$year = $_POST['year'];
	$cinsiyet = $_POST['cinsiyet'];
	$sifre = $_POST['sifre'];
	$sifre2 = $_POST['sifre2'];

	if (!$valid)
	{
		$error = PrintErrorMessage("$language[register_code_alert]","danger");
	}

	if ($nick == "" or $email == "" or $day == "" or $month == "" or $year == "" or $cinsiyet == "" or $sehir == "")
	{
		$error = PrintErrorMessage("$language[error_password_fillEverywhere]","danger");
	}

	if(!preg_match("/^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,6}$/i", $email))
	{
		$error = PrintErrorMessage("$language[error_mail]","danger");
	}

	$sorgu = "SELECT email FROM user WHERE email = '$email'";
	$sorgulama = mysqli_query($baglan,$sorgu);

	if (mysqli_num_rows($sorgulama))
	{
		$email = $kayit["email"];
		$error = PrintErrorMessage("$language[error_already_mail]","danger");
	}

	if ($reg == "off")
	{
		if (!$sifre or !$sifre2)
		{
			$error = PrintErrorMessage("$language[error_choose_password]","danger");
		}

		if ($sifre != $sifre2)
		{
			$error = PrintErrorMessage("$language[error_passwords_wrong]","danger");
		}
	}

	if ($nick[0]==" ")
	{
		$error = PrintErrorMessage("$language[error_first_char]","danger");
	}


	$nick = str_replace("ş","s",$nick);
	$nick = str_replace("Ş","s",$nick);
	$nick = str_replace("ç","c",$nick);
	$nick = str_replace("Ç","c",$nick);
	$nick = str_replace("ı","i",$nick);
	$nick = str_replace("İ","i",$nick);
	$nick = str_replace("ğ","g",$nick);
	$nick = str_replace("Ğ","g",$nick);
	$nick = str_replace("ö","o",$nick);
	$nick = str_replace("Ö","o",$nick);
	$nick = str_replace("ü","u",$nick);
	$nick = str_replace("Ü","u",$nick);
	$nick = str_replace("\'","",$nick);

	$email = str_replace("ş","s",$email);
	$email = str_replace("Ş","S",$email);
	$email = str_replace("ç","c",$email);
	$email = str_replace("Ç","C",$email);
	$email = str_replace("ı","i",$email);
	$email = str_replace("İ","I",$email);
	$email = str_replace("ğ","g",$email);
	$email = str_replace("Ğ","G",$email);
	$email = str_replace("ö","o",$email);
	$email = str_replace("Ö","O",$email);
	$email = str_replace("ü","u",$email);
	$email = str_replace("Ü","U",$email);
	$email = str_replace("Ö","O",$email);

	$nick = strtolower($nick);
	$email = strtolower($email);

	$sorgu = "SELECT nick,id FROM user WHERE `nick`='$nick'";
	$sorgulama = mysqli_query($baglan,$sorgu);

	if (mysqli_num_rows($sorgulama) > 0)
	{
		while ($kayit=mysqli_fetch_array($sorgulama))
		{
			$id=$kayit["id"];
			if ($id)
			{
				$error = PrintErrorMessage("$language[error_already_user]","danger");
			}
		}
	}

	if(!$error) {
		$tarih = tarihYarat("Y/m/d G:i");
		$dt = "$day/$month/$year";


		if ($rookie == "1")
			$durum = "off";
		else
			$durum = "on";

		if ($reg == "on") {
			$ifade = md5(rand(0, 99999));
			$sifre = substr($ifade, 17, 6);
			$betasifre = sha1($sifre);
		} else
			$betasifre = sha1($sifre);

		$yetki = "user";

		$sorgu = "INSERT INTO user ";
		$sorgu .= "(isim,nick,sifre,email,dt,cinsiyet,sehir,durum,yetki,regip,regtarih,tema,referans,credate)";
		$sorgu .= " VALUES ";
		$sorgu .= "('$isim','$nick','$betasifre','$email','$dt','$cinsiyet','$sehir','$durum','$yetki','$ip','$tarih','$anaTema',$referans,'$now')";
		mysqli_query($baglan, $sorgu);

		$kime = $email;
		$konu = $language[registeration_mail];
		$icerik = "$language[hello_mail]\n
	\n
	$language[registeration_username]: $nick\n
	$language[registeration_mail]: $email\n
	$language[password]: $sifre\n
	\n
	$language[dictionaryName] - $language[dictionarySlogan] - $language[dictionaryUrl] 
	";

		$tarih = tarihYarat("YmdHM");
		$gun = tarihYarat("d");
		$ay = tarihYarat("m");
		$yil = tarihYarat("Y");
		$saat = tarihYarat("H:i");

		//çaylaksa özel mesaj atalım
		if ($rookie == "1") {
			$konu = "$language[message_welcome_header]";
			$system = "$language[dictionaryName]";
			$yazi = "
		$language[message_welcome_note]";

			$yazi = str_replace("\n", "<br>", $yazi);

			$sorgu = "INSERT INTO privmsg ";
			$sorgu .= "(kime,konu,mesaj,gonderen,tarih,okundu,gun,ay,yil,saat)";
			$sorgu .= " VALUES ";
			$sorgu .= "('$nick','$konu','$yazi','$system','$tarih','1','$gun','$ay','$yil','$saat')";
			mysqli_query($baglan,$sorgu);
		}

		$mailkonu = "$language[message_welcome_header]";
		mail("$kime", "$mailkonu", "$icerik", "From: $language[dictionaryName] <$language[dictionaryMail]>");

		$success = PrintErrorMessage($language[message_welcome_accepted], "success");
		echo "<META HTTP-EQUIV=\"REFRESH\" CONTENT=\"5;URL=login.php\">";
	}
}

function PrintErrorMessage($message,$color)
{
	$alert = "<div class=\"alert alert-".$color." alert-dismissible\" role=\"alert\">
				<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
				" . $message . "</div>";
	return $alert;
}


?>
<body>
<div class="container-fluid">
	<div class="page-header">
		<h1><? echo $language[topbutton_register]; ?></h1>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<?php echo $error; ?>
			<?php echo $success; ?>
		</div>
		<?php if(!$success): ?>
		<div class="col-xs-6 col-xs-offset-2">
			<form class="form-horizontal" method="post" role="form">
				<div class="form-group">
					<label for="nick" class="col-xs-3 control-label"><?=$language[username];?></label>
					<div class="col-xs-9">
						<input type="text" class="form-control" id="nick" name="nick" required>
						<span class="help-block"><?=$language[username_tips];?></span>
					</div>
				</div>
				<?php
				$sorgu1 = "SELECT * FROM ayar";
				$sorgu2 = mysqli_query($baglan, $sorgu1);
				mysqli_num_rows($sorgu2);
				$kayit2=mysqli_fetch_array($sorgu2);
				$reg=$kayit2["reg"];
				$anatema=$kayit2["anatema"];

				if ($reg == "off"): ?>
					<div class="form-group">
						<label for="sifre" class="col-xs-3 control-label"><?=$language[password];?></label>
						<div class="col-xs-9">
							<input type="password" class="form-control" id="sifre" name="sifre" required>
						</div>
					</div>
					<div class="form-group">
						<label for="sifre2" class="col-xs-3 control-label"><?php echo $language[password]." (".$language[confirm].")";?></label>
						<div class="col-xs-9">
							<input type="password" class="form-control" id="sifre2" name="sifre2" required>
							<span class="help-block"><?=$language[password_tips];?></span>
						</div>
					</div>
				<?php endif; ?>
				<div class="form-group">
					<label for="email" class="col-xs-3 control-label"><?=$language[register_mail];?></label>
					<div class="col-xs-9">
						<input type="email" class="form-control" id="email" name="email" required>
						<span class="help-block"><?=$language[register_mail_tips];?></span>
					</div>
				</div>
				<div class="form-group">
					<label for="isim" class="col-xs-3 control-label"><?=$language[your_name_surname];?></label>
					<div class="col-xs-9">
						<input type="text" class="form-control" id="isim" name="isim" required>
						<span class="help-block"><?=$language[register_name_tips];?></span>
					</div>
				</div>
				<div class="form-group">
					<label for="isim" class="col-xs-3 control-label"><?=$language[register_birthDate];?></label>
					<div class="col-xs-9">
						<div class="row">
							<div class="col-xs-3">
								<select class="form-control" name="day" id="day">
									<option selected></option>
									<?php for($i=1; $i<=31; $i++){
										echo "<option>".$i."</option>";
									} ?>
								</select>
							</div>
							<div class="col-xs-5">
								<select class="form-control" name="month" id="month">
									<option selected></option>
									<option value=1><? echo $language[january]; ?></option>
									<option value=2><? echo $language[february]; ?></option>
									<option value=3><? echo $language[march]; ?></option>
									<option value=4><? echo $language[april]; ?></option>
									<option value=5><? echo $language[may]; ?></option>
									<option value=6><? echo $language[june]; ?></option>
									<option value=7><? echo $language[july]; ?></option>
									<option value=8><? echo $language[august]; ?></option>
									<option value=9><? echo $language[september]; ?></option>
									<option value=10><? echo $language[october]; ?></option>
									<option value=11><? echo $language[november]; ?></option>
									<option value=12><? echo $language[december]; ?></option>
								</select>
							</div>
							<div class="col-xs-4">
								<select class="form-control" class="form-control" name="year" id="year">
									<option selected></option>
									<?php for($i=1900; $i<=date("Y"); $i++){
										echo "<option>".$i."</option>";
									} ?>
								</select>
							</div>
							<span class="col-xs-12 help-block"><?=$language[register_birth_tips];?></span>
						</div>
					</div>

				</div>
				<div class="form-group">
					<label for="cinsiyet" class="col-xs-3 control-label"><?=$language[gender];?></label>
					<div class="col-xs-9">
						<select class="form-control" id="cinsiyet" name="cinsiyet">
							<option value="m"><? echo $language[male]; ?> </option>
							<option value="f"><? echo $language[female]; ?> </option>
						</select>
						<span class="help-block"><?=$language[register_gender_tips];?></span>
					</div>
				</div>
				<div class="form-group">
					<label for="sehir" class="col-xs-3 control-label"><?=$language[city];?></label>
					<div class="col-xs-9">
						<input type="text" class="form-control" id="sehir" name="sehir">
						<span class="help-block"><?=$language[register_city_tips];?></span>
					</div>
				</div>
				<div class="form-group">
					<label for="sehir" class="col-xs-3 control-label"><?=$language[register_code];?></label>
					<div class="col-xs-9">
						<div id='cpholder'>
							<img id="siimage" src="captcha/securimage_show.php?sid=<?php echo md5(uniqid(time())); ?>">
						</div>
						<br/>
						<div class="col-xs-6 input-group">
							<input type="text" name="code" id="code" class="form-control">
									<span class="input-group-btn">
        								<button class="btn btn-default" type="button" onclick="document.getElementById('siimage').src = 'captcha/securimage_show.php?sid=' + Math.random(); return false">
											Refresh
										</button>
      								</span>
						</div>
						<span class="help-block"><?=$language[register_code_message];?></span>
					</div>
				</div>
				<div class="form-group">
					<div class="col-xs-offset-3 col-xs-9">
						<input class="btn btn-primary" type='submit' name='submit' onclick="return control();" id='btnsubmit' value='<? echo $language[button_makeMeAuthor]; ?>' />
					</div>
				</div>
			</form>
		</div>
		<?php endif; ?>
	</div>
</div>

<script type="text/javascript">
	function control()
	{
		var txtUsername = document.getElementById("nick");
		var regex = /^[abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_ ]*$/ ;//regex varaible

		if (txtUsername.value=="")
		{
			alert("Nick?");
			document.getElementById("nick").style.backgroundColor="#ffcccc";
			document.getElementById("nick").focus();
			return false;
		}

		if(!regex.test(txtUsername.value))
		{
			alert("<?=$language[register_username_message]?>");
			txtUsername.focus();
			return false;
		}

		if (document.getElementById("email").value == "")
		{
			alert("Email?");
			document.getElementById("email").style.backgroundColor="#ffcccc";
			document.getElementById("email").focus();
			return false;
		}

		if (document.getElementById("isim").value == "")
		{
			alert("<?=$language[register_namesurname_message]?>");
			document.getElementById("isim").style.backgroundColor="#ffcccc";
			document.getElementById("isim").focus();
			return false;
		}

		if (document.getElementById("cinsiyet").value == "")
		{
			alert("<?=$language[register_gender_message]?>");
			document.getElementById("cinsiyet").style.backgroundColor="#ffcccc";
			document.getElementById("cinsiyet").focus();
			return false;
		}

		if (document.getElementById("sehir").value == "")
		{
			alert("<?=$language[register_gender_message]?>");
			document.getElementById("sehir").style.backgroundColor="#ffcccc";
			document.getElementById("sehir").focus();
			return false;
		}

		if (document.getElementById("code").value == "")
		{
			alert("<?=$language[register_code_message]?>");
			document.getElementById("code").style.backgroundColor="#ffcccc";
			document.getElementById("code").focus();
			return false;
		}
	}

	$('#nick').val('<?=$_POST['nick'];?>');
	$('#email').val('<?=$_POST['email'];?>');
	$('#sehir').val('<?=$_POST['sehir'];?>');
	$('#isim').val('<?=$_POST['isim'];?>');
	$('#day').val('<?=$_POST['day'];?>');
	$('#month').val('<?=$_POST['month'];?>');
	$('#year').val('<?=$_POST['year'];?>');
	$('#cinsiyet').val('<?=$_POST['cinsiyet'];?>');
</script>

</body>
</html>