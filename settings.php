<?php
error_reporting(E_ALL ^ E_NOTICE);

//gerekli ayarları çekiyoruz.
$SQL = "SELECT * FROM ayar";
$Query =@ mysqli_query($baglan, $SQL);
$Select = mysqli_fetch_array($Query);
$language = $Select["dil"];
$seoMode = $Select["seo"];
$turkce_karakter = $Select["turkce_karakter"];
$maillogin = $Select["maillogin"];
$baslikiciara  = $Select["baslikiciara"];
$kazandakiler  = $Select["kazandakiler"];
$profilresmi  = $Select["profilresmi"];       //profil resmi gosteril sin mi için selected
$icerisi  = $Select["icerisi"];      
$rookie  = $Select["rookie"];      
$nasilsin  = $Select["nasilsin"];      
$istatistikkutusu  = $Select["istatistikkutusu"];      
$facetwit  = $Select["facetwit"];      
$opening_type  = $Select["entries"];
$defaultentry  = $Select["defaultentry"];

if ($opening_type == "today")
	$leftBarUrl = "left.php?t=tod";
else
	$leftBarUrl = "left.php";

$verified_user = $_SESSION["verified_user"];
$sid = $_SESSION["sid"];
$kat = $_SESSION["kat"];
$verified_kat = $_SESSION["verified_kat"];
$durum = $_SESSION["durum"];

//sistem default dili 
$langdefault = "en";

//language dosyamız nerede?
$SQLuser = "SELECT * FROM user ";
$Queryuser =@ mysqli_query($baglan, $SQLuser);
$Selectuser = mysqli_fetch_array($Queryuser);
$languageuser = $Selectuser["dil"];
$id = $Selectuser["id"];

//if(!$verified_user)
	$languageuser = $_SESSION["dil"];  		
	
if(!$languageuser) $languageuser=$langdefault;

$language_path = "language/$languageuser.php";  

require_once($language_path);

function AnalyticsCode($code)
{
	if ($code)
	return "
	<script type=\"text/javascript\">

	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', '$code']);
	  _gaq.push(['_trackPageview']);

	  (function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();

	</script>";
}