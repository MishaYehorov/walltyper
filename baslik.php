<? 
error_reporting(E_ALL ^ E_NOTICE);

require_once('inc/requestUtils.class.php'); 
require_once('inc/class.inputfilter.php'); 
require_once('inc/baglan.php');
require_once('settings.php');

if ($site_title == "")
	$site_title = $language['dictionaryName'] ." &raquo; ". $language['dictionarySlogan'];
if ($site_description == "")
	$site_description = $language['dictionaryDescription'];

//banlananlar şutlansın
$sorgu = "SELECT ip FROM ipban";
$sorgulama = mysqli_query($baglan, $sorgu);

if (mysqli_num_rows($sorgulama) > 0)
{
	while ($kayit=mysqli_fetch_array($sorgulama))
	{
		$ip = $kayit["ip"];
		
		if ($ip == $REMOTE_ADDR)
		{
			die("$language[youarekicked]");
			return;
		}
	}
}

function selfURL() { $s = empty($_SERVER["HTTPS"]) ? '' : ($_SERVER["HTTPS"] == "on") ? "s" : ""; $protocol = strleft(strtolower($_SERVER["SERVER_PROTOCOL"]), "/").$s; $port = ($_SERVER["SERVER_PORT"] == "80") ? "" : (":".$_SERVER["SERVER_PORT"]); return $protocol."://".$_SERVER['SERVER_NAME'].$port.$_SERVER['REQUEST_URI']; } function strleft($s1, $s2) { return substr($s1, 0, strpos($s1, $s2)); }
$url = selfURL();

if(isset($_COOKIE['s']) && $verified_user == "")
{
	$mail = $_COOKIE['s']['m']; 
	$password = $_COOKIE['s']['pi']; 
	
	if ($mail != "" && $password != "")
	{
		$sorgu = "SELECT * FROM user WHERE email='$mail'";
		$sorgulama = mysqli_query($baglan,$sorgu);
		
		if (mysqli_num_rows($sorgulama) > 0)
		{
			while ($kayit=mysqli_fetch_array($sorgulama))
			{
				$sifre = md5($kayit["sifre"]);
				$email = $kayit["email"];
				$nick = $kayit["nick"];
				$yetki = $kayit["yetki"];
			
				if ($email == $mail and $sifre == $password) 
				{
					$verified_user = $nick;
					$verified_kat = $yetki;
					$sid = $PHPSESSID;
					$ip = getenv('REMOTE_ADDR');
					$islem_zamani = time();
					
					$sorgu1 = "SELECT nick FROM online WHERE `nick` = '$nick'";
					$sorgu2 = mysqli_query($baglan,$sorgu1);
					mysqli_num_rows($sorgu2);

					$kayit2=mysqli_fetch_array($sorgu2);
					$onnick=$kayit2["nick"];

					if (!$onnick) 
					{
						$sorgu = "INSERT INTO online ";
						$sorgu .= "(nick,islem_zamani,ip,ondurum)";
						$sorgu .= " VALUES ";
						$sorgu .= "('$nick','$islem_zamani','$ip','$durum')";
						mysqli_query($baglan,$sorgu);
					}
					

					else 
					{
						$simdikizaman = time();
						$sorgu = "UPDATE online SET islem_zamani=$simdikizaman WHERE nick='$verified_user'";
						mysqli_query($baglan,$sorgu);
					}

					//Session'a atalım
					$_SESSION["verified_user"] = $verified_user;
					$_SESSION["sid"] = $sid;
					$_SESSION["kat"] = $kat;
					$_SESSION["verified_kat"] = $verified_kat;
					$_SESSION["durum"] = $durum;
					
					$_SESSION['loggedin'] = 1;			
				}
			}
		}
	}
}

$tema_path = $language[dictionaryUrl] . "/images/";

if ($verified_user) 
{
	$sorgu1 = "SELECT tema,ip FROM user WHERE `nick` = '$verified_user'";
	$sorgu2 = mysqli_query($baglan,$sorgu1);
	$kayit2 =@ mysqli_fetch_array($sorgu2);
	$tema = $kayit2["tema"];
	$old_ip = $kayit2["ip"];

	if (!$tema)
		$tema = "default";
		
	$ip=@$REMOTE_ADDR;
	
	if ($old_ip != $ip)
	{
		$sorgu = "UPDATE user SET ip='$ip' WHERE nick='$verified_user'";
		mysqli_query($baglan,$sorgu);
	}	
}
else 
{
	$sqlTema = "SELECT anatema FROM ayar";
	$list =@ mysqli_fetch_array(mysqli_query($baglan,$sqlTema));
	$tema = "$list[anatema]";
	
	if(isset($_SESSION['anatema']))
		$tema = "default";
}

if (stristr($url, "/post/") || stristr($url, "/entry/") || stristr($url, "/duyuru/") )
$post = "../";

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="tr,ar" lang="tr,ar" dir="ltr">
<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="text/html; charset=utf8">
<meta http-equiv="Content-Language" content="tr,ar"/>
<title><?=$site_title?></title>
<meta  name="keywords" content="<?=$language['dictionaryKeywords']?>" />
<script src="<?=$post?>java/top.js" type="text/javascript"></script>
<script language="javascript" src="<?=$post?>java/jquery.js"></script>
<script language="javascript" src="<?=$post?>java/jsozluk.js"></script>
<script language="javascript" src="<?=$post?>java/sozluk.js"></script>
<link href="favicon.ico" rel="shortcut Icon"><link href="favicon.ico" rel="icon">
<link href="<?=$tema_path?>adminmenu/menu.css" type="text/css" rel="stylesheet">
<link rel="toc contents" title="Sitemap" href="<?=$language['dictionaryUrl']?>/sitemap.xml" />
<?=AnalyticsCode($language['googleAnalyticsCode']);?>
<link type="text/css" rel="stylesheet" href="css/font-awesome.min.css"/>
<?php if($_GET["process"] == "adm"): ?>
	<link href="<?=$tema_path?>reset.css" type="text/css" rel="stylesheet">
	<link href="<?=$tema_path?>gray.css" type="text/css" rel="stylesheet">
	<link href="<?=$tema_path.$tema?>.css" type="text/css" rel="stylesheet">
<?php else: ?>
	<link type="text/css" rel="stylesheet" href="<?=$post?>css/style.css"/>
	<link type="text/css" rel="stylesheet" href="<?=$post?>css/bootstrap.css"/>
	<script language="javascript" src="<?=$post?>js/jquery.min.js"></script>
	<script language="javascript" src="<?=$post?>js/bootstrap.min.js"></script>
	<script language="javascript" src="js/moment.js"></script>
<?php endif; ?>
</head>
