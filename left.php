<?
#ini_set('display_errors', '0');
session_start();
include "inc/baglan.php";
include_once('baslik.php');
require_once("inc/func.inc.php");
require_once('settings.php');
if ($verified_user) {
	$userquery = @ mysqli_query($baglan,"select * FROM user WHERE nick='$verified_user'");
	$userlist = @ mysqli_fetch_array($userquery);
	$user_id = $userlist["id"];
	$useravatar = GetBigAvatar($user_id);
}
?>
<body class="bgleft">
<div class="container-fluid">
<?php if($verified_user): ?>
	<div class="row">
		<div class="col-xs-12 profile">
			<div class="pull-left">
				<?php
				if ($profilresmi == "1")
				{
					if ($useravatar){
						echo "<a target=\"main\" href=\"profil.php?u=".urlencode($verified_user)."\"><div class=\"image\" style=\"background-image: url('".$useravatar."')\"></div>";
					} else {
						echo "<a target=\"main\" href=\"profil.php?u=".urlencode($verified_user)."\"><div class=\"image\" style=\"background-image: url('images/no_profile.jpg');\"></div></a><a target=\"main\" href=\"profil.php?u=".urlencode($verified_user)."\">";
					$_SESSION['id_user'] = $userlist["id"];
					}
				} ?>
			</div>
			<div class="pull-left info">
				<b><?=$userlist["isim"];?></b><br/>
	            <small class="text-muted">(<?=$userlist["nick"];?>)</small>

			</div>
			</a>
		</div>
	</div>
    <div class="row">
		<div class="col-xs-12 text-center">
            <div class="btn-group btn-group-xs" role="group">
                <a href="sozluk.php?process=privmsg" target="main" class="btn btn-primary">&nbsp;<span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>&nbsp;<?=$language[button_message]; ?></a>
                <a href="sozluk.php?process=imha" target="main" class="btn btn-primary">&nbsp;<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>&nbsp;<?=$language[button_bin]; ?></a>
                <a href="sozluk.php?process=onlines" target="main" class="btn btn-primary">&nbsp;<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>&nbsp;<?=$language[button_event]; ?></a>
            </div>
        </div>
	</div>
<?php endif;
include "inc/vampara.php";

$bugun = bugun();
$dun = dun();
$ay = tarihYarat("m");
$max = 50;
$yazar2 = $verified_user;

$gunVar=tarihYarat("d");
$ayVar=tarihYarat("m");
$yilVar=tarihYarat("Y");

$sayfa = RequestUtil::Get('sayfa');
if (!$sayfa)
	$sayfa = 1;
$alt = ($sayfa - 1) * $max;

/***********************************
	Gösterim tipi
	all - tümü
	tod - bugün
	yes - dün
	mix - karışık
 */
$type = RequestUtil::Get('t');

if ($type == "")
	$type = "all";

$url = "left.php?t=$type";

/***********************************/

//tüm girilen başlık sayısı
if ($type == "all")
{
	$sor = mysqli_query($baglan,"select distinct a.id FROM konular a
						INNER JOIN mesajlar b on b.sira = a.id
						INNER JOIN user u on u.nick = b.yazar
						WHERE a.statu= '' and  b.statu = '' and u.durum = 'on'
						ORDER BY a.tarih desc");

	$entrysayisi = execute_scalar("select COUNT(gun) from mesajlar b
									INNER JOIN user u on u.nick = b.yazar
									WHERE statu='' AND u.durum = 'on'
									ORDER BY 'tarih'");
	$title = $language['all'];
}
else if ($type == "tod")
{
	$sor = mysqli_query($baglan,"select distinct a.id FROM konular a
						INNER JOIN mesajlar b on b.sira = a.id
						INNER JOIN user u on u.nick = b.yazar
						WHERE a.statu= '' and a.bugun = '$bugun' AND b.statu = ''
						AND u.durum = 'on' AND DATE_FORMAT(tarih2,'%Y-%m-%d') = '$bugun'");

	$entrysayisi = execute_scalar("select COUNT(gun) from mesajlar b
									INNER JOIN user u on u.nick = b.yazar
									WHERE b.statu='' and DATE_FORMAT(b.tarih2,'%Y-%m-%d') = '$bugun' AND u.durum = 'on'
									ORDER BY b.tarih");
	$title = $language['today'];
}
else if ($type == "buddy")
{
	$sor = mysqli_query($baglan,"select m.id, m.yazar,k.baslik from mesajlar m
							inner join rehber r on r.kim = m.yazar
							inner join konular k on k.id = m.sira
							where kimin = '$verified_user' And k.statu = '' and m.statu = ''");

	$entrysayisi = mysqli_num_rows($sor);

	$title = $language['buddyList'];
}
else if ($type == "yes")
{
	$sor = mysqli_query($baglan,"select distinct a.id FROM konular a
						INNER JOIN mesajlar b on b.sira = a.id
						WHERE a.statu= '' and (a.bugun='$dun' or a.dun='$dun') AND b.statu = ''");
	$entrysayisi = execute_scalar("select COUNT(gun) from mesajlar WHERE statu='' and DATE_FORMAT(tarih2,'%Y-%m-%d') = '$dun'");
	$title = $language['yesterday'];
}
else if ($type == "mix")
{
	$sor = mysqli_query($baglan,"SELECT distinct a.id FROM konular a
						INNER JOIN mesajlar b on b.sira = a.id
						WHERE a.statu= '' and b.statu = ''
						ORDER BY RAND() limit 0,50");

	$title = $language['mixthings'];
}

$n =@ mysqli_num_rows($sor);
$w = $n;

$ws = ceil($w / $max);
$goster = $w / $max;
$goster = ceil($goster);


/*if ($type == "buddy")
	echo "<br /><div class=\"pagi\">($entrysayisi)  $language[entries]<br>";
else if ($type == "mix")
		echo "<div class=\"pagi\">$title <br>($w) $language[topics] <br  />";
else
	echo "<h4>$title <br></h4><ul><li>($w) $language[topics]</li><li> ($entrysayisi) $language[entries]</li></ul>";*/

if ($goster > 1)
{
	if ($sayfa >= 1 or !$sayfa)
	{
		$linksayfa = $sayfa - 1;

		if ($sayfa > 1 or $sayfa)
		{
			if ($sayfa != 1)
				echo "<a class=but href=$url&sayfa=$linksayfa><<</a> ";
		}
	}

	echo "<div class=\"pagi\" > $language[page]
	<select class=\"pagis\" onchange=\"jm('self',this,0);\" name=sayfa>";

	for ($i =1;$i<=$goster;$i++)
	{
		if ($sayfa == $i)
			echo "<option value=$url&sayfa=$i selected>$i</option>";
		else
			echo "<option value=$url&sayfa=$i>$i</option>";
	}
	echo "</select> / $ws ";

	if ($sayfa >= 1 or !$sayfa)
	{
		if (!$sayfa)
		$sayfa = 1;
		$linksayfa = $sayfa + 1;

		if ($linksayfa <= $goster)
			echo " <a class=but href=$url&sayfa=$linksayfa>>></a>";
	}
}
echo "</div></div></div><br>";

$today=bugun();
if ($type == "all")
{
	$listele = mysqli_query($baglan,"SELECT distinct a.id,a.baslik,a.tarih,a.gun,a.statu FROM konular a
						INNER JOIN mesajlar b on b.sira = a.id
						INNER JOIN user u on u.nick = b.yazar
						WHERE a.statu= '' and b.statu = '' AND u.durum = 'on'
						ORDER BY a.tarih desc limit $alt,$max");
}
else if ($type == "tod")
{
	$listele = mysqli_query($baglan,"SELECT distinct a.id,a.baslik,a.tarih,a.gun,a.statu FROM konular a
						INNER JOIN mesajlar b on b.sira = a.id
						INNER JOIN user u on u.nick = b.yazar
						WHERE a.statu= '' and a.bugun = '$bugun' AND b.statu = ''
						AND u.durum = 'on' AND DATE_FORMAT(tarih2,'%Y-%m-%d') = '$bugun'
						ORDER BY a.tarih desc limit $alt,$max");
}
else if ($type == "buddy")
{
	$listele = mysqli_query($baglan,"select m.id, m.yazar,k.baslik from mesajlar m
							inner join rehber r on r.kim = m.yazar
							inner join konular k on k.id = m.sira
							where kimin = '$verified_user' And k.statu = '' and m.statu = ''
							order by m.tarih2 desc limit $alt,$max");

}
else if ($type == "yes")
{
	$listele = mysqli_query($baglan,"SELECT distinct a.id,a.baslik,a.tarih,a.gun,a.statu FROM konular a
						INNER JOIN mesajlar b on b.sira = a.id
						INNER JOIN user u on u.nick = b.yazar
						WHERE a.statu= '' and (a.bugun = '$dun' or dun='$dun') AND b.statu = ''
						AND u.durum = 'on'
						ORDER BY a.tarih desc limit $alt,$max");
}
else if ($type == "mix")
{
	$listele =mysqli_query($baglan,"SELECT distinct a.id,a.baslik,a.tarih,a.gun,a.statu FROM konular a
						INNER JOIN mesajlar b on b.sira = a.id
						WHERE a.statu= '' and b.statu = ''
						ORDER BY RAND() limit 0,50");
}


echo '<ul class="nav nav-left">';
while ($kayit =@ mysqli_fetch_array($listele))
{
	$id = $kayit["id"];
	$gun = $kayit["gun"];
	$baslik = $kayit["baslik"];
	$baslik_yazar = $kayit["yazar"];
	$statu = $kayit["statu"];
	$link = str_replace(" ","+",$baslik);
	$toplamkac = execute_scalar("select count(id) from mesajlar where sira ='$id' and statu =''");
	$bugunkac = execute_scalar("select count(id) from mesajlar where sira ='$id' and DATE_FORMAT(tarih2,'%Y-%m-%d') = '$bugun' and statu =''");

	if ($type == "yes")
		$bugunkac = execute_scalar("select COUNT(id) from mesajlar WHERE sira ='$id' AND statu='' AND DATE_FORMAT(tarih2,'%Y-%m-%d') = '$dun'");

	$saydir++;

	if ($bugunkac > 1)
		$bugunkac = "($bugunkac)";
	else $bugunkac = "";

	if ($type == "tod")
		echo "<li><a href=\"nedir.php?&show=today&q=$baslik\" target='main'><div class='pull-right'><span>$bugunkac</span></div>#$baslik</a></li>";
	else if ($type == "yes")
		echo "<li><a href=\"nedir.php?&show=dun&q=$baslik\" target='main'><div class='pull-right'><span>$bugunkac</span></div>#$baslik</a></li>";
	else if ($type == "buddy")
		echo "<li><a onmouseover=\"document.getElementById('m$id').style.visibility = 'visible'\" onmouseout=\"document.getElementById('m$id').style.visibility = 'hidden'\" href=\"post.php?eid=$id\" target='main'><div style='margin-left: 5px;'>$baslik</div></a> <span id=\"m$id\" style=\"visibility:hidden\"><small>$baslik_yazar</small></span></li>";
	else
		echo "<li><a href=\"nedir.php?q=$link\" target='main'><div class='pull-right'><span>$bugunkac</span></div>#$baslik</a></li>";
}
echo "</ul>";

$goster = $w/$max;
$goster=ceil($goster);

if ($goster > 1)
{
	if ($type == "buddy")
		echo "<br /><div class=\"pagi\">($entrysayisi)  $language[entries]<br>";
	else if ($type == "mix")
		echo "<div class=\"pagi\">$title <br>($w) $language[topics] <br  />";
	else
		echo "<br /><div class=\"pagi\">($w)$language[topics]  ($entrysayisi)  $language[entries]<br>";

	if ($sayfa >= 1 or !$sayfa)
	{
		$linksayfa = $sayfa - 1;

		if ($sayfa > 1 or $sayfa)
		{
			if ($sayfa != 1)
			{
				echo "<a class=but href=$url&sayfa=$linksayfa><<</a>";
			}
		}
	}
	echo "<span> $language[page]
	<select class=pagis onchange=\"jm('self',this,0);\" name=sayfa>";
	for ($i = 1; $i <= $goster; $i++)
	{
		if ($sayfa == $i)
			echo "<option value=$url&sayfa=$i selected>$i</option>";
		else
			echo "<option value=$url&sayfa=$i>$i</option>";
	}
	echo "</select> / $ws ";

	if ($sayfa >= 1 or !$sayfa)
	{
		if (!$sayfa)
		$sayfa = 1;
		$linksayfa = $sayfa + 1;

		if ($linksayfa <= $goster)
			if (!$yesterday)
				echo "<a class=but href=$url&sayfa=$linksayfa>>></a>";
	}
}
echo "</span></div></body>";
?>